package net.tardis.mixin;

import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.ClientHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/* Created by Craig on 15/03/2021 */
@Mixin(LightTexture.class)
public class LightTextureMixin {


    @Inject(method = "updateLightmap(F)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/world/ClientWorld;getDimensionRenderInfo()Lnet/minecraft/client/world/DimensionRenderInfo;"), locals = LocalCapture.CAPTURE_FAILSOFT)
    public void updateLightmap(float partialTicks, CallbackInfo ci, ClientWorld clientworld, float f, float f1, float f3, float f2, Vector3f vector3f, float f4, Vector3f vector3f1, int i, int j, float f5, float f6, float f7, float f8) {
        ClientHelper.updateLightmap(partialTicks, clientworld, f, f1, f3, f2, vector3f, f4, vector3f1, i, j, f5, f6, f7, f8);
    }

/*    @ModifyVariable(
            method = "updateLightmap(F)V",
            at = @At(value = "STORE"), ordinal = 1
    )
    private Vector3f mixin(Vector3f vector3f1) {
        System.out.println(vector3f1);
        return new Vector3f(1,0,0);
    }*/
}
