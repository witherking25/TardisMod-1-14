package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.VMContainer;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.VortexManipItem;

import java.util.Map;

public class BatteryFunction extends BaseVortexMFunction {
    private final TranslationTextComponent container_title = new TranslationTextComponent("container.tardis.vm_battery");

    @Override
    public void sendActionOnServer(World world, ServerPlayerEntity player) {
        ItemStack vm = new ItemStack(TItems.VORTEX_MANIP.get());
        //This checks for which itemstack we want to open the container for
        //If we are holding a VM, use it as the itemstack. Otherwise, get the stack closest to slot 0
        ItemStack currentItem = PlayerHelper.getHeldOrNearestStack(player, vm);
        if (currentItem.getItem() instanceof VortexManipItem) {
            currentItem.getCapability(Capabilities.VORTEX_MANIP).ifPresent((cap) -> {
                NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {
                            @Override
                            public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
                                return new VMContainer(id, inv, currentItem);
                            }

                            @Override
                            public ITextComponent getDisplayName() {
                                return container_title;
                            }
                        },
                        buf -> buf.writeItemStack(currentItem)
                );
                super.sendActionOnServer(world, player);
            });
        }
    }

    @Override
    public boolean stateComplete() {
        return true;
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.SERVER;
    }


}
