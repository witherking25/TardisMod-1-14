package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;


public class TeleportFunction extends BaseVortexMFunction {

    @Override
    public void sendActionOnClient(World world, PlayerEntity player) {
        if (world.isRemote && WorldHelper.canVMTravelToDimension(world)) {
            ClientHelper.openGUI(Constants.Gui.VORTEX_TELE, null);
            super.sendActionOnClient(world, player);
        } else {
            player.sendStatusMessage(new TranslationTextComponent("message.vm.forbidden"), false);
        }
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.CLIENT;
    }
}
