package net.tardis.mod.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TardisNameGuiMessage;


/**
 * Created by 50ap5ud5
 * on 9 Jul 2020 @ 7:56:02 pm
 */
public class DistressFunction extends BaseVortexMFunction {
    @Override
    public void sendActionOnClient(World world, PlayerEntity player) {
        if (world.isRemote && WorldHelper.canVMTravelToDimension(world)) {
            ClientHelper.openGUI(Constants.Gui.VORTEX_DISTRESS, null);
            super.sendActionOnClient(world, player);
        } else {
            player.sendStatusMessage(new TranslationTextComponent("message.vm.forbidden"), false);
        }
    }
    
    @Override
    public void sendActionOnServer(World world, ServerPlayerEntity player) {
    	Network.sendTo(TardisNameGuiMessage.create(player.getServer()), player);
    }

    @Override
    public VortexMUseType getLogicalSide() {
        return VortexMUseType.BOTH;
    }

}
