package net.tardis.mod.client.models;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class RotateMonitorModel extends EntityModel<Entity> {
    private final ModelRenderer monitor;
    private final ModelRenderer spin_y;
    private final ModelRenderer sholder;
    private final ModelRenderer forearm;
    private final ModelRenderer wrist;
    private final ModelRenderer screen;
    private final ModelRenderer glow_screen;
    private final ModelRenderer fixed_base;
    private TextLabelModelRenderer label;
    
    private static WorldText TEXT = new WorldText(0.75F, 0.47F, 0.007F, TextFormatting.WHITE);
    
    private String[] text = new String[0];
    
    private ResourceLocation textureToRebind;
    public RotateMonitorModel() {
        textureWidth = 64;
        textureHeight = 64;

        monitor = new ModelRenderer(this);
        monitor.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        spin_y = new ModelRenderer(this);
        spin_y.setRotationPoint(0.0F, 0.0F, 0.0F);
        monitor.addChild(spin_y);
        spin_y.setTextureOffset(37, 8).addBox(-2.0F, -3.0F, -2.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);

        sholder = new ModelRenderer(this);
        sholder.setRotationPoint(0.0F, -2.5F, 0.0F);
        spin_y.addChild(sholder);
        setRotationAngle(sholder, -0.9599F, 0.0F, 0.0F);
        sholder.setTextureOffset(21, 26).addBox(-1.5F, -0.5F, -11.0F, 1.0F, 1.0F, 11.0F, 0.0F, false);
        sholder.setTextureOffset(24, 12).addBox(0.5F, -0.5F, -11.0F, 1.0F, 1.0F, 11.0F, 0.0F, false);

        forearm = new ModelRenderer(this);
        forearm.setRotationPoint(0.5F, 0.0155F, -10.5401F);
        sholder.addChild(forearm);
        setRotationAngle(forearm, 2.3998F, 0.0F, 0.0F);
        forearm.setTextureOffset(0, 31).addBox(-1.0F, -0.5F, -7.5F, 1.0F, 1.0F, 8.0F, 0.0F, false);

        wrist = new ModelRenderer(this);
        wrist.setRotationPoint(-0.5F, 0.183F, -7.0F);
        forearm.addChild(wrist);
        setRotationAngle(wrist, -2.3562F, 0.0F, 0.0F);
        wrist.setTextureOffset(34, 26).addBox(-1.25F, -0.5F, -8.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);
        wrist.setTextureOffset(10, 32).addBox(0.25F, -0.5F, -8.0F, 1.0F, 1.0F, 8.0F, 0.0F, false);

        screen = new ModelRenderer(this);
        screen.setRotationPoint(0.0F, -0.1F, -7.25F);
        wrist.addChild(screen);
        setRotationAngle(screen, 1.0908F, 0.0F, 0.0F);
        screen.setTextureOffset(0, 0).addBox(-7.0F, -4.4F, -2.0F, 14.0F, 9.0F, 1.0F, 0.0F, false);
        screen.setTextureOffset(28, 38).addBox(-2.0F, -3.4F, -1.0F, 1.0F, 7.0F, 2.0F, 0.0F, false);
        screen.setTextureOffset(37, 14).addBox(1.0F, -3.4F, -1.0F, 1.0F, 7.0F, 2.0F, 0.0F, false);
        screen.setTextureOffset(44, 24).addBox(0.0625F, 4.1114F, -2.1198F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        screen.setTextureOffset(27, 24).addBox(-0.5F, 4.0988F, -2.3714F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        screen.setTextureOffset(27, 24).addBox(-1.0F, 4.2798F, -2.2446F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        screen.setTextureOffset(24, 24).addBox(-7.0625F, 4.1114F, -2.1198F, 7.0F, 1.0F, 1.0F, 0.0F, false);

        glow_screen = new ModelRenderer(this);
        glow_screen.setRotationPoint(0.0F, 5.6F, 23.25F);
        screen.addChild(glow_screen);
        glow_screen.setTextureOffset(0, 12).addBox(-6.5F, -9.7538F, -25.5434F, 13.0F, 8.0F, 1.0F, 0.0F, false);

        fixed_base = new ModelRenderer(this);
        fixed_base.setRotationPoint(0.0F, 0.0F, 7.0F);
        monitor.addChild(fixed_base);
        fixed_base.setTextureOffset(0, 22).addBox(-4.0F, -1.0F, -11.0F, 8.0F, 1.0F, 8.0F, 0.0F, false);
        
        label = new TextLabelModelRenderer(this, TEXT, () -> text, () -> textureToRebind);
		label.setRotationPoint(-6F, -4, -2.3F);
		this.screen.addChild(label);
    }

    public void render(RotateMonitorTile tile, ResourceLocation texture, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        this.textureToRebind = texture;
        this.text = tile.getInfo();
        float rot = tile.previousRot + (tile.rotation - tile.previousRot) * Minecraft.getInstance().getRenderPartialTicks();
        
        float progress = tile.extendAmount;
        
        this.sholder.rotateAngleX = -(float)Math.toRadians(25 + (50 * progress));
        this.forearm.rotateAngleX = (float)Math.toRadians(60 + (80 * progress));
        this.wrist.rotateAngleX = -(float)Math.toRadians(90 + (30 * progress));
        this.screen.rotateAngleX = (float)Math.toRadians(60);
        
        this.spin_y.rotateAngleY = (float)Math.toRadians(rot);
        monitor.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    public void translateToScreenBone(MatrixStack matrixStack){

    }
    
    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
	        float red, float green, float blue, float alpha) {
		
	}
}