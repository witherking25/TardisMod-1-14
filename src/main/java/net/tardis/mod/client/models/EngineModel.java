package net.tardis.mod.client.models;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.Direction;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.TardisEngineTile;

public class EngineModel extends EntityModel<Entity> implements TileAnimation<TardisEngineTile> {
	private final LightModelRenderer glow;
	private final LightModelRenderer rotorglow1;
	private final LightModelRenderer rotorglow2;
	private final LightModelRenderer rotorglow3;
	private final ModelRenderer basiccubes;
	private final ModelRenderer compartmentoutline;
	private final ModelRenderer cross;
	private final ModelRenderer rotorstand;
	private final ModelRenderer rotorstand1;
	private final ModelRenderer rotorstand2;
	private final ModelRenderer rotorstand3;
	private final ModelRenderer door1_rotate_y;
	private final ModelRenderer door2_rotate_y;
	private final ModelRenderer door3_rotate_y;
	private final ModelRenderer door4_rotate_y;

	public EngineModel() {
		textureWidth = 128;
		textureHeight = 128;

		glow = new LightModelRenderer(this);
		glow.setBright(1.0F);
		glow.setRotationPoint(0.0F, -2.0F, 0.0F);
		glow.setTextureOffset(0, 62).addBox(-6.0F, -16.0F, -6.0F, 12.0F, 7.0F, 12.0F, 0.0F, false);

		rotorglow1 = new LightModelRenderer(this);
		this.rotorglow1.setBright(1.0F);
		rotorglow1.setRotationPoint(0.0F, 26.0F, 0.0F);
		glow.addChild(rotorglow1);
		rotorglow1.setTextureOffset(12, 22).addBox(-0.5F, -30.5F, -5.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		rotorglow1.setTextureOffset(48, 22).addBox(-0.5F, -30.5F, 4.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		rotorglow2 = new LightModelRenderer(this);
		this.rotorglow2.setBright(1.0F);
		rotorglow2.setRotationPoint(0.0F, 26.0F, 0.0F);
		glow.addChild(rotorglow2);
		setRotationAngle(rotorglow2, 0.0F, -1.0472F, 0.0F);
		rotorglow2.setTextureOffset(12, 22).addBox(-0.5F, -30.5F, -5.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		rotorglow2.setTextureOffset(48, 22).addBox(-0.5F, -30.5F, 4.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		rotorglow3 = new LightModelRenderer(this);
		this.rotorglow3.setBright(1.0F);
		rotorglow3.setRotationPoint(0.0F, 26.0F, 0.0F);
		glow.addChild(rotorglow3);
		setRotationAngle(rotorglow3, 0.0F, -2.0944F, 0.0F);
		rotorglow3.setTextureOffset(12, 22).addBox(-0.5F, -30.5F, -5.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);
		rotorglow3.setTextureOffset(48, 22).addBox(-0.5F, -30.5F, 4.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);

		basiccubes = new ModelRenderer(this);
		basiccubes.setRotationPoint(0.0F, 24.0F, 0.0F);
		basiccubes.setTextureOffset(0, 22).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, 0.0F, false);
		basiccubes.setTextureOffset(0, 0).addBox(-8.0F, -48.0F, -8.0F, 16.0F, 6.0F, 16.0F, 0.0F, false);
		basiccubes.setTextureOffset(0, 44).addBox(-7.0F, -39.0F, -7.0F, 14.0F, 2.0F, 14.0F, 0.0F, false);
		basiccubes.setTextureOffset(48, 0).addBox(-7.0F, -41.0F, -7.0F, 14.0F, 1.0F, 14.0F, 0.0F, false);
		basiccubes.setTextureOffset(78, 52).addBox(-6.0F, -17.0F, -6.0F, 12.0F, 1.0F, 12.0F, 0.0F, false);
		basiccubes.setTextureOffset(48, 22).addBox(-7.0F, -35.0F, -7.0F, 14.0F, 1.0F, 14.0F, 0.0F, false);

		compartmentoutline = new ModelRenderer(this);
		compartmentoutline.setRotationPoint(0.0F, 24.0F, 0.0F);
		compartmentoutline.setTextureOffset(48, 0).addBox(-6.5F, -15.0F, -6.5F, 3.0F, 8.0F, 3.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(0, 22).addBox(-6.5F, -15.0F, 3.5F, 3.0F, 8.0F, 3.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(42, 44).addBox(3.5F, -15.0F, -6.5F, 3.0F, 8.0F, 3.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(0, 44).addBox(3.5F, -15.0F, 3.5F, 3.0F, 8.0F, 3.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(58, 37).addBox(-7.0F, -7.0F, -7.0F, 14.0F, 1.0F, 14.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(39, 79).addBox(-4.5F, -16.0F, -4.5F, 9.0F, 10.0F, 9.0F, 0.0F, false);
		compartmentoutline.setTextureOffset(65, 65).addBox(-6.5F, -16.0F, -6.5F, 13.0F, 1.0F, 13.0F, 0.0F, false);

		cross = new ModelRenderer(this);
		cross.setRotationPoint(0.0F, -11.0F, 0.0F);
		compartmentoutline.addChild(cross);
		setRotationAngle(cross, 0.0F, -0.7854F, 0.0F);
		cross.setTextureOffset(36, 44).addBox(-0.5F, -4.8F, -10.0F, 1.0F, 10.0F, 20.0F, 0.0F, false);
		cross.setTextureOffset(75, 79).addBox(-10.0F, -4.8F, -0.5F, 20.0F, 10.0F, 1.0F, 0.0F, false);

		rotorstand = new ModelRenderer(this);
		rotorstand.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		rotorstand1 = new ModelRenderer(this);
		rotorstand1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotorstand.addChild(rotorstand1);
		rotorstand1.setTextureOffset(8, 33).addBox(-1.0F, -18.0F, -5.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand1.setTextureOffset(0, 33).addBox(-1.0F, -18.0F, 3.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand1.setTextureOffset(8, 9).addBox(-1.0F, -34.75F, -5.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand1.setTextureOffset(0, 9).addBox(-1.0F, -34.75F, 3.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand1.setTextureOffset(52, 22).addBox(-0.5F, -21.25F, -5.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand1.setTextureOffset(48, 32).addBox(-0.5F, -21.25F, 4.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand1.setTextureOffset(48, 11).addBox(-0.5F, -31.5F, -5.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);
		rotorstand1.setTextureOffset(9, 44).addBox(-0.5F, -31.5F, 4.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);

		rotorstand2 = new ModelRenderer(this);
		rotorstand2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotorstand.addChild(rotorstand2);
		setRotationAngle(rotorstand2, 0.0F, -1.0472F, 0.0F);
		rotorstand2.setTextureOffset(8, 33).addBox(-1.0F, -18.0F, -5.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand2.setTextureOffset(0, 33).addBox(-1.0F, -18.0F, 3.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand2.setTextureOffset(8, 9).addBox(-1.0F, -34.75F, -5.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand2.setTextureOffset(0, 9).addBox(-1.0F, -34.75F, 3.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand2.setTextureOffset(52, 22).addBox(-0.5F, -21.25F, -5.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand2.setTextureOffset(48, 32).addBox(-0.5F, -21.25F, 4.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand2.setTextureOffset(48, 11).addBox(-0.5F, -31.5F, -5.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);
		rotorstand2.setTextureOffset(9, 44).addBox(-0.5F, -31.5F, 4.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);

		rotorstand3 = new ModelRenderer(this);
		rotorstand3.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotorstand.addChild(rotorstand3);
		setRotationAngle(rotorstand3, 0.0F, -2.0944F, 0.0F);
		rotorstand3.setTextureOffset(8, 33).addBox(-1.0F, -18.0F, -5.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand3.setTextureOffset(0, 33).addBox(-1.0F, -18.0F, 3.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		rotorstand3.setTextureOffset(8, 9).addBox(-1.0F, -34.75F, -5.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand3.setTextureOffset(0, 9).addBox(-1.0F, -34.75F, 3.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);
		rotorstand3.setTextureOffset(52, 22).addBox(-0.5F, -21.25F, -5.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand3.setTextureOffset(48, 32).addBox(-0.5F, -21.25F, 4.0F, 1.0F, 3.0F, 1.0F, 0.25F, false);
		rotorstand3.setTextureOffset(48, 11).addBox(-0.5F, -31.5F, -5.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);
		rotorstand3.setTextureOffset(9, 44).addBox(-0.5F, -31.5F, 4.0F, 1.0F, 2.0F, 1.0F, 0.25F, false);

		door1_rotate_y = new ModelRenderer(this);
		door1_rotate_y.setRotationPoint(-3.5F, 13.0F, -6.25F);
		door1_rotate_y.setTextureOffset(7, 105).addBox(0.0F, -4.0F, -0.5F, 7.0F, 8.0F, 1.0F, 0.0F, false);

		door2_rotate_y = new ModelRenderer(this);
		door2_rotate_y.setRotationPoint(6.25F, 13.0F, -3.5F);
		door2_rotate_y.setTextureOffset(25, 99).addBox(-0.5F, -4.0F, 0.0F, 1.0F, 8.0F, 7.0F, 0.0F, false);

		door3_rotate_y = new ModelRenderer(this);
		door3_rotate_y.setRotationPoint(3.5F, 13.0F, 6.25F);
		door3_rotate_y.setTextureOffset(43, 105).addBox(-7.0F, -4.0F, -0.5F, 7.0F, 8.0F, 1.0F, 0.0F, false);

		door4_rotate_y = new ModelRenderer(this);
		door4_rotate_y.setRotationPoint(-6.25F, 13.0F, 3.5F);
		door4_rotate_y.setTextureOffset(61, 99).addBox(-0.5F, -4.0F, -7.0F, 1.0F, 8.0F, 7.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		glow.render(matrixStack, buffer, packedLight, packedOverlay);
		basiccubes.render(matrixStack, buffer, packedLight, packedOverlay);
		compartmentoutline.render(matrixStack, buffer, packedLight, packedOverlay);
		rotorstand.render(matrixStack, buffer, packedLight, packedOverlay);
		door1_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		door2_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		door3_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
		door4_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	
	@Override
	public void animate(TardisEngineTile engine) {
		if(engine.openDoors.contains(Direction.NORTH))
			this.door1_rotate_y.rotateAngleY = (float)Math.toRadians(120);
		else this.door1_rotate_y.rotateAngleY = 0;

		if(engine.openDoors.contains(Direction.EAST))
			this.door4_rotate_y.rotateAngleY = (float)Math.toRadians(120);
		else this.door4_rotate_y.rotateAngleY = 0;

		if(engine.openDoors.contains(Direction.SOUTH))
			this.door3_rotate_y.rotateAngleY = (float) Math.toRadians(120);
		else this.door3_rotate_y.rotateAngleY = 0;

		if(engine.openDoors.contains(Direction.WEST))
			this.door2_rotate_y.rotateAngleY = (float) Math.toRadians(120);
		else this.door2_rotate_y.rotateAngleY = 0;
	}
}