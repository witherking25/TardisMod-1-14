package net.tardis.mod.client.models.entity.drones;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.entity.SecDroidEntity;

public class SecDroneModel extends EntityModel<SecDroidEntity> {
    private final ModelRenderer head;
    private final ModelRenderer offset;
    private final ModelRenderer forcesheild0;
    private final ModelRenderer forcesheild1;
    private final ModelRenderer forcesheild2;

    public SecDroneModel() {
        textureWidth = 64;
        textureHeight = 64;

        head = new ModelRenderer(this);
        head.setRotationPoint(0.0F, 0.0F, 0.0F);
        

        offset = new ModelRenderer(this);
        offset.setRotationPoint(0.5F, 0.0F, 0.5F);
        head.addChild(offset);
        setRotationAngle(offset, 0.0F, 0.0F, 0.7854F);
        offset.setTextureOffset(0, 0).addBox(-5.0F, -5.0F, -5.0F, 9.0F, 9.0F, 9.0F, 0.0F, false);

        forcesheild0 = new ModelRenderer(this);
        forcesheild0.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(forcesheild0, 0.3491F, 1.0472F, 0.0F);
        forcesheild0.setTextureOffset(0, 18).addBox(-3.0F, -3.0F, -11.0F, 6.0F, 12.0F, 1.0F, 0.0F, false);
        forcesheild0.setTextureOffset(14, 18).addBox(-2.0F, -2.0F, -10.5F, 4.0F, 10.0F, 1.0F, 0.0F, false);

        forcesheild1 = new ModelRenderer(this);
        forcesheild1.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(forcesheild1, 0.3491F, 3.1416F, 0.0F);
        forcesheild1.setTextureOffset(0, 18).addBox(-3.0F, -3.0F, -11.0F, 6.0F, 12.0F, 1.0F, 0.0F, false);
        forcesheild1.setTextureOffset(14, 18).addBox(-2.0F, -2.0F, -10.5F, 4.0F, 10.0F, 1.0F, 0.0F, false);

        forcesheild2 = new ModelRenderer(this);
        forcesheild2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(forcesheild2, 0.3491F, -1.0472F, 0.0F);
        forcesheild2.setTextureOffset(0, 18).addBox(-3.0F, -3.0F, -11.0F, 6.0F, 12.0F, 1.0F, 0.0F, false);
        forcesheild2.setTextureOffset(14, 18).addBox(-2.0F, -2.0F, -10.5F, 4.0F, 10.0F, 1.0F, 0.0F, false);
    }

    @Override
    public void setRotationAngles(SecDroidEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }
    
    public void render(SecDroidEntity entity, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay){
        matrixStack.push();
        matrixStack.translate(0, 0.85, 0);
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(180));
        
        float prevRot = ((entity.ticksExisted - 1) * 5F) % 360;
        float rot = (entity.ticksExisted * 5F) % 360;

        float real = prevRot + (rot - prevRot) * Minecraft.getInstance().getRenderPartialTicks();

        this.forcesheild0.rotateAngleY = (float) Math.toRadians(real);
        this.forcesheild1.rotateAngleY = (float) Math.toRadians(real + 120);
        this.forcesheild2.rotateAngleY = (float) Math.toRadians(real + 240);

        head.render(matrixStack, buffer, packedLight, packedOverlay);
        forcesheild0.render(matrixStack, buffer, packedLight, packedOverlay);
        forcesheild1.render(matrixStack, buffer, packedLight, packedOverlay);
        forcesheild2.render(matrixStack, buffer, packedLight, packedOverlay);
        
        matrixStack.pop();
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}