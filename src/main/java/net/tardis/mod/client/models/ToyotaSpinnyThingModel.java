package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.entity.Entity;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ToyotaSpinnyThingModel extends EntityModel implements TileModel<ToyotaSpinnyTile> {
	private final ModelRenderer first_rotor;
	private final ModelRenderer bone627;
	private final ModelRenderer bone628;
	private final ModelRenderer bone230;
	private final ModelRenderer bone231;
	private final ModelRenderer bone232;
	private final ModelRenderer bone240;
	private final ModelRenderer bone241;
	private final ModelRenderer bone242;
	private final ModelRenderer bone243;
	private final ModelRenderer bone244;
	private final ModelRenderer bone245;
	private final ModelRenderer bone246;
	private final ModelRenderer bone611;
	private final LightModelRenderer glow_first_rotor12;
	private final ModelRenderer bone612;
	private final LightModelRenderer glow_first_rotor11;
	private final ModelRenderer bone191;
	private final LightModelRenderer glow_first_rotor10;
	private final ModelRenderer bone193;
	private final LightModelRenderer glow_first_rotor9;
	private final ModelRenderer bone195;
	private final LightModelRenderer glow_first_rotor8;
	private final ModelRenderer bone213;
	private final LightModelRenderer glow_first_rotor7;
	private final ModelRenderer bone214;
	private final LightModelRenderer glow_first_rotor6;
	private final ModelRenderer bone216;
	private final LightModelRenderer glow_first_rotor5;
	private final ModelRenderer bone217;
	private final LightModelRenderer glow_first_rotor4;
	private final ModelRenderer bone218;
	private final LightModelRenderer glow_first_rotor3;
	private final ModelRenderer bone226;
	private final LightModelRenderer glow_first_rotor2;
	private final ModelRenderer bone229;
	private final LightModelRenderer glow_first_rotor;
	private final ModelRenderer second_rotor;
	private final ModelRenderer bone247;
	private final ModelRenderer bone248;
	private final ModelRenderer bone249;
	private final ModelRenderer bone250;
	private final ModelRenderer bone251;
	private final ModelRenderer bone252;
	private final ModelRenderer bone253;
	private final ModelRenderer bone254;
	private final ModelRenderer bone255;
	private final ModelRenderer bone256;
	private final ModelRenderer bone257;
	private final ModelRenderer bone258;
	private final ModelRenderer bone259;
	private final LightModelRenderer glow_second_rotor12;
	private final ModelRenderer bone260;
	private final LightModelRenderer glow_second_rotor11;
	private final ModelRenderer bone261;
	private final LightModelRenderer glow_second_rotor10;
	private final ModelRenderer bone262;
	private final LightModelRenderer glow_second_rotor9;
	private final ModelRenderer bone263;
	private final LightModelRenderer glow_second_rotor8;
	private final ModelRenderer bone264;
	private final LightModelRenderer glow_second_rotor7;
	private final ModelRenderer bone265;
	private final LightModelRenderer glow_second_rotor6;
	private final ModelRenderer bone266;
	private final LightModelRenderer glow_second_rotor5;
	private final ModelRenderer bone267;
	private final LightModelRenderer glow_second_rotor4;
	private final ModelRenderer bone268;
	private final LightModelRenderer glow_second_rotor3;
	private final ModelRenderer bone269;
	private final LightModelRenderer glow_second_rotor2;
	private final ModelRenderer bone270;
	private final LightModelRenderer glow_second_rotor;
	private final ModelRenderer third_rotor;
	private final ModelRenderer bone271;
	private final ModelRenderer bone272;
	private final ModelRenderer bone273;
	private final ModelRenderer bone274;
	private final ModelRenderer bone275;
	private final ModelRenderer bone276;
	private final ModelRenderer bone277;
	private final ModelRenderer bone278;
	private final ModelRenderer bone279;
	private final ModelRenderer bone280;
	private final ModelRenderer bone281;
	private final ModelRenderer bone282;
	private final ModelRenderer bone283;
	private final LightModelRenderer glow_third_rotor12;
	private final ModelRenderer bone284;
	private final LightModelRenderer glow_third_rotor11;
	private final ModelRenderer bone285;
	private final LightModelRenderer glow_third_rotor10;
	private final ModelRenderer bone286;
	private final LightModelRenderer glow_third_rotor9;
	private final ModelRenderer bone287;
	private final LightModelRenderer glow_third_rotor8;
	private final ModelRenderer bone288;
	private final LightModelRenderer glow_third_rotor7;
	private final ModelRenderer bone289;
	private final LightModelRenderer glow_third_rotor6;
	private final ModelRenderer bone290;
	private final LightModelRenderer glow_third_rotor5;
	private final ModelRenderer bone291;
	private final LightModelRenderer glow_third_rotor4;
	private final ModelRenderer bone292;
	private final LightModelRenderer glow_third_rotor3;
	private final ModelRenderer bone293;
	private final LightModelRenderer glow_third_rotor2;
	private final ModelRenderer bone294;
	private final LightModelRenderer glow_third_rotor;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer bone13;
	private final ModelRenderer bottom;
	private final ModelRenderer bone175;
	private final ModelRenderer bone579;
	private final ModelRenderer bone580;
	private final ModelRenderer bone581;
	private final ModelRenderer bone582;
	private final ModelRenderer bone315;
	private final ModelRenderer bone316;
	private final ModelRenderer bone317;
	private final ModelRenderer bone318;
	private final ModelRenderer bone319;
	private final ModelRenderer bone320;
	private final ModelRenderer bone321;
	private final ModelRenderer bone322;
	private final ModelRenderer bone323;
	private final ModelRenderer bone324;
	private final ModelRenderer bone325;
	private final ModelRenderer bone326;
	private final ModelRenderer bone327;
	private final ModelRenderer bone328;
	private final ModelRenderer bone329;
	private final ModelRenderer bone330;
	private final ModelRenderer bone331;
	private final ModelRenderer bone332;
	private final ModelRenderer bone333;
	private final ModelRenderer bone334;
	private final ModelRenderer bone532;
	private final ModelRenderer bone553;
	private final ModelRenderer bone534;
	private final ModelRenderer bone549;
	private final ModelRenderer bone295;
	private final ModelRenderer bone296;
	private final ModelRenderer bone297;
	private final ModelRenderer bone298;
	private final ModelRenderer bone299;
	private final ModelRenderer bone300;
	private final ModelRenderer bone301;
	private final ModelRenderer bone302;
	private final ModelRenderer bone303;
	private final ModelRenderer bone304;
	private final ModelRenderer bone305;
	private final ModelRenderer bone306;
	private final ModelRenderer bone307;
	private final ModelRenderer bone308;
	private final ModelRenderer bone309;
	private final ModelRenderer bone310;
	private final ModelRenderer bone311;
	private final ModelRenderer bone312;
	private final ModelRenderer bone313;
	private final ModelRenderer bone314;
	private final ModelRenderer bone335;
	private final ModelRenderer bone336;
	private final ModelRenderer bone337;
	private final ModelRenderer bone338;
	private final ModelRenderer bone339;
	private final ModelRenderer bone340;
	private final ModelRenderer bone341;
	private final ModelRenderer bone342;
	private final ModelRenderer bone343;
	private final ModelRenderer bone344;
	private final ModelRenderer bone345;
	private final ModelRenderer bone346;
	private final ModelRenderer bone347;

	public ToyotaSpinnyThingModel() {
		textureWidth = 128;
		textureHeight = 128;

		first_rotor = new ModelRenderer(this);
		first_rotor.setRotationPoint(0.0F, 14.375F, 0.0F);
		setRotationAngle(first_rotor, 0.0F, -0.2182F, 0.0F);
		

		bone627 = new ModelRenderer(this);
		bone627.setRotationPoint(0.0F, 9.25F, 0.0F);
		first_rotor.addChild(bone627);
		setRotationAngle(bone627, 0.0F, -0.2618F, 0.0F);
		bone627.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone628 = new ModelRenderer(this);
		bone628.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone627.addChild(bone628);
		setRotationAngle(bone628, 0.0F, -0.5236F, 0.0F);
		bone628.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone230 = new ModelRenderer(this);
		bone230.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone628.addChild(bone230);
		setRotationAngle(bone230, 0.0F, -0.5236F, 0.0F);
		bone230.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone231 = new ModelRenderer(this);
		bone231.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone230.addChild(bone231);
		setRotationAngle(bone231, 0.0F, -0.5236F, 0.0F);
		bone231.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone232 = new ModelRenderer(this);
		bone232.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone231.addChild(bone232);
		setRotationAngle(bone232, 0.0F, -0.5236F, 0.0F);
		bone232.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone240 = new ModelRenderer(this);
		bone240.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone232.addChild(bone240);
		setRotationAngle(bone240, 0.0F, -0.5236F, 0.0F);
		bone240.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone241 = new ModelRenderer(this);
		bone241.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone240.addChild(bone241);
		setRotationAngle(bone241, 0.0F, -0.5236F, 0.0F);
		bone241.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone242 = new ModelRenderer(this);
		bone242.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone241.addChild(bone242);
		setRotationAngle(bone242, 0.0F, -0.5236F, 0.0F);
		bone242.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone243 = new ModelRenderer(this);
		bone243.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone242.addChild(bone243);
		setRotationAngle(bone243, 0.0F, -0.5236F, 0.0F);
		bone243.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone244 = new ModelRenderer(this);
		bone244.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone243.addChild(bone244);
		setRotationAngle(bone244, 0.0F, -0.5236F, 0.0F);
		bone244.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone245 = new ModelRenderer(this);
		bone245.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone244.addChild(bone245);
		setRotationAngle(bone245, 0.0F, -0.5236F, 0.0F);
		bone245.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone246 = new ModelRenderer(this);
		bone246.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone245.addChild(bone246);
		setRotationAngle(bone246, 0.0F, -0.5236F, 0.0F);
		bone246.setTextureOffset(48, 49).addBox(-1.5F, -14.775F, 11.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone611 = new ModelRenderer(this);
		bone611.setRotationPoint(0.0F, 9.25F, 0.0F);
		first_rotor.addChild(bone611);
		bone611.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor12 = new LightModelRenderer(this);
		glow_first_rotor12.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone611.addChild(glow_first_rotor12);
		glow_first_rotor12.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone612 = new ModelRenderer(this);
		bone612.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone611.addChild(bone612);
		setRotationAngle(bone612, 0.0F, -0.5236F, 0.0F);
		bone612.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor11 = new LightModelRenderer(this);
		glow_first_rotor11.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone612.addChild(glow_first_rotor11);
		glow_first_rotor11.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone191 = new ModelRenderer(this);
		bone191.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone612.addChild(bone191);
		setRotationAngle(bone191, 0.0F, -0.5236F, 0.0F);
		bone191.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor10 = new LightModelRenderer(this);
		glow_first_rotor10.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone191.addChild(glow_first_rotor10);
		glow_first_rotor10.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone193 = new ModelRenderer(this);
		bone193.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone191.addChild(bone193);
		setRotationAngle(bone193, 0.0F, -0.5236F, 0.0F);
		bone193.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor9 = new LightModelRenderer(this);
		glow_first_rotor9.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone193.addChild(glow_first_rotor9);
		glow_first_rotor9.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone195 = new ModelRenderer(this);
		bone195.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone193.addChild(bone195);
		setRotationAngle(bone195, 0.0F, -0.5236F, 0.0F);
		bone195.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor8 = new LightModelRenderer(this);
		glow_first_rotor8.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone195.addChild(glow_first_rotor8);
		glow_first_rotor8.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone213 = new ModelRenderer(this);
		bone213.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone195.addChild(bone213);
		setRotationAngle(bone213, 0.0F, -0.5236F, 0.0F);
		bone213.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor7 = new LightModelRenderer(this);
		glow_first_rotor7.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone213.addChild(glow_first_rotor7);
		glow_first_rotor7.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone214 = new ModelRenderer(this);
		bone214.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone213.addChild(bone214);
		setRotationAngle(bone214, 0.0F, -0.5236F, 0.0F);
		bone214.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor6 = new LightModelRenderer(this);
		glow_first_rotor6.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone214.addChild(glow_first_rotor6);
		glow_first_rotor6.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone216 = new ModelRenderer(this);
		bone216.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone214.addChild(bone216);
		setRotationAngle(bone216, 0.0F, -0.5236F, 0.0F);
		bone216.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor5 = new LightModelRenderer(this);
		glow_first_rotor5.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone216.addChild(glow_first_rotor5);
		glow_first_rotor5.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone217 = new ModelRenderer(this);
		bone217.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone216.addChild(bone217);
		setRotationAngle(bone217, 0.0F, -0.5236F, 0.0F);
		bone217.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor4 = new LightModelRenderer(this);
		glow_first_rotor4.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone217.addChild(glow_first_rotor4);
		glow_first_rotor4.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone218 = new ModelRenderer(this);
		bone218.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone217.addChild(bone218);
		setRotationAngle(bone218, 0.0F, -0.5236F, 0.0F);
		bone218.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor3 = new LightModelRenderer(this);
		glow_first_rotor3.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone218.addChild(glow_first_rotor3);
		glow_first_rotor3.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone226 = new ModelRenderer(this);
		bone226.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone218.addChild(bone226);
		setRotationAngle(bone226, 0.0F, -0.5236F, 0.0F);
		bone226.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor2 = new LightModelRenderer(this);
		glow_first_rotor2.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone226.addChild(glow_first_rotor2);
		glow_first_rotor2.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		bone229 = new ModelRenderer(this);
		bone229.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone226.addChild(bone229);
		setRotationAngle(bone229, 0.0F, -0.5236F, 0.0F);
		bone229.setTextureOffset(46, 0).addBox(-4.5F, -14.525F, 11.7F, 9.0F, 9.0F, 6.0F, 0.0F, false);

		glow_first_rotor = new LightModelRenderer(this);
		glow_first_rotor.setRotationPoint(4.0675F, -4.0F, 13.5495F);
		bone229.addChild(glow_first_rotor);
		glow_first_rotor.setTextureOffset(55, 1).addBox(-6.5675F, -2.475F, 0.1505F, 5.0F, 1.0F, 3.0F, 0.0F, false);

		second_rotor = new ModelRenderer(this);
		second_rotor.setRotationPoint(0.0F, 5.875F, 0.0F);
		setRotationAngle(second_rotor, 0.0F, -0.2182F, 0.0F);
		

		bone247 = new ModelRenderer(this);
		bone247.setRotationPoint(0.0F, 9.25F, 0.0F);
		second_rotor.addChild(bone247);
		setRotationAngle(bone247, 0.0F, -0.2618F, 0.0F);
		bone247.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone248 = new ModelRenderer(this);
		bone248.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone247.addChild(bone248);
		setRotationAngle(bone248, 0.0F, -0.5236F, 0.0F);
		bone248.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone249 = new ModelRenderer(this);
		bone249.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone248.addChild(bone249);
		setRotationAngle(bone249, 0.0F, -0.5236F, 0.0F);
		bone249.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone250 = new ModelRenderer(this);
		bone250.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone249.addChild(bone250);
		setRotationAngle(bone250, 0.0F, -0.5236F, 0.0F);
		bone250.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone251 = new ModelRenderer(this);
		bone251.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone250.addChild(bone251);
		setRotationAngle(bone251, 0.0F, -0.5236F, 0.0F);
		bone251.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone252 = new ModelRenderer(this);
		bone252.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone251.addChild(bone252);
		setRotationAngle(bone252, 0.0F, -0.5236F, 0.0F);
		bone252.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone253 = new ModelRenderer(this);
		bone253.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone252.addChild(bone253);
		setRotationAngle(bone253, 0.0F, -0.5236F, 0.0F);
		bone253.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone254 = new ModelRenderer(this);
		bone254.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone253.addChild(bone254);
		setRotationAngle(bone254, 0.0F, -0.5236F, 0.0F);
		bone254.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone255 = new ModelRenderer(this);
		bone255.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone254.addChild(bone255);
		setRotationAngle(bone255, 0.0F, -0.5236F, 0.0F);
		bone255.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone256 = new ModelRenderer(this);
		bone256.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone255.addChild(bone256);
		setRotationAngle(bone256, 0.0F, -0.5236F, 0.0F);
		bone256.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone257 = new ModelRenderer(this);
		bone257.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone256.addChild(bone257);
		setRotationAngle(bone257, 0.0F, -0.5236F, 0.0F);
		bone257.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone258 = new ModelRenderer(this);
		bone258.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone257.addChild(bone258);
		setRotationAngle(bone258, 0.0F, -0.5236F, 0.0F);
		bone258.setTextureOffset(28, 49).addBox(-1.5F, -14.775F, 16.95F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone259 = new ModelRenderer(this);
		bone259.setRotationPoint(0.0F, 9.25F, 0.0F);
		second_rotor.addChild(bone259);
		bone259.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor12 = new LightModelRenderer(this);
		glow_second_rotor12.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone259.addChild(glow_second_rotor12);
		glow_second_rotor12.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone260 = new ModelRenderer(this);
		bone260.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone259.addChild(bone260);
		setRotationAngle(bone260, 0.0F, -0.5236F, 0.0F);
		bone260.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor11 = new LightModelRenderer(this);
		glow_second_rotor11.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone260.addChild(glow_second_rotor11);
		glow_second_rotor11.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone261 = new ModelRenderer(this);
		bone261.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone260.addChild(bone261);
		setRotationAngle(bone261, 0.0F, -0.5236F, 0.0F);
		bone261.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor10 = new LightModelRenderer(this);
		glow_second_rotor10.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone261.addChild(glow_second_rotor10);
		glow_second_rotor10.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone262 = new ModelRenderer(this);
		bone262.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone261.addChild(bone262);
		setRotationAngle(bone262, 0.0F, -0.5236F, 0.0F);
		bone262.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor9 = new LightModelRenderer(this);
		glow_second_rotor9.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone262.addChild(glow_second_rotor9);
		glow_second_rotor9.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone263 = new ModelRenderer(this);
		bone263.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone262.addChild(bone263);
		setRotationAngle(bone263, 0.0F, -0.5236F, 0.0F);
		bone263.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor8 = new LightModelRenderer(this);
		glow_second_rotor8.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone263.addChild(glow_second_rotor8);
		glow_second_rotor8.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone264 = new ModelRenderer(this);
		bone264.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone263.addChild(bone264);
		setRotationAngle(bone264, 0.0F, -0.5236F, 0.0F);
		bone264.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor7 = new LightModelRenderer(this);
		glow_second_rotor7.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone264.addChild(glow_second_rotor7);
		glow_second_rotor7.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone265 = new ModelRenderer(this);
		bone265.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone264.addChild(bone265);
		setRotationAngle(bone265, 0.0F, -0.5236F, 0.0F);
		bone265.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor6 = new LightModelRenderer(this);
		glow_second_rotor6.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone265.addChild(glow_second_rotor6);
		glow_second_rotor6.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone266 = new ModelRenderer(this);
		bone266.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone265.addChild(bone266);
		setRotationAngle(bone266, 0.0F, -0.5236F, 0.0F);
		bone266.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor5 = new LightModelRenderer(this);
		glow_second_rotor5.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone266.addChild(glow_second_rotor5);
		glow_second_rotor5.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone267 = new ModelRenderer(this);
		bone267.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone266.addChild(bone267);
		setRotationAngle(bone267, 0.0F, -0.5236F, 0.0F);
		bone267.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor4 = new LightModelRenderer(this);
		glow_second_rotor4.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone267.addChild(glow_second_rotor4);
		glow_second_rotor4.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone268 = new ModelRenderer(this);
		bone268.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone267.addChild(bone268);
		setRotationAngle(bone268, 0.0F, -0.5236F, 0.0F);
		bone268.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor3 = new LightModelRenderer(this);
		glow_second_rotor3.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone268.addChild(glow_second_rotor3);
		glow_second_rotor3.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone269 = new ModelRenderer(this);
		bone269.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone268.addChild(bone269);
		setRotationAngle(bone269, 0.0F, -0.5236F, 0.0F);
		bone269.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor2 = new LightModelRenderer(this);
		glow_second_rotor2.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone269.addChild(glow_second_rotor2);
		glow_second_rotor2.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		bone270 = new ModelRenderer(this);
		bone270.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone269.addChild(bone270);
		setRotationAngle(bone270, 0.0F, -0.5236F, 0.0F);
		bone270.setTextureOffset(34, 34).addBox(-5.5F, -14.525F, 16.7F, 11.0F, 9.0F, 6.0F, 0.0F, false);

		glow_second_rotor = new LightModelRenderer(this);
		glow_second_rotor.setRotationPoint(0.0F, -3.5F, 12.0F);
		bone270.addChild(glow_second_rotor);
		glow_second_rotor.setTextureOffset(42, 34).addBox(-3.5F, -3.0F, 6.675F, 7.0F, 1.0F, 4.0F, 0.0F, false);

		third_rotor = new ModelRenderer(this);
		third_rotor.setRotationPoint(0.0F, -2.625F, 0.0F);
		setRotationAngle(third_rotor, 0.0F, -0.2182F, 0.0F);
		

		bone271 = new ModelRenderer(this);
		bone271.setRotationPoint(0.0F, 9.25F, 0.0F);
		third_rotor.addChild(bone271);
		setRotationAngle(bone271, 0.0F, -0.2618F, 0.0F);
		bone271.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone272 = new ModelRenderer(this);
		bone272.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone271.addChild(bone272);
		setRotationAngle(bone272, 0.0F, -0.5236F, 0.0F);
		bone272.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone273 = new ModelRenderer(this);
		bone273.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone272.addChild(bone273);
		setRotationAngle(bone273, 0.0F, -0.5236F, 0.0F);
		bone273.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone274 = new ModelRenderer(this);
		bone274.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone273.addChild(bone274);
		setRotationAngle(bone274, 0.0F, -0.5236F, 0.0F);
		bone274.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone275 = new ModelRenderer(this);
		bone275.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone274.addChild(bone275);
		setRotationAngle(bone275, 0.0F, -0.5236F, 0.0F);
		bone275.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone276 = new ModelRenderer(this);
		bone276.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone275.addChild(bone276);
		setRotationAngle(bone276, 0.0F, -0.5236F, 0.0F);
		bone276.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone277 = new ModelRenderer(this);
		bone277.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone276.addChild(bone277);
		setRotationAngle(bone277, 0.0F, -0.5236F, 0.0F);
		bone277.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone278 = new ModelRenderer(this);
		bone278.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone277.addChild(bone278);
		setRotationAngle(bone278, 0.0F, -0.5236F, 0.0F);
		bone278.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone279 = new ModelRenderer(this);
		bone279.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone278.addChild(bone279);
		setRotationAngle(bone279, 0.0F, -0.5236F, 0.0F);
		bone279.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone280 = new ModelRenderer(this);
		bone280.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone279.addChild(bone280);
		setRotationAngle(bone280, 0.0F, -0.5236F, 0.0F);
		bone280.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone281 = new ModelRenderer(this);
		bone281.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone280.addChild(bone281);
		setRotationAngle(bone281, 0.0F, -0.5236F, 0.0F);
		bone281.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone282 = new ModelRenderer(this);
		bone282.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone281.addChild(bone282);
		setRotationAngle(bone282, 0.0F, -0.5236F, 0.0F);
		bone282.setTextureOffset(0, 0).addBox(-1.5F, -14.775F, 22.2F, 3.0F, 10.0F, 7.0F, 0.0F, false);

		bone283 = new ModelRenderer(this);
		bone283.setRotationPoint(0.0F, 9.25F, 0.0F);
		third_rotor.addChild(bone283);
		bone283.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor12 = new LightModelRenderer(this);
		glow_third_rotor12.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone283.addChild(glow_third_rotor12);
		glow_third_rotor12.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone284 = new ModelRenderer(this);
		bone284.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone283.addChild(bone284);
		setRotationAngle(bone284, 0.0F, -0.5236F, 0.0F);
		bone284.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor11 = new LightModelRenderer(this);
		glow_third_rotor11.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone284.addChild(glow_third_rotor11);
		glow_third_rotor11.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone285 = new ModelRenderer(this);
		bone285.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone284.addChild(bone285);
		setRotationAngle(bone285, 0.0F, -0.5236F, 0.0F);
		bone285.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor10 = new LightModelRenderer(this);
		glow_third_rotor10.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone285.addChild(glow_third_rotor10);
		glow_third_rotor10.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone286 = new ModelRenderer(this);
		bone286.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone285.addChild(bone286);
		setRotationAngle(bone286, 0.0F, -0.5236F, 0.0F);
		bone286.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor9 = new LightModelRenderer(this);
		glow_third_rotor9.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone286.addChild(glow_third_rotor9);
		glow_third_rotor9.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone287 = new ModelRenderer(this);
		bone287.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone286.addChild(bone287);
		setRotationAngle(bone287, 0.0F, -0.5236F, 0.0F);
		bone287.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor8 = new LightModelRenderer(this);
		glow_third_rotor8.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone287.addChild(glow_third_rotor8);
		glow_third_rotor8.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone288 = new ModelRenderer(this);
		bone288.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone287.addChild(bone288);
		setRotationAngle(bone288, 0.0F, -0.5236F, 0.0F);
		bone288.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor7 = new LightModelRenderer(this);
		glow_third_rotor7.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone288.addChild(glow_third_rotor7);
		glow_third_rotor7.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone289 = new ModelRenderer(this);
		bone289.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone288.addChild(bone289);
		setRotationAngle(bone289, 0.0F, -0.5236F, 0.0F);
		bone289.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor6 = new LightModelRenderer(this);
		glow_third_rotor6.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone289.addChild(glow_third_rotor6);
		glow_third_rotor6.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone290 = new ModelRenderer(this);
		bone290.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone289.addChild(bone290);
		setRotationAngle(bone290, 0.0F, -0.5236F, 0.0F);
		bone290.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor5 = new LightModelRenderer(this);
		glow_third_rotor5.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone290.addChild(glow_third_rotor5);
		glow_third_rotor5.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone291 = new ModelRenderer(this);
		bone291.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone290.addChild(bone291);
		setRotationAngle(bone291, 0.0F, -0.5236F, 0.0F);
		bone291.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor4 = new LightModelRenderer(this);
		glow_third_rotor4.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone291.addChild(glow_third_rotor4);
		glow_third_rotor4.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone292 = new ModelRenderer(this);
		bone292.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone291.addChild(bone292);
		setRotationAngle(bone292, 0.0F, -0.5236F, 0.0F);
		bone292.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor3 = new LightModelRenderer(this);
		glow_third_rotor3.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone292.addChild(glow_third_rotor3);
		glow_third_rotor3.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone293 = new ModelRenderer(this);
		bone293.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone292.addChild(bone293);
		setRotationAngle(bone293, 0.0F, -0.5236F, 0.0F);
		bone293.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor2 = new LightModelRenderer(this);
		glow_third_rotor2.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone293.addChild(glow_third_rotor2);
		glow_third_rotor2.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone294 = new ModelRenderer(this);
		bone294.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone293.addChild(bone294);
		setRotationAngle(bone294, 0.0F, -0.5236F, 0.0F);
		bone294.setTextureOffset(0, 23).addBox(-7.0F, -14.525F, 21.7F, 14.0F, 9.0F, 6.0F, 0.0F, false);

		glow_third_rotor = new LightModelRenderer(this);
		glow_third_rotor.setRotationPoint(4.9234F, -3.5F, 22.9891F);
		bone294.addChild(glow_third_rotor);
		glow_third_rotor.setTextureOffset(10, 24).addBox(-8.9484F, -3.0F, -0.2891F, 9.0F, 1.0F, 4.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 1.25F, 0.0F);
		third_rotor.addChild(bone2);
		bone2.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -0.5236F, 0.0F);
		bone3.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -0.5236F, 0.0F);
		bone4.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -0.5236F, 0.0F);
		bone5.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -0.5236F, 0.0F);
		bone6.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone6.addChild(bone7);
		setRotationAngle(bone7, 0.0F, -0.5236F, 0.0F);
		bone7.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone7.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -0.5236F, 0.0F);
		bone8.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone8.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -0.5236F, 0.0F);
		bone9.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, 0.0F, -0.5236F, 0.0F);
		bone10.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -0.5236F, 0.0F);
		bone11.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, 0.0F, -0.5236F, 0.0F);
		bone12.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone12.addChild(bone13);
		setRotationAngle(bone13, 0.0F, -0.5236F, 0.0F);
		bone13.setTextureOffset(0, 0).addBox(-6.0F, -6.525F, -0.25F, 12.0F, 1.0F, 22.0F, 0.0F, false);

		bottom = new ModelRenderer(this);
		bottom.setRotationPoint(0.0F, 66.875F, 0.0F);
		

		bone175 = new ModelRenderer(this);
		bone175.setRotationPoint(0.0F, 4.875F, 0.0F);
		bottom.addChild(bone175);
		

		bone579 = new ModelRenderer(this);
		bone579.setRotationPoint(0.0F, -44.75F, 0.0F);
		bone175.addChild(bone579);
		setRotationAngle(bone579, 0.0F, -0.2618F, 0.0F);
		

		bone580 = new ModelRenderer(this);
		bone580.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone579.addChild(bone580);
		setRotationAngle(bone580, 0.6109F, 0.0F, 0.0F);
		bone580.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone581 = new ModelRenderer(this);
		bone581.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone579.addChild(bone581);
		setRotationAngle(bone581, 0.0F, -0.5236F, 0.0F);
		

		bone582 = new ModelRenderer(this);
		bone582.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone581.addChild(bone582);
		setRotationAngle(bone582, 0.6109F, 0.0F, 0.0F);
		bone582.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone315 = new ModelRenderer(this);
		bone315.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone581.addChild(bone315);
		setRotationAngle(bone315, 0.0F, -0.5236F, 0.0F);
		

		bone316 = new ModelRenderer(this);
		bone316.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone315.addChild(bone316);
		setRotationAngle(bone316, 0.6109F, 0.0F, 0.0F);
		bone316.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone317 = new ModelRenderer(this);
		bone317.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone315.addChild(bone317);
		setRotationAngle(bone317, 0.0F, -0.5236F, 0.0F);
		

		bone318 = new ModelRenderer(this);
		bone318.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone317.addChild(bone318);
		setRotationAngle(bone318, 0.6109F, 0.0F, 0.0F);
		bone318.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone319 = new ModelRenderer(this);
		bone319.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone317.addChild(bone319);
		setRotationAngle(bone319, 0.0F, -0.5236F, 0.0F);
		

		bone320 = new ModelRenderer(this);
		bone320.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone319.addChild(bone320);
		setRotationAngle(bone320, 0.6109F, 0.0F, 0.0F);
		bone320.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone321 = new ModelRenderer(this);
		bone321.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone319.addChild(bone321);
		setRotationAngle(bone321, 0.0F, -0.5236F, 0.0F);
		

		bone322 = new ModelRenderer(this);
		bone322.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone321.addChild(bone322);
		setRotationAngle(bone322, 0.6109F, 0.0F, 0.0F);
		bone322.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone323 = new ModelRenderer(this);
		bone323.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone321.addChild(bone323);
		setRotationAngle(bone323, 0.0F, -0.5236F, 0.0F);
		

		bone324 = new ModelRenderer(this);
		bone324.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone323.addChild(bone324);
		setRotationAngle(bone324, 0.6109F, 0.0F, 0.0F);
		bone324.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone325 = new ModelRenderer(this);
		bone325.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone323.addChild(bone325);
		setRotationAngle(bone325, 0.0F, -0.5236F, 0.0F);
		

		bone326 = new ModelRenderer(this);
		bone326.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone325.addChild(bone326);
		setRotationAngle(bone326, 0.6109F, 0.0F, 0.0F);
		bone326.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone327 = new ModelRenderer(this);
		bone327.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone325.addChild(bone327);
		setRotationAngle(bone327, 0.0F, -0.5236F, 0.0F);
		

		bone328 = new ModelRenderer(this);
		bone328.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone327.addChild(bone328);
		setRotationAngle(bone328, 0.6109F, 0.0F, 0.0F);
		bone328.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone329 = new ModelRenderer(this);
		bone329.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone327.addChild(bone329);
		setRotationAngle(bone329, 0.0F, -0.5236F, 0.0F);
		

		bone330 = new ModelRenderer(this);
		bone330.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone329.addChild(bone330);
		setRotationAngle(bone330, 0.6109F, 0.0F, 0.0F);
		bone330.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone331 = new ModelRenderer(this);
		bone331.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone329.addChild(bone331);
		setRotationAngle(bone331, 0.0F, -0.5236F, 0.0F);
		

		bone332 = new ModelRenderer(this);
		bone332.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone331.addChild(bone332);
		setRotationAngle(bone332, 0.6109F, 0.0F, 0.0F);
		bone332.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone333 = new ModelRenderer(this);
		bone333.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone331.addChild(bone333);
		setRotationAngle(bone333, 0.0F, -0.5236F, 0.0F);
		

		bone334 = new ModelRenderer(this);
		bone334.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone333.addChild(bone334);
		setRotationAngle(bone334, 0.6109F, 0.0F, 0.0F);
		bone334.setTextureOffset(0, 49).addBox(-1.0F, -0.5F, -0.375F, 2.0F, 1.0F, 12.0F, 0.0F, false);

		bone532 = new ModelRenderer(this);
		bone532.setRotationPoint(0.0F, -44.75F, 0.0F);
		bone175.addChild(bone532);
		

		bone553 = new ModelRenderer(this);
		bone553.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone532.addChild(bone553);
		setRotationAngle(bone553, 0.6109F, 0.0F, 0.0F);
		bone553.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone534 = new ModelRenderer(this);
		bone534.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone532.addChild(bone534);
		setRotationAngle(bone534, 0.0F, -0.5236F, 0.0F);
		

		bone549 = new ModelRenderer(this);
		bone549.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone534.addChild(bone549);
		setRotationAngle(bone549, 0.6109F, 0.0F, 0.0F);
		bone549.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone295 = new ModelRenderer(this);
		bone295.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone534.addChild(bone295);
		setRotationAngle(bone295, 0.0F, -0.5236F, 0.0F);
		

		bone296 = new ModelRenderer(this);
		bone296.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone295.addChild(bone296);
		setRotationAngle(bone296, 0.6109F, 0.0F, 0.0F);
		bone296.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone297 = new ModelRenderer(this);
		bone297.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone295.addChild(bone297);
		setRotationAngle(bone297, 0.0F, -0.5236F, 0.0F);
		

		bone298 = new ModelRenderer(this);
		bone298.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone297.addChild(bone298);
		setRotationAngle(bone298, 0.6109F, 0.0F, 0.0F);
		bone298.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone299 = new ModelRenderer(this);
		bone299.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone297.addChild(bone299);
		setRotationAngle(bone299, 0.0F, -0.5236F, 0.0F);
		

		bone300 = new ModelRenderer(this);
		bone300.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone299.addChild(bone300);
		setRotationAngle(bone300, 0.6109F, 0.0F, 0.0F);
		bone300.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone301 = new ModelRenderer(this);
		bone301.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone299.addChild(bone301);
		setRotationAngle(bone301, 0.0F, -0.5236F, 0.0F);
		

		bone302 = new ModelRenderer(this);
		bone302.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone301.addChild(bone302);
		setRotationAngle(bone302, 0.6109F, 0.0F, 0.0F);
		bone302.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone303 = new ModelRenderer(this);
		bone303.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone301.addChild(bone303);
		setRotationAngle(bone303, 0.0F, -0.5236F, 0.0F);
		

		bone304 = new ModelRenderer(this);
		bone304.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone303.addChild(bone304);
		setRotationAngle(bone304, 0.6109F, 0.0F, 0.0F);
		bone304.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone305 = new ModelRenderer(this);
		bone305.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone303.addChild(bone305);
		setRotationAngle(bone305, 0.0F, -0.5236F, 0.0F);
		

		bone306 = new ModelRenderer(this);
		bone306.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone305.addChild(bone306);
		setRotationAngle(bone306, 0.6109F, 0.0F, 0.0F);
		bone306.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone307 = new ModelRenderer(this);
		bone307.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone305.addChild(bone307);
		setRotationAngle(bone307, 0.0F, -0.5236F, 0.0F);
		

		bone308 = new ModelRenderer(this);
		bone308.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone307.addChild(bone308);
		setRotationAngle(bone308, 0.6109F, 0.0F, 0.0F);
		bone308.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone309 = new ModelRenderer(this);
		bone309.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone307.addChild(bone309);
		setRotationAngle(bone309, 0.0F, -0.5236F, 0.0F);
		

		bone310 = new ModelRenderer(this);
		bone310.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone309.addChild(bone310);
		setRotationAngle(bone310, 0.6109F, 0.0F, 0.0F);
		bone310.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone311 = new ModelRenderer(this);
		bone311.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone309.addChild(bone311);
		setRotationAngle(bone311, 0.0F, -0.5236F, 0.0F);
		

		bone312 = new ModelRenderer(this);
		bone312.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone311.addChild(bone312);
		setRotationAngle(bone312, 0.6109F, 0.0F, 0.0F);
		bone312.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone313 = new ModelRenderer(this);
		bone313.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone311.addChild(bone313);
		setRotationAngle(bone313, 0.0F, -0.5236F, 0.0F);
		

		bone314 = new ModelRenderer(this);
		bone314.setRotationPoint(0.0F, -3.525F, 6.2F);
		bone313.addChild(bone314);
		setRotationAngle(bone314, 0.6109F, 0.0F, 0.0F);
		bone314.setTextureOffset(1, 38).addBox(-3.0F, -1.0F, 0.0F, 6.0F, 1.0F, 10.0F, 0.0F, false);

		bone335 = new ModelRenderer(this);
		bone335.setRotationPoint(0.0F, -19.5F, 0.0F);
		bottom.addChild(bone335);
		

		bone336 = new ModelRenderer(this);
		bone336.setRotationPoint(0.0F, -18.0F, 0.0F);
		bone335.addChild(bone336);
		bone336.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone337 = new ModelRenderer(this);
		bone337.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone336.addChild(bone337);
		setRotationAngle(bone337, 0.0F, -1.0472F, 0.0F);
		bone337.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone338 = new ModelRenderer(this);
		bone338.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone337.addChild(bone338);
		setRotationAngle(bone338, 0.0F, -1.0472F, 0.0F);
		bone338.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone339 = new ModelRenderer(this);
		bone339.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone338.addChild(bone339);
		setRotationAngle(bone339, 0.0F, -1.0472F, 0.0F);
		bone339.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone340 = new ModelRenderer(this);
		bone340.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone339.addChild(bone340);
		setRotationAngle(bone340, 0.0F, -1.0472F, 0.0F);
		bone340.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone341 = new ModelRenderer(this);
		bone341.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone340.addChild(bone341);
		setRotationAngle(bone341, 0.0F, -1.0472F, 0.0F);
		bone341.setTextureOffset(40, 23).addBox(-3.0F, -6.25F, -0.8F, 6.0F, 1.0F, 7.0F, 0.0F, false);

		bone342 = new ModelRenderer(this);
		bone342.setRotationPoint(0.0F, -18.0F, 0.0F);
		bone335.addChild(bone342);
		setRotationAngle(bone342, 0.0F, -0.5236F, 0.0F);
		bone342.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone343 = new ModelRenderer(this);
		bone343.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone342.addChild(bone343);
		setRotationAngle(bone343, 0.0F, -1.0472F, 0.0F);
		bone343.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone344 = new ModelRenderer(this);
		bone344.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone343.addChild(bone344);
		setRotationAngle(bone344, 0.0F, -1.0472F, 0.0F);
		bone344.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone345 = new ModelRenderer(this);
		bone345.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone344.addChild(bone345);
		setRotationAngle(bone345, 0.0F, -1.0472F, 0.0F);
		bone345.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone346 = new ModelRenderer(this);
		bone346.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone345.addChild(bone346);
		setRotationAngle(bone346, 0.0F, -1.0472F, 0.0F);
		bone346.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		bone347 = new ModelRenderer(this);
		bone347.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone346.addChild(bone347);
		setRotationAngle(bone347, 0.0F, -1.0472F, 0.0F);
		bone347.setTextureOffset(0, 0).addBox(-0.5F, -6.25F, 5.875F, 1.0F, 1.0F, 1.0F, 0.0F, false);
	}
	
	@Override
	public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ToyotaSpinnyTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		float rot = (float)Math.toRadians(tile.prevRotation + (tile.rotation - tile.prevRotation) * Minecraft.getInstance().getRenderPartialTicks());
		this.third_rotor.rotateAngleY = this.first_rotor.rotateAngleY = rot;
		this.second_rotor.rotateAngleY = -rot;
		
		setBrightParts(1F);
        this.first_rotor.render(matrixStack, buffer, packedLight, packedOverlay);
        this.second_rotor.render(matrixStack, buffer, packedLight, packedOverlay);
        this.third_rotor.render(matrixStack, buffer, packedLight, packedOverlay);
		bottom.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {

	}
	
	public void setBrightParts(float amount) {
		this.glow_first_rotor.setBright(amount);
		this.glow_first_rotor2.setBright(amount);
		this.glow_first_rotor3.setBright(amount);
		this.glow_first_rotor4.setBright(amount);
		this.glow_first_rotor5.setBright(amount);
		this.glow_first_rotor6.setBright(amount);
		this.glow_first_rotor7.setBright(amount);
		this.glow_first_rotor8.setBright(amount);
		this.glow_first_rotor9.setBright(amount);
		this.glow_first_rotor10.setBright(amount);
		this.glow_first_rotor11.setBright(amount);
		this.glow_first_rotor12.setBright(amount);
		
		this.glow_second_rotor.setBright(amount);
		this.glow_second_rotor2.setBright(amount);
		this.glow_second_rotor3.setBright(amount);
		this.glow_second_rotor4.setBright(amount);
		this.glow_second_rotor5.setBright(amount);
		this.glow_second_rotor6.setBright(amount);
		this.glow_second_rotor7.setBright(amount);
		this.glow_second_rotor8.setBright(amount);
		this.glow_second_rotor9.setBright(amount);
		this.glow_second_rotor10.setBright(amount);
		this.glow_second_rotor11.setBright(amount);
		this.glow_second_rotor12.setBright(amount);
		
		this.glow_third_rotor.setBright(amount);
		this.glow_third_rotor2.setBright(amount);
		this.glow_third_rotor3.setBright(amount);
		this.glow_third_rotor4.setBright(amount);
		this.glow_third_rotor5.setBright(amount);
		this.glow_third_rotor6.setBright(amount);
		this.glow_third_rotor7.setBright(amount);
		this.glow_third_rotor8.setBright(amount);
		this.glow_third_rotor9.setBright(amount);
		this.glow_third_rotor10.setBright(amount);
		this.glow_third_rotor11.setBright(amount);
		this.glow_third_rotor12.setBright(amount);
	}
}