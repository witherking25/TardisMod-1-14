package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;

public interface IHaveMonitor {

	public void translateMonitorPos(MatrixStack stack);
}
