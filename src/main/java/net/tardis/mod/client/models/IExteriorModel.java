package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.tardis.mod.entity.TardisEntity;

public interface IExteriorModel {

	void renderEntity(TardisEntity ent, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, int red, int green, int blue, float alpha);
}
