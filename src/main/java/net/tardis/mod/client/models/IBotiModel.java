package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.tardis.mod.client.renderers.boti.BOTIRenderer.IDrawable;

public interface IBotiModel {
	
	IDrawable setInteriorDoorRender(MatrixStack stack);
	IDrawable setPortalRender(MatrixStack stack);
	
	void translateModel(MatrixStack stack);
	
}
