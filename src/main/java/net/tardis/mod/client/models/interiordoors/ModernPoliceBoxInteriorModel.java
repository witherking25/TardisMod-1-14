package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.DoorRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ModernPoliceBoxExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class ModernPoliceBoxInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
    private final ModelRenderer doors;
    private final ModelRenderer left2;
    private final ModelRenderer window3;
    private final ModelRenderer frame3;
    private final ModelRenderer glow2;
    private final ModelRenderer right2;
    private final ModelRenderer window4;
    private final ModelRenderer frame4;
    private final ModelRenderer glow5;
    private final ModelRenderer bone;
    private final ModelRenderer roof;
    private final ModelRenderer soto;
    private final ModelRenderer frame;

    public ModernPoliceBoxInteriorModel() {
        textureWidth = 151;
        textureHeight = 151;

        doors = new ModelRenderer(this);
        doors.setRotationPoint(0.0F, 23.0F, 0.0F);
        setRotationAngle(doors, 0.0F, 1.5708F, 0.0F);
        

        left2 = new ModelRenderer(this);
        left2.setRotationPoint(-0.4333F, 1.1833F, 8.25F);
        doors.addChild(left2);
        left2.setTextureOffset(92, 6).addBox(-0.5667F, -2.1833F, -7.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(86, 86).addBox(-0.5667F, -31.1833F, -1.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
        left2.setTextureOffset(16, 87).addBox(-0.5667F, -31.1833F, -7.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
        left2.setTextureOffset(91, 25).addBox(-0.5667F, -31.1833F, -7.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(99, 21).addBox(-0.5667F, -9.1833F, -7.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(99, 21).addBox(-0.5667F, -16.1833F, -7.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(98, 92).addBox(-0.5667F, -23.1833F, -7.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(36, 88).addBox(0.1833F, -8.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(24, 88).addBox(-0.3167F, -8.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(36, 88).addBox(0.1833F, -15.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(24, 88).addBox(-0.3167F, -15.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(36, 88).addBox(0.1833F, -22.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(24, 88).addBox(-0.3167F, -22.1833F, -7.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        left2.setTextureOffset(64, 59).addBox(-0.0667F, -17.1833F, -7.25F, 1.0F, 3.0F, 0.0F, 0.0F, false);

        window3 = new ModelRenderer(this);
        window3.setRotationPoint(0.1833F, -1.1833F, -8.0F);
        left2.addChild(window3);
        

        frame3 = new ModelRenderer(this);
        frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
        window3.addChild(frame3);
        frame3.setTextureOffset(0, 72).addBox(0.0F, -28.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        frame3.setTextureOffset(0, 66).addBox(-0.5F, -28.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

        glow2 = new ModelRenderer(this);
        glow2.setRotationPoint(0.0F, 0.0F, 0.0F);
        window3.addChild(glow2);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow2.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

        right2 = new ModelRenderer(this);
        right2.setRotationPoint(-0.9F, -1.0F, -8.0F);
        doors.addChild(right2);
        right2.setTextureOffset(92, 6).addBox(-0.1F, 0.0F, 0.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(20, 87).addBox(-0.1F, -29.0F, 6.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
        right2.setTextureOffset(16, 87).addBox(-0.1F, -29.0F, 0.0F, 1.0F, 31.0F, 1.0F, 0.0F, false);
        right2.setTextureOffset(91, 25).addBox(-0.1F, -29.0F, 0.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(99, 21).addBox(-0.1F, -7.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(99, 21).addBox(-0.1F, -14.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(98, 92).addBox(-0.1F, -21.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(36, 82).addBox(0.65F, -6.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(24, 82).addBox(0.15F, -6.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(36, 82).addBox(0.65F, -13.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(24, 82).addBox(0.15F, -13.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(24, 82).addBox(0.15F, -20.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        right2.setTextureOffset(0, 0).addBox(0.65F, -20.5F, 0.25F, 0.0F, 7.0F, 7.0F, 0.0F, false);
        right2.setTextureOffset(0, 0).addBox(0.9F, -19.0F, 2.25F, 0.0F, 3.0F, 3.0F, 0.0F, false);
        right2.setTextureOffset(14, 14).addBox(0.9F, -18.0F, 2.75F, 0.0F, 3.0F, 2.0F, 0.0F, false);
        right2.setTextureOffset(68, 14).addBox(0.15F, -18.0F, 1.25F, 1.0F, 2.0F, 0.0F, 0.0F, false);
        right2.setTextureOffset(12, 87).addBox(0.15F, -29.0F, 7.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);

        window4 = new ModelRenderer(this);
        window4.setRotationPoint(0.65F, 1.0F, -0.25F);
        right2.addChild(window4);
        

        frame4 = new ModelRenderer(this);
        frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
        window4.addChild(frame4);
        frame4.setTextureOffset(0, 72).addBox(0.0F, -28.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
        frame4.setTextureOffset(0, 66).addBox(-0.5F, -28.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

        glow5 = new ModelRenderer(this);
        glow5.setRotationPoint(0.0F, 0.0F, 0.0F);
        window4.addChild(glow5);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -27.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -27.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(0.125F, -24.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 37).addBox(-0.625F, -24.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -26.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -26.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(0.125F, -23.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
        glow5.setTextureOffset(63, 38).addBox(-0.625F, -23.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

        bone = new ModelRenderer(this);
        bone.setRotationPoint(8.25F, -32.0F, 0.0F);
        doors.addChild(bone);
        setRotationAngle(bone, 0.0F, 3.1416F, 0.0F);
        bone.setTextureOffset(0, 66).addBox(9.0F, -1.5F, -9.0F, 2.0F, 3.0F, 18.0F, 0.0F, false);
        bone.setTextureOffset(22, 54).addBox(11.25F, -1.0F, -7.0F, 0.0F, 2.0F, 14.0F, 0.0F, false);

        roof = new ModelRenderer(this);
        roof.setRotationPoint(0.0F, 24.75F, 0.0F);
        roof.setTextureOffset(22, 70).addBox(-9.5F, -36.25F, -0.5F, 19.0F, 4.0F, 2.0F, 0.0F, false);
        roof.setTextureOffset(22, 75).addBox(-8.5F, -37.25F, -0.5F, 17.0F, 1.0F, 1.0F, 0.0F, false);

        soto = new ModelRenderer(this);
        soto.setRotationPoint(0.0F, 24.0F, -1.0F);
        soto.setTextureOffset(119, 119).addBox(-8.0F, -32.0F, 0.0F, 16.0F, 32.0F, 1.0F, 0.0F, false);

        frame = new ModelRenderer(this);
        frame.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(frame, 0.0F, 1.5708F, 0.0F);
        frame.setTextureOffset(76, 64).addBox(-0.5F, -31.5F, -8.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
        frame.setTextureOffset(66, 0).addBox(-1.75F, -32.0F, -8.0F, 2.0F, 1.0F, 16.0F, 0.0F, false);
        frame.setTextureOffset(58, 85).addBox(-2.0F, -35.0F, 8.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
        frame.setTextureOffset(50, 85).addBox(-2.0F, -35.0F, -10.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
    }

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrixStack.push();
        matrixStack.translate(0, -0.2, -0.5f); 
        matrixStack.scale(1.1f, 1.1f, 1.1f);
        EnumDoorState state = door.getOpenState();

        switch(state) {
            case ONE:
                this.left2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

                this.right2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

                break;


            case BOTH:
                this.left2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

                this.right2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

                break;


            case CLOSED://close both doors
                this.left2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
                this.right2.rotateAngleY = (float) Math.toRadians(
                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
                break;
            default:
                break;
        }
        doors.render(matrixStack, buffer, packedLight, packedOverlay);
        roof.render(matrixStack, buffer, packedLight, packedOverlay);
        //soto.render(matrixStack, buffer, packedLight, packedOverlay);
        frame.render(matrixStack, buffer, packedLight, packedOverlay);
    
        matrixStack.pop();
        
        if(Minecraft.getInstance().world != null){
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                matrixStack.push();
                PortalInfo info = new PortalInfo();
                info.setPosition(door.getPositionVec());
                info.setWorldShell(data.getBotiWorld());

                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door.rotationYaw - 180, door.getHorizontalFacing());
                    matrix.translate(0, -0.097, -0.5);
                });
                info.setTranslatePortal(matrix -> {
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    matrix.translate(0, 0, 0);
                });

                info.setRenderPortal((matrix, impl) -> {
                    matrix.push();
                    matrix.scale(1.1F, 1.1F, 1.1F);
                    this.soto.render(matrix, impl.getBuffer(RenderType.getEntityCutout(this.getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });

                BOTIRenderer.addPortal(info);
                matrixStack.pop();
            });
        }
        
    }

    @Override
    public ResourceLocation getTexture() {
        return ModernPoliceBoxExteriorRenderer.TEXTURE;
    }
}