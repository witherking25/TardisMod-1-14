package net.tardis.mod.client.models.consoles;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.*;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;

public class KeltConsoleModel extends EntityModel<Entity> implements TileModel<KeltConsoleTile> {
    public static final WorldText WORLD_TEXT = new WorldText(0.22F, 0.15F, 0.002F, 0xFFFFFF);
    private final ModelRenderer base;
    private final ModelRenderer ring;
    private final ModelRenderer bone2;
    private final LightModelRenderer glow_blue_keyboard;
    private final ModelRenderer monitor_2;
    private final ModelRenderer bone;
    private final ModelRenderer bone3;
    private final ModelRenderer bone4;
    private final ModelRenderer bone6;
    private final ModelRenderer bone5;
    private final ModelRenderer bone37;
    private final ModelRenderer bone38;
    private final LightModelRenderer glow_white_2;
    private final LightModelRenderer glow_white_1;
    private final LightModelRenderer glow_red_green_2;
    private final LightModelRenderer glow_red_green_1;
    private final ModelRenderer bone61;
    private final ModelRenderer bone7;
    private final ModelRenderer bone8;
    private final ModelRenderer ring5;
    private final ModelRenderer bone42;
    private final ModelRenderer monitor;
    private final ModelRenderer bone43;
    private final ModelRenderer bone44;
    private final ModelRenderer bone45;
    private final ModelRenderer bone46;
    private final ModelRenderer bone47;
    private final ModelRenderer bone48;
    private final ModelRenderer bone49;
    private final LightModelRenderer glow_red;
    private final LightModelRenderer glow_white;
    private final ModelRenderer bone50;
    private final ModelRenderer bone66;
    private final ModelRenderer bone67;
    private final ModelRenderer ring3;
    private final ModelRenderer bone19;
    private final ModelRenderer communicator;
    private final ModelRenderer com_but;
    private final ModelRenderer com_but2;
    private final ModelRenderer com_but3;
    private final ModelRenderer bone20;
    private final ModelRenderer bone21;
    private final ModelRenderer bone22;
    private final ModelRenderer bone23;
    private final ModelRenderer bone24;
    private final ModelRenderer bone25;
    private final ModelRenderer bone26;
    private final LightModelRenderer glow_white_3;
    private final LightModelRenderer glow_red_green_white;
    private final LightModelRenderer glow_red_green_white_2;
    private final ModelRenderer sonicport;
    private final ModelRenderer bone27;
    private final ModelRenderer bone28;
    private final ModelRenderer bone41;
    private final ModelRenderer ring6;
    private final ModelRenderer bone51;
    private final ModelRenderer bone52;
    private final ModelRenderer bone53;
    private final ModelRenderer bone54;
    private final ModelRenderer bone55;
    private final ModelRenderer bone56;
    private final ModelRenderer bone57;
    private final ModelRenderer bone58;
    private final LightModelRenderer x_control;
    private final LightModelRenderer y_control;
    private final LightModelRenderer z_control;
    private final ModelRenderer bone62;
    private final ModelRenderer door;
    private final ModelRenderer increment_increase_rotate_x;
    private final ModelRenderer XYZ;
    private final ModelRenderer refueler;
    private final LightModelRenderer telepathic_circuit;
    private final ModelRenderer bone59;
    private final ModelRenderer bone60;
    private final ModelRenderer ring4;
    private final ModelRenderer bone29;
    private final ModelRenderer bone30;
    private final ModelRenderer bone31;
    private final ModelRenderer bone32;
    private final ModelRenderer bone33;
    private final ModelRenderer bone34;
    private final ModelRenderer bone35;
    private final ModelRenderer bone36;
    private final LightModelRenderer glow_blue;
    private final LightModelRenderer glow_green;
    private final LightModelRenderer glow_white_4;
    private final LightModelRenderer glow1;
    private final ModelRenderer handbreak_rotate_X;
    private final ModelRenderer throttle_rotate_X;
    private final ModelRenderer randomizer;
    private final ModelRenderer random_but_1_rotate_x;
    private final ModelRenderer random_but_3_rotate_x;
    private final ModelRenderer random_but_2_rotate_x;
    private final ModelRenderer random_but_4_rotate_x;
    private final ModelRenderer random_but_5_rotate_x;
    private final ModelRenderer random_but_6_rotate_x;
    private final ModelRenderer random_but_7_rotate_x;
    private final ModelRenderer random_but_8_rotate_x;
    private final ModelRenderer random_but_9_rotate_x;
    private final ModelRenderer bone39;
    private final ModelRenderer bone40;
    private final ModelRenderer ring2;
    private final ModelRenderer bone9;
    private final ModelRenderer bone10;
    private final ModelRenderer bone11;
    private final ModelRenderer bone12;
    private final ModelRenderer bone13;
    private final ModelRenderer bone14;
    private final ModelRenderer bone15;
    private final ModelRenderer bone16;
    private final LightModelRenderer glow_blue_green;
    private final LightModelRenderer glow_yellow;
    private final LightModelRenderer glow_red2;
    private final ModelRenderer throttle2;
    private final ModelRenderer throttle3;
    private final ModelRenderer bone17;
    private final ModelRenderer bone18;
    private final LightModelRenderer timerotar;
    private final LightModelRenderer timerotar1;
    private final LightModelRenderer timerotar2;
    private final LightModelRenderer timerotar3;

    public KeltConsoleModel() {
        textureWidth = 256;
        textureHeight = 256;

        base = new ModelRenderer(this);
        base.setRotationPoint(-1.5F, 23.8F, -2.6F);
        

        ring = new ModelRenderer(this);
        ring.setRotationPoint(0.0F, 1.2F, -5.0F);
        base.addChild(ring);
        

        bone2 = new ModelRenderer(this);
        bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring.addChild(bone2);
        bone2.setTextureOffset(0, 16).addBox(-8.0F, -13.0F, -9.0F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(126, 68).addBox(-1.492F, -18.4463F, 2.2794F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(0, 40).addBox(-0.65F, -16.3329F, -1.1754F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(34, 39).addBox(-0.65F, -16.3329F, -2.1754F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(79, 106).addBox(2.35F, -16.3329F, -2.1754F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(22, 39).addBox(-0.65F, -16.3329F, -3.1754F, 4.0F, 3.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(113, 55).addBox(-0.65F, -13.5829F, -6.5754F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        bone2.setTextureOffset(246, 112).addBox(-0.8F, -13.9579F, -4.5754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(1.825F, -13.9579F, -4.5254F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(2.4F, -13.9579F, -4.5254F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(-0.8F, -13.9579F, -5.2504F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(1.825F, -13.9579F, -5.2004F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(2.4F, -13.9579F, -5.2004F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(246, 112).addBox(-0.225F, -13.9579F, -4.5754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(238, 72).addBox(0.575F, -13.9579F, -4.5754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(238, 72).addBox(0.575F, -13.9579F, -5.1754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(238, 72).addBox(1.15F, -13.9579F, -4.5754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(238, 72).addBox(1.15F, -13.9579F, -5.1754F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone2.setTextureOffset(64, 155).addBox(-1.675F, -14.5579F, -3.9754F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(113, 154).addBox(3.35F, -14.5579F, -3.9754F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(60, 155).addBox(-1.675F, -14.2579F, -4.5754F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(109, 154).addBox(3.35F, -14.2579F, -4.5754F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(79, 27).addBox(-0.65F, -16.2329F, -2.1754F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(139, 61).addBox(-1.5F, -17.2F, 1.7F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone2.setTextureOffset(139, 61).addBox(-1.5F, -2.0F, 1.7F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone2.setTextureOffset(173, 208).addBox(-8.0F, -12.5F, -9.1F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone2.setTextureOffset(173, 208).addBox(-7.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(-5.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(-3.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(-1.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(1.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(3.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(5.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(7.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone2.setTextureOffset(173, 208).addBox(9.0F, -12.5F, -8.9F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        glow_blue_keyboard = new LightModelRenderer(this);
        glow_blue_keyboard.setRotationPoint(1.5F, -1.0F, 7.6F);
        bone2.addChild(glow_blue_keyboard);
        glow_blue_keyboard.setTextureOffset(252, 40).addBox(1.1F, -12.9829F, -13.9754F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_blue_keyboard.setTextureOffset(252, 40).addBox(-0.4F, -12.9829F, -13.9754F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        glow_blue_keyboard.setTextureOffset(252, 40).addBox(-1.9F, -12.9829F, -13.9754F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        glow_blue_keyboard.setTextureOffset(252, 40).addBox(-2.4F, -12.9829F, -13.9754F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        monitor_2 = new ModelRenderer(this);
        monitor_2.setRotationPoint(-11.6F, -16.4F, 2.3F);
        bone2.addChild(monitor_2);
        monitor_2.setTextureOffset(42, 128).addBox(10.95F, 0.0671F, -5.6754F, 4.0F, 3.0F, 1.0F, -0.1F, false);

        bone = new ModelRenderer(this);
        bone.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone2.addChild(bone);
        setRotationAngle(bone, 0.3491F, 0.5236F, 0.0F);
        bone.setTextureOffset(30, 68).addBox(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone3 = new ModelRenderer(this);
        bone3.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone2.addChild(bone3);
        setRotationAngle(bone3, 0.3491F, -0.5236F, 0.0F);
        bone3.setTextureOffset(30, 68).addBox(1.5981F, -0.513F, -1.4095F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone2.addChild(bone4);
        setRotationAngle(bone4, -0.1745F, 0.5236F, 0.0F);
        bone4.setTextureOffset(64, 46).addBox(0.0F, -1.0F, 0.0F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone4.setTextureOffset(125, 129).addBox(0.0F, -1.0F, 12.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone2.addChild(bone6);
        setRotationAngle(bone6, -0.0873F, 0.2618F, 0.0F);
        

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone2.addChild(bone5);
        setRotationAngle(bone5, -0.1745F, -0.5236F, 0.0F);
        bone5.setTextureOffset(15, 67).addBox(1.5981F, -0.7395F, -1.4772F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone37 = new ModelRenderer(this);
        bone37.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring.addChild(bone37);
        setRotationAngle(bone37, -0.192F, 0.0F, 0.0F);
        bone37.setTextureOffset(126, 127).addBox(-3.0F, -0.9189F, 9.5472F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(112, 42).addBox(-4.5F, -0.9189F, 7.5472F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(8, 118).addBox(-4.0F, -0.9189F, 8.5472F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(54, 8).addBox(-5.0F, -0.9189F, 6.5472F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(104, 36).addBox(-5.5F, -0.9189F, 5.5472F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(93, 95).addBox(-6.0F, -0.9189F, 4.5472F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(94, 10).addBox(-6.5F, -0.9189F, 3.5472F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(94, 0).addBox(-7.0F, -0.9189F, 2.5472F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(86, 93).addBox(-7.5F, -0.9189F, 1.5472F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(90, 78).addBox(-8.0F, -0.9189F, 0.5472F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone37.setTextureOffset(79, 55).addBox(-9.0F, -0.9189F, -0.4528F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone38 = new ModelRenderer(this);
        bone38.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring.addChild(bone38);
        setRotationAngle(bone38, 0.4014F, 0.0F, 0.0F);
        bone38.setTextureOffset(66, 78).addBox(-3.2F, -0.0189F, 8.9972F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(121, 16).addBox(-2.275F, 0.2311F, 8.9972F, 6.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(249, 114).addBox(-2.475F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(1.225F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(249, 114).addBox(-2.475F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(1.225F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(243, 144).addBox(-1.975F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(1.725F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(249, 114).addBox(-1.475F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(2.225F, -0.2689F, 9.7972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(250, 111).addBox(-0.65F, -0.2689F, 9.7972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(250, 111).addBox(-0.65F, -0.2689F, 9.3972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(243, 144).addBox(-1.975F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(1.725F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(249, 114).addBox(-1.475F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(234, 27).addBox(2.225F, -0.2689F, 9.3972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(6, 51).addBox(3.075F, -0.0189F, 8.9972F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(57, 158).addBox(-4.275F, -0.1189F, 7.9472F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone38.setTextureOffset(67, 130).addBox(4.025F, -0.1189F, 6.1472F, 1.0F, 1.0F, 3.0F, -0.1F, false);
        bone38.setTextureOffset(155, 105).addBox(-4.275F, -0.1189F, 8.0972F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone38.setTextureOffset(112, 88).addBox(-4.0F, -0.2689F, 8.6972F, 9.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(123, 85).addBox(-3.325F, -0.2689F, 10.3722F, 7.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(134, 93).addBox(-4.55F, -0.2689F, 7.2972F, 3.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(20, 115).addBox(-2.45F, -0.2689F, 3.2972F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone38.setTextureOffset(92, 114).addBox(2.05F, -0.2689F, 3.2972F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone38.setTextureOffset(45, 63).addBox(2.3F, -0.0189F, 4.9972F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(80, 134).addBox(2.3F, -0.0189F, 6.9972F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(72, 39).addBox(3.3F, -0.0189F, 6.1972F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone38.setTextureOffset(132, 125).addBox(2.3F, 0.1811F, 7.9972F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(45, 68).addBox(2.3F, -0.0189F, 1.9972F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        bone38.setTextureOffset(79, 2).addBox(4.25F, -0.4689F, 1.4722F, 1.0F, 1.0F, 4.0F, -0.45F, false);
        bone38.setTextureOffset(30, 120).addBox(3.3F, 0.0811F, 1.9972F, 3.0F, 1.0F, 4.0F, 0.0F, false);
        bone38.setTextureOffset(75, 130).addBox(6.25F, -0.0189F, 2.2972F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone38.setTextureOffset(25, 154).addBox(7.3F, -0.0189F, 2.2972F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(100, 116).addBox(6.25F, -0.0189F, 1.9972F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(152, 149).addBox(6.55F, -0.0189F, 2.5972F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(135, 16).addBox(-4.15F, 0.0811F, 7.9972F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(175, 214).addBox(-3.7F, -0.3189F, 7.7972F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone38.setTextureOffset(175, 214).addBox(-3.325F, -0.3189F, 7.7972F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone38.setTextureOffset(175, 214).addBox(-2.95F, -0.3189F, 7.7972F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone38.setTextureOffset(66, 134).addBox(-4.15F, 0.0811F, 5.9972F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(241, 112).addBox(-4.275F, -0.2439F, 6.7472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(241, 112).addBox(-4.275F, -0.2439F, 6.1722F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(241, 112).addBox(-3.725F, -0.2439F, 6.7472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(241, 112).addBox(-3.725F, -0.2439F, 6.1722F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(241, 112).addBox(-3.2F, -0.2439F, 6.7472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(241, 112).addBox(-3.2F, -0.2439F, 6.1722F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(238, 152).addBox(-4.3F, -0.4189F, 5.6972F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone38.setTextureOffset(238, 152).addBox(-3.9F, -0.4189F, 5.6972F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone38.setTextureOffset(238, 152).addBox(-3.5F, -0.4189F, 5.6972F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone38.setTextureOffset(238, 152).addBox(-3.1F, -0.4189F, 5.6972F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone38.setTextureOffset(99, 156).addBox(-5.15F, -0.0189F, 5.9972F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(95, 156).addBox(-4.9F, -0.2689F, 6.6972F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(91, 156).addBox(-4.9F, -0.2689F, 7.1972F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(128, 31).addBox(-5.55F, -0.2689F, 5.2472F, 4.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(39, 158).addBox(-6.45F, -0.0189F, 3.9972F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(15, 77).addBox(-5.75F, 0.0811F, 3.4972F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(15, 77).addBox(-5.75F, 0.0811F, 3.2722F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(249, 109).addBox(-4.025F, -0.3689F, 1.7972F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(249, 109).addBox(-3.525F, -0.3689F, 1.7972F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(249, 109).addBox(-3.025F, -0.3689F, 1.7972F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(249, 109).addBox(-4.025F, -0.3689F, 2.3722F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(249, 109).addBox(-3.525F, -0.3689F, 2.3722F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(249, 109).addBox(-3.025F, -0.3689F, 2.3722F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(225, 6).addBox(4.525F, -0.4189F, 1.7972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(4.75F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(3.25F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(244, 111).addBox(3.05F, -0.4189F, 3.6972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(244, 111).addBox(3.05F, -0.4189F, 3.1972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(244, 111).addBox(3.05F, -0.4189F, 2.6972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(111, 133).addBox(4.55F, -0.4189F, 2.6972F, 2.0F, 1.0F, 2.0F, -0.375F, false);
        bone38.setTextureOffset(232, 145).addBox(3.05F, -0.2689F, 2.2472F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(232, 145).addBox(3.975F, -0.2689F, 2.2472F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(246, 107).addBox(3.05F, -0.2689F, 1.7472F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(246, 107).addBox(3.525F, -0.2689F, 1.7472F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(246, 107).addBox(4.0F, -0.2689F, 1.7472F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone38.setTextureOffset(242, 8).addBox(4.525F, -0.4189F, 2.1972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(5.05F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(3.55F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(235, 111).addBox(-5.575F, -0.2689F, 4.4972F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(235, 111).addBox(-5.575F, -0.2689F, 3.8472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(235, 111).addBox(-5.025F, -0.2689F, 4.4972F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(235, 111).addBox(-5.025F, -0.2689F, 3.8472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(235, 111).addBox(-4.475F, -0.2689F, 4.4972F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(235, 111).addBox(-4.475F, -0.2689F, 3.8472F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(134, 114).addBox(-3.85F, -0.4189F, 4.4972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(88, 130).addBox(-3.85F, -0.4189F, 4.1972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(5.35F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(240, 190).addBox(3.85F, -0.4189F, 4.1972F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(80, 130).addBox(-3.85F, -0.4189F, 3.8972F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone38.setTextureOffset(30, 78).addBox(-6.45F, -0.2689F, 2.9972F, 5.0F, 1.0F, 1.0F, -0.25F, false);
        bone38.setTextureOffset(6, 64).addBox(-6.2F, -0.2689F, 2.9972F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone38.setTextureOffset(21, 58).addBox(-6.7F, -0.2689F, 2.9972F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone38.setTextureOffset(45, 73).addBox(-6.2F, 0.0811F, 1.2972F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(6, 58).addBox(-2.45F, -0.2689F, 1.5222F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone38.setTextureOffset(81, 62).addBox(-7.125F, -0.0189F, 1.2972F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone38.setTextureOffset(90, 74).addBox(-8.05F, -0.0189F, 0.9972F, 17.0F, 1.0F, 1.0F, 0.0F, false);
        bone38.setTextureOffset(90, 60).addBox(-8.55F, -0.0189F, -0.0028F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        glow_white_2 = new LightModelRenderer(this);
        glow_white_2.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone38.addChild(glow_white_2);
        glow_white_2.setTextureOffset(175, 214).addBox(2.9F, -12.1189F, -8.2528F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_2.setTextureOffset(175, 214).addBox(2.15F, -12.1189F, -8.2528F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_2.setTextureOffset(175, 214).addBox(2.525F, -12.1189F, -7.8528F, 1.0F, 1.0F, 1.0F, -0.35F, false);

        glow_white_1 = new LightModelRenderer(this);
        glow_white_1.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone38.addChild(glow_white_1);
        glow_white_1.setTextureOffset(175, 214).addBox(2.15F, -12.1189F, -7.8528F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_1.setTextureOffset(175, 214).addBox(2.525F, -12.1189F, -8.2528F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_1.setTextureOffset(175, 214).addBox(2.9F, -12.1189F, -7.8528F, 1.0F, 1.0F, 1.0F, -0.35F, false);

        glow_red_green_2 = new LightModelRenderer(this);
        glow_red_green_2.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone38.addChild(glow_red_green_2);
        glow_red_green_2.setTextureOffset(245, 144).addBox(-5.275F, -12.1689F, -13.6778F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_2.setTextureOffset(247, 59).addBox(-6.175F, -12.1689F, -14.2528F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_2.setTextureOffset(247, 59).addBox(-6.75F, -12.1689F, -13.6778F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        glow_red_green_1 = new LightModelRenderer(this);
        glow_red_green_1.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone38.addChild(glow_red_green_1);
        glow_red_green_1.setTextureOffset(247, 59).addBox(-6.75F, -12.1689F, -14.2528F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_1.setTextureOffset(247, 59).addBox(-6.175F, -12.1689F, -13.6778F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_1.setTextureOffset(245, 144).addBox(-5.275F, -12.1689F, -14.2528F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        bone61 = new ModelRenderer(this);
        bone61.setRotationPoint(-0.05F, 0.9811F, 5.9972F);
        bone38.addChild(bone61);
        setRotationAngle(bone61, -0.3491F, 0.0F, 0.0F);
        

        bone7 = new ModelRenderer(this);
        bone7.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring.addChild(bone7);
        setRotationAngle(bone7, -0.1571F, 0.0F, 0.0F);
        bone7.setTextureOffset(66, 196).addBox(-3.0F, -7.0F, 0.0F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(42, 244).addBox(-3.5F, -4.0F, 0.0F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone7.setTextureOffset(54, 220).addBox(-3.5F, -2.0988F, -0.0156F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone8 = new ModelRenderer(this);
        bone8.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring.addChild(bone8);
        setRotationAngle(bone8, -0.1745F, 0.0F, 0.0F);
        bone8.setTextureOffset(123, 83).addBox(-3.5F, 0.8012F, -0.7156F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(75, 78).addBox(-3.5F, -1.1988F, -0.7156F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(75, 67).addBox(-3.5F, -3.0409F, -0.6343F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(30, 73).addBox(-3.4F, -4.9815F, -0.5703F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(60, 78).addBox(2.5F, -1.1988F, -0.7156F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(75, 63).addBox(2.5F, -3.0409F, -0.6343F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone8.setTextureOffset(15, 72).addBox(2.4F, -4.9815F, -0.5703F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        ring5 = new ModelRenderer(this);
        ring5.setRotationPoint(0.0F, 1.2F, -5.0F);
        base.addChild(ring5);
        setRotationAngle(ring5, 0.0F, 2.0944F, 0.0F);
        

        bone42 = new ModelRenderer(this);
        bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring5.addChild(bone42);
        bone42.setTextureOffset(22, 36).addBox(-16.7058F, -13.0F, -18.8827F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(8, 126).addBox(-10.1978F, -18.4463F, -7.6033F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(0, 29).addBox(-9.3558F, -16.3329F, -11.0581F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(0, 22).addBox(-9.3558F, -16.3329F, -12.0581F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(79, 2).addBox(-6.3558F, -16.3329F, -12.0581F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(8, 129).addBox(-9.3558F, -16.3329F, -13.0581F, 4.0F, 3.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(32, 113).addBox(-9.3558F, -13.5829F, -16.4581F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.5808F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.0058F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-8.0058F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-7.4558F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-6.8808F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-6.3308F, -13.9579F, -14.4581F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.0058F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-8.0058F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-7.4558F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-6.8808F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-6.3308F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.5808F, -13.9579F, -15.1831F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.1308F, -14.0579F, -16.2581F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        bone42.setTextureOffset(243, 113).addBox(-7.6308F, -14.0579F, -16.2581F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        bone42.setTextureOffset(243, 113).addBox(-6.1558F, -14.0579F, -16.2581F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone42.setTextureOffset(243, 113).addBox(-9.6058F, -14.0579F, -16.2581F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone42.setTextureOffset(146, 138).addBox(-10.3808F, -14.5579F, -13.8581F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(146, 125).addBox(-5.3558F, -14.5579F, -13.8581F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(146, 119).addBox(-10.3808F, -14.2579F, -14.4581F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(146, 117).addBox(-5.3558F, -14.2579F, -14.4581F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(60, 88).addBox(-9.3558F, -16.2329F, -12.0581F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(139, 61).addBox(-10.2058F, -17.2F, -8.1827F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(139, 61).addBox(-10.2058F, -2.0F, -8.1827F, 6.0F, 1.0F, 3.0F, 0.0F, false);
        bone42.setTextureOffset(173, 208).addBox(-16.7058F, -12.5F, -18.9827F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone42.setTextureOffset(173, 208).addBox(-15.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-13.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-11.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-9.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-7.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-5.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-3.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(-1.7058F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone42.setTextureOffset(173, 208).addBox(0.2942F, -12.5F, -18.7827F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        monitor = new ModelRenderer(this);
        monitor.setRotationPoint(-11.6F, -16.4F, 2.3F);
        bone42.addChild(monitor);
        monitor.setTextureOffset(60, 128).addBox(2.2442F, 0.0671F, -15.5581F, 4.0F, 3.0F, 1.0F, -0.1F, false);

        bone43 = new ModelRenderer(this);
        bone43.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone42.addChild(bone43);
        setRotationAngle(bone43, 0.3491F, 0.5236F, 0.0F);
        bone43.setTextureOffset(30, 68).addBox(-2.5981F, -4.416F, -12.1329F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone44 = new ModelRenderer(this);
        bone44.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone42.addChild(bone44);
        setRotationAngle(bone44, 0.3491F, -0.5236F, 0.0F);
        

        bone45 = new ModelRenderer(this);
        bone45.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone42.addChild(bone45);
        setRotationAngle(bone45, -0.1745F, 0.5236F, 0.0F);
        bone45.setTextureOffset(45, 77).addBox(-2.5981F, 1.2421F, -12.7154F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone45.setTextureOffset(22, 126).addBox(-2.5981F, 1.2421F, -0.7154F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone46 = new ModelRenderer(this);
        bone46.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone42.addChild(bone46);
        setRotationAngle(bone46, -0.0873F, 0.2618F, 0.0F);
        

        bone47 = new ModelRenderer(this);
        bone47.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone42.addChild(bone47);
        setRotationAngle(bone47, -0.1745F, -0.5236F, 0.0F);
        bone47.setTextureOffset(60, 78).addBox(-10.8827F, -0.0092F, -5.6191F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone47.setTextureOffset(184, 35).addBox(-10.8827F, -0.0092F, 6.3809F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone48 = new ModelRenderer(this);
        bone48.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring5.addChild(bone48);
        setRotationAngle(bone48, -0.192F, 0.0F, 0.0F);
        bone48.setTextureOffset(98, 127).addBox(-11.7058F, 0.9668F, -0.1539F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(111, 111).addBox(-13.2058F, 0.9668F, -2.1539F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(118, 48).addBox(-12.7058F, 0.9668F, -1.1539F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(107, 103).addBox(-13.7058F, 0.9668F, -3.1539F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(64, 60).addBox(-14.2058F, 0.9668F, -4.1539F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(94, 46).addBox(-14.7058F, 0.9668F, -5.1539F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(94, 22).addBox(-15.2058F, 0.9668F, -6.1539F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(94, 8).addBox(-15.7058F, 0.9668F, -7.1539F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(24, 93).addBox(-16.2058F, 0.9668F, -8.1539F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(90, 82).addBox(-16.7058F, 0.9668F, -9.1539F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone48.setTextureOffset(90, 64).addBox(-17.7058F, 0.9668F, -10.1539F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone49 = new ModelRenderer(this);
        bone49.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring5.addChild(bone49);
        setRotationAngle(bone49, 0.4014F, 0.0F, 0.0F);
        bone49.setTextureOffset(81, 67).addBox(-11.9058F, -3.8804F, -0.0999F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(121, 22).addBox(-10.9808F, -3.6304F, -0.0999F, 6.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(246, 36).addBox(-11.0808F, -4.0054F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(246, 36).addBox(-10.4808F, -4.0054F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-7.7808F, -4.1304F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-7.7808F, -4.1304F, 0.1251F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-7.1808F, -4.1304F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-7.1808F, -4.1304F, 0.1251F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-6.5808F, -4.1304F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(239, 112).addBox(-6.5808F, -4.1304F, 0.1251F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(245, 69).addBox(-9.3558F, -4.1304F, 0.7001F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(245, 69).addBox(-9.3558F, -4.1304F, 0.3001F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(245, 69).addBox(-9.8808F, -4.0054F, 0.7001F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(36, 68).addBox(-5.6308F, -3.8804F, -0.0999F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(11, 146).addBox(-12.9808F, -3.9804F, -1.1499F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone49.setTextureOffset(97, 101).addBox(-4.6808F, -3.9804F, -2.9499F, 1.0F, 1.0F, 3.0F, -0.1F, false);
        bone49.setTextureOffset(145, 143).addBox(-12.9808F, -3.9804F, -0.9999F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone49.setTextureOffset(66, 96).addBox(-12.7058F, -4.1304F, -0.3999F, 9.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(119, 95).addBox(-12.0308F, -4.1304F, 1.2751F, 7.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(134, 79).addBox(-13.2558F, -4.1304F, -1.7999F, 3.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(58, 114).addBox(-11.1558F, -4.1304F, -5.7999F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone49.setTextureOffset(0, 115).addBox(-6.6558F, -4.1304F, -5.7999F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone49.setTextureOffset(15, 63).addBox(-6.4058F, -3.8804F, -4.0999F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(134, 52).addBox(-6.4058F, -3.8804F, -2.0999F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(74, 215).addBox(-5.4058F, -3.8804F, -2.8999F, 1.0F, 1.0F, 1.0F, 0.1F, false);
        bone49.setTextureOffset(74, 215).addBox(-5.6058F, -3.8804F, -3.1999F, 1.0F, 1.0F, 1.0F, 0.1F, false);
        bone49.setTextureOffset(28, 115).addBox(-6.4058F, -3.6804F, -1.0999F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(64, 48).addBox(-6.4058F, -3.8804F, -7.0999F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        bone49.setTextureOffset(75, 83).addBox(-4.4558F, -4.3304F, -7.6249F, 1.0F, 1.0F, 4.0F, -0.45F, false);
        bone49.setTextureOffset(42, 119).addBox(-5.4058F, -3.7804F, -7.0999F, 3.0F, 1.0F, 4.0F, 0.0F, false);
        bone49.setTextureOffset(94, 35).addBox(-2.4558F, -3.8804F, -6.7999F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone49.setTextureOffset(122, 145).addBox(-1.4058F, -3.8804F, -6.7999F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(71, 10).addBox(-2.4558F, -3.8804F, -7.0999F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(102, 145).addBox(-2.1558F, -3.8804F, -6.4999F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(135, 45).addBox(-12.8558F, -3.7804F, -1.0999F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(245, 117).addBox(-12.4058F, -4.1804F, -1.2999F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone49.setTextureOffset(251, 113).addBox(-6.1058F, -4.1804F, -1.2999F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone49.setTextureOffset(245, 117).addBox(-12.0308F, -4.1804F, -1.2999F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone49.setTextureOffset(245, 117).addBox(-11.6558F, -4.1804F, -1.2999F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone49.setTextureOffset(252, 102).addBox(-5.3558F, -4.1804F, -1.2999F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone49.setTextureOffset(97, 133).addBox(-12.8558F, -3.7804F, -3.0999F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(246, 127).addBox(-13.3308F, -4.2304F, -3.1749F, 3.0F, 1.0F, 2.0F, -0.4F, false);
        bone49.setTextureOffset(237, 141).addBox(-13.1558F, -4.2804F, -3.4249F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone49.setTextureOffset(237, 141).addBox(-12.7558F, -4.2804F, -3.4249F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone49.setTextureOffset(237, 141).addBox(-12.3558F, -4.2804F, -3.4249F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone49.setTextureOffset(237, 141).addBox(-11.9558F, -4.2804F, -3.4249F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone49.setTextureOffset(237, 141).addBox(-11.5558F, -4.2804F, -3.4249F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone49.setTextureOffset(144, 2).addBox(-13.8558F, -3.8804F, -3.0999F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(144, 0).addBox(-13.6058F, -4.1304F, -2.3999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(142, 144).addBox(-13.6058F, -4.1304F, -1.8999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(79, 46).addBox(-14.2558F, -4.1304F, -3.8499F, 4.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(115, 144).addBox(-15.1558F, -3.8804F, -5.0999F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(79, 12).addBox(-14.4558F, -3.7804F, -5.5999F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(79, 12).addBox(-14.4558F, -3.5804F, -5.9999F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(245, 128).addBox(-14.4558F, -4.0804F, -4.5999F, 2.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(245, 128).addBox(-12.6558F, -4.0804F, -4.5999F, 2.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(246, 126).addBox(-14.4558F, -4.0804F, -5.0999F, 2.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(246, 126).addBox(-12.6558F, -4.0804F, -5.0999F, 2.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(250, 41).addBox(-15.2058F, -4.3304F, -7.4999F, 2.0F, 1.0F, 2.0F, -0.475F, false);
        bone49.setTextureOffset(243, 67).addBox(-12.2808F, -4.0804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(243, 67).addBox(-11.6808F, -4.0804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(241, 111).addBox(-5.4558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(237, 144).addBox(-3.9558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(252, 64).addBox(-3.9558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(237, 152).addBox(-5.4558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(243, 109).addBox(-3.3558F, -4.2804F, -6.3999F, 1.0F, 1.0F, 2.0F, -0.375F, false);
        bone49.setTextureOffset(243, 109).addBox(-3.9558F, -4.2804F, -6.3999F, 1.0F, 1.0F, 2.0F, -0.375F, false);
        bone49.setTextureOffset(241, 111).addBox(-5.4558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(187, 210).addBox(-3.9558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(241, 111).addBox(-5.1558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(237, 144).addBox(-3.6558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(252, 64).addBox(-3.6558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(237, 152).addBox(-5.1558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(241, 111).addBox(-5.1558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(187, 210).addBox(-3.6558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(247, 104).addBox(-13.8558F, -4.0804F, -6.6999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(247, 104).addBox(-12.8558F, -4.0804F, -6.6999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(247, 104).addBox(-11.9808F, -4.0804F, -6.6999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(247, 104).addBox(-12.8558F, -4.0804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(247, 104).addBox(-13.8558F, -4.0804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(241, 111).addBox(-4.8558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(251, 110).addBox(-3.3558F, -4.2804F, -7.2999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(237, 152).addBox(-3.3558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(252, 64).addBox(-4.8558F, -4.2804F, -4.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(241, 111).addBox(-4.8558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(187, 210).addBox(-3.3558F, -4.2804F, -6.8999F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone49.setTextureOffset(0, 69).addBox(-15.1558F, -4.1304F, -6.0999F, 5.0F, 1.0F, 1.0F, -0.25F, false);
        bone49.setTextureOffset(66, 83).addBox(-14.9058F, -4.1304F, -6.0999F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone49.setTextureOffset(81, 78).addBox(-15.4058F, -4.1304F, -6.0999F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone49.setTextureOffset(0, 77).addBox(-14.9058F, -3.7804F, -7.7999F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(6, 72).addBox(-11.1558F, -4.1304F, -7.5749F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone49.setTextureOffset(70, 53).addBox(-15.8308F, -3.8804F, -7.7999F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone49.setTextureOffset(90, 68).addBox(-16.7558F, -3.8804F, -8.0999F, 17.0F, 1.0F, 1.0F, 0.0F, false);
        bone49.setTextureOffset(90, 66).addBox(-17.2558F, -3.8804F, -9.0999F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        glow_red = new LightModelRenderer(this);
        glow_red.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone49.addChild(glow_red);
        glow_red.setTextureOffset(240, 154).addBox(-6.5558F, -15.9804F, -16.9499F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_red.setTextureOffset(240, 154).addBox(-6.1808F, -15.9804F, -16.9499F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_red.setTextureOffset(240, 154).addBox(-5.8058F, -15.9804F, -16.9499F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_red.setTextureOffset(240, 154).addBox(-6.1808F, -15.9804F, -17.3499F, 1.0F, 1.0F, 1.0F, -0.35F, false);

        glow_white = new LightModelRenderer(this);
        glow_white.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone49.addChild(glow_white);
        glow_white.setTextureOffset(148, 196).addBox(-6.1058F, -16.0804F, -22.4499F, 2.0F, 1.0F, 2.0F, -0.375F, false);

        bone50 = new ModelRenderer(this);
        bone50.setRotationPoint(-0.05F, 0.9811F, 5.9972F);
        bone49.addChild(bone50);
        setRotationAngle(bone50, -0.3491F, 0.0F, 0.0F);
        

        bone66 = new ModelRenderer(this);
        bone66.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring5.addChild(bone66);
        setRotationAngle(bone66, -0.1571F, 0.0F, 0.0F);
        bone66.setTextureOffset(66, 196).addBox(-11.7058F, -5.454F, -9.761F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone66.setTextureOffset(42, 244).addBox(-12.2058F, -2.454F, -9.761F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone66.setTextureOffset(54, 220).addBox(-12.2058F, -0.5528F, -9.7767F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone67 = new ModelRenderer(this);
        bone67.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring5.addChild(bone67);
        setRotationAngle(bone67, -0.1745F, 0.0F, 0.0F);
        bone67.setTextureOffset(120, 46).addBox(-12.2058F, 2.5173F, -10.4482F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(79, 7).addBox(-12.2058F, 0.5173F, -10.4482F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(79, 17).addBox(-12.2058F, -1.3248F, -10.3668F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(79, 22).addBox(-12.1058F, -3.2654F, -10.3028F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(0, 80).addBox(-6.2058F, 0.5173F, -10.4482F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(60, 83).addBox(-6.2058F, -1.3248F, -10.3668F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone67.setTextureOffset(75, 83).addBox(-6.4058F, -3.2654F, -10.3028F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        ring3 = new ModelRenderer(this);
        ring3.setRotationPoint(0.0F, 1.2F, 1.0F);
        base.addChild(ring3);
        setRotationAngle(ring3, 0.0F, -2.0944F, 0.0F);
        

        bone19 = new ModelRenderer(this);
        bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring3.addChild(bone19);
        bone19.setTextureOffset(84, 127).addBox(-2.4824F, -18.4463F, -7.2014F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(129, 13).addBox(-1.6404F, -16.3329F, -10.8562F, 4.0F, 2.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(0, 44).addBox(-1.6404F, -16.3329F, -11.8562F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(28, 44).addBox(1.3596F, -16.3329F, -11.8562F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(97, 129).addBox(-1.6404F, -16.3329F, -12.8562F, 4.0F, 3.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(82, 112).addBox(-1.6404F, -13.5829F, -16.2562F, 4.0F, 1.0F, 4.0F, 0.0F, false);
        bone19.setTextureOffset(231, 45).addBox(-1.8154F, -13.9579F, -14.2562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(-1.8154F, -13.9579F, -14.8562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(-1.2654F, -13.9579F, -14.2562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(0.6596F, -13.9579F, -14.2562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(-1.2654F, -13.9579F, -14.8562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(0.0846F, -13.9579F, -14.8562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(231, 45).addBox(1.2846F, -13.9579F, -14.8562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone19.setTextureOffset(241, 114).addBox(-1.3654F, -13.9829F, -16.0562F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        bone19.setTextureOffset(241, 114).addBox(-1.8404F, -13.9829F, -16.0562F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone19.setTextureOffset(241, 114).addBox(1.5846F, -13.9829F, -16.0562F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone19.setTextureOffset(241, 114).addBox(0.1096F, -13.9829F, -16.0562F, 2.0F, 1.0F, 1.0F, -0.275F, false);
        bone19.setTextureOffset(151, 111).addBox(-2.6654F, -14.5579F, -13.6562F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(151, 109).addBox(2.3596F, -14.5579F, -13.6562F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(151, 105).addBox(-2.6654F, -14.2579F, -14.2562F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(151, 91).addBox(2.3596F, -14.2579F, -14.2562F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(32, 8).addBox(-1.6404F, -16.2329F, -11.8562F, 4.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(139, 61).addBox(-2.4904F, -17.2F, -7.9808F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(139, 61).addBox(-2.4904F, -2.0F, -7.9808F, 6.0F, 1.0F, 3.0F, 0.0F, false);
        bone19.setTextureOffset(179, 200).addBox(-8.9904F, -12.5F, -18.6058F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(166, 148).addBox(-8.9904F, -13.0F, -18.4808F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone19.setTextureOffset(173, 208).addBox(-7.9904F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(-5.9904F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(-3.9904F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(-1.9904F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(0.0096F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(2.0096F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(4.0096F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(6.0096F, -12.5F, -18.4058F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone19.setTextureOffset(173, 208).addBox(8.0096F, -12.5F, -18.3808F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        communicator = new ModelRenderer(this);
        communicator.setRotationPoint(-11.6F, -16.4F, 2.3F);
        bone19.addChild(communicator);
        communicator.setTextureOffset(111, 129).addBox(9.9596F, 0.0671F, -15.3562F, 4.0F, 3.0F, 1.0F, -0.1F, false);

        com_but = new ModelRenderer(this);
        com_but.setRotationPoint(12.2544F, 3.0F, -16.1283F);
        communicator.addChild(com_but);
        com_but.setTextureOffset(231, 45).addBox(-0.5698F, -0.5579F, -0.4331F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        com_but2 = new ModelRenderer(this);
        com_but2.setRotationPoint(12.75F, 3.0F, -16.7F);
        communicator.addChild(com_but2);
        com_but2.setTextureOffset(231, 45).addBox(-0.4904F, -0.5579F, -0.4562F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        com_but3 = new ModelRenderer(this);
        com_but3.setRotationPoint(13.35F, 3.05F, -16.05F);
        communicator.addChild(com_but3);
        com_but3.setTextureOffset(231, 45).addBox(-0.4654F, -0.6079F, -0.5062F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        bone20 = new ModelRenderer(this);
        bone20.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone19.addChild(bone20);
        setRotationAngle(bone20, 0.3491F, 0.5236F, 0.0F);
        bone20.setTextureOffset(30, 68).addBox(3.9827F, -3.0368F, -8.3435F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone21 = new ModelRenderer(this);
        bone21.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone19.addChild(bone21);
        setRotationAngle(bone21, 0.3491F, -0.5236F, 0.0F);
        bone21.setTextureOffset(30, 68).addBox(-4.1F, -3.2111F, -8.8224F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone22 = new ModelRenderer(this);
        bone22.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone19.addChild(bone22);
        setRotationAngle(bone22, -0.1745F, 0.5236F, 0.0F);
        bone22.setTextureOffset(64, 3).addBox(3.9827F, 0.5418F, -8.7441F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone22.setTextureOffset(70, 145).addBox(3.9827F, 0.5418F, 3.2559F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone23 = new ModelRenderer(this);
        bone23.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone19.addChild(bone23);
        setRotationAngle(bone23, -0.0873F, 0.2618F, 0.0F);
        

        bone24 = new ModelRenderer(this);
        bone24.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone19.addChild(bone24);
        setRotationAngle(bone24, -0.1745F, -0.5236F, 0.0F);
        bone24.setTextureOffset(64, 17).addBox(-4.1F, 0.6303F, -9.246F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone24.setTextureOffset(164, 101).addBox(-4.1F, 0.6303F, 2.754F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone25 = new ModelRenderer(this);
        bone25.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring3.addChild(bone25);
        setRotationAngle(bone25, -0.192F, 0.0F, 0.0F);
        bone25.setTextureOffset(112, 127).addBox(-3.9904F, 0.9282F, 0.0443F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(112, 44).addBox(-5.4904F, 0.9282F, -1.9557F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(116, 52).addBox(-4.9904F, 0.9282F, -0.9557F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(107, 14).addBox(-5.9904F, 0.9282F, -2.9557F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(104, 38).addBox(-6.4904F, 0.9282F, -3.9557F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(103, 72).addBox(-6.9904F, 0.9282F, -4.9557F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(94, 20).addBox(-7.4904F, 0.9282F, -5.9557F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(94, 6).addBox(-7.9904F, 0.9282F, -6.9557F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(55, 94).addBox(-8.4904F, 0.9282F, -7.9557F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(90, 84).addBox(-8.9904F, 0.9282F, -8.9557F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone25.setTextureOffset(75, 89).addBox(-9.9904F, 0.9282F, -9.9557F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone26 = new ModelRenderer(this);
        bone26.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring3.addChild(bone26);
        setRotationAngle(bone26, 0.4014F, 0.0F, 0.0F);
        bone26.setTextureOffset(85, 2).addBox(-4.1904F, -3.8015F, 0.086F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(56, 121).addBox(-3.2654F, -3.5515F, 0.086F, 6.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(243, 122).addBox(-3.2654F, -4.0515F, 0.086F, 3.0F, 1.0F, 2.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(-0.3654F, -4.0515F, 0.886F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(-0.3654F, -4.0515F, 0.486F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(0.1346F, -4.0515F, 0.886F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(0.6346F, -4.0515F, 0.886F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(0.1346F, -4.0515F, 0.486F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 190).addBox(0.6346F, -4.0515F, 0.486F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(51, 68).addBox(2.0846F, -3.8015F, 0.086F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(150, 22).addBox(-5.2654F, -3.9015F, -0.964F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone26.setTextureOffset(42, 132).addBox(3.0346F, -3.9015F, -2.764F, 1.0F, 1.0F, 3.0F, -0.1F, false);
        bone26.setTextureOffset(150, 18).addBox(-5.2654F, -3.9015F, -0.814F, 1.0F, 1.0F, 1.0F, -0.1F, false);
        bone26.setTextureOffset(9, 111).addBox(-4.9904F, -4.0515F, -0.214F, 9.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(121, 25).addBox(-4.3154F, -4.0515F, 1.461F, 7.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(128, 59).addBox(-5.5404F, -4.0515F, -1.614F, 3.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(100, 115).addBox(-3.4404F, -4.0515F, -5.614F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone26.setTextureOffset(15, 44).addBox(1.0596F, -4.0515F, -5.614F, 1.0F, 1.0F, 6.0F, -0.25F, false);
        bone26.setTextureOffset(15, 53).addBox(1.3096F, -3.8015F, -3.914F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(88, 134).addBox(1.3096F, -3.8015F, -1.914F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(134, 33).addBox(1.3096F, -3.6015F, -0.914F, 3.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(64, 53).addBox(1.3096F, -3.8015F, -6.914F, 1.0F, 1.0F, 4.0F, 0.0F, false);
        bone26.setTextureOffset(60, 71).addBox(3.2596F, -4.2515F, -7.439F, 1.0F, 1.0F, 4.0F, -0.45F, false);
        bone26.setTextureOffset(9, 106).addBox(2.3096F, -3.7015F, -6.914F, 3.0F, 1.0F, 4.0F, 0.0F, false);
        bone26.setTextureOffset(97, 97).addBox(5.2596F, -3.8015F, -6.614F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone26.setTextureOffset(91, 150).addBox(6.3096F, -3.8015F, -6.614F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(40, 120).addBox(5.2596F, -3.8015F, -6.914F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(83, 150).addBox(5.5596F, -3.8015F, -6.314F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(14, 120).addBox(-5.1404F, -3.7015F, -0.914F, 2.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(247, 108).addBox(-4.6904F, -4.1015F, -1.114F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(252, 128).addBox(1.6096F, -4.1015F, -0.714F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(247, 108).addBox(-4.3154F, -4.1015F, -1.114F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(252, 128).addBox(1.9846F, -4.1015F, -1.114F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(247, 108).addBox(-3.9404F, -4.1015F, -1.114F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(252, 128).addBox(2.3596F, -4.1015F, -0.714F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone26.setTextureOffset(132, 87).addBox(-5.1404F, -3.7015F, -2.914F, 2.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(244, 124).addBox(-5.1904F, -4.2015F, -3.114F, 2.0F, 1.0F, 2.0F, -0.4F, false);
        bone26.setTextureOffset(149, 124).addBox(-6.1404F, -3.8015F, -2.914F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(149, 120).addBox(-5.8904F, -4.0515F, -2.214F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(149, 118).addBox(-5.8904F, -4.0515F, -1.714F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(22, 33).addBox(-6.5404F, -4.0515F, -3.664F, 4.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(149, 113).addBox(-7.4404F, -3.8015F, -4.914F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(15, 67).addBox(-6.7404F, -3.7015F, -5.414F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(15, 67).addBox(-6.7404F, -3.7015F, -5.814F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(243, 70).addBox(-6.8404F, -4.0765F, -6.514F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(243, 70).addBox(-6.8404F, -4.0765F, -7.014F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(246, 102).addBox(2.2596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 7).addBox(3.7596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(3.7596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(2.2596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(242, 106).addBox(2.0596F, -4.2015F, -6.214F, 2.0F, 1.0F, 2.0F, -0.375F, false);
        bone26.setTextureOffset(247, 126).addBox(4.3596F, -4.2015F, -6.214F, 1.0F, 1.0F, 2.0F, -0.375F, false);
        bone26.setTextureOffset(247, 126).addBox(3.7596F, -4.2015F, -6.214F, 1.0F, 1.0F, 2.0F, -0.375F, false);
        bone26.setTextureOffset(237, 151).addBox(2.2596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 6).addBox(3.7596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 102).addBox(2.5596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 6).addBox(4.0596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(4.0596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(2.5596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 102).addBox(2.5596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 7).addBox(4.0596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(185, 194).addBox(-5.1404F, -4.0765F, -6.514F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-6.5404F, -4.0765F, -4.489F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-6.5404F, -4.0765F, -4.989F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(185, 194).addBox(-4.6404F, -4.0765F, -6.514F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-6.0404F, -4.0765F, -4.489F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(185, 194).addBox(-4.6404F, -4.0765F, -7.014F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-6.0404F, -4.0765F, -4.989F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-5.5404F, -4.0765F, -4.489F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(240, 107).addBox(-5.5404F, -4.0765F, -4.989F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone26.setTextureOffset(112, 131).addBox(-4.9404F, -4.2015F, -4.414F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(112, 131).addBox(-4.9404F, -4.2015F, -4.714F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 102).addBox(2.8596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(246, 7).addBox(4.3596F, -4.2015F, -7.114F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(4.3596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(197, 208).addBox(2.8596F, -4.2015F, -4.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(237, 151).addBox(2.8596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(228, 6).addBox(4.3596F, -4.2015F, -6.714F, 1.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(112, 131).addBox(-4.9404F, -4.2015F, -5.014F, 2.0F, 1.0F, 1.0F, -0.375F, false);
        bone26.setTextureOffset(64, 27).addBox(-7.4404F, -4.0515F, -5.914F, 5.0F, 1.0F, 1.0F, -0.25F, false);
        bone26.setTextureOffset(81, 83).addBox(-7.1904F, -4.0515F, -5.914F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone26.setTextureOffset(7, 44).addBox(-7.6904F, -4.0515F, -5.914F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone26.setTextureOffset(75, 74).addBox(-7.1904F, -3.7015F, -7.614F, 4.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(71, 12).addBox(-3.4404F, -4.0515F, -7.389F, 1.0F, 1.0F, 2.0F, -0.25F, false);
        bone26.setTextureOffset(6, 80).addBox(-8.1154F, -3.8015F, -7.614F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone26.setTextureOffset(90, 70).addBox(-9.0404F, -3.8015F, -7.914F, 17.0F, 1.0F, 1.0F, 0.0F, false);
        bone26.setTextureOffset(90, 62).addBox(-9.5404F, -3.8015F, -8.914F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        glow_white_3 = new LightModelRenderer(this);
        glow_white_3.setRotationPoint(0.45F, 11.8F, 10.05F);
        bone26.addChild(glow_white_3);
        glow_white_3.setTextureOffset(199, 198).addBox(1.5346F, -15.9015F, -10.764F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_3.setTextureOffset(199, 198).addBox(1.9096F, -15.9015F, -11.164F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        glow_white_3.setTextureOffset(199, 198).addBox(1.1596F, -15.9015F, -11.164F, 1.0F, 1.0F, 1.0F, -0.35F, false);

        glow_red_green_white = new LightModelRenderer(this);
        glow_red_green_white.setRotationPoint(0.45F, 11.8F, 10.05F);
        bone26.addChild(glow_red_green_white);
        glow_red_green_white.setTextureOffset(185, 194).addBox(-4.5904F, -15.8765F, -17.064F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_white.setTextureOffset(185, 194).addBox(-5.5904F, -15.8765F, -17.064F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_white.setTextureOffset(234, 142).addBox(-6.4904F, -15.8765F, -16.564F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_white.setTextureOffset(243, 70).addBox(-7.7904F, -15.8765F, -17.064F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        glow_red_green_white_2 = new LightModelRenderer(this);
        glow_red_green_white_2.setRotationPoint(0.45F, 11.8F, 10.05F);
        bone26.addChild(glow_red_green_white_2);
        glow_red_green_white_2.setTextureOffset(234, 142).addBox(-6.4904F, -15.8765F, -17.039F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_white_2.setTextureOffset(243, 70).addBox(-7.7904F, -15.8765F, -16.564F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        glow_red_green_white_2.setTextureOffset(185, 194).addBox(-4.5904F, -15.8765F, -16.564F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        sonicport = new ModelRenderer(this);
        sonicport.setRotationPoint(-3.55F, 0.9811F, 6.9972F);
        bone26.addChild(sonicport);
        sonicport.setTextureOffset(135, 150).addBox(5.8596F, -4.7576F, -9.7112F, 1.0F, 1.0F, 1.0F, 0.1F, false);
        sonicport.setTextureOffset(135, 150).addBox(5.8596F, -4.8826F, -9.7112F, 1.0F, 1.0F, 1.0F, 0.05F, false);

        bone27 = new ModelRenderer(this);
        bone27.setRotationPoint(-0.05F, 0.9811F, 5.9972F);
        bone26.addChild(bone27);
        setRotationAngle(bone27, -0.3491F, 0.0F, 0.0F);
        

        bone28 = new ModelRenderer(this);
        bone28.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring3.addChild(bone28);
        setRotationAngle(bone28, -0.1571F, 0.0F, 0.0F);
        bone28.setTextureOffset(66, 196).addBox(-3.9904F, -5.4856F, -9.5616F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone28.setTextureOffset(42, 244).addBox(-4.4904F, -2.4856F, -9.5616F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone28.setTextureOffset(54, 220).addBox(-4.4904F, -0.5844F, -9.5772F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone41 = new ModelRenderer(this);
        bone41.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring3.addChild(bone41);
        setRotationAngle(bone41, -0.1745F, 0.0F, 0.0F);
        bone41.setTextureOffset(87, 125).addBox(-4.4904F, 2.4823F, -10.2493F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(64, 17).addBox(-4.4904F, 0.4823F, -10.2493F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(64, 22).addBox(-4.4904F, -1.3598F, -10.168F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(64, 48).addBox(-4.3904F, -3.3004F, -10.104F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(64, 53).addBox(1.5096F, 0.4823F, -10.2493F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(30, 68).addBox(1.5096F, -1.3598F, -10.168F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone41.setTextureOffset(45, 68).addBox(1.4096F, -3.3004F, -10.104F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        ring6 = new ModelRenderer(this);
        ring6.setRotationPoint(0.0F, 1.2F, -5.0F);
        base.addChild(ring6);
        setRotationAngle(ring6, 0.0F, 1.0472F, 0.0F);
        

        bone51 = new ModelRenderer(this);
        bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring6.addChild(bone51);
        bone51.setTextureOffset(22, 22).addBox(-15.2058F, -13.0F, -11.4282F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone51.setTextureOffset(125, 56).addBox(-8.72F, -18.4463F, -0.1524F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone51.setTextureOffset(139, 61).addBox(-8.7058F, -17.2F, -0.7282F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone51.setTextureOffset(139, 61).addBox(-8.7058F, -2.0F, -0.7282F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone51.setTextureOffset(173, 208).addBox(-15.2058F, -12.5F, -11.5282F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone51.setTextureOffset(173, 208).addBox(-14.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-12.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-10.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-8.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-6.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-4.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-2.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(-0.2058F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone51.setTextureOffset(173, 208).addBox(1.7942F, -12.5F, -11.3282F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        bone52 = new ModelRenderer(this);
        bone52.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone51.addChild(bone52);
        setRotationAngle(bone52, 0.3491F, 0.5236F, 0.0F);
        bone52.setTextureOffset(19, 39).addBox(-5.0263F, -1.9515F, -5.3617F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone53 = new ModelRenderer(this);
        bone53.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone51.addChild(bone53);
        setRotationAngle(bone53, 0.3491F, -0.5236F, 0.0F);
        

        bone54 = new ModelRenderer(this);
        bone54.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone51.addChild(bone54);
        setRotationAngle(bone54, -0.1745F, 0.5236F, 0.0F);
        bone54.setTextureOffset(0, 44).addBox(-5.0263F, -0.0092F, -5.6191F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone55 = new ModelRenderer(this);
        bone55.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone51.addChild(bone55);
        setRotationAngle(bone55, -0.0873F, 0.2618F, 0.0F);
        

        bone56 = new ModelRenderer(this);
        bone56.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone51.addChild(bone56);
        setRotationAngle(bone56, -0.1745F, -0.5236F, 0.0F);
        bone56.setTextureOffset(49, 49).addBox(-5.8564F, -1.0F, 0.0F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone56.setTextureOffset(107, 129).addBox(-5.8564F, -1.0F, 12.0F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone57 = new ModelRenderer(this);
        bone57.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring6.addChild(bone57);
        setRotationAngle(bone57, -0.192F, 0.0F, 0.0F);
        bone57.setTextureOffset(127, 65).addBox(-10.2058F, -0.4556F, 7.1636F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(29, 111).addBox(-11.7058F, -0.4556F, 5.1636F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(118, 50).addBox(-11.2058F, -0.4556F, 6.1636F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(40, 20).addBox(-12.2058F, -0.4556F, 4.1636F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(104, 40).addBox(-12.7058F, -0.4556F, 3.1636F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(104, 34).addBox(-13.2058F, -0.4556F, 2.1636F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(94, 24).addBox(-13.7058F, -0.4556F, 1.1636F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(28, 84).addBox(-14.2058F, -0.4556F, 0.1636F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(87, 91).addBox(-14.7058F, -0.4556F, -0.8364F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(90, 80).addBox(-15.2058F, -0.4556F, -1.8364F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone57.setTextureOffset(15, 82).addBox(-16.2058F, -0.4556F, -2.8364F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone58 = new ModelRenderer(this);
        bone58.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring6.addChild(bone58);
        setRotationAngle(bone58, 0.4014F, 0.0F, 0.0F);
        bone58.setTextureOffset(123, 7).addBox(-10.3558F, -0.9677F, 7.762F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(156, 98).addBox(-11.2558F, -0.9677F, 5.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 58).addBox(-10.7558F, -0.9677F, 5.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 56).addBox(-3.3808F, -0.9677F, 5.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 25).addBox(-3.7308F, -0.9677F, 5.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(156, 96).addBox(-10.7558F, -0.9677F, 6.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 42).addBox(-3.7308F, -0.9677F, 6.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(107, 99).addBox(-11.7558F, -0.9677F, 4.762F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(44, 25).addBox(-10.7558F, -0.6927F, 5.762F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone58.setTextureOffset(156, 86).addBox(-12.2558F, -0.9677F, 3.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(64, 17).addBox(-10.2558F, -1.2177F, 1.762F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone58.setTextureOffset(64, 22).addBox(-4.4308F, -1.2177F, 1.762F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone58.setTextureOffset(0, 72).addBox(-7.3058F, -1.2177F, 1.762F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone58.setTextureOffset(54, 2).addBox(-11.2558F, -0.7427F, 2.762F, 9.0F, 1.0F, 2.0F, 0.0F, false);
        bone58.setTextureOffset(139, 98).addBox(-2.2558F, -0.9677F, 3.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(156, 68).addBox(-12.7558F, -0.9677F, 2.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 95).addBox(-2.2558F, -0.9677F, 2.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 124).addBox(-12.2558F, -0.9677F, 2.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(139, 113).addBox(-1.7558F, -0.9677F, 2.762F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(49, 46).addBox(-13.2558F, -0.9677F, 1.762F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(131, 41).addBox(-14.0558F, -0.9677F, -1.238F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone58.setTextureOffset(30, 73).addBox(-10.2558F, -1.2177F, -1.513F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone58.setTextureOffset(0, 51).addBox(-2.1558F, -1.3177F, -1.588F, 1.0F, 2.0F, 4.0F, -0.35F, false);
        bone58.setTextureOffset(0, 33).addBox(-3.6058F, -1.3177F, -1.588F, 1.0F, 3.0F, 4.0F, -0.35F, false);
        bone58.setTextureOffset(245, 106).addBox(-1.5058F, -1.0177F, -1.863F, 1.0F, 1.0F, 4.0F, -0.4F, false);
        bone58.setTextureOffset(60, 132).addBox(-0.4558F, -0.9677F, -1.238F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone58.setTextureOffset(130, 118).addBox(-1.4558F, -0.5677F, -1.238F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone58.setTextureOffset(127, 33).addBox(-2.9558F, -0.5677F, -1.238F, 2.0F, 1.0F, 3.0F, 0.0F, false);
        bone58.setTextureOffset(70, 17).addBox(-2.8808F, -0.6677F, 0.662F, 1.0F, 1.0F, 2.0F, 0.0F, false);
        bone58.setTextureOffset(64, 35).addBox(-13.2558F, -0.5677F, -1.238F, 10.0F, 1.0F, 3.0F, 0.0F, false);
        bone58.setTextureOffset(123, 1).addBox(-9.9808F, -0.7677F, 1.062F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(243, 64).addBox(-7.2558F, -1.0677F, 0.162F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.7058F, -0.9927F, 0.162F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.7058F, -0.9927F, -0.438F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.7058F, -0.9927F, -1.038F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.1558F, -0.9927F, 0.162F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-8.6058F, -0.9927F, 0.162F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.1558F, -0.9927F, -0.438F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-8.6058F, -0.9927F, -0.438F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-9.1558F, -0.9927F, -1.038F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(243, 113).addBox(-8.6058F, -0.9927F, -1.038F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(250, 109).addBox(-11.4808F, -1.2677F, 3.387F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone58.setTextureOffset(250, 109).addBox(-11.4808F, -1.2677F, 2.812F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone58.setTextureOffset(250, 109).addBox(-10.8308F, -1.2677F, 3.387F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone58.setTextureOffset(250, 109).addBox(-10.8308F, -1.2677F, 2.812F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone58.setTextureOffset(0, 139).addBox(-11.6058F, -1.2677F, 3.987F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(238, 42).addBox(-7.2558F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-7.2558F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-7.2558F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(238, 42).addBox(-6.1558F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-6.1558F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-6.1558F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(238, 42).addBox(-5.0558F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-5.0558F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-5.0558F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(238, 42).addBox(-6.7058F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-6.7058F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-6.7058F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(238, 42).addBox(-5.6058F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-5.6058F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-5.6058F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(238, 42).addBox(-4.5058F, -1.2927F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-4.5058F, -1.2927F, 6.187F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(249, 108).addBox(-4.5058F, -1.2927F, 5.562F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(232, 74).addBox(-9.7558F, -1.2677F, 0.762F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(234, 64).addBox(-9.7558F, -1.2677F, 1.062F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-9.6808F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-6.7808F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-9.0308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-6.1308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-8.4308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(118, 138).addBox(-5.5308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(232, 74).addBox(-9.1558F, -1.2677F, 0.762F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(234, 64).addBox(-9.1558F, -1.2677F, 1.062F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.5558F, -1.0677F, 0.162F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.5558F, -1.0677F, -0.238F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.5558F, -1.0677F, -0.638F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.5558F, -1.0677F, -1.038F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.8808F, -1.1677F, 5.637F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.8808F, -1.1677F, 6.237F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.8808F, -1.1677F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.2558F, -1.1677F, 5.637F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.2558F, -1.1677F, 6.237F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(244, 112).addBox(-9.2558F, -1.1677F, 6.862F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone58.setTextureOffset(234, 64).addBox(-9.4558F, -1.2677F, 0.762F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(232, 74).addBox(-9.4558F, -1.2677F, 1.062F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-9.3808F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-6.4808F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(138, 86).addBox(-8.7308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-5.8308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-8.1308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-5.2308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-7.8308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(250, 109).addBox(-4.9308F, -1.2927F, 2.587F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(129, 129).addBox(-9.7308F, -1.2927F, 3.012F, 3.0F, 1.0F, 2.0F, -0.4F, false);
        bone58.setTextureOffset(243, 110).addBox(-3.9558F, -1.2927F, 2.737F, 2.0F, 1.0F, 2.0F, -0.4F, false);
        bone58.setTextureOffset(234, 64).addBox(-8.8558F, -1.2677F, 0.762F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(232, 74).addBox(-8.8558F, -1.2677F, 1.062F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(247, 116).addBox(-5.1558F, -1.2677F, 1.062F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(247, 116).addBox(-5.1558F, -1.2677F, 0.762F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.2558F, -1.0677F, 0.162F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.2558F, -1.0677F, -0.238F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.2558F, -1.0677F, -0.638F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(243, 113).addBox(-4.2558F, -1.0677F, -1.038F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone58.setTextureOffset(242, 70).addBox(-7.2558F, -1.0677F, -0.438F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(243, 64).addBox(-7.2558F, -1.0677F, -1.038F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(242, 70).addBox(-6.5558F, -1.0677F, 0.162F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(221, 189).addBox(-5.1558F, -1.0677F, 0.162F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(168, 206).addBox(-5.8558F, -1.0677F, -0.938F, 1.0F, 1.0F, 2.0F, -0.3F, false);
        bone58.setTextureOffset(243, 64).addBox(-6.5558F, -1.0677F, -0.438F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(221, 189).addBox(-5.1558F, -1.0677F, -0.438F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(242, 70).addBox(-6.5558F, -1.0677F, -1.038F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(221, 189).addBox(-5.1558F, -1.0677F, -1.038F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone58.setTextureOffset(156, 26).addBox(-15.0558F, -0.9677F, -1.238F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(141, 5).addBox(0.5442F, -0.9677F, -1.238F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(141, 10).addBox(-14.7558F, -0.9677F, -0.238F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(141, 1).addBox(0.2442F, -0.9677F, -0.238F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone58.setTextureOffset(20, 86).addBox(-15.7558F, -0.9677F, -2.238F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        x_control = new LightModelRenderer(this);
        x_control.setRotationPoint(-7.3808F, -0.7677F, 1.362F);
        bone58.addChild(x_control);
        x_control.setTextureOffset(252, 40).addBox(-0.6F, -0.446F, -0.4805F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        y_control = new LightModelRenderer(this);
        y_control.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone58.addChild(y_control);
        y_control.setTextureOffset(243, 108).addBox(-7.4308F, -13.0137F, -15.1685F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        z_control = new LightModelRenderer(this);
        z_control.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone58.addChild(z_control);
        z_control.setTextureOffset(244, 140).addBox(-6.4308F, -13.0137F, -15.1685F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        bone62 = new ModelRenderer(this);
        bone62.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone58.addChild(bone62);
        bone62.setTextureOffset(247, 57).addBox(-8.4808F, -12.8677F, -16.988F, 1.0F, 1.0F, 2.0F, -0.3F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(-2.8418F, -0.0469F, 1.633F);
        bone58.addChild(door);
        setRotationAngle(door, 0.445F, 0.0F, 0.0F);
        door.setTextureOffset(0, 86).addBox(-0.034F, -1.5591F, -0.4999F, 1.0F, 2.0F, 1.0F, -0.35F, false);
        door.setTextureOffset(230, 143).addBox(-0.259F, -1.9341F, -0.4999F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        door.setTextureOffset(230, 143).addBox(0.216F, -1.9341F, -0.4999F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        increment_increase_rotate_x = new ModelRenderer(this);
        increment_increase_rotate_x.setRotationPoint(-0.9873F, 6.1685F, 0.1396F);
        bone58.addChild(increment_increase_rotate_x);
        setRotationAngle(increment_increase_rotate_x, 0.2618F, 0.0F, 0.0F);
        increment_increase_rotate_x.setTextureOffset(230, 143).addBox(-0.275F, -7.2F, 0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        increment_increase_rotate_x.setTextureOffset(230, 143).addBox(-0.725F, -7.2F, 0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        XYZ = new ModelRenderer(this);
        XYZ.setRotationPoint(-5.05F, 0.9811F, 1.9972F);
        bone58.addChild(XYZ);
        

        refueler = new ModelRenderer(this);
        refueler.setRotationPoint(-5.05F, 0.9811F, 1.9972F);
        bone58.addChild(refueler);
        refueler.setTextureOffset(128, 101).addBox(-1.7808F, -2.2738F, 1.0148F, 3.0F, 1.0F, 2.0F, -0.4F, false);

        telepathic_circuit = new LightModelRenderer(this);
        telepathic_circuit.setRotationPoint(-5.05F, 0.9811F, 1.9972F);
        bone58.addChild(telepathic_circuit);
        telepathic_circuit.setTextureOffset(227, 144).addBox(-8.0058F, -1.8488F, -3.1852F, 3.0F, 1.0F, 3.0F, -0.1F, false);

        bone59 = new ModelRenderer(this);
        bone59.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring6.addChild(bone59);
        setRotationAngle(bone59, -0.1571F, 0.0F, 0.0F);
        bone59.setTextureOffset(66, 196).addBox(-10.2058F, -6.6201F, -2.3983F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone59.setTextureOffset(42, 244).addBox(-10.7058F, -3.6201F, -2.3983F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone59.setTextureOffset(54, 220).addBox(-10.7058F, -1.7189F, -2.414F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone60 = new ModelRenderer(this);
        bone60.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring6.addChild(bone60);
        setRotationAngle(bone60, -0.1745F, 0.0F, 0.0F);
        bone60.setTextureOffset(123, 3).addBox(-10.7058F, 1.2229F, -3.107F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(0, 33).addBox(-10.7058F, -0.7771F, -3.107F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(28, 25).addBox(-10.7058F, -2.6192F, -3.0256F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(22, 25).addBox(-10.6058F, -4.5598F, -2.9616F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(0, 72).addBox(-4.7058F, -0.7771F, -3.107F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(6, 22).addBox(-4.7058F, -2.6192F, -3.0256F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone60.setTextureOffset(6, 33).addBox(-4.8808F, -4.5598F, -2.9616F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        ring4 = new ModelRenderer(this);
        ring4.setRotationPoint(0.0F, 1.2F, -5.0F);
        base.addChild(ring4);
        setRotationAngle(ring4, 0.0F, 3.1416F, 0.0F);
        

        bone29 = new ModelRenderer(this);
        bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring4.addChild(bone29);
        bone29.setTextureOffset(32, 11).addBox(-11.0F, -13.0F, -23.909F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone29.setTextureOffset(70, 127).addBox(-4.5142F, -18.4463F, -12.6332F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone29.setTextureOffset(139, 61).addBox(-4.5F, -17.2F, -13.209F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone29.setTextureOffset(152, 60).addBox(-4.5F, -2.0F, -13.209F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone29.setTextureOffset(203, 202).addBox(-11.0F, -12.5F, -24.009F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone29.setTextureOffset(203, 202).addBox(-10.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(-8.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(-6.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(-4.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(-2.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(0.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(2.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(4.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone29.setTextureOffset(203, 202).addBox(6.0F, -12.5F, -23.809F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        bone30 = new ModelRenderer(this);
        bone30.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone29.addChild(bone30);
        setRotationAngle(bone30, 0.3491F, 0.5236F, 0.0F);
        

        bone31 = new ModelRenderer(this);
        bone31.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone29.addChild(bone31);
        setRotationAngle(bone31, 0.3491F, -0.5236F, 0.0F);
        

        bone32 = new ModelRenderer(this);
        bone32.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone29.addChild(bone32);
        setRotationAngle(bone32, -0.1745F, 0.5236F, 0.0F);
        bone32.setTextureOffset(30, 54).addBox(4.8564F, 1.5025F, -14.1926F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone33 = new ModelRenderer(this);
        bone33.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone29.addChild(bone33);
        setRotationAngle(bone33, -0.0873F, 0.2618F, 0.0F);
        

        bone34 = new ModelRenderer(this);
        bone34.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone29.addChild(bone34);
        setRotationAngle(bone34, -0.1745F, -0.5236F, 0.0F);
        bone34.setTextureOffset(15, 53).addBox(-8.4545F, 1.2421F, -12.7154F, 1.0F, 1.0F, 13.0F, 0.0F, false);

        bone35 = new ModelRenderer(this);
        bone35.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring4.addChild(bone35);
        setRotationAngle(bone35, -0.192F, 0.0F, 0.0F);
        bone35.setTextureOffset(127, 39).addBox(-6.0F, 1.9258F, -5.0878F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(16, 113).addBox(-7.5F, 1.9258F, -7.0878F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(115, 105).addBox(-7.0F, 1.9258F, -6.0878F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(107, 28).addBox(-8.0F, 1.9258F, -8.0878F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(0, 104).addBox(-8.5F, 1.9258F, -9.0878F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(103, 30).addBox(-9.0F, 1.9258F, -10.0878F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(94, 16).addBox(-9.5F, 1.9258F, -11.0878F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(94, 2).addBox(-10.0F, 1.9258F, -12.0878F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(55, 92).addBox(-10.5F, 1.9258F, -13.0878F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(90, 76).addBox(-11.0F, 1.9258F, -14.0878F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone35.setTextureOffset(79, 51).addBox(-12.0F, 1.9258F, -15.0878F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone36 = new ModelRenderer(this);
        bone36.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring4.addChild(bone36);
        setRotationAngle(bone36, 0.4014F, 0.0F, 0.0F);
        bone36.setTextureOffset(123, 5).addBox(-6.15F, -5.8443F, -3.7266F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(28, 50).addBox(-7.05F, -5.8443F, -5.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(0, 47).addBox(-6.55F, -5.8443F, -5.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(44, 28).addBox(0.825F, -5.8443F, -5.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(34, 42).addBox(0.475F, -5.8443F, -5.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(149, 158).addBox(-6.55F, -5.8443F, -4.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(145, 158).addBox(0.475F, -5.8443F, -4.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(107, 97).addBox(-7.55F, -5.8443F, -6.7266F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(8, 115).addBox(-6.55F, -5.5693F, -5.7266F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone36.setTextureOffset(105, 158).addBox(-8.05F, -5.8443F, -7.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(79, 17).addBox(-6.05F, -6.0943F, -9.7266F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone36.setTextureOffset(79, 22).addBox(-0.225F, -6.0943F, -9.7266F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone36.setTextureOffset(15, 58).addBox(-3.1F, -6.0943F, -9.7266F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone36.setTextureOffset(54, 5).addBox(-7.05F, -5.6193F, -8.7266F, 9.0F, 1.0F, 2.0F, 0.0F, false);
        bone36.setTextureOffset(25, 158).addBox(1.95F, -5.8443F, -7.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(21, 158).addBox(-8.55F, -5.8443F, -8.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(157, 145).addBox(1.95F, -5.8443F, -8.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(157, 143).addBox(-8.05F, -5.8443F, -8.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(157, 141).addBox(2.45F, -5.8443F, -8.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(94, 26).addBox(-9.05F, -5.8443F, -9.7266F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(91, 130).addBox(-9.85F, -5.8443F, -12.7266F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone36.setTextureOffset(0, 64).addBox(-6.05F, -6.0943F, -13.3516F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone36.setTextureOffset(34, 46).addBox(2.05F, -6.1943F, -13.3516F, 1.0F, 2.0F, 4.0F, -0.35F, false);
        bone36.setTextureOffset(0, 22).addBox(0.6F, -6.1943F, -13.3516F, 1.0F, 3.0F, 4.0F, -0.35F, false);
        bone36.setTextureOffset(240, 107).addBox(2.725F, -5.9193F, -13.3516F, 1.0F, 1.0F, 4.0F, -0.4F, false);
        bone36.setTextureOffset(240, 107).addBox(1.35F, -5.9193F, -13.3516F, 1.0F, 1.0F, 4.0F, -0.4F, false);
        bone36.setTextureOffset(83, 130).addBox(3.75F, -5.8443F, -12.7266F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone36.setTextureOffset(129, 114).addBox(2.75F, -5.4693F, -12.7266F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone36.setTextureOffset(128, 27).addBox(1.25F, -5.4693F, -12.7266F, 2.0F, 1.0F, 3.0F, 0.0F, false);
        bone36.setTextureOffset(94, 110).addBox(-5.825F, -5.4443F, -12.7266F, 7.0F, 1.0F, 3.0F, 0.0F, false);
        bone36.setTextureOffset(123, 77).addBox(-5.75F, -5.6443F, -10.4266F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(188, 205).addBox(-4.05F, -5.9443F, -11.3266F, 2.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(225, 144).addBox(-7.35F, -6.1693F, -9.1016F, 1.0F, 1.0F, 2.0F, -0.4F, false);
        bone36.setTextureOffset(230, 145).addBox(-7.35F, -6.1693F, -7.5016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(239, 109).addBox(-5.7F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(239, 109).addBox(-5.125F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(239, 109).addBox(-4.55F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(239, 109).addBox(-1.65F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(239, 109).addBox(-4.0F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(239, 109).addBox(-1.1F, -5.9943F, -4.6766F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone36.setTextureOffset(240, 124).addBox(-5.65F, -6.1693F, -6.3266F, 6.0F, 1.0F, 2.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-5.55F, -6.1443F, -10.7266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-5.55F, -6.1443F, -10.4266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(240, 42).addBox(-5.475F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-2.575F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-1.925F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-1.325F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(246, 126).addBox(-4.95F, -6.1443F, -10.7266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(246, 126).addBox(-4.95F, -6.1443F, -10.4266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(225, 144).addBox(-7.05F, -6.1693F, -9.1016F, 1.0F, 1.0F, 2.0F, -0.4F, false);
        bone36.setTextureOffset(230, 145).addBox(-7.075F, -6.1693F, -7.5016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(237, 112).addBox(-6.55F, -6.1693F, -9.1016F, 1.0F, 1.0F, 2.0F, -0.4F, false);
        bone36.setTextureOffset(244, 147).addBox(-6.55F, -6.1693F, -7.5016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(239, 72).addBox(-6.8F, -6.1693F, -7.5016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-5.25F, -6.1443F, -10.7266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-5.25F, -6.1443F, -10.4266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(240, 42).addBox(-5.175F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-2.275F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-1.625F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 71).addBox(-1.025F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(238, 69).addBox(-3.625F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(242, 115).addBox(-0.725F, -6.1693F, -8.9016F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(242, 104).addBox(-2.625F, -6.1693F, -8.4766F, 3.0F, 1.0F, 2.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-4.65F, -6.1443F, -10.7266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(224, 153).addBox(-4.65F, -6.1443F, -10.4266F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(183, 197).addBox(-0.95F, -6.1443F, -10.4266F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(187, 218).addBox(-3.15F, -6.1443F, -10.4266F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(183, 197).addBox(-0.95F, -6.1443F, -10.7266F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(187, 218).addBox(-3.15F, -6.1443F, -10.7266F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone36.setTextureOffset(188, 205).addBox(-4.05F, -5.9443F, -11.9266F, 2.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(188, 205).addBox(-4.05F, -5.9443F, -12.5266F, 2.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(230, 154).addBox(-2.35F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-4.55F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.05F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.55F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.85F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.85F, -5.9443F, -11.8016F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(236, 38).addBox(-0.85F, -5.9443F, -12.7516F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.85F, -5.9443F, -12.2766F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.4F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.4F, -5.9443F, -11.8016F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(236, 38).addBox(-0.4F, -5.9443F, -12.7516F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(-0.4F, -5.9443F, -12.2766F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(0.05F, -5.9443F, -11.3266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(0.05F, -5.9443F, -11.8016F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(236, 38).addBox(0.05F, -5.9443F, -12.7516F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(240, 116).addBox(0.05F, -5.9443F, -12.2766F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(245, 111).addBox(-1.55F, -5.9443F, -12.4266F, 1.0F, 1.0F, 2.0F, -0.3F, false);
        bone36.setTextureOffset(230, 154).addBox(-2.35F, -5.9443F, -11.9266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-4.55F, -5.9443F, -11.9266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.05F, -5.9443F, -11.9266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.55F, -5.9443F, -11.9266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(230, 154).addBox(-2.35F, -5.9443F, -12.5266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-4.55F, -5.9443F, -12.5266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.05F, -5.9443F, -12.5266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(84, 217).addBox(-5.55F, -5.9443F, -12.5266F, 1.0F, 1.0F, 1.0F, -0.3F, false);
        bone36.setTextureOffset(147, 132).addBox(-10.85F, -5.8443F, -12.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(147, 130).addBox(4.75F, -5.8443F, -12.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(147, 127).addBox(-10.55F, -5.8443F, -11.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(147, 122).addBox(4.45F, -5.8443F, -11.7266F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone36.setTextureOffset(20, 88).addBox(-11.55F, -5.8443F, -13.7266F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        glow_blue = new LightModelRenderer(this);
        glow_blue.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone36.addChild(glow_blue);
        glow_blue.setTextureOffset(241, 32).addBox(-3.125F, -17.7693F, -20.7516F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        glow_blue.setTextureOffset(241, 32).addBox(-3.7F, -17.7693F, -20.7516F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        glow_green = new LightModelRenderer(this);
        glow_green.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone36.addChild(glow_green);
        glow_green.setTextureOffset(238, 69).addBox(-5.275F, -17.9693F, -24.9516F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        glow_green.setTextureOffset(238, 69).addBox(-4.975F, -17.9693F, -24.9516F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        glow_green.setTextureOffset(238, 69).addBox(-4.675F, -17.9693F, -24.9516F, 1.0F, 1.0F, 1.0F, -0.4F, false);
        glow_green.setTextureOffset(238, 69).addBox(-4.375F, -17.9693F, -24.9516F, 1.0F, 1.0F, 1.0F, -0.4F, false);

        glow_white_4 = new LightModelRenderer(this);
        glow_white_4.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone36.addChild(glow_white_4);
        glow_white_4.setTextureOffset(128, 97).addBox(-5.975F, -17.9693F, -24.5266F, 3.0F, 1.0F, 2.0F, -0.4F, false);

        glow1 = new LightModelRenderer(this);
        glow1.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone36.addChild(glow1);
        glow1.setTextureOffset(248, 155).addBox(-0.2F, -17.9693F, -24.8016F, 2.0F, 1.0F, 2.0F, -0.4F, false);

        handbreak_rotate_X = new ModelRenderer(this);
        handbreak_rotate_X.setRotationPoint(3.2685F, 0.5656F, -11.5915F);
        bone36.addChild(handbreak_rotate_X);
        setRotationAngle(handbreak_rotate_X, 0.2269F, 0.0F, 0.0F);
        handbreak_rotate_X.setTextureOffset(245, 118).addBox(-0.5F, -6.5183F, 0.4965F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        handbreak_rotate_X.setTextureOffset(249, 128).addBox(-0.5F, -6.3317F, 0.5034F, 1.0F, 1.0F, 1.0F, -0.125F, false);

        throttle_rotate_X = new ModelRenderer(this);
        throttle_rotate_X.setRotationPoint(1.8435F, 0.5281F, -11.5916F);
        bone36.addChild(throttle_rotate_X);
        setRotationAngle(throttle_rotate_X, 0.2225F, 0.0F, 0.0F);
        throttle_rotate_X.setTextureOffset(245, 118).addBox(-0.5F, -6.4058F, 0.4965F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        throttle_rotate_X.setTextureOffset(249, 128).addBox(-0.5F, -6.2442F, 0.5035F, 1.0F, 1.0F, 1.0F, -0.125F, false);

        randomizer = new ModelRenderer(this);
        randomizer.setRotationPoint(-5.05F, 0.9811F, 1.9972F);
        bone36.addChild(randomizer);
        randomizer.setTextureOffset(123, 79).addBox(-4.3F, -6.4254F, -14.7238F, 4.0F, 1.0F, 3.0F, 0.0F, false);
        randomizer.setTextureOffset(244, 113).addBox(-3.925F, -6.9254F, -12.6238F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        randomizer.setTextureOffset(244, 113).addBox(-3.925F, -6.9254F, -13.2238F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        randomizer.setTextureOffset(244, 113).addBox(-2.125F, -6.9254F, -14.4738F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        randomizer.setTextureOffset(244, 113).addBox(-1.55F, -6.9254F, -14.4738F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        randomizer.setTextureOffset(168, 197).addBox(-3.925F, -6.9254F, -14.3238F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        randomizer.setTextureOffset(168, 197).addBox(-3.325F, -6.9254F, -14.3238F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_1_rotate_x = new ModelRenderer(this);
        random_but_1_rotate_x.setRotationPoint(-2.225F, -6.3254F, -12.1238F);
        randomizer.addChild(random_but_1_rotate_x);
        random_but_1_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.6F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_3_rotate_x = new ModelRenderer(this);
        random_but_3_rotate_x.setRotationPoint(-1.05F, -6.4254F, -12.1238F);
        randomizer.addChild(random_but_3_rotate_x);
        random_but_3_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_2_rotate_x = new ModelRenderer(this);
        random_but_2_rotate_x.setRotationPoint(-1.625F, -6.4254F, -12.1238F);
        randomizer.addChild(random_but_2_rotate_x);
        random_but_2_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_4_rotate_x = new ModelRenderer(this);
        random_but_4_rotate_x.setRotationPoint(-2.225F, -6.4254F, -12.7238F);
        randomizer.addChild(random_but_4_rotate_x);
        random_but_4_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_5_rotate_x = new ModelRenderer(this);
        random_but_5_rotate_x.setRotationPoint(-1.625F, -6.4254F, -12.7238F);
        randomizer.addChild(random_but_5_rotate_x);
        random_but_5_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_6_rotate_x = new ModelRenderer(this);
        random_but_6_rotate_x.setRotationPoint(-1.05F, -6.4254F, -12.7238F);
        randomizer.addChild(random_but_6_rotate_x);
        random_but_6_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_7_rotate_x = new ModelRenderer(this);
        random_but_7_rotate_x.setRotationPoint(-2.225F, -6.4254F, -13.3238F);
        randomizer.addChild(random_but_7_rotate_x);
        random_but_7_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_8_rotate_x = new ModelRenderer(this);
        random_but_8_rotate_x.setRotationPoint(-1.625F, -6.4254F, -13.3238F);
        randomizer.addChild(random_but_8_rotate_x);
        random_but_8_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        random_but_9_rotate_x = new ModelRenderer(this);
        random_but_9_rotate_x.setRotationPoint(-1.05F, -6.4254F, -13.3238F);
        randomizer.addChild(random_but_9_rotate_x);
        random_but_9_rotate_x.setTextureOffset(244, 113).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        bone39 = new ModelRenderer(this);
        bone39.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring4.addChild(bone39);
        setRotationAngle(bone39, -0.1571F, 0.0F, 0.0F);
        bone39.setTextureOffset(66, 196).addBox(-6.0F, -4.6677F, -14.7254F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone39.setTextureOffset(42, 244).addBox(-6.5F, -1.6677F, -14.7254F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone39.setTextureOffset(54, 220).addBox(-6.5F, 0.2335F, -14.7411F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone40 = new ModelRenderer(this);
        bone40.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring4.addChild(bone40);
        setRotationAngle(bone40, -0.1745F, 0.0F, 0.0F);
        bone40.setTextureOffset(71, 125).addBox(-6.5F, 3.3901F, -15.3981F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(87, 35).addBox(-6.5F, 1.3901F, -15.3981F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(87, 39).addBox(-6.5F, -0.452F, -15.3168F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(24, 95).addBox(-6.4F, -2.3926F, -15.2528F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(79, 103).addBox(-0.5F, 1.3901F, -15.3981F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(9, 106).addBox(-0.5F, -0.452F, -15.3168F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone40.setTextureOffset(19, 106).addBox(-0.6F, -2.3926F, -15.2528F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        ring2 = new ModelRenderer(this);
        ring2.setRotationPoint(0.0F, 1.2F, -5.0F);
        base.addChild(ring2);
        setRotationAngle(ring2, 0.0F, -1.0472F, 0.0F);
        

        bone9 = new ModelRenderer(this);
        bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
        ring2.addChild(bone9);
        bone9.setTextureOffset(0, 19).addBox(-2.2942F, -13.0F, -14.0263F, 19.0F, 2.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(126, 74).addBox(4.1916F, -18.4463F, -2.7505F, 6.0F, 2.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(139, 61).addBox(4.2058F, -17.2F, -3.3263F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone9.setTextureOffset(139, 61).addBox(4.2058F, -2.0F, -3.3263F, 6.0F, 1.0F, 10.0F, 0.0F, false);
        bone9.setTextureOffset(173, 208).addBox(-2.2942F, -12.5F, -14.1263F, 19.0F, 1.0F, 1.0F, 0.0F, false);
        bone9.setTextureOffset(173, 208).addBox(-1.2942F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(0.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(2.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(4.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(6.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(8.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(10.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(12.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);
        bone9.setTextureOffset(173, 208).addBox(14.7058F, -12.5F, -13.9263F, 1.0F, 1.0F, 1.0F, 0.2F, false);

        bone10 = new ModelRenderer(this);
        bone10.setRotationPoint(-8.0F, -13.0F, -8.0F);
        bone9.addChild(bone10);
        setRotationAngle(bone10, 0.3491F, 0.5236F, 0.0F);
        

        bone11 = new ModelRenderer(this);
        bone11.setRotationPoint(8.0F, -13.0F, -8.0F);
        bone9.addChild(bone11);
        setRotationAngle(bone11, 0.3491F, -0.5236F, 0.0F);
        

        bone12 = new ModelRenderer(this);
        bone12.setRotationPoint(-8.0F, -11.0F, -8.0F);
        bone9.addChild(bone12);
        setRotationAngle(bone12, -0.1745F, 0.5236F, 0.0F);
        bone12.setTextureOffset(0, 72).addBox(7.4545F, -0.7395F, -1.4772F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone12.setTextureOffset(79, 150).addBox(7.4545F, -0.7395F, 10.5228F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone13 = new ModelRenderer(this);
        bone13.setRotationPoint(-8.0F, -10.0F, -8.0F);
        bone9.addChild(bone13);
        setRotationAngle(bone13, -0.0873F, 0.2618F, 0.0F);
        

        bone14 = new ModelRenderer(this);
        bone14.setRotationPoint(8.0F, -11.0F, -8.0F);
        bone9.addChild(bone14);
        setRotationAngle(bone14, -0.1745F, -0.5236F, 0.0F);
        bone14.setTextureOffset(79, 2).addBox(4.0263F, 0.5117F, -8.5735F, 1.0F, 1.0F, 13.0F, 0.0F, false);
        bone14.setTextureOffset(52, 128).addBox(4.0263F, 0.5117F, 3.4265F, 1.0F, 9.0F, 1.0F, 0.0F, false);

        bone15 = new ModelRenderer(this);
        bone15.setRotationPoint(1.5F, -11.1F, -8.0F);
        ring2.addChild(bone15);
        setRotationAngle(bone15, -0.192F, 0.0F, 0.0F);
        bone15.setTextureOffset(127, 37).addBox(2.7058F, 0.0401F, 4.6133F, 6.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(48, 112).addBox(1.2058F, 0.0401F, 2.6133F, 9.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(28, 118).addBox(1.7058F, 0.0401F, 3.6133F, 8.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(97, 108).addBox(0.7058F, 0.0401F, 1.6133F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(0, 102).addBox(0.2058F, 0.0401F, 0.6133F, 11.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(103, 32).addBox(-0.2942F, 0.0401F, -0.3867F, 12.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(94, 18).addBox(-0.7942F, 0.0401F, -1.3867F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(94, 4).addBox(-1.2942F, 0.0401F, -2.3867F, 14.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(24, 91).addBox(-1.7942F, 0.0401F, -3.3867F, 15.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(90, 86).addBox(-2.2942F, 0.0401F, -4.3867F, 16.0F, 1.0F, 1.0F, 0.0F, false);
        bone15.setTextureOffset(79, 53).addBox(-3.2942F, 0.0401F, -5.3867F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        bone16 = new ModelRenderer(this);
        bone16.setRotationPoint(1.05F, -12.8F, -8.45F);
        ring2.addChild(bone16);
        setRotationAngle(bone16, 0.4014F, 0.0F, 0.0F);
        bone16.setTextureOffset(118, 90).addBox(2.5558F, -1.9829F, 5.3705F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(119, 141).addBox(1.6558F, -1.9829F, 3.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(105, 141).addBox(2.1558F, -1.9829F, 3.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(74, 141).addBox(9.5308F, -1.9829F, 3.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(70, 141).addBox(9.1808F, -1.9829F, 3.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(66, 141).addBox(2.1558F, -1.9829F, 4.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(140, 28).addBox(9.1808F, -1.9829F, 4.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(107, 101).addBox(1.1558F, -1.9829F, 2.3705F, 10.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(66, 113).addBox(2.1558F, -1.7079F, 3.3705F, 7.0F, 1.0F, 2.0F, 0.0F, false);
        bone16.setTextureOffset(7, 140).addBox(0.6558F, -1.9829F, 1.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(75, 62).addBox(2.6558F, -2.2329F, -0.6295F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone16.setTextureOffset(75, 67).addBox(8.4808F, -2.2329F, -0.6295F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone16.setTextureOffset(60, 78).addBox(5.6058F, -2.2329F, -0.6295F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone16.setTextureOffset(39, 17).addBox(1.6558F, -1.7579F, 0.3705F, 9.0F, 1.0F, 2.0F, 0.0F, false);
        bone16.setTextureOffset(139, 13).addBox(10.6558F, -1.9829F, 1.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(125, 139).addBox(0.1558F, -1.9829F, 0.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(75, 139).addBox(10.6558F, -1.9829F, 0.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(138, 20).addBox(0.6558F, -1.9829F, 0.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(138, 11).addBox(11.1558F, -1.9829F, 0.3705F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(94, 12).addBox(-0.3442F, -1.9829F, -0.6295F, 13.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(128, 110).addBox(-1.1442F, -1.9829F, -3.6295F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone16.setTextureOffset(75, 78).addBox(2.6558F, -2.2329F, -3.9045F, 1.0F, 1.0F, 4.0F, -0.25F, false);
        bone16.setTextureOffset(0, 58).addBox(10.7558F, -2.3329F, -3.9795F, 1.0F, 2.0F, 4.0F, -0.35F, false);
        bone16.setTextureOffset(22, 25).addBox(9.3058F, -2.3329F, -3.9795F, 1.0F, 3.0F, 4.0F, -0.35F, false);
        bone16.setTextureOffset(241, 109).addBox(11.4808F, -2.0329F, -4.2545F, 1.0F, 1.0F, 4.0F, -0.4F, false);
        bone16.setTextureOffset(241, 109).addBox(10.0558F, -2.0329F, -4.2545F, 1.0F, 1.0F, 4.0F, -0.4F, false);
        bone16.setTextureOffset(36, 95).addBox(12.4558F, -1.9829F, -3.6295F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone16.setTextureOffset(94, 39).addBox(11.4558F, -1.5829F, -3.6295F, 1.0F, 1.0F, 3.0F, 0.0F, false);
        bone16.setTextureOffset(70, 100).addBox(9.9558F, -1.5829F, -3.6295F, 2.0F, 1.0F, 3.0F, 0.0F, false);
        bone16.setTextureOffset(64, 39).addBox(-0.3442F, -1.5829F, -3.6295F, 10.0F, 1.0F, 3.0F, 0.0F, false);
        bone16.setTextureOffset(66, 98).addBox(2.9558F, -1.7829F, -1.3295F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(227, 177).addBox(3.2558F, -1.9579F, -4.0545F, 6.0F, 1.0F, 3.0F, -0.3F, false);
        bone16.setTextureOffset(252, 112).addBox(1.0808F, -1.9579F, -1.5045F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(252, 112).addBox(2.1308F, -1.9579F, -1.5045F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(246, 143).addBox(1.0808F, -1.9579F, -2.2545F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(246, 143).addBox(2.1308F, -1.9579F, -2.2545F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 142).addBox(1.0808F, -1.9579F, -3.7045F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 142).addBox(2.1308F, -1.9579F, -3.7045F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(252, 112).addBox(1.0808F, -1.9579F, -2.9795F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(252, 112).addBox(2.1308F, -1.9579F, -2.9795F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(241, 118).addBox(-0.4442F, -2.0829F, -2.5295F, 2.0F, 1.0F, 2.0F, -0.4F, false);
        bone16.setTextureOffset(240, 118).addBox(1.4808F, -2.1329F, 1.5205F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 118).addBox(1.4808F, -2.1329F, 0.9205F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(208, 195).addBox(1.4808F, -2.1329F, 0.1955F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 118).addBox(2.0558F, -2.1329F, 1.5205F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 118).addBox(2.0558F, -2.1329F, 0.9205F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(208, 195).addBox(2.0558F, -2.1329F, 0.1955F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 115).addBox(3.0058F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(3.0058F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(3.6058F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(6.2808F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(3.6058F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(6.2808F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(4.1808F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(6.8558F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(238, 140).addBox(7.7308F, -2.1579F, 4.1205F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(4.1808F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 115).addBox(6.8558F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(238, 140).addBox(7.7308F, -2.1579F, 3.4705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 39).addBox(3.2308F, -2.1079F, 1.5955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(6.1808F, -2.2329F, 1.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(6.1808F, -2.2329F, 0.7705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(6.7558F, -2.2329F, 1.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(6.7558F, -2.2329F, 0.7705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(7.3308F, -2.2329F, 1.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(7.3308F, -2.2329F, 0.7705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(7.9058F, -2.2329F, 1.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(240, 112).addBox(7.9058F, -2.2329F, 0.7705F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(235, 72).addBox(3.8808F, -2.1079F, 0.9955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(4.4808F, -2.1079F, 1.5955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(5.0308F, -2.1079F, 1.5955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(4.4808F, -2.1079F, 0.9955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(5.0308F, -2.1079F, 0.9955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(4.4808F, -2.1079F, 0.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(246, 115).addBox(5.0308F, -2.1079F, 0.3955F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        bone16.setTextureOffset(252, 32).addBox(-0.3192F, -2.0829F, -2.8795F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone16.setTextureOffset(244, 8).addBox(-0.3192F, -2.0829F, -3.3795F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone16.setTextureOffset(252, 32).addBox(0.4808F, -2.0829F, -2.8795F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone16.setTextureOffset(244, 8).addBox(0.4808F, -2.0829F, -3.3795F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone16.setTextureOffset(252, 32).addBox(0.0808F, -2.0829F, -2.8795F, 1.0F, 1.0F, 1.0F, -0.35F, false);
        bone16.setTextureOffset(243, 66).addBox(1.6058F, -1.9579F, -1.4795F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(243, 66).addBox(1.6058F, -1.9579F, -2.9545F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 112).addBox(6.5808F, -2.4079F, 0.0955F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(243, 114).addBox(8.9558F, -2.3079F, 0.3455F, 2.0F, 1.0F, 2.0F, -0.4F, false);
        bone16.setTextureOffset(240, 141).addBox(7.7558F, -2.2829F, -1.3295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(242, 111).addBox(5.5558F, -2.2829F, -1.3295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(242, 111).addBox(3.3558F, -2.2829F, -1.3295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(240, 141).addBox(7.7558F, -2.2829F, -1.6295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(242, 111).addBox(5.5558F, -2.2829F, -1.6295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(224, 201).addBox(3.3558F, -2.2829F, -1.6295F, 2.0F, 1.0F, 1.0F, -0.4F, false);
        bone16.setTextureOffset(141, 146).addBox(8.7808F, -1.9579F, -2.1295F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(141, 146).addBox(8.7808F, -1.9579F, -2.6795F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(141, 146).addBox(8.7808F, -1.9579F, -3.2295F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(246, 110).addBox(1.6058F, -1.9579F, -2.2545F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(240, 108).addBox(1.6058F, -1.9579F, -3.7045F, 1.0F, 1.0F, 1.0F, -0.275F, false);
        bone16.setTextureOffset(150, 135).addBox(-2.1442F, -1.9829F, -3.6295F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(150, 137).addBox(13.4558F, -1.9829F, -3.6295F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(0, 151).addBox(-1.8442F, -1.9829F, -2.6295F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(4, 151).addBox(13.1558F, -1.9829F, -2.6295F, 1.0F, 1.0F, 1.0F, 0.0F, false);
        bone16.setTextureOffset(79, 57).addBox(-2.8442F, -1.9829F, -4.6295F, 18.0F, 1.0F, 1.0F, 0.0F, false);

        glow_blue_green = new LightModelRenderer(this);
        glow_blue_green.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone16.addChild(glow_blue_green);
        glow_blue_green.setTextureOffset(235, 72).addBox(3.4308F, -13.9079F, -14.4545F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        glow_blue_green.setTextureOffset(240, 39).addBox(2.7808F, -13.9079F, -15.0545F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        glow_blue_green.setTextureOffset(235, 72).addBox(3.4308F, -13.9079F, -15.6545F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        glow_blue_green.setTextureOffset(240, 39).addBox(2.7808F, -13.9079F, -15.6545F, 1.0F, 1.0F, 1.0F, -0.25F, false);

        glow_yellow = new LightModelRenderer(this);
        glow_yellow.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone16.addChild(glow_yellow);
        glow_yellow.setTextureOffset(244, 8).addBox(-0.3692F, -13.8829F, -19.4295F, 1.0F, 1.0F, 1.0F, -0.35F, false);

        glow_red2 = new LightModelRenderer(this);
        glow_red2.setRotationPoint(0.45F, 11.8F, 16.05F);
        bone16.addChild(glow_red2);
        glow_red2.setTextureOffset(231, 142).addBox(8.3308F, -13.7579F, -19.8295F, 1.0F, 1.0F, 1.0F, -0.275F, false);

        throttle2 = new ModelRenderer(this);
        throttle2.setRotationPoint(10.6869F, 4.8066F, -2.7311F);
        bone16.addChild(throttle2);
        setRotationAngle(throttle2, 0.1745F, 0.0F, 0.0F);
        throttle2.setTextureOffset(246, 112).addBox(-0.6443F, -6.8253F, 0.5257F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        throttle2.setTextureOffset(249, 123).addBox(-0.6385F, -6.6481F, 0.5694F, 1.0F, 1.0F, 1.0F, -0.125F, false);

        throttle3 = new ModelRenderer(this);
        throttle3.setRotationPoint(11.6869F, 4.8066F, -2.7311F);
        bone16.addChild(throttle3);
        setRotationAngle(throttle3, 0.1745F, 0.0F, 0.0F);
        throttle3.setTextureOffset(246, 112).addBox(-0.2193F, -6.8253F, 0.5007F, 1.0F, 1.0F, 1.0F, -0.25F, false);
        throttle3.setTextureOffset(249, 123).addBox(-0.2135F, -6.6481F, 0.5444F, 1.0F, 1.0F, 1.0F, -0.125F, false);

        bone17 = new ModelRenderer(this);
        bone17.setRotationPoint(1.5F, -2.8F, 1.5F);
        ring2.addChild(bone17);
        setRotationAngle(bone17, -0.1571F, 0.0F, 0.0F);
        bone17.setTextureOffset(66, 196).addBox(2.7058F, -6.2137F, -4.9644F, 6.0F, 3.0F, 1.0F, 0.0F, false);
        bone17.setTextureOffset(42, 244).addBox(2.2058F, -3.2137F, -4.9644F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        bone17.setTextureOffset(54, 220).addBox(2.2058F, -1.3125F, -4.98F, 7.0F, 4.0F, 1.0F, 0.0F, false);

        bone18 = new ModelRenderer(this);
        bone18.setRotationPoint(1.5F, -2.7F, 1.7F);
        ring2.addChild(bone18);
        setRotationAngle(bone18, -0.1745F, 0.0F, 0.0F);
        bone18.setTextureOffset(125, 54).addBox(2.2058F, 1.674F, -5.6656F, 7.0F, 1.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(58, 48).addBox(2.2058F, -0.326F, -5.6656F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(0, 51).addBox(2.2058F, -2.1681F, -5.5842F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(34, 46).addBox(2.3558F, -4.1087F, -5.5202F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(0, 58).addBox(8.2058F, -0.326F, -5.6656F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(15, 58).addBox(8.2058F, -2.1681F, -5.5842F, 1.0F, 2.0F, 1.0F, 0.0F, false);
        bone18.setTextureOffset(0, 64).addBox(8.1058F, -4.1087F, -5.5202F, 1.0F, 2.0F, 1.0F, 0.0F, false);

        timerotar = new LightModelRenderer(this);
        timerotar.setRotationPoint(0.4F, 23.0F, 0.0F);
        

        timerotar1 = new LightModelRenderer(this);
        timerotar1.setRotationPoint(-0.4F, 1.0F, 0.0F);
        timerotar.addChild(timerotar1);
        timerotar1.setTextureOffset(97, 97).addBox(-0.6F, -19.0F, -4.2F, 1.0F, 3.0F, 8.0F, 0.0F, false);
        timerotar1.setTextureOffset(0, 125).addBox(-0.6F, -25.0F, -0.7F, 1.0F, 13.0F, 1.0F, 0.0F, false);
        timerotar1.setTextureOffset(23, 44).addBox(-0.6F, -25.0F, -1.7F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        timerotar1.setTextureOffset(133, 104).addBox(-1.6F, -25.0F, -0.7F, 3.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar1.setTextureOffset(45, 77).addBox(-2.6F, -23.0F, -0.7F, 5.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar1.setTextureOffset(88, 122).addBox(-3.6F, -21.0F, -0.7F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar1.setTextureOffset(24, 0).addBox(-4.1F, -19.0F, -0.7F, 8.0F, 3.0F, 1.0F, 0.0F, false);
        timerotar1.setTextureOffset(34, 39).addBox(-0.6F, -23.0F, -2.7F, 1.0F, 2.0F, 5.0F, 0.0F, false);
        timerotar1.setTextureOffset(70, 104).addBox(-0.6F, -21.0F, -3.7F, 1.0F, 2.0F, 7.0F, 0.0F, false);

        timerotar2 = new LightModelRenderer(this);
        timerotar2.setRotationPoint(-0.2121F, 0.0F, 0.3536F);
        timerotar.addChild(timerotar2);
        setRotationAngle(timerotar2, 0.0F, -0.7854F, 0.0F);
        timerotar2.setTextureOffset(4, 125).addBox(-1.0F, -24.0F, -0.7F, 1.0F, 13.0F, 1.0F, 0.0F, false);
        timerotar2.setTextureOffset(0, 0).addBox(-1.0F, -24.0F, -1.7F, 1.0F, 2.0F, 3.0F, 0.0F, false);
        timerotar2.setTextureOffset(133, 110).addBox(-2.0F, -24.0F, -0.7F, 3.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar2.setTextureOffset(49, 39).addBox(-3.0F, -22.0F, -0.7F, 5.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar2.setTextureOffset(122, 19).addBox(-4.0F, -20.0F, -0.7F, 7.0F, 2.0F, 1.0F, 0.0F, false);
        timerotar2.setTextureOffset(24, 4).addBox(-4.5F, -18.0F, -0.7F, 8.0F, 3.0F, 1.0F, 0.0F, false);
        timerotar2.setTextureOffset(0, 44).addBox(-1.0F, -22.0F, -2.7F, 1.0F, 2.0F, 5.0F, 0.0F, false);
        timerotar2.setTextureOffset(0, 106).addBox(-1.0F, -20.0F, -3.7F, 1.0F, 2.0F, 7.0F, 0.0F, false);
        timerotar2.setTextureOffset(94, 35).addBox(-1.0F, -18.0F, -4.2F, 1.0F, 3.0F, 8.0F, 0.0F, false);

        timerotar3 = new LightModelRenderer(this);
        timerotar3.setRotationPoint(-0.4F, 1.0F, 0.0F);
        timerotar.addChild(timerotar3);
        timerotar3.setTextureOffset(34, 54).addBox(-1.5F, -25.0F, 0.65F, 1.0F, 11.0F, 1.0F, -0.2F, false);
        timerotar3.setTextureOffset(34, 54).addBox(-1.45F, -25.0F, -1.8F, 1.0F, 11.0F, 1.0F, -0.2F, false);
        timerotar3.setTextureOffset(30, 53).addBox(0.4F, -25.0F, 0.6F, 1.0F, 11.0F, 1.0F, -0.2F, false);
        timerotar3.setTextureOffset(30, 53).addBox(0.325F, -25.0F, -1.8F, 1.0F, 11.0F, 1.0F, -0.2F, false);
    }

    @Override
    public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
        //previously the render function, render code was moved to a method below
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void render(KeltConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        matrixStack.push();
        float prevRotor = tile.flightTicks == 0 ? 0 : 0.2F - ((float) Math.cos((tile.flightTicks - 1) * 0.1F) * 0.2F);
        float rotor = 0.2F - ((float) Math.cos(tile.flightTicks * 0.1F) * 0.2F);
        
        int glowTime = (int)(tile.getWorld().getGameTime() % 120);
        
        this.timerotar1.setBright(glowTime > 60 ? 1.0F : 0.0F);
        this.timerotar2.setBright(glowTime < 60 ? 1.0F : 0.0F);
        this.timerotar3.setBright(glowTime > 20 ? 1.0F : 0.0F);
        
        matrixStack.push();
        matrixStack.translate(0, prevRotor - (prevRotor - rotor) * Minecraft.getInstance().getRenderPartialTicks(), 0);
        this.timerotar.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        tile.getControl(RandomiserControl.class).ifPresent(random -> {
            float randomOff = random.getAnimationTicks() == 0 ? 0 : 0.006F;
            this.random_but_1_rotate_x.rotateAngleX = this.random_but_6_rotate_x.rotateAngleX = this.random_but_8_rotate_x.rotateAngleX = randomOff;
        });
        
        tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
            this.throttle_rotate_X.rotateAngleX = throttle.getAmount() * 0.12F;
        });
        

        tile.getControl(HandbrakeControl.class).ifPresent(handbrake -> {
            this.handbreak_rotate_X.rotateAngleX = handbrake.isFree() ? 0F : 0.12F;
        });

        tile.getControl(CommunicatorControl.class).ifPresent(communicator -> {
             this.com_but.rotateAngleX = this.com_but2.rotateAngleX = this.com_but3.rotateAngleX = communicator.getAnimationTicks() == 0 ? 0 : 0.003F;
        });
       

        tile.getDoor().ifPresent(door -> {
            this.door.rotateAngleX = (float) Math.toRadians(door.getOpenState() == EnumDoorState.CLOSED ? 25 : 0);
        });

        tile.getControl(IncModControl.class).ifPresent(inc -> {
            this.increment_increase_rotate_x.rotateAngleX = (inc.index / (float) IncModControl.COORD_MODS.length) * 0.17F;
        });

        tile.getControl(XControl.class).ifPresent(x -> {
            this.x_control.rotateAngleX = x.getAnimationTicks() == 0 ? 0 : 0.006F;
            this.x_control.setBright(x.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        tile.getControl(YControl.class).ifPresent(y -> {
            this.y_control.setBright(y.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        tile.getControl(ZControl.class).ifPresent(z -> {
            this.z_control.setBright(z.getAnimationTicks() != 0 ? 1.0F : 0.0F);
        });
        
        this.glow_blue_green.setBright(glowTime > 60 ? 1.0F : 0.0F);
        this.glow_white.setBright(glowTime < 100 ? 1.0F : 0.0F);
        
        this.glow_red.setBright(glowTime < 50 ? 1.0F : 0.0F);
        this.glow_blue_keyboard.setBright(1F);
        
        this.glow_white_1.setBright(glowTime > 50 ? 1.0F : 0.0F);
        this.glow_white_2.setBright(glowTime < 50 ? 1.0F : 0.0F);
        
        this.glow_white_3.setBright(glowTime > 40 ? 1.0F : 0.0F);
        this.glow_white_4.setBright(glowTime < 30 ? 1.0F : 0.0F);
        
        this.glow_yellow.setBright(glowTime <= 30 ? 1.0F : 0.0F);
        
        this.glow_red2.setBright(glowTime >= 45 ? 1.0F : 0.0F);
        
        this.glow_red_green_1.setBright(glowTime > 50 ? 1.0F : 0.0F);
        
        this.telepathic_circuit.setBright(1F);
        
        this.glow_green.setBright(1F);

        base.render(matrixStack, buffer, packedLight, packedOverlay);
        
        
        tile.getControl(MonitorControl.class).ifPresent(monitor -> {
            matrixStack.push();

            matrixStack.translate(-0.12, 0.555, -0.68);
            //WORLD_TEXT.renderText(matrixStack, buffer, packedLight, Helper.getConsoleText(tile));
            matrixStack.pop();
        });
            

        matrixStack.pop();
    }
}