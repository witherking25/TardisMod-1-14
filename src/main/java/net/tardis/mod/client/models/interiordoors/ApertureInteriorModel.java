package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ApertureInteriorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
	
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/exteriors/interior/aperture.png");
	
	private final ModelRenderer left_door;
	private final ModelRenderer right_door;
	private final ModelRenderer frame;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer BOTI;

	public ApertureInteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		left_door = new ModelRenderer(this);
		left_door.setRotationPoint(5.0F, 8.5F, 7.25F);
		setRotationAngle(left_door, 0.0F, -1.0908F, 0.0F);
		left_door.setTextureOffset(34, 34).addBox(-5.0F, -14.5F, -1.0F, 5.0F, 29.0F, 1.0F, 0.0F, false);

		right_door = new ModelRenderer(this);
		right_door.setRotationPoint(-5.0F, 8.5F, 7.25F);
		setRotationAngle(right_door, 0.0F, 0.8727F, 0.0F);
		right_door.setTextureOffset(8, 33).addBox(0.0F, -14.5F, -1.0F, 5.0F, 29.0F, 1.0F, 0.0F, false);

		frame = new ModelRenderer(this);
		frame.setRotationPoint(0.0F, 26.0F, 14.35F);
		frame.setTextureOffset(0, 0).addBox(-6.0F, -34.0F, -7.35F, 12.0F, 32.0F, 1.0F, 0.0F, false);
		frame.setTextureOffset(26, 3).addBox(-6.0F, -34.0F, -8.35F, 12.0F, 2.0F, 1.0F, 0.0F, false);
		frame.setTextureOffset(26, 0).addBox(-6.0F, -3.0F, -8.65F, 12.0F, 1.0F, 2.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(-5.9952F, -18.0F, -6.3481F);
		frame.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -0.5236F, 0.0F);
		bone2.setTextureOffset(0, 33).addBox(-2.0F, -16.0F, -2.0F, 2.0F, 32.0F, 2.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(6.0048F, -18.0F, -6.3481F);
		frame.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.setTextureOffset(26, 26).addBox(-2.0F, -16.0F, -2.0F, 2.0F, 32.0F, 2.0F, 0.0F, false);

		BOTI = new ModelRenderer(this);
		BOTI.setRotationPoint(0.0F, 24.0F, 8.6F);
		BOTI.setTextureOffset(58, 0).addBox(-6.0F, -32.0F, -1.85F, 12.0F, 32.0F, 1.0F, -0.025F, false);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public ResourceLocation getTexture() {
		return TEXTURE;
	}

	@Override
	public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
	        int packedOverlay) {
		matrixStack.rotate(Vector3f.YP.rotationDegrees(180));
		
		EnumDoorState open = door.getOpenState();
		if(open == EnumDoorState.CLOSED) {
			this.left_door.rotateAngleY = this.right_door.rotateAngleY = 0;
		}
		else if(open == EnumDoorState.ONE) {
			this.left_door.rotateAngleY = -(float) Math.toRadians(EnumDoorType.APERTURE.getRotationForState(open));
			this.right_door.rotateAngleY = 0;
		}
		else if(open == EnumDoorState.BOTH) {
			this.left_door.rotateAngleY = -(float)Math.toRadians(EnumDoorType.APERTURE.getRotationForState(open));
			this.right_door.rotateAngleY = (float)Math.toRadians(EnumDoorType.APERTURE.getRotationForState(open));
		}
		
		left_door.render(matrixStack, buffer, packedLight, packedOverlay);
		right_door.render(matrixStack, buffer, packedLight, packedOverlay);
		frame.render(matrixStack, buffer, packedLight, packedOverlay);
		BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
		
	}

	@Override
	public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
	        float netHeadYaw, float headPitch) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
	        float red, float green, float blue, float alpha) {
		// TODO Auto-generated method stub
		
	}
}