package net.tardis.mod.client.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;

import java.util.function.Supplier;

public class TextLabelModelRenderer extends ModelRenderer {

	public WorldText textRenderer;
	Supplier<String[]> label;
	Supplier<ResourceLocation> texture;
	
	public TextLabelModelRenderer(Model model, WorldText renderer, Supplier<String[]> text, Supplier<ResourceLocation> texture) {
		super(model);
		this.textRenderer = renderer;
		this.label = text;
		this.texture = texture;
	}
	
	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
		matrixStackIn.push();
		matrixStackIn.translate(this.rotationPointX, this.rotationPointY , this.rotationPointZ );
        if (this.rotateAngleZ != 0.0F) {
        	matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(this.rotateAngleZ * (180F / (float)Math.PI)));
        }

        if (this.rotateAngleY != 0.0F) {
        	matrixStackIn.rotate(Vector3f.YP.rotationDegrees(this.rotateAngleY * (180F / (float)Math.PI)));
        }

        if (this.rotateAngleX != 0.0F) {
        	matrixStackIn.rotate(Vector3f.XP.rotationDegrees(this.rotateAngleX * (180F / (float)Math.PI)));
        }
        //TODO: Make text render on here again
        //this.textRenderer.renderText(matrixStackIn, , packedLightIn, label.get());
        matrixStackIn.pop();
        
        Minecraft.getInstance().getTextureManager().bindTexture(this.texture.get());
	}


	@Override
	public void translateRotate(MatrixStack matrixStackIn) {
		super.translateRotate(matrixStackIn);
	}
	
	


}
