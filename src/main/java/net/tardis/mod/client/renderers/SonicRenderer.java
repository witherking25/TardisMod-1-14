package net.tardis.mod.client.renderers;

import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.model.ItemTransformVec3f;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.sonic.capability.SonicCapability;

/**
 * Created by Swirtzly
 * on 21/03/2020 @ 20:27
 */
/*@Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)*/
public class SonicRenderer extends ItemStackTileEntityRenderer {

    private ItemStack sonicEmmiter = new ItemStack(TItems.SONIC_EMITTER.get());
    private ItemStack sonicActivator = new ItemStack(TItems.SONIC_ACTIVATOR.get());
    private ItemStack sonicHandle = new ItemStack(TItems.SONIC_HANDLE.get());
    private ItemStack sonicEnd = new ItemStack(TItems.SONIC_END.get());

    @Override
    public void func_239207_a_(ItemStack stack, ItemCameraTransforms.TransformType transformType, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay) {
        ClientPlayerEntity player = Minecraft.getInstance().player;

        matrixStack.push();
        SonicItem.readCapability(stack);
        
       SonicCapability.getForStack(stack).ifPresent((data) -> {
            SonicBasePart.setType(sonicEmmiter, data.getSonicPart(ISonicPart.SonicPart.EMITTER));
            SonicBasePart.setType(sonicActivator, data.getSonicPart(ISonicPart.SonicPart.ACTIVATOR));
            SonicBasePart.setType(sonicHandle, data.getSonicPart(ISonicPart.SonicPart.HANDLE));
            SonicBasePart.setType(sonicEnd, data.getSonicPart(ISonicPart.SonicPart.END));
        });
       if (!SonicCapability.getForStack(stack).isPresent()) { //We need this check because otherwise if the sonic cap is null, it will crash the game
    	   SonicBasePart.setType(sonicEmmiter, SonicBasePart.SonicComponentTypes.MK_2); //We set the type to mk 2, because we want to match the default sonic type, which is 1. The mk 2's enum ordinal is 1, so it's correct
           SonicBasePart.setType(sonicActivator, SonicBasePart.SonicComponentTypes.MK_2);
           SonicBasePart.setType(sonicHandle, SonicBasePart.SonicComponentTypes.MK_2);
           SonicBasePart.setType(sonicEnd, SonicBasePart.SonicComponentTypes.MK_2);	
       }
       renderSonic(matrixStack, buffer, combinedLight, combinedOverlay);
       matrixStack.pop();
    }
    
    private void renderSonic(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
    	matrixStackIn.push();
		Minecraft.getInstance().getItemRenderer().renderItem(sonicEmmiter, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicActivator, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicHandle, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicEnd, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
		matrixStackIn.pop();
    }

   /* @SubscribeEvent
    public static void onDoTheThing(ModelBakeEvent event) {
        event.getModelRegistry().put(new ModelResourceLocation(TItems.SONIC.getRegistryName(), "inventory"), new SonicRenderer());
    }
*/

}
