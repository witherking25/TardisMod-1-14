package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ClockExteriorModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.ClockExteriorTile;

public class ClockExteriorRenderer extends ExteriorRenderer<ClockExteriorTile> {

	public static ClockExteriorModel model = new ClockExteriorModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/clock.png");
	
	public static final WorldText TEXT = new WorldText(0.25F, 0.1F, 0.0145F, 0x000000);
	
	
	public ClockExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void renderExterior(ClockExteriorTile tile, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		matrixStackIn.translate(0, 0.125, 0);

        matrixStackIn.scale(0.25F, 0.25F, 0.25F);
		model.render(tile, 0.25F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();

		matrixStackIn.push();
		matrixStackIn.translate(-0.11, -1.63, -0.1 / 16.0F);
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());

		matrixStackIn.pop();
		
	}
	

}
