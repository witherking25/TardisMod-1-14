package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.exteriors.ApertureExteriorModel;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.exteriors.ApertureExteriorTile;

public class ApertureExteriorRenderer extends ExteriorRenderer<ApertureExteriorTile> {
    
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/exteriors/aperture.png");
	public static final ApertureExteriorModel MODEL = new ApertureExteriorModel();
	
	public ApertureExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void renderExterior(ApertureExteriorTile tile, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		matrixStackIn.translate(0, -1, 0);
		MODEL.render(tile, 0.25F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
	}

}
