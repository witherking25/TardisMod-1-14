package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.PoliceBoxExteriorModel;
//import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class PoliceBoxExteriorRenderer extends ExteriorRenderer<PoliceBoxExteriorTile>{
	
	public PoliceBoxExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

//	private static BotiManager boti = new BotiManager();
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
			"textures/exteriors/police_box.png"); 
	
	private PoliceBoxExteriorModel model = new PoliceBoxExteriorModel();
	public static WorldText TEXT = new WorldText(0.24F, 0.339F, 1, 0x313234);
	
	@Override
	public void renderExterior(PoliceBoxExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		if(tile.getOpen() != EnumDoorState.BOTH) {
			matrixStackIn.push();
			matrixStackIn.translate(-5.6 / 16.0, -15.5 / 16.0, -8.8 / 16.0);
			TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
			matrixStackIn.pop();
		}
		
		matrixStackIn.translate(0, -0.25, 0); //This renders the box upwards on the y axis by 0.25
		matrixStackIn.scale(0.5f, 0.5f, 0.5f); //Scales the model down by 2
		
		this.model.render(tile, 0.5F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
	}

}
