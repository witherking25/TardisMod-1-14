package net.tardis.mod.client.renderers;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.renderer.RenderState;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderState.CullState;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

/**
 * Pre defined GL states that allows for rendering to be batched under the hood.
 * <Br> Use this to replace calls to RenderSystem/GlStateManager for everything except guis
 * @author Swirtzly
 *
 */
public class TRenderTypes extends RenderType {
    public TRenderTypes(String nameIn, VertexFormat formatIn, int drawModeIn, int bufferSizeIn, boolean useDelegateIn, boolean needsSortingIn, Runnable setupTaskIn, Runnable clearTaskIn) {
        super(nameIn, formatIn, drawModeIn, bufferSizeIn, useDelegateIn, needsSortingIn, setupTaskIn, clearTaskIn);
    }

    protected static final RenderState.TransparencyState GENERIC = new RenderState.TransparencyState("generic", () -> {
        RenderSystem.enableBlend();
        RenderSystem.enableAlphaTest();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        //RenderSystem.color4f(1, 1, 1, 0.4F);
    }, () -> {
        RenderSystem.disableAlphaTest();
        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
    });
    
    protected static final RenderState.TransparencyState HOLOGRAPHIC = new RenderState.TransparencyState("holo", () -> {
        RenderSystem.enableBlend();
        RenderSystem.enableAlphaTest();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        RenderSystem.color4f(0.7F, 0.7F, 1F, 0.5F);
    }, () -> {
        RenderSystem.disableAlphaTest();
        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
    });
    
    
    protected static final RenderState.FogState ENTITY_WITH_GLOW = new RenderState.FogState("entity_with_glow", () -> {
        RenderSystem.enableBlend();
        RenderSystem.enableAlphaTest();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL13.glMultiTexCoord2f(GL13.GL_TEXTURE1, 61680.0F, 0.0F);
    }, () -> {
        RenderSystem.disableAlphaTest();
        RenderSystem.disableBlend();
        RenderSystem.defaultBlendFunc();
    });

    public static final RenderType LASER = makeType(Tardis.MODID + ":laser", DefaultVertexFormats.POSITION_COLOR_LIGHTMAP, GL11.GL_QUADS, 256, false, false, State.getBuilder()
            .texture(RenderState.NO_TEXTURE)
            .cull(RenderState.CULL_ENABLED)
            .alpha(DEFAULT_ALPHA)
            .transparency(RenderState.LIGHTNING_TRANSPARENCY)
            .build(true));
    

    public static RenderType getTardis(ResourceLocation locationIn) {
        RenderType.State state = RenderType.State.getBuilder().texture(new RenderState.TextureState(locationIn, false, false)).diffuseLighting(DIFFUSE_LIGHTING_ENABLED).transparency(GENERIC).shadeModel(SHADE_ENABLED).cull(CULL_DISABLED).lightmap(RenderState.LIGHTMAP_ENABLED).build(true);
        return RenderType.makeType("tardis", DefaultVertexFormats.ENTITY, 7, 256, state);
    }
    
    public static RenderType getZeroWidthPartTardis(ResourceLocation locationIn) {
        RenderType.State state = RenderType.State.getBuilder().texture(new RenderState.TextureState(locationIn, false, false)).transparency(NO_TRANSPARENCY).diffuseLighting(DIFFUSE_LIGHTING_ENABLED).alpha(DEFAULT_ALPHA).cull(CULL_DISABLED).lightmap(LIGHTMAP_ENABLED).overlay(OVERLAY_ENABLED).layer(VIEW_OFFSET_Z_LAYERING).build(true);
        return RenderType.makeType("tardis_zero_part", DefaultVertexFormats.ENTITY, 7, 256, true, false, state);
    }
    
    public static RenderType getHoloPilot(ResourceLocation locationIn) {
        RenderType.State state = RenderType.State.getBuilder().texture(new RenderState.TextureState(locationIn, false, false)).diffuseLighting(DIFFUSE_LIGHTING_ENABLED).transparency(HOLOGRAPHIC).shadeModel(SHADE_ENABLED).cull(CULL_DISABLED).lightmap(RenderState.LIGHTMAP_ENABLED).build(true);
        return RenderType.makeType("holo_pilot", DefaultVertexFormats.ENTITY, 7, 256, state);
    }
    
    public static RenderType getBessie(ResourceLocation locationIn) {
        RenderType.State state = RenderType.State.getBuilder().texture(new RenderState.TextureState(locationIn, false, false)).diffuseLighting(DIFFUSE_LIGHTING_ENABLED).transparency(GENERIC).shadeModel(SHADE_ENABLED).cull(CULL_DISABLED).lightmap(RenderState.LIGHTMAP_ENABLED).fog(ENTITY_WITH_GLOW).build(true);
        return RenderType.makeType("bessie", DefaultVertexFormats.ENTITY, 7, 256, state);
    }
    
    public static RenderType getBoti() {
    	State state = RenderType.State.getBuilder().texture(TextureState.BLOCK_SHEET_MIPPED).alpha(AlphaState.DEFAULT_ALPHA).lightmap(LightmapState.LIGHTMAP_ENABLED).build(false);
    	return RenderType.makeType(Tardis.MODID + ".boti", DefaultVertexFormats.BLOCK, GL11.GL_QUADS, 256, state);
    }
    
}
