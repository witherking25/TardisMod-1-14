package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.TTCapsuleExteriorModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.tileentities.exteriors.TTCapsuleExteriorTile;

public class TTCapsuleExteriorRenderer extends ExteriorRenderer<TTCapsuleExteriorTile> {

	public TTCapsuleExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/tt_capsule.png");
	private static final TTCapsuleExteriorModel MODEL = new TTCapsuleExteriorModel();
	
	@Override
	public void renderExterior(TTCapsuleExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		matrixStackIn.translate(0, -1, 0);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile, 1.0F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
	}

}
