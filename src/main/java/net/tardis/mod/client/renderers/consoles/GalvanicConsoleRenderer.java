package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.GalvanicConsoleModel;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;

import java.text.DecimalFormat;


public class GalvanicConsoleRenderer extends TileEntityRenderer<GalvanicConsoleTile> {
    
    public static final GalvanicConsoleModel MODEL = new GalvanicConsoleModel(tex -> RenderType.getEntityCutout(tex));
	
    public static final DecimalFormat FORMAT = new DecimalFormat("###");
    public static final WorldText TEXT = new WorldText(0.3F, 0.22F, 0.0025F, 0x000000);
	
	
	public GalvanicConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(GalvanicConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();

		matrixStackIn.translate(0.5, 0.35, 0.5);
		matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(180));
		matrixStackIn.scale(0.25F, 0.25F, 0.25F);
		
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic.png");
		if(console.getVariant() != null)
			texture = console.getVariant().getTexture();
		MODEL.render(console, 0.25F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		
		//Sonic
		matrixStackIn.push();
		matrixStackIn.scale(1.5F, 1.5F, 1.5F);
		matrixStackIn.translate(-0.6, -1.5, 1.4);
		matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(22));
		matrixStackIn.rotate(Vector3f.XN.rotationDegrees(10));
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();

        //Monitor
		console.getControl(MonitorControl.class).ifPresent(monitor -> {
			matrixStackIn.push();
			MODEL.translateMonitorPos(matrixStackIn);
			Minecraft.getInstance().gameRenderer.getLightTexture().disableLightmap();
			matrixStackIn.scale(4, 4, 4);
			matrixStackIn.rotate(Vector3f.XN.rotationDegrees(90));
			matrixStackIn.translate(-0.135, -0.14, -0.02F);
			//TEXT.renderText(matrixStackIn, Helper.getConsoleText(console));
			Minecraft.getInstance().gameRenderer.getLightTexture().enableLightmap();
			matrixStackIn.pop();
		});
		
        matrixStackIn.pop();
	}

}
