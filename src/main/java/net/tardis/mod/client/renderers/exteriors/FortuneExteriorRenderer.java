package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.FortuneExteriorModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;

public class FortuneExteriorRenderer extends ExteriorRenderer<FortuneExteriorTile> {

	public static final WorldText TEXT = new WorldText(0.65F, 0.1F, 0.01F, 0xFFFFFF);
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/fortune.png");
	public static final FortuneExteriorModel MODEL = new FortuneExteriorModel();

	public FortuneExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void renderExterior(FortuneExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		
		matrixStackIn.translate(0, 0.13, 0);
		ResourceLocation texture = TEXTURE;
		if(tile.getVariant() != null)
			texture = tile.getVariant().getTexture();
		MODEL.render(tile, 0.25F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, alpha);

		matrixStackIn.push();
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180));
		matrixStackIn.translate(-5 / 16.0, -32 / 16.0, -7.6 / 16.0);
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
		matrixStackIn.pop();
		
	}

}
