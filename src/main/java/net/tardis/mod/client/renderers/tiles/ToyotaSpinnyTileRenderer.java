package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.ToyotaSpinnyThingModel;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;

public class ToyotaSpinnyTileRenderer extends TileEntityRenderer<ToyotaSpinnyTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/spinny_thing.png");
	public static final ToyotaSpinnyThingModel MODEL = new ToyotaSpinnyThingModel();

	public ToyotaSpinnyTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(ToyotaSpinnyTile tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 1.5, 0.5);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		float scale = 1.0F;
		matrixStackIn.scale(scale, scale, scale);
		MODEL.render(tileEntityIn, 1, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(TEXTURE)), combinedLightIn, combinedOverlayIn, 1,1,1,1);
		matrixStackIn.pop();
	}
}
