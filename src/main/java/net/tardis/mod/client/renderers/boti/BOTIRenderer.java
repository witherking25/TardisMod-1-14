package net.tardis.mod.client.renderers.boti;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.block.BlockRenderType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.FogRenderer.FogType;
import net.minecraft.client.renderer.IRenderTypeBuffer.Impl;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TClipperHelper;
import net.tardis.mod.misc.BiFunctionVoid;
import net.tardis.mod.tileentities.console.misc.PlayerTelepathicConnection;
import org.apache.logging.log4j.Level;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class BOTIRenderer {

    public static final BotiManager BOTI = new BotiManager();
    public static final List<PortalInfo> PORTALS = Lists.newArrayList();
    public static final BotiVBO BOTI_VBOS = new BotiVBO();

    public static void addPortal(PortalInfo info) {
    	PORTALS.add(info);
    }
    
    public static void renderBOTI(MatrixStack ms, PortalInfo info) {
    	
    	if(info.getWorldShell() == null)
    		return;
    	
    	ms.push();

        Minecraft.getInstance().getFramebuffer().unbindFramebuffer();
        
        BOTI.setupFramebuffer();
        
        Vector3d skyColor = Minecraft.getInstance().world.getSkyColor(Minecraft.getInstance().player.getPosition(), Minecraft.getInstance().getRenderPartialTicks());
        BOTI.fbo.setFramebufferColor((float)skyColor.x, (float)skyColor.y, (float)skyColor.z, 0);

        //Render World
        ms.push();
        //ms.translate(-0.5, -0.5, -0.5);
        info.translatePortal(ms);
        
        
        //GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT | GL11.GL_COLOR_BUFFER_BIT);
        GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
        
        ClientWorld oldWorld = Minecraft.getInstance().world;
        if(info.getWorldShell().getWorld() != null) {
        	info.getWorldShell().getWorld().setShell(info.getWorldShell());
        	Minecraft.getInstance().world = info.getWorldShell().getWorld(); 
        }
        
        FogRenderer.setupFog(Minecraft.getInstance().getRenderManager().info, FogType.FOG_TERRAIN, 80, false);

        PlayerEntity player = Minecraft.getInstance().player;

        Minecraft.getInstance().worldRenderer.renderSky(ms, Minecraft.getInstance().getRenderPartialTicks());
        //Minecraft.getInstance().worldRenderer.renderClouds(ms, Minecraft.getInstance().getRenderPartialTicks(),0, 100, 0);

        //setupClipPlanes(ms);
        renderWorld(ms, info.getWorldShell(), LightModelRenderer.MAX_LIGHT, OverlayTexture.NO_OVERLAY, BOTI);

        Minecraft.getInstance().world = oldWorld;
        FogRenderer.resetFog();
        
        ms.pop();

        BufferBuilder underlyingBuffer = Tessellator.getInstance().getBuffer();
        Impl imBuffer = IRenderTypeBuffer.getImpl(underlyingBuffer);

        info.renderDoor(ms, imBuffer);
        imBuffer.finish();
        Minecraft.getInstance().getFramebuffer().bindFramebuffer(false);

        setupStencil(info::renderPortal, ms, imBuffer);
        BOTI.fbo.framebufferRenderExt(Minecraft.getInstance().getMainWindow().getFramebufferWidth(), Minecraft.getInstance().getMainWindow().getFramebufferHeight(), true);
        endStencil(info::renderPortal, ms, imBuffer);

        BOTI.endFBO();
        
        ms.pop();
        
    }

    public static void setupStencil(BiFunctionVoid< MatrixStack, IRenderTypeBuffer.Impl > drawPortal, MatrixStack stack, IRenderTypeBuffer.Impl buffer) {
        GL11.glEnable(GL11.GL_STENCIL_TEST);

        // Always write to stencil buffer
        GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 0xFF);
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_REPLACE);
        GL11.glStencilMask(0xFF);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

        RenderSystem.depthMask(false);
        drawPortal.run(stack, buffer);
        buffer.finish();
        RenderSystem.depthMask(true);

        // Only pass stencil test if equal to 1(So only if rendered before)
        GL11.glStencilMask(0x00);
        GL11.glStencilFunc(GL11.GL_EQUAL, 1, 0xFF);

    }

    public static void endStencil(BiFunctionVoid< MatrixStack, IRenderTypeBuffer.Impl > drawPortal, MatrixStack stack, IRenderTypeBuffer.Impl buffer) {
        GL11.glDisable(GL11.GL_STENCIL_TEST);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

        GL11.glColorMask(false, false, false, false);
        RenderSystem.depthMask(false);
        drawPortal.run(stack, buffer);
        buffer.finish();

        //Set things back
        RenderSystem.depthMask(true);
        GL11.glColorMask(true, true, true, true);
    }

    @SuppressWarnings("deprecation")
	public static void renderWorld(MatrixStack matrixStack, WorldShell shell, int combinedLight, int combinedOverlay, BotiManager boti) {
    	
    	matrixStack.push();
    	
        Minecraft.getInstance().textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);

        BlockPos offset = shell.getOffset();

        matrixStack.translate(-offset.getX(), -offset.getY(), -offset.getZ());

        //if(shell.needsUpdate()){
          //  shell.setNeedsUpdate(false);
           // System.out.println("Rendered World!");

            BlockRenderType[] types = BlockRenderType.values();
            int typeIndex = 0;
            for(HashMap<BlockPos, BlockStore> map : shell.getSortedBlocks()){
                BlockRenderType blockRenderType = types[typeIndex];

                //BOTI_VBOS.beginBuffer(blockRenderType);
                RenderType type = RenderType.getBlockRenderTypes().get(typeIndex);
                BufferBuilder bb = BOTI_VBOS.getBuilder(type);
                Impl impl = IRenderTypeBuffer.getImpl(bb);

                for(Entry<BlockPos, BlockStore> entry : map.entrySet()){
                    matrixStack.push();
                    matrixStack.translate(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
                    if (Helper.canRenderInBOTI(entry.getValue().getState())) {

                        IVertexBuilder buf = impl.getBuffer(RenderTypeLookup.func_239221_b_(entry.getValue().getState()));

                        Minecraft.getInstance().getBlockRendererDispatcher()
                                .renderModel(entry.getValue().getState(), entry.getKey(), shell.getWorld(), matrixStack, buf, true, Minecraft.getInstance().world.rand);
                    }
                    matrixStack.pop();

                }
                impl.finish();
                //BOTI_VBOS.upload(blockRenderType, bb);
                //BOTI_VBOS.unbind();
                ++typeIndex;
            }
        //}
        //((Impl)buffer).finish();

        //Draw VBO


        for(BlockRenderType type : BlockRenderType.values()){
            BOTI_VBOS.draw(type, matrixStack.getLast().getMatrix());
        }




        //Tiles
        for (Entry< BlockPos, TileEntity > entry : shell.getTiles().entrySet()) {
            matrixStack.push();
            matrixStack.translate(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
            try {

                int light = combinedLight;
                if(shell.getMap().containsKey(entry.getKey())){
                    light = shell.getMap().get(entry.getKey()).getLight();
                }

                if (Helper.canRenderInBOTI(entry.getValue().getBlockState())) {
                    //renderTile(entry.getValue(), matrixStack, buffer, light, OverlayTexture.NO_OVERLAY);
                }
            }
            catch (Exception e) {
                Tardis.LOGGER.catching(Level.DEBUG, e);
            }
            finally{
                matrixStack.pop();
            }
        }

        //TODO
        // - Some entities don't like being rendered like this, Notably, Minecarts, which is weird in itself
        // - Animations
        // - Using PB BOTI makes entities be up by one, weirdly
        // - Find a better way of getting the "offset"
        // - Ensure rotation yaw rotation is correct
        for (Entity e : shell.getEntities()) {
            try {
                if (Helper.canRenderInBOTI(e)) { //If entity is not blacklisted, render it.
                    matrixStack.push();
                    BlockPos pos = e.getPosition().subtract(offset);
                    matrixStack.translate(pos.getX(), pos.getY(), pos.getZ());
                    EntityRenderer< ? super Entity > renderer = Minecraft.getInstance().getRenderManager().getRenderer(e);
                    //renderer.render(e, e.rotationYaw, 0, matrixStack, buffer, OverlayTexture.NO_OVERLAY);
                    matrixStack.pop();
                }
            }
            catch (Exception error) {
                error.printStackTrace();
            }
        }
        
        matrixStack.pop();

    }

    public static < T extends TileEntity > void renderTile(T te, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay) {
        TileEntityRenderer< T > render = TileEntityRendererDispatcher.instance.getRenderer(te);
        if (render != null)
            render.render(te, Minecraft.getInstance().getRenderPartialTicks(), matrixStack, buffer, combinedLight, OverlayTexture.NO_OVERLAY);
    }

    public static interface IDrawable {
        void render(IRenderTypeBuffer.Impl buffer);
    }
    
    public static void setupClipPlanes(MatrixStack stack, Vector3d pos) {
    	ActiveRenderInfo info = Minecraft.getInstance().gameRenderer.getActiveRenderInfo();
    	Matrix4f projMatrix = Minecraft.getInstance().gameRenderer.getProjectionMatrix(info, Minecraft.getInstance().getRenderPartialTicks(), false);
    	Matrix4f matrix4f = stack.getLast().getMatrix();
    	Vector3d projectedView = info.getProjectedView();
//        double viewX = projectedView.getX();
//        double viewY = projectedView.getY();
//        double viewZ = projectedView.getZ();
    	double viewX = pos.getX();
        double viewY = pos.getY();
        double viewZ = pos.getZ();
        
//        Minecraft.getInstance().gameRenderer.resetProjectionMatrix(matrix4f);
        TClipperHelper clippingHelper = new TClipperHelper(matrix4f, projMatrix);
    	clippingHelper.setCameraPosition(viewX, viewY, viewZ);
    	clippingHelper.setFrustumPlane(matrix4f, -5F, 0, 0.01F, 0);
    	
    }
    
    public static void setClipPlanes() {
        double[] eqn = { 0.0, 0.0, -0.5, 0};
        double[] eqn2 = { 0.0, -0.0, 0, 0};
        double[] eqnK = { -0.1, 0.0, 0.0, 0.0 };
        
        GL11.glClipPlane(GL11.GL_CLIP_PLANE0, eqn);
        GL11.glEnable(GL11.GL_CLIP_PLANE0);
        
        GL11.glClipPlane(GL11.GL_CLIP_PLANE1, eqn2);
        GL11.glEnable(GL11.GL_CLIP_PLANE1);
        
        GL11.glClipPlane(GL11.GL_CLIP_PLANE2, eqnK);
        GL11.glEnable(GL11.GL_CLIP_PLANE2);
    }
    
    public static void disableClipPlanes() {
      GL11.glDisable(GL11.GL_CLIP_PLANE0);
      GL11.glDisable(GL11.GL_CLIP_PLANE1);
      GL11.glDisable(GL11.GL_CLIP_PLANE2);
    }
}
