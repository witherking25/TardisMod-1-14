package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.blocks.AntiGravBlock;
import net.tardis.mod.tileentities.AntiGravityTile;

public class AntiGravTileRenderer extends TileEntityRenderer<AntiGravityTile>{

	public AntiGravTileRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void render(AntiGravityTile tile, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		ITextComponent text = tile.getDisplayName();
		BlockState state = tile.getBlockState();
		if (state.hasProperty(AntiGravBlock.ACTIVATED) && state.get(AntiGravBlock.ACTIVATED)) {
			matrixStackIn.push();
            matrixStackIn.translate(0.5D, 1.5, 0.5D);
            matrixStackIn.rotate(Minecraft.getInstance().getRenderManager().getCameraOrientation());
            matrixStackIn.scale(-0.025F, -0.025F, 0.025F);
            Matrix4f matrix4f = matrixStackIn.getLast().getMatrix();
            FontRenderer fontrenderer = Minecraft.getInstance().fontRenderer;
            float f2 = (float) (-fontrenderer.getStringPropertyWidth(tile.getDisplayName()) / 2);
            fontrenderer.func_243247_a(tile.getDisplayName(), f2, (float) 1, -1, false, matrix4f, bufferIn, false, 0, combinedLightIn);
            matrixStackIn.pop();
		}
		
	}

}
