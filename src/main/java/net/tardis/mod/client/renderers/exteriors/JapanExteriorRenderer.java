package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.models.exteriors.JapanExteriorModel;
import net.tardis.mod.client.renderers.TRenderTypes;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.exteriors.JapanExteriorTile;

public class JapanExteriorRenderer extends ExteriorRenderer<JapanExteriorTile> {
    
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/exteriors/japan_box.png");
	public static final JapanExteriorModel MODEL = new JapanExteriorModel();
	
	public JapanExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	@Override
	public void renderExterior(JapanExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180));
		matrixStackIn.translate(0, -1, 0);

		MODEL.render(tile, 1F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
		
	}



}
