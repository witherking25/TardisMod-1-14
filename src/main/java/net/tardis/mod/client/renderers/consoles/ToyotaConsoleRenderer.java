package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ToyotaConsoleModel;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;

import java.text.DecimalFormat;

public class ToyotaConsoleRenderer extends TileEntityRenderer<ToyotaConsoleTile> {

	public static final ToyotaConsoleModel MODEL = new ToyotaConsoleModel();
	
	public static final DecimalFormat FORMAT = new DecimalFormat("###");
	public static final WorldText TEXT = new WorldText(0.5F, 0.3F, 0.004F, 0x000000);
	
	public ToyotaConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}
	
	@Override
	public void render(ToyotaConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 0.8, 0.5);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/toyota.png");
		
		//Monitor text
        console.getControl(MonitorControl.class).ifPresent(monitor -> {
			matrixStackIn.push();
			float scale = 0.65F;
			matrixStackIn.scale(scale, scale, scale);
			matrixStackIn.rotate(Vector3f.YP.rotationDegrees(60));
			matrixStackIn.rotate(Vector3f.XN.rotationDegrees(60));
			matrixStackIn.translate(-0.25, 10 / 16.0, -15/ 16.0);
			//TEXT.renderText(matrixStackIn, Helper.getConsoleText(console));
			matrixStackIn.pop();
        });
		
		float scale = 0.5F;
		matrixStackIn.scale(scale, scale, scale);
		if(console.getVariant() != null)
			texture = console.getVariant().getTexture();
		MODEL.render(console, 1F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);

		matrixStackIn.push();
		matrixStackIn.translate(1.75, -0.15, 1);
		matrixStackIn.scale(0.75F, 0.75F, 0.75F);
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(60));
		matrixStackIn.rotate(Vector3f.XN.rotationDegrees(22));
		matrixStackIn.translate(0.9, 0, -0.275);
		matrixStackIn.scale(0.75F, 0.75F, 0.75F);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
        
		matrixStackIn.pop();
	}
}
