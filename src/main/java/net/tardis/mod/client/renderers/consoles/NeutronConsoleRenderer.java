package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.NeutronConsoleModel;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;

public class NeutronConsoleRenderer extends TileEntityRenderer<NeutronConsoleTile> {
	
	public static final NeutronConsoleModel MODEL = new NeutronConsoleModel(tex -> RenderType.getEntityCutout(tex));
	
	public NeutronConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

		@Override
	public void render(NeutronConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		RenderSystem.enableRescaleNormal();
		matrixStackIn.translate(0.5F, 0.45F, 0.5F);
		matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
		float scale = 0.3F;
		matrixStackIn.scale(scale, scale, scale); //Use a local variable to scale console easier
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/neutron_thaumic.png");
		if(console.getVariant() != null)
			texture = console.getVariant().getTexture();
		
		MODEL.render(console, 0.3F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		RenderSystem.disableRescaleNormal();
		
		 //Sonics
		matrixStackIn.push();
        matrixStackIn.translate(2.45, -1.75, 2.275);
        matrixStackIn.rotate(Vector3f.XN.rotationDegrees(15));
        matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(30));
//      GlStateManager.rotated(30, -0.5, 0, 1);
        float sonic_scale = 1.25F;
        matrixStackIn.scale(sonic_scale, sonic_scale, sonic_scale);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
		matrixStackIn.pop();
	}

}
