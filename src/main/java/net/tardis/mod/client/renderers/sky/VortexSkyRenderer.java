package net.tardis.mod.client.renderers.sky;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ISkyRenderHandler;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TRenderHelper;

@OnlyIn(Dist.CLIENT)
public class VortexSkyRenderer implements ISkyRenderHandler {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/sky/vortex.png");
	
	@Override
	public void render(int ticks, float partialTicks, MatrixStack matrixStack, ClientWorld world, Minecraft mc) {
		matrixStack.push();
		RenderSystem.disableFog();
		RenderSystem.disableCull();
		RenderSystem.color3f(1, 1, 1);
		matrixStack.translate(0, -mc.player.getPosY(), 0);
		matrixStack.rotate(Vector3f.YP.rotationDegrees((float)(world.getGameTime() + partialTicks) % 360));
		mc.getTextureManager().bindTexture(TEXTURE);
		for(int i = 0; i < 200; ++i) {
			TRenderHelper.renderInsideBox(-10, -20 * i, -10, 10, 20 * i, 10);
		}
		RenderSystem.enableFog();
		RenderSystem.enableCull();
		matrixStack.pop();
	}
	
	public void drawQuad(BufferBuilder bb, int minX, int minY, int minZ, int maxX, int maxY, int maxZ) {
		bb.pos(minX, minY, minZ).tex(0, 0).endVertex();
		bb.pos(maxX, minY, minZ).tex(1, 0).endVertex();
		bb.pos(maxX, maxY, minZ).tex(1, 1).endVertex();
		bb.pos(minX, maxY, minZ).tex(0, 1).endVertex();
	}

}
