package net.tardis.mod.client;

import java.text.DecimalFormat;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.DrawHighlightEvent;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.InputEvent.MouseScrollEvent;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.api.events.DimensionLightMapModificationEvent;
import net.tardis.api.events.TardisWorldLastRender;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.DebugItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BessieHornMessage;
import net.tardis.mod.network.packets.SnapMessage;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.world.dimensions.TDimensions;


@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {

    public static DecimalFormat FORMAT = new DecimalFormat("###");

    public static boolean BOBBING = false;

    @SubscribeEvent
    public static void onBlockHighlight(DrawHighlightEvent event) {
        ActiveRenderInfo information = event.getInfo();
        MatrixStack matrixStack = event.getMatrix();
        
        //Cancel outline for exteriors
        if(event.getTarget() instanceof BlockRayTraceResult) {
        	BlockRayTraceResult result = (BlockRayTraceResult)event.getTarget();
        	BlockState state = Minecraft.getInstance().world.getBlockState(result.getPos());
        	if(state.getBlock() instanceof ExteriorBlock)
        		event.setCanceled(true);
        	
        }

        // Squareness gun
        ClientPlayerEntity player = Minecraft.getInstance().player;
        TRenderHelper.drawSquarenessGunOutline(player, matrixStack, information, event);

    }


    @SubscribeEvent
    public static void onRenderWorldLast(TardisWorldLastRender event) {}

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void onRenderWorldLast(RenderWorldLastEvent event) {

    	MatrixStack matrixStack = event.getMatrixStack();
    	Vector3d proj = Minecraft.getInstance().gameRenderer.getActiveRenderInfo().getProjectedView();
    	
    	//Vector3d proj = TileEntityRendererDispatcher.instance.renderInfo.getProjectedView();
    	
        for(PortalInfo info : BOTIRenderer.PORTALS) {
        	matrixStack.push();
            
        	matrixStack.translate(-proj.x, -proj.y, -proj.z);
        	matrixStack.translate(info.getPosition().getX(), info.getPosition().getY(), info.getPosition().getZ());
            info.translate(matrixStack);
            
           	BOTIRenderer.renderBOTI(matrixStack, info);

            matrixStack.pop();
            
            //Minecraft.getInstance().gameRenderer.renderWorld(partialTicks, finishTimeNano, matrixStackIn);
        }
        BOTIRenderer.PORTALS.clear();
        
        //Debug Bounding Boxes
        ClientPlayerEntity player = Minecraft.getInstance().player;

        final Vector3d projectedView1 = player.getPositionVec();
        double correctedRenderPosX1 = projectedView1.getX();
        double correctedRenderPosY1 = projectedView1.getY();
        double correctedRenderPosZ1 = projectedView1.getZ();
        
        IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
        IVertexBuilder builder = buffer.getBuffer(RenderType.getLines());
        
        TRenderHelper.drawDebugBoundingBoxes(player, matrixStack, builder, correctedRenderPosX1, correctedRenderPosY1, correctedRenderPosZ1);
    }

    @SubscribeEvent
    public static void input(InputUpdateEvent event) {
        if (event.getMovementInput().jump) {
            Network.sendToServer(new BessieHornMessage());
        }
    }

    @SubscribeEvent
    public static void handleScroll(MouseScrollEvent event) {
        if (Minecraft.getInstance().player != null && Minecraft.getInstance().player.getHeldItemMainhand().getItem() == TItems.DEBUG.get()) {
            event.setCanceled(true);
            if (Minecraft.getInstance().player.isSneaking()) {
                ((DebugItem) net.tardis.mod.items.TItems.DEBUG.get()).width = ((DebugItem) TItems.DEBUG.get()).height += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
            } else ((DebugItem) TItems.DEBUG.get()).offsetY += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
        }
    }

    @SubscribeEvent
    public static void onRenderOverlayPost(RenderGameOverlayEvent.Post event) {
        FontRenderer fr = Minecraft.getInstance().fontRenderer;
        int scaledWidth = Minecraft.getInstance().getMainWindow().getScaledWidth();
        int scaledHeight = Minecraft.getInstance().getMainWindow().getScaledHeight();
        MatrixStack matrixStack = event.getMatrixStack();
        PlayerEntity player = ClientHelper.getClientPlayer();
        TRenderHelper.drawPlayerCapabilityText(player, matrixStack, fr, scaledWidth, scaledHeight);
        if (event.getType() == ElementType.TEXT) {
        	TRenderHelper.drawDiagnosticText(player, matrixStack, fr, scaledHeight, scaledHeight, FORMAT);
        }

    }

    @SubscribeEvent
    public static void onMove(InputUpdateEvent update) {
        LivingEntity liv = update.getEntityLiving();
        if (liv.world.getDimensionKey() == TDimensions.SPACE_DIM) {
            if (update.getMovementInput().sneaking) {
                if (liv.getMotion().y > -0.1)
                    liv.setMotion(liv.getMotion().subtract(0, 0.1, 0));
            } else if (update.getMovementInput().jump) {
                if (liv.getMotion().y < 0.1)
                    liv.setMotion(update.getEntityLiving().getMotion().add(0, 0.1, 0));
            }
        }
    }

    @SubscribeEvent
    public static void onKey(InputEvent.KeyInputEvent event) {
        if (TClientRegistry.SNAP_KEY.isPressed())
            Network.sendToServer(new SnapMessage());
    }
    
    @SubscribeEvent
    public static void onDimLightMapUpdate(DimensionLightMapModificationEvent event) {
    	if (WorldHelper.areDimensionTypesSame(event.getWorld(), TDimensions.DimensionTypes.TARDIS_TYPE)) {
        	float[] colors = new float[3];
        	TileEntity te = event.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				if(((ConsoleTile)te).getInteriorManager().isAlarmOn()){
					colors[0] = 1F;
					colors[1] = event.getLightMapColors().getY() * 1 - ((float)(Math.sin(event.getWorld().getGameTime() * 0.1)) * 0.5F);
					colors[2] = event.getLightMapColors().getZ() * 1 - ((float)(Math.sin(event.getWorld().getGameTime() * 0.1)) * 0.5F);
				}
				else {
					if(console.getEmotionHandler().getMood() <= EnumHappyState.SAD.getTreshold()) {
						colors[0] = event.getLightMapColors().getX() * 0.4F;
						colors[1] = event.getLightMapColors().getY() * 0.4F;
						colors[2] = event.getLightMapColors().getZ() * 0.75F;
					}
					else if(console.getEmotionHandler().getMood() >= EnumHappyState.HAPPY.getTreshold()) {
						//TODO: Fix blue colours applying to game menus
						colors[0] = event.getLightMapColors().getX() * 1.5F;
						colors[1] = event.getLightMapColors().getY() * 1.5F;
						colors[2] = event.getLightMapColors().getZ() * 1.5F;
					}
					//Reset to colours parsed in by vanilla if we're not doing anything
					else {
						colors[0] = event.getLightMapColors().getX();
						colors[1] = event.getLightMapColors().getY();
						colors[2] = event.getLightMapColors().getZ();
					}
				}
			}
			else { //Reset colours so we don't get an initial blue tint when teleporting between dimensions
				colors[0] = event.getLightMapColors().getX();
				colors[1] = event.getLightMapColors().getY();
				colors[2] = event.getLightMapColors().getZ();
			}
			event.getLightMapColors().set(colors);
        }
    }
    
    @SubscribeEvent
    public static void onItemTooltipRender(ItemTooltipEvent event) {
    	TRenderHelper.handleTooltipRendering(event.getItemStack(), event.getToolTip(), event.getFlags(), event.getPlayer());
    }

}
