package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;

public class TextButton extends Button {

    public TextButton(int x, int y, String text, IPressable onPress) {
        super(x, y, Minecraft.getInstance().fontRenderer.getStringWidth(text), Minecraft.getInstance().fontRenderer.FONT_HEIGHT, new TranslationTextComponent(text), onPress);
    }
    
    public TextButton(int x, int y, TranslationTextComponent text, IPressable onPress) {
        super(x, y, Minecraft.getInstance().fontRenderer.getStringWidth(text.getString()), Minecraft.getInstance().fontRenderer.FONT_HEIGHT, text, onPress);
    }

    @Override
    public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        Minecraft.getInstance().fontRenderer.drawStringWithShadow(matrixStack, this.getMessage().getString(), x, y,
                Helper.isInBounds(mouseX, mouseY, x, y, x + width, y + height) ? 0x25dbf0 : 0xFFFFFF);
    }
}
