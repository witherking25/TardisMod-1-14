package net.tardis.mod.client.guis;

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Partition;
import net.tardis.mod.misc.TelepathicUtils.Search;
import net.tardis.mod.misc.TelepathicUtils.SearchType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicMessage;

public class TelepathicScreen extends Screen implements IGetStructures{

    public static final TranslationTextComponent TITLE = new TranslationTextComponent("gui.tardis.telepathic");
    private static final int MAX_ELEM_PER_PAGE = 10;
    private final List<Search> searchNames = new ArrayList<Search>();
    private final List<Search> structureNames = new ArrayList<Search>();
    private List<List<Search>> searchPages = Partition.ofSize(searchNames, MAX_ELEM_PER_PAGE);
    private int page = 0;
    private TextFieldWidget textFieldWidget = null;
    private String searchTerm = "";
    

    public TelepathicScreen() {
        super(TITLE);
    }

    @Override
    public void init() {
        super.init();
        this.textFieldWidget = new TextFieldWidget(this.font, this.width / 2 - 100, 50, 200, 20, this.textFieldWidget, new TranslationTextComponent("selectWorld.search"));
        this.children.add(this.textFieldWidget);

        for (Widget b : this.buttons)
            b.active = false;

        this.buttons.clear();

        createFilteredList(searchTerm, true);
        textFieldWidget.setEnabled(true);
    }

    @Override
    public void tick() {
        super.tick();
        textFieldWidget.tick();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, new StringTextComponent("Telepathic Circuits"), this.width / 2, this.height / 2 - 90, 0xFFFFFF);
        for (Widget w : this.buttons) {
            w.renderWidget(matrixStack, mouseX, mouseY, partialTicks);
        }
        textFieldWidget.render(matrixStack, mouseX, mouseY, partialTicks);

        if (this.textFieldWidget.isFocused()) {
            this.textFieldWidget.setSuggestion("");
        } else if (this.textFieldWidget.getText().isEmpty()) {
            this.textFieldWidget.setSuggestion("Search...");
        }
    }


    public void createFilteredList(String searchTerm, boolean addBackButtons) {

        for (Widget w : this.buttons) {
            w.active = false;
        }

        this.buttons.clear();
        this.searchNames.clear();

        //Spawn

        this.searchNames.add(new Search(new StringTextComponent("World Spawn"), "", SearchType.SPAWN));

        //Add all biomes from the dynamic registries list synced from the server via the ClientNetHandler
        //Allows for datapack biomes to be gotten
        Minecraft.getInstance().getConnection().getWorld().func_241828_r().getRegistry(Registry.BIOME_KEY).getEntries().stream().forEach(biome -> {
        	this.searchNames.add(new Search(new StringTextComponent(WorldHelper.formatBiomeKey(biome.getKey())), biome.getKey().getLocation().toString(), SearchType.BIOME));
        });
        

        //Add structures from the server if the upgrade is installed and activated. The list is populated when you right click the control
        this.searchNames.addAll(this.structureNames);
        
        //Sort
        this.searchNames.sort((one, two) -> {
            return one.trans.getString().compareToIgnoreCase(two.trans.getString());
        });

        int index = 0;

        if (!searchTerm.isEmpty()) {
            searchNames.removeIf(biome -> !biome.trans.getUnformattedComponentText().toLowerCase().contains(searchTerm.toLowerCase()));

        }

        searchPages = Partition.ofSize(searchNames, MAX_ELEM_PER_PAGE);

        if (!searchNames.isEmpty()) {
            for (Search biome : searchPages.get(page)) {
                this.addButton(new TextButton(width / 2 - 50, height / 2 + 50 - (index * font.FONT_HEIGHT + 2), biome.trans.getString(), but -> {
                    Network.sendToServer(new TelepathicMessage(biome.type, biome.key));
                    Minecraft.getInstance().displayGuiScreen(null);
                }));
                ++index;
            }
        }

        if (addBackButtons) {
            this.addButton(new Button(width / 2 + 50, height / 2 + 75, 20, 20, new StringTextComponent(">"), but -> mod(1)));
            this.addButton(new Button(width / 2 - 80, height / 2 + 75, 20, 20, new StringTextComponent("<"), but -> mod(-1)));
        }

    }

    public void mod(int m) {
        int pages = searchPages.size();
        if (page + m >= 0 && page + m < pages)
            page += m;
        init();
    }
    
    

    @Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	@Override
    public boolean keyReleased(int keyCode, int scanCode, int modifiers) {
        if (textFieldWidget.isFocused()) {
            this.page = 0;
            searchTerm = textFieldWidget.getText();
            createFilteredList(searchTerm, true);
        }
        return super.keyReleased(keyCode, scanCode, modifiers);
    }

    @Override
    public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
        textFieldWidget.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
        return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
    }

    @Override
    public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
        return this.textFieldWidget.charTyped(p_charTyped_1_, p_charTyped_2_);
    }

	@Override
	public void setTelepathicStructureNamesFromServer(List<Search> searches) {
		this.structureNames.clear();
		this.structureNames.addAll(searches);
		init();
	}
}
