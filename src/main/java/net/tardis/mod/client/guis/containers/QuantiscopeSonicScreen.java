package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.containers.QuantiscopeSonicContainer;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.items.sonicparts.SonicBasePart.SonicComponentTypes;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.network.packets.SonicPartChangeMessage;
import net.tardis.mod.sonic.ISonicPart.SonicPart;
import net.tardis.mod.sonic.capability.SonicCapability;
import net.tardis.mod.tileentities.QuantiscopeTile;
import net.tardis.mod.tileentities.QuantiscopeTile.EnumMode;

public class QuantiscopeSonicScreen extends ContainerScreen<QuantiscopeSonicContainer> {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/sonic.png");
    public static final ResourceLocation TEXTURE_IRON = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/sonic_iron.png");
    private ItemStack emitter = new ItemStack(TItems.SONIC_EMITTER.get());
    private ItemStack activator = new ItemStack(TItems.SONIC_ACTIVATOR.get());
    private ItemStack handle = new ItemStack(TItems.SONIC_HANDLE.get());
    private ItemStack end = new ItemStack(TItems.SONIC_END.get());
    private QuantiscopeTile tile;

    public QuantiscopeSonicScreen(QuantiscopeSonicContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
        this.tile = screenContainer.getQuantiscope();
    }

    @Override
    protected void init() {
        super.init();

        //Reset parts
        emitter = new ItemStack(TItems.SONIC_EMITTER.get());
        activator = new ItemStack(TItems.SONIC_ACTIVATOR.get());
        handle = new ItemStack(TItems.SONIC_HANDLE.get());
        end = new ItemStack(TItems.SONIC_END.get());

        this.readFromSlot();

        this.getContainer().setSlotChangeAction(() -> this.readFromSlot());

        //Up Arrow buttons
        this.addSonicButton(60, 65, 178, 1, this.emitter);
        this.addSonicButton(22, 65, 178, 1, this.activator);
        this.addSonicButton(-15, 65, 178, 1, this.handle);
        this.addSonicButton(-54, 65, 178, 1, this.end);

        //Down Arrow buttons
        this.addSonicButton(60, 41, 178, -1, this.emitter);
        this.addSonicButton(22, 41, 178, -1, this.activator);
        this.addSonicButton(-15, 41, 178, -1, this.handle);
        this.addSonicButton(-54, 41, 178, -1, this.end);
        
        //Toggle Modes
        addModeButton(-71, 21, 193, 1);
        addModeButton(-7, 21, 188, -1);

    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        this.renderBackground(matrixStack);
        int texWidth = 176;
        int textHeight = 166;
        ResourceLocation texture = this.tile.getBlockState().getBlock() == TBlocks.quantiscope_brass.get() ? TEXTURE : TEXTURE_IRON;
        Minecraft.getInstance().getTextureManager().bindTexture(texture);
        blit(matrixStack, width / 2 - texWidth / 2, height / 2 - textHeight / 2, 0, 0, texWidth, textHeight);
        
        //TODO: Make render not move with sonic item being clicked by mouse
        matrixStack.push(); //matrixStack start
        
//        matrixStack.translate(2.25, 2.25, 0); //translate so sonics are placed on gui by defalt
        
        //Dirty hack to manually translate the sonic back if there is a sonic in the slot
        //I think the true cause of the render shifting is because of the way the sonic is renderered
        if (this.tile != null && this.tile.getStackInSlot(0).isEmpty()) {
        	matrixStack.translate(2.25, 2.3, 0);
        }
        
        RenderSystem.enableDepthTest();
        RenderHelper.enableStandardItemLighting();
        
        this.itemRenderer.zLevel = 10F;
        
        RenderSystem.enableRescaleNormal();
        matrixStack.scale(2, 2, 2);
        RenderSystem.disableRescaleNormal();
        
        matrixStack.rotate(Vector3f.ZP.rotationDegrees(90));
        
        matrixStack.push(); //matrixStack start
        RenderSystem.pushMatrix(); //RenderSystem start
        RenderSystem.multMatrix(matrixStack.getLast().getMatrix());
        
        TRenderHelper.renderItemOnScreenBackground(matrixStack, itemRenderer, emitter, 0.7f, 0.5f, 0);
        RenderSystem.popMatrix(); //RenderSystem end
        matrixStack.pop(); //matrixStack end
        
        RenderSystem.pushMatrix(); //RenderSystem start
        RenderSystem.multMatrix(matrixStack.getLast().getMatrix());
        TRenderHelper.renderItemOnScreenBackground(matrixStack, itemRenderer, activator, 0.7f, -0.6f, 0);
        RenderSystem.popMatrix(); //RenderSystem end
        
        RenderSystem.pushMatrix(); //RenderSystem start
        RenderSystem.multMatrix(matrixStack.getLast().getMatrix());
        TRenderHelper.renderItemOnScreenBackground(matrixStack, itemRenderer, handle, 0.7f, -1.75f, 0);
        RenderSystem.popMatrix(); //RenderSystem end
        
        RenderSystem.pushMatrix(); //RenderSystem start
        RenderSystem.multMatrix(matrixStack.getLast().getMatrix());
        TRenderHelper.renderItemOnScreenBackground(matrixStack, itemRenderer, end, 0.7f, -2.95f, 0);
        
        RenderSystem.popMatrix(); //RenderSystem end
        
        RenderHelper.disableStandardItemLighting();
        RenderSystem.disableDepthTest();
        
        matrixStack.pop(); //matrixStack end
    }


    public void readFromSlot() {
        if (this.getContainer().getSlot(0).getStack().getItem() instanceof SonicItem) {
            SonicCapability.getForStack(this.getContainer().getSlot(0).getStack()).ifPresent(cap -> {
                SonicBasePart.setType(this.emitter, cap.getSonicPart(SonicPart.EMITTER));
                SonicBasePart.setType(this.activator, cap.getSonicPart(SonicPart.ACTIVATOR));
                SonicBasePart.setType(this.handle, cap.getSonicPart(SonicPart.HANDLE));
                SonicBasePart.setType(this.end, cap.getSonicPart(SonicPart.END));
            });
        }
    }

    public void addSonicButton(int x, int y, int u, int mod, ItemStack stack) {
        this.addButton(new ImageButton(width / 2 - x, height / 2 - y, 6, 6, u, 1, 7, TEXTURE, but -> {
            int id = SonicBasePart.getType(stack) + mod;
            SonicComponentTypes type = SonicComponentTypes.values()[0];
            if (id < SonicComponentTypes.values().length && id >= 0) {
                type = SonicComponentTypes.values()[id];
            } else if (id < 0) {
                type = SonicComponentTypes.values()[SonicComponentTypes.values().length - 1];
            }
            update(stack, type);
        }));
    }


    public void update(ItemStack part, SonicComponentTypes type) {
        SonicBasePart.setType(part, type);
        Network.sendToServer(new SonicPartChangeMessage(part, 0, this.getContainer().pos));
    }


    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        matrixStack.push();
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 10, this.guiTop + 24, this.guiLeft + 50, this.guiTop + 39)) {
            this.renderTooltip(matrixStack, this.emitter, mouseX, mouseY);
        }
        if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 52, this.guiTop + 24, this.guiLeft + 85, this.guiTop + 39)) {
            this.renderTooltip(matrixStack, this.activator, mouseX, mouseY);
        }
        if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 92, this.guiTop + 24, this.guiLeft + 125, this.guiTop + 39)) {
            this.renderTooltip(matrixStack, this.handle, mouseX, mouseY);
        }
        if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 130, this.guiTop + 24, this.guiLeft + 160, this.guiTop + 39)) {
            this.renderTooltip(matrixStack, this.end, mouseX, mouseY);
        }
        TranslationTextComponent modeTitle = this.tile.getMode() == EnumMode.WELD ? QuantiscopeWeldScreen.weld_tab : QuantiscopeWeldScreen.sonic_tab;
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, modeTitle.mergeStyle(TextFormatting.GREEN), this.width / 2 + 42, this.guiTop + 62, 0xFFFFFF);
        matrixStack.pop();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int mouseX, int mouseY) {
    	this.font.drawText(matrixStack, this.playerInventory.getDisplayName(), (float)this.playerInventoryTitleX, (float)this.playerInventoryTitleY, 4210752);
    }
    
    public void addModeButton(int x, int y, int uOffset, int modIndex) {
		 this.addButton(new ImageButton(width / 2 - x, height / 2 - y, 4, 8, uOffset, 17, 9, TEXTURE, but -> {
			 EnumMode currentMode = tile.getMode();
			 int index = currentMode.ordinal() + modIndex;
			 if (index < EnumMode.values().length && index >= 0) {
				 currentMode = 	EnumMode.values()[index];
			 }
			 else if (index < 0) {
				 currentMode = EnumMode.values()[EnumMode.values().length - 1];
			 }
			 else if (index == EnumMode.values().length) {
				 index = 0;
				 currentMode = 	EnumMode.values()[index];
			 }
			 Network.sendToServer(new QuantiscopeTabMessage(this.getContainer().getQuantiscope().getPos(), currentMode));
		 }));
	}
    
    
}
