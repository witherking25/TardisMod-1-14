package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.contexts.gui.GuiContextSubsytem;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.DiagnosticMessage.ArtronUseInfo;
import net.tardis.mod.subsystem.SubsystemInfo;

import java.text.DecimalFormat;
import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;

public class DiagnosticGui extends Screen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/diagnostic.png");
    public static DecimalFormat FORMAT = new DecimalFormat("###");
    List<SubsystemInfo> list;
    public Window SUBSYSTEM = (matrixStack) -> {
        int i = 0;
        int x = this.width / 2 - 70;
        int y = this.height / 2 - 80;
        net.minecraft.client.renderer.RenderHelper.setupGuiFlatDiffuseLighting();
        TranslationTextComponent subsystem_title = new TranslationTextComponent("overlay.tardis.system_title");
        //Needs a MatrixStack
        drawString(matrixStack, this.font, subsystem_title, x - 20, y + 10, 0xFFFFFF);
        for (SubsystemInfo info : list) {
            y = (this.height / 2 - 55) + (i * (font.FONT_HEIGHT + 2));
            float percent = info.health * 100.0F;

            TextFormatting color = TextFormatting.GREEN;

            if (percent <= 100) {
                color = TextFormatting.DARK_GREEN;

                if (percent <= 75) {
                    color = TextFormatting.GREEN;

                    if (percent <= 50) {
                        color = TextFormatting.YELLOW;

                        if (percent <= 25)
                            color = TextFormatting.DARK_RED;
                    }
                }
            }
            StringTextComponent percentToString = new StringTextComponent(FORMAT.format(percent) + "%");
            drawString(matrixStack, this.font, new TranslationTextComponent(info.translationKey).getString() + ": " +
                    percentToString.mergeStyle(color).getString(), x, y, 0xFFFFFF);
            Minecraft.getInstance().getItemRenderer()
                    .renderItemAndEffectIntoGUI(new ItemStack(info.key), x - 16, y - 4);
            ++i;
        }
        net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
    };
    private Window window = null;
    private SpaceTimeCoord location;
    private List<ArtronUseInfo> artronInfos;

    public DiagnosticGui(GuiContextSubsytem cont) {
        super(new StringTextComponent(""));
        this.list = cont.infos;
        this.location = cont.location;
        this.artronInfos = cont.artronInfo;
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {

        this.renderBackground(matrixStack);

        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int width = 255, height = 182;
        this.blit(matrixStack, this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

        super.render(matrixStack, mouseX, mouseY, partialTicks);

        if (this.window != null)
            this.window.render(matrixStack);

    }

    @Override
    protected void init() {
        super.init();

        if (this.window != null)
            return;

        int x = width / 2 - 95, y = height / 2;
        this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 0), "> Subsystem Info", but -> {
            this.setWindow(SUBSYSTEM);

        }));

        this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 1), "> Timeship Location", but -> {
            this.setWindow((matrixStack) -> {
                int locX = this.width / 2, locY = this.height / 2 - 70;
                drawCenteredString(matrixStack, this.font, "Timeship Location", locX, locY, 0xFFFFFF);
                drawCenteredString(matrixStack, this.font, String.format("Located at %s in %s",
                        WorldHelper.formatBlockPos(this.location.getPos()),
                        WorldHelper.formatDimName(this.location.getDim())),
                        locX, locY + font.FONT_HEIGHT + 6, 0xFFFFFF);
            });

        }));

        this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 2), "> Artron Use", but -> {
            this.setWindow((matrixStack) -> {
                int locX = this.width / 2, locY = this.height / 2 - 70;
                drawCenteredString(matrixStack, this.font, "Artron Uses", locX, locY, 0xFFFFFF);
                int index = 0;
                for (ArtronUseInfo info : this.artronInfos) {

                    DecimalFormat FORMAT = new DecimalFormat("#.##");

                    drawCenteredString(matrixStack, this.font, info.key.getString() + " " + FORMAT.format(info.use * 20) + "AU/s", locX, locY + 20 + (index * (font.FONT_HEIGHT + 2)), 0xFFFFFF);
                    ++index;
                }
            });
        }));

    }

    public void setWindow(Window win) {
        this.window = win;

        for (Widget w : this.buttons) {
            w.active = false;
        }
        this.buttons.clear();
    }

    public static interface Window {
        void render(MatrixStack matrixStack);
    }

}
