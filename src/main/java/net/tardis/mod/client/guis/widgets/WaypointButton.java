package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.helper.Helper;

public class WaypointButton extends Widget{
	
	public static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/containers/waypoint.png");
	private boolean isChecked = false;
	private int id;
	private boolean isFromBank = false;

	public WaypointButton(int x, int y, int width, int height, StringTextComponent text, int id, boolean isInBank) {
		super(x, y, width, height, text);
		this.id = id;
		this.isFromBank = isInBank;
	}

	@Override
	public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		//super.renderWidget(mouseX, mouseY, partialTicks);
		
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		
		int offset = this.isChecked ? 18 : 0;
		
		this.blit(matrixStack, x, y, 180, offset, width, 0 + height);
		
		FontRenderer fr = Minecraft.getInstance().fontRenderer;
		fr.drawStringWithShadow(matrixStack, this.getMessage().getString(), x + width / 2 - 12, y + (fr.FONT_HEIGHT / 2), 14737632);
		
		
	}

	public boolean getChecked() {
		return this.isChecked;
	}
	
	public void setChecked(boolean checked) {
		this.isChecked = checked;
	}
	
	public void setFromBank(boolean bank) {
		this.isFromBank = bank;
	}
	
	public boolean getIsFromBank() {
		return this.isFromBank;
	}
	
	public int getId() {
		return this.id;
	}

}
