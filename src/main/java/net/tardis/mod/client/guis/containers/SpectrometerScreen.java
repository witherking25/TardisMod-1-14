package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.containers.SpectrometerContainer;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.SonicItem;

public class SpectrometerScreen extends ContainerScreen<SpectrometerContainer> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/containers/spectrometer.png");

    public SpectrometerScreen(SpectrometerContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        this.renderBackground(matrixStack);
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        //this.blit(matrixStack, (this.width - 177) / 2, (this.height - 166) / 2, 0, 0, 177, 166);
        this.blit(matrixStack, this.guiLeft, this.guiTop, 0, 0, 177, 166);
        this.renderProgressBar(matrixStack, container.getProgress());
        this.renderDownloadProgress(matrixStack, container.getDownloadProgress());

        LampColor upperLampColor = LampColor.OFF;

        if(container.getDownloadProgress() > 0)
            upperLampColor = LampColor.RED;
        else if(container.hasSchematics()){
            upperLampColor = LampColor.GREEN;
        }

        this.renderLamp(matrixStack, (width + 29) / 2, (height - 152) / 2, upperLampColor);

        if(container.getSlot(1).getStack().getItem() instanceof SonicItem){
            this.renderLamp(matrixStack, (width + 119) / 2, (height - 112) / 2, container.getDownloadProgress() > 0 ? LampColor.RED : LampColor.GREEN);
        }

        this.font.drawText(matrixStack, new StringTextComponent("Test"), this.guiLeft + 128, this.guiTop + 11, 0x699F63);

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, title.getString(), this.width / 2, this.guiTop - 15, 0xFFFFFF);
    }
    
    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int mouseX, int mouseY) {
        this.font.drawString(matrixStack, this.playerInventory.getDisplayName().getString(), 8.0F, (float) (this.ySize - 76), 4210752);
    }

    public void renderProgressBar(MatrixStack stack, float percent){

        int segment = (int) Math.ceil(percent * 8);

        int x = this.guiLeft + 33, y = this.guiTop + 19;

        if(segment > 0){
            this.blit(stack, x + 11, y, 188, 1, 10, 5);
        }
        if(segment > 1){
            this.blit(stack, x + 22, y, 199, 1, 10, 10);
        }
        if(segment > 2){
            this.blit(stack, x + 27, y + 11, 204, 12, 5, 10);
        }
        if(segment > 3){
            this.blit(stack, x + 22, y + 22, 199, 23, 10, 10);
        }
        if(segment > 4){
            this.blit(stack, x + 11, y + 27, 188, 1, 10, 5);
        }
        if(segment > 5){
            this.blit(stack, x, y + 22, 177, 23, 10, 10);
        }
        if(segment > 6){
            this.blit(stack, x, y + 11, 204, 12, 5, 10);
        }
        if(segment > 7){
            this.blit(stack, x, y, 177, 1, 10, 10);
        }


    }

    public void renderDownloadProgress(MatrixStack matrix, float percent){

        int segments = (int) Math.ceil(percent * 5);

        if(segments > 0){
            this.blit(matrix, this.guiLeft + 108, this.guiTop + 25, 177, 34, 5, 10);
        }
        if(segments > 1){
            this.blit(matrix, this.guiLeft + 108, this.guiTop + 36, 177, 34, 5, 10);
        }
        if(segments > 2){
            this.blit(matrix, this.guiLeft + 108, this.guiTop + 47, 177, 56, 10, 10);
        }
        if(segments > 3){
            this.blit(matrix, this.guiLeft + 119, this.guiTop + 52, 188, 61, 10, 5);
        }
        if(segments > 4){
            this.blit(matrix, this.guiLeft + 130, this.guiTop + 49, 199, 58, 11, 11);
        }


    }

    public void renderLamp(MatrixStack stack, int x, int y, LampColor color){

        if(color == LampColor.OFF)
            return;


        int u = 0, v = 1;

        if(color == LampColor.GREEN)
            u = 227;
        else if(color == LampColor.RED)
            u = 210;

        this.blit(stack, x, y, u, v, 16, 16);


    }

    public enum LampColor{
        RED, GREEN, OFF
    }
}
