package net.tardis.mod.client.guis;

import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.ars.ARSPieceCategory;
import net.tardis.mod.client.guis.ars.ARSCategoryButton;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.sounds.TSounds;

public class ARSTabletScreen extends Screen {

    private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
    private List<ARSPieceCategory> categories = Lists.newArrayList();
    protected ARSPieceCategory openCategory = null;

    private int index = 0;

    public ARSTabletScreen() {
        super(new StringTextComponent(""));
    }

    public ARSTabletScreen(GuiContext cont) {
        this();
    }

    @Override
    protected void init() {
        super.init();

        this.addButtons(width / 2 - 100, height / 2 - 20);

    }

    public void addButtons(int x, int y) {
    	Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.SCREEN_BEEP_SINGLE.get(), 1.0F));
        this.categories.clear();
        //Build trees
        for(ResourceLocation loc : ARSPiece.getRegistry().keySet()){
            String[] path = loc.getPath().split("/");

            //Build categories out of all but the last
            ARSPieceCategory last = null;
            for(int i = 0; i < path.length - 1; ++i){
                String name = path[i];
                if(last == null)
                    last = this.getOrCreateCategories(name, loc.getNamespace());
                else last = last.add(name, loc.getNamespace());
            }
            //Add actual piece
            if(last != null)
                last.addPieceToList(ARSPiece.getRegistry().get(loc));

        }

        //Actually add buttons
        int i = 0;
        for(ARSPieceCategory cat : this.categories){
            this.addButton(new ARSCategoryButton(x, y + (i * (font.FONT_HEIGHT + 2)), cat, this));
            ++i;
        }

    }

    public ARSPieceCategory getOrCreateCategories(String name, String modid){
        for(ARSPieceCategory cat : categories){
            if(cat.categoryMatches(name))
                return cat;
        }
        ARSPieceCategory cat = new ARSPieceCategory(name, modid);
        this.categories.add(cat);
        return cat;
    }

    public void modIndex(int mod, int size) {
        if (this.index + mod > size)
            this.index = 0;
        else if (this.index + mod < 0)
            this.index = size;
        else this.index += mod;

        this.init();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {

        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int width = 256, height = 173;
        this.blit(matrixStack, this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(Constants.Strings.GUI + "ars_tablet.gui_title").getString(), this.width / 2, this.height / 2 - (height / 2 - 30), 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(Constants.Strings.GUI + "ars_tablet.gui_info").getString(), this.width / 2, this.height / 2 - (height / 2 - 50), 0xFFFFFF);
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    public void openCategory(ARSPieceCategory cate){
        this.openCategory = cate;
        Minecraft.getInstance().displayGuiScreen(new ARSTabletCategoryScreen(this.openCategory));
    }

    protected void addDefaultButtons(int x, int maxIndex) {
        //Add next, prev, etc
    	this.addButton(new TextButton(x, height / 2 + 45, Constants.Translations.GUI_NEXT.getString(), but -> modIndex(1, maxIndex)));
        this.addButton(new TextButton(x, height / 2 + 55, Constants.Translations.GUI_PREV.getString(), but -> modIndex(-1, maxIndex)));
    }

}
