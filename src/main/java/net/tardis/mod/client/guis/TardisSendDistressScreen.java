package net.tardis.mod.client.guis;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SendTardisDistressSignal;

public class TardisSendDistressScreen extends Screen implements INeedTardisNames{

    public static final StringTextComponent TITLE = new StringTextComponent("");
    private final TranslationTextComponent message_desc = new TranslationTextComponent(Constants.Strings.GUI + "distress_signal.message_desc");
    private final TranslationTextComponent select_ship = new TranslationTextComponent(Constants.Strings.GUI + "distress_signal.select_ship");
    
    private Map<ResourceLocation, String> names = Maps.newHashMap();
    private TextFieldWidget message;

    public TardisSendDistressScreen() {
        super(TITLE);
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        this.renderBackground(matrixStack);
        drawCenteredString(matrixStack, this.font, message_desc.getString(), width / 2, height / 2 - 60, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, select_ship.getString(), width / 2, height / 2 - 10, 0xFFFFFF);

        int index = 1;
        for (int i = 0; i < this.buttons.size(); ++i) {
            if (this.buttons.get(i).isHovered()) {
                index = i + 1;
                break;
            }
        }

        GlStateManager.disableTexture();
        TRenderHelper.renderSineWave(width / 2 - 70, height / 2 - 80, 200, 10, 0.4 * index);
        GlStateManager.enableTexture();
        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);

    }

    @Override
    protected void init() {
        super.init();

        this.buttons.clear();
        
        
        int x = width / 2 - 75, y = height / 2 - 40;
        this.message = this.addButton(new TextFieldWidget(this.font, x, y, 150, this.font.FONT_HEIGHT + 8, new TranslationTextComponent("")));
        
        int x2 = width / 2 - 50, y2 = height / 2 + 10;

        int index = 1;

        this.addButton(new TextButton(x2, y2, "All", but -> {
            Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), null));
        }));

        for(Entry<ResourceLocation, String> e : this.names.entrySet()) {
        	this.addButton(new TextButton(x2, y2 + ((this.font.FONT_HEIGHT + 2) * index), e.getValue(), but -> {
                Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), e.getKey()));
                closeScreen();
            }));
            ++index;
        }
    }

	@Override
	public void setNamesFromServer(Map<ResourceLocation, String> nameMap) {
		this.names.clear();
		this.names.putAll(nameMap);
		init();
	}

}
