package net.tardis.mod.client.guis.containers;


import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.RegistryObject;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.guis.minigame.WireGameScreen;
import net.tardis.mod.client.guis.widgets.ImageToggleButton;
import net.tardis.mod.containers.EngineContainer;
import net.tardis.mod.containers.slot.EngineSlot;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SubsystemToggleMessage;
import net.tardis.mod.registries.SubsystemRegistry;
import net.tardis.mod.registries.UpgradeRegistry;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.upgrades.UpgradeEntry;
import org.lwjgl.glfw.GLFW;


public class EngineContainerScreen extends ContainerScreen<EngineContainer> {

    public static final ResourceLocation TEXTURE = new ResourceLocation("textures/gui/container/generic_54.png");
    
    public static final ResourceLocation UPGRADES = Helper.createRL("textures/gui/engine/upgrade.png");
    public static final ResourceLocation SUBSYSTEM = Helper.createRL("textures/gui/engine/subsystem.png");
    public static final ResourceLocation CAPACITOR = Helper.createRL("textures/gui/engine/capacitor.png");
    public static final ResourceLocation ATTUNEMENT = Helper.createRL("textures/gui/engine/attunement.png");
    

    public EngineContainerScreen(EngineContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
        this.xSize = 176;
        this.ySize = 97 + 35;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
        this.renderBackground(matrixStack);
        if(this.getContainer().getPanelDirection() == Direction.SOUTH) {
        	Minecraft.getInstance().getTextureManager().bindTexture(UPGRADES);
        	this.blit(matrixStack, width / 2 - 176  /2, height / 2 - 166 / 2, 0, 0, 176, 166);
        }
        else if(this.getContainer().getPanelDirection() == Direction.NORTH) {
        	Minecraft.getInstance().textureManager.bindTexture(SUBSYSTEM);
        	this.blit(matrixStack, width /2 - 176 / 2, height / 2 - 166 / 2, 0, 0, 176, 166);
        }
        else if(this.getContainer().getPanelDirection() == Direction.WEST) {
        	Minecraft.getInstance().textureManager.bindTexture(CAPACITOR);
        	this.blit(matrixStack, width /2 - 176 / 2, height / 2 - 166 / 2, 0, 0, 176, 166);
        }
        else if(this.getContainer().getPanelDirection() == Direction.EAST) {
        	Minecraft.getInstance().textureManager.bindTexture(ATTUNEMENT);
        	this.blit(matrixStack, width /2 - 176 / 2, height / 2 - 166 / 2, 0, 0, 176, 166);
        	
        	Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        		float progress = data.getAttunementHandler().getClientAttunementProgress();

        		int amtToDraw = (int) Math.floor(progress * 8.1F); //Multiple a bit more than 8 to account for rounding
            	
            	if(amtToDraw >= 1)
            		this.blit(matrixStack, width / 2 + 45, height / 2 - 64, 199, 1, 10, 10);
            	if(amtToDraw >= 2)
            		this.blit(matrixStack, width / 2 + 50, height / 2 - 53, 204, 12, 5, 10);
            	if(amtToDraw >= 3)
            		this.blit(matrixStack, width / 2 + 45, height / 2 - 42, 199, 23, 10, 10);
            	if(amtToDraw >= 4)
            		this.blit(matrixStack, width / 2 + 34, height / 2 - 37, 188, 28, 10, 5);
            	if(amtToDraw >= 5)
            		this.blit(matrixStack, width / 2 + 23, height / 2 - 42, 177, 23, 10, 10);
            	if(amtToDraw >= 6)
            		this.blit(matrixStack, width / 2 + 23, height / 2 - 53, 177, 12, 5, 10);
            	if(amtToDraw >= 7)
            		this.blit(matrixStack, width / 2 + 23, height / 2 - 64, 177, 1, 10, 10);
            	if(amtToDraw >= 8)
            		this.blit(matrixStack, width / 2 + 34, height / 2 - 64, 188, 1, 10, 5);
        		
        	});
        }
        else {
        	Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        	int y = this.height / 2 - 35;
            this.blit(matrixStack, width / 2 - this.xSize / 2, y, 0, 125, this.xSize, 97);
            this.blit(matrixStack, width / 2 - this.xSize / 2, y - 35, 0, 0, this.xSize, 35);
        }
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, title.getString(), this.width / 2, this.guiTop - 30, 4210752);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int mouseX, int mouseY) {
        this.font.drawString(matrixStack, this.playerInventory.getDisplayName().getString(), 8.0F, (float) (this.ySize - 76), 4210752);

    }

    @Override
	protected void init() {
		super.init();
		
		this.buttons.clear();
		
		
		if(this.getContainer().getPanelDirection() == Direction.NORTH) {
			
			this.addEngineSlider(SubsystemRegistry.FLIGHT, width / 2 - 71, height / 2 - 75);
			this.addEngineSlider(SubsystemRegistry.NAV_COM, width / 2 - 29, height / 2 - 75);
			this.addEngineSlider(SubsystemRegistry.CHAMELEON, width / 2 + 13, height / 2 - 75);
			this.addEngineSlider(SubsystemRegistry.TEMPORAL_GRACE, width / 2 + 55, height / 2 - 75);
			this.addEngineSlider(SubsystemRegistry.FLUID_LINKS, width / 2 - 71, height / 2 - 25);
			this.addEngineSlider(SubsystemRegistry.STABILIZERS, width / 2 - 29, height / 2 - 25);
			this.addEngineSlider(SubsystemRegistry.ANTENNA, width / 2 + 13, height / 2 - 25);
			this.addEngineSlider(SubsystemRegistry.SHIELD_GENERATOR, width / 2 + 55, height / 2 - 25);
		}

		//Upgrade
		if(this.getContainer().getPanelDirection() == Direction.SOUTH) {
			//Top row
			this.addUpgradeSlider(0, width / 2 - 73,height / 2 - 75);
			this.addUpgradeSlider(2, width / 2 - 73 + 33,height / 2 - 75);
			this.addUpgradeSlider(4, width / 2 - 73 + 65,height / 2 - 75);
			this.addUpgradeSlider(6, width / 2 - 73 + 98,height / 2 - 75);
			this.addUpgradeSlider(8, width / 2 - 73 + 130,height / 2 - 75);

			//Lower row
			this.addUpgradeSlider(1, width / 2 - 73,height / 2 - 25);
			this.addUpgradeSlider(3, width / 2 - 73 + 33,height / 2 - 25);
			this.addUpgradeSlider(5, width / 2 - 73 + 65,height / 2 - 25);
			this.addUpgradeSlider(7, width / 2 - 73 + 98,height / 2 - 25);
			this.addUpgradeSlider(9, width / 2 - 73 + 130,height / 2 - 25);
		}
		
	}
    
    private <T extends SubsystemEntry> void addEngineSlider(RegistryObject<T> sys, int x, int y) {
    	ImageToggleButton button = this.addButton(new ImageToggleButton(x, y, 177, 2, 18, 7, (but) -> {
			ImageToggleButton img = (ImageToggleButton)but;
			img.setChecked(!img.isChecked());
			Network.sendToServer(new SubsystemToggleMessage(sys.get().getRegistryName(), true, img.isChecked()));
			
			TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
				tile.getSubsystem(sys.get().getRegistryName()).ifPresent(system -> {
					system.setActivated(img.isChecked());
				});
			});
		}));
    	
    	TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
    		tile.getSubsystem(sys.get().getRegistryName()).ifPresent(system -> {
    			button.setChecked(system.isActivated());
    		});
    	});
    	
    }

	private <T extends UpgradeEntry> void addUpgradeSlider(int slotID, int x, int y) {
		ImageToggleButton button = this.addButton(new ImageToggleButton(x, y, 177, 2, 18, 7, (but) -> {
			//Get the upgrade entry from the slot for this switch
			UpgradeEntry entry = UpgradeRegistry.getUpgradeFromItem(this.getContainer().getSlot(slotID).getStack().getItem());
			//If that has a vaild upgrade attached
			if(entry != null){
				TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
					//Get the upgrade the tile has for this entry
					tile.getUpgrade(entry.getRegistryName()).ifPresent(upgrade -> {
						//Toggle the upgrade and send it to the server
						boolean active = !upgrade.isActivated();
						upgrade.setActivated(active);
						((ImageToggleButton)but).setChecked(active);
						Network.sendToServer(new SubsystemToggleMessage(entry.getRegistryName(), false, active));
					});
				});

			}

		}));

		TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(tile -> {
			UpgradeEntry entry = UpgradeRegistry.getUpgradeFromItem(this.getContainer().getSlot(slotID).getStack().getItem());
			if(entry != null){
				tile.getUpgrade(entry.getRegistryName()).ifPresent(upgrade -> {
					button.setChecked(upgrade.isActivated());
				});
			}
		});

	}

	@Override
	protected void handleMouseClick(Slot slotIn, int slotId, int mouseButton, ClickType type) {
		ItemStack held = this.playerInventory.getItemStack();
		if(!held.isEmpty() && slotIn instanceof EngineSlot) {
			if(slotIn.isItemValid(held))
				Minecraft.getInstance().enqueue(() -> Minecraft.getInstance().displayGuiScreen(new WireGameScreen(slotId, this.container.getPanelDirection())));
		}
		super.handleMouseClick(slotIn, slotId, mouseButton, type);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
		if(keyCode == GLFW.GLFW_KEY_ESCAPE)
			return super.keyPressed(keyCode, scanCode, modifiers);
		return false;
	}

}
