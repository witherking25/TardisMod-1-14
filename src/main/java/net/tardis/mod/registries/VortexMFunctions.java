package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.vm.AbstractVortexMFunction;
import net.tardis.mod.vm.BatteryFunction;
import net.tardis.mod.vm.DistressFunction;
import net.tardis.mod.vm.ScannerFunction;
import net.tardis.mod.vm.TeleportFunction;

public class VortexMFunctions {

	public static final DeferredRegister<AbstractVortexMFunction> FUNCTIONS = DeferredRegister.create(AbstractVortexMFunction.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<AbstractVortexMFunction>> FUNCTIONS_REGISTRY = FUNCTIONS.makeRegistry("vm_function", () -> new RegistryBuilder<AbstractVortexMFunction>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<AbstractVortexMFunction> TELEPORT = FUNCTIONS.register("teleport", () -> new TeleportFunction());
    public static final RegistryObject<AbstractVortexMFunction> DISTRESS_SIGNAL = FUNCTIONS.register("distress_signal", () -> new DistressFunction());
    public static final RegistryObject<AbstractVortexMFunction> SCANNER = FUNCTIONS.register("scanner", () -> new ScannerFunction());
    public static final RegistryObject<AbstractVortexMFunction> BATTERY = FUNCTIONS.register("battery", () -> new BatteryFunction());
    
    /**
     * Assign a Function to a category.
     * Call this in FMLCommonSetup in an enqueueWork lambda.
     * <br> This ensures all registry objects have been registered correctly before modifying them
     */
    public static void addFunctionToCategories() {
    	VortexMFunctionCategories.TELEPORT.get().appendFunctionToList(TELEPORT.get());
    	VortexMFunctionCategories.TELEPORT.get().appendFunctionToList(DISTRESS_SIGNAL.get());
    	VortexMFunctionCategories.DIAGNOSTIC.get().appendFunctionToList(SCANNER.get());
    	VortexMFunctionCategories.MAINTENANCE.get().appendFunctionToList(BATTERY.get());
    }
}
