package net.tardis.mod.registries;

import java.util.HashMap;
import java.util.Map.Entry;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
/**
 * Custom Registry
 * @author Spectre0987
 *
 * @param <T>
 */
@Deprecated
public class Registry<T> {
	
	private HashMap<ResourceLocation, T> registry = new HashMap<ResourceLocation, T>();
	
	public HashMap<ResourceLocation, T> getRegistry(){
		return this.registry;
	}
	
	public T getValue(ResourceLocation key) {
		return this.registry.get(key);
	}
	
	/**
	 * 
	 * @param val
	 * @return Returns the first key associated with this value, or null if none exist
	 */
	public ResourceLocation getKeyFromValue(T val) {
		for(Entry<ResourceLocation, T> entry : registry.entrySet()) {
			if(entry.getValue().equals(val))
				return entry.getKey();
		}
		return null;
	}
	
	public void register(String key, T value) {
		ResourceLocation resourceLocation = new ResourceLocation(Tardis.MODID, key);
		if(value instanceof IRegisterable)
			((IRegisterable<?>)value).setRegistryName(resourceLocation);
		this.registry.put(resourceLocation, value);
	}
	
	public void register(ResourceLocation key, T value) {
		if(value instanceof IRegisterable)
			((IRegisterable<?>)value).setRegistryName(key);
		this.registry.put(key, value);

	}

}
