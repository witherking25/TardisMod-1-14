package net.tardis.mod.registries;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.subsystem.FlightSubsystem;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;
import net.tardis.mod.upgrades.AtriumUpgrade;
import net.tardis.mod.upgrades.ElectrolyticConvertersUpgrade;
import net.tardis.mod.upgrades.KeyFobUpgrade;
import net.tardis.mod.upgrades.StructureLocatorUpgrade;
import net.tardis.mod.upgrades.TimeLinkUpgrade;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;
import net.tardis.mod.upgrades.UpgradeEntry.IConsoleSpawner;
import net.tardis.mod.upgrades.ZeroRoomUpgrade;

public class UpgradeRegistry {
	
    public static final DeferredRegister<UpgradeEntry> UPGRADES = DeferredRegister.create(UpgradeEntry.class, Tardis.MODID);
    
    public static Supplier<IForgeRegistry<UpgradeEntry>> UPGRADE_REGISTRY = UPGRADES.makeRegistry("upgrade", () -> new RegistryBuilder<UpgradeEntry>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<UpgradeEntry> ATRIUM = UPGRADES.register("atrium",  () -> setupUpgrade(AtriumUpgrade::new, TItems.ATRIUM_UPGRADE.get(), FlightSubsystem.class));
    public static final RegistryObject<UpgradeEntry> ELECTRO_CONVERT = UPGRADES.register("electro_converters",  () -> setupUpgrade(ElectrolyticConvertersUpgrade::new, TItems.ELECTRO_CONVERT_UPGRADE.get(), TemporalGraceSubsystem.class));
    public static final RegistryObject<UpgradeEntry> KEY_FOB = UPGRADES.register("key_fob",  () -> setupUpgrade(KeyFobUpgrade::new, TItems.KEY_FOB_UPGRADE.get(), ShieldGeneratorSubsystem.class));
    public static final RegistryObject<UpgradeEntry> TIME_LINK = UPGRADES.register("time_link",  () -> setupUpgrade(TimeLinkUpgrade::new, TItems.TIME_LINK_UPGRADE.get(), FlightSubsystem.class));
    public static final RegistryObject<UpgradeEntry> ZERO_ROOM = UPGRADES.register("zero_room",  () -> setupUpgrade(ZeroRoomUpgrade::new, TItems.ZERO_ROOM.get(), TemporalGraceSubsystem.class));
    //Please register your upgrades Spectre, don't just use a hardcoded check for the upgrade item
    //Now that Upgrades are toggleable and you are finding upgrades via items, if you don't register an upgrade entry the game will crash
    public static final RegistryObject<UpgradeEntry> STRUCTURE = UPGRADES.register("structure",  () -> setupUpgrade(StructureLocatorUpgrade::new, TItems.TELE_STRUCTURE_UPGRADE.get(), FlightSubsystem.class));
    
    public static UpgradeEntry setupUpgrade(IConsoleSpawner<Upgrade> spawn, Item item, Class<? extends Subsystem> sys){
		UpgradeEntry entry = new UpgradeEntry(spawn, item, sys);
		return entry;
	}
    
    public static UpgradeEntry getUpgradeFromItem(Item item){
    	UpgradeEntry foundEntry = null;
    	List<UpgradeEntry> upgrades = Lists.newArrayList(UPGRADE_REGISTRY.get().getValues());
    	for (UpgradeEntry entry : upgrades) {
    		if (item != null && entry.getItem() == item) {
    			foundEntry = entry;
    		}
    	}
    	return foundEntry;
    }
	

}