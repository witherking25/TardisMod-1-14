package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.Console;

public class ConsoleRegistry {
	
	public static final DeferredRegister<Console> CONSOLES = DeferredRegister.create(Console.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<Console>> CONSOLE_REGISTRY = CONSOLES.makeRegistry("console", () -> new RegistryBuilder<Console>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<Console> STEAM = CONSOLES.register("steam", () -> new Console(() -> TBlocks.console_steam.get().getDefaultState(), "steam"));
	public static final RegistryObject<Console> NEMO = CONSOLES.register("nemo", () -> new Console(() -> TBlocks.console_nemo.get().getDefaultState(), "nemo"));
	public static final RegistryObject<Console> GALVANIC = CONSOLES.register("galvanic", () -> new Console(() -> TBlocks.console_galvanic.get().getDefaultState(), "galvanic"));
	public static final RegistryObject<Console> CORAL = CONSOLES.register("coral", () -> new Console(() -> TBlocks.console_coral.get().getDefaultState(), "coral"));
	public static final RegistryObject<Console> HARTNELL = CONSOLES.register("hartnell", () -> new Console(() -> TBlocks.console_hartnell.get().getDefaultState(), "hartnell"));
	public static final RegistryObject<Console> TOYOTA = CONSOLES.register("toyota", () -> new Console(() -> TBlocks.console_toyota.get().getDefaultState(), "toyota"));
	public static final RegistryObject<Console> XION = CONSOLES.register("xion", () -> new Console(() -> TBlocks.console_xion.get().getDefaultState(), "xion"));
	public static final RegistryObject<Console> NEUTRON = CONSOLES.register("neutron", () -> new Console(() -> TBlocks.console_neutron.get().getDefaultState(), "neutron"));
	public static final RegistryObject<Console> POLYMEDICAL = CONSOLES.register("polymedical", () -> new Console(() -> TBlocks.console_polymedical.get().getDefaultState(), "polymedical"));
	

}
