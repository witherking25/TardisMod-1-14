package net.tardis.mod.registries;

import java.util.function.BiFunction;
import java.util.function.Supplier;

import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.missions.DroneStationMission;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.missions.MiniMissionType.IMissionBuilder;
import net.tardis.mod.world.structures.TStructures;


public class MissionRegistry {
    
    public static final DeferredRegister<MiniMissionType> MISSIONS = DeferredRegister.create(MiniMissionType.class, Tardis.MODID);
    
    public static Supplier<IForgeRegistry<MiniMissionType>> MISSION_REGISTRY = MISSIONS.makeRegistry("mission", () -> new RegistryBuilder<MiniMissionType>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<MiniMissionType> STATION_DRONE = MISSIONS.register("drone_station", () -> registerMission(DroneStationMission::new, TStructures.ConfiguredStructures.SPACE_STATION_KEY));
    
    public static MiniMissionType registerMission(IMissionBuilder<MiniMission> builder, BiFunction<ServerWorld, BlockPos, BlockPos> spawner){
        MiniMissionType type = new MiniMissionType(builder, spawner);
        return type;
    }
    
    public static MiniMissionType registerMission(IMissionBuilder<MiniMission> builder, RegistryKey<StructureFeature<?,?>> structure){
        MiniMissionType type = new MiniMissionType(builder, structure);
        return type;
    }
}
