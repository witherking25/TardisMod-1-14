package net.tardis.mod.trades;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.merchant.villager.VillagerTrades.ITrade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MerchantOffer;

public class ItemTrade implements ITrade{

	private ItemStack coin;
	private ItemStack wares;
	
	private int xp;
	private int stock;
	
	public ItemTrade(ItemStack coin, ItemStack wares, int stock, int xp){
		this.xp = xp;
		this.stock = stock;
		this.wares = wares;
		this.coin = coin;
	}
	
	@Override
	public MerchantOffer getOffer(Entity trader, Random rand) {
		return new MerchantOffer(coin, wares, stock, xp, 0F);
	
	}
}
