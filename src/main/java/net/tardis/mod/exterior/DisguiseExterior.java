package net.tardis.mod.exterior;

import java.util.function.Supplier;

import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.misc.DoorSounds;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.subsystem.ChameleonSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class DisguiseExterior extends TwoBlockBasicExterior{

	public DisguiseExterior(Supplier<BlockState> state, boolean inChameleon, IDoorType type, ResourceLocation blueprint) {
		super(state, inChameleon, type, DoorSounds.BASE, blueprint);
	}

	@Override
	public int getWidth(ConsoleTile console) {
		ObjectWrapper<Integer> width = new ObjectWrapper<Integer>(super.getWidth(console));
		console.getSubsystem(ChameleonSubsystem.class).ifPresent(sys -> {
			if(sys.isActiveCamo(false) && !sys.canBeUsed()) {
				if(console.getExteriorManager().getDisguise() != null)
					width.setValue(console.getExteriorManager().getDisguise().getWidth());
			}
		});
		return width.getValue();
	}

	@Override
	public int getHeight(ConsoleTile console) {
		ObjectWrapper<Integer> height = new ObjectWrapper<Integer>(super.getHeight(console));
		console.getSubsystem(ChameleonSubsystem.class).ifPresent(sys -> {
			if(sys.isActiveCamo(false) && !sys.canBeUsed()) {
				if(console.getExteriorManager().getDisguise() != null)
					height.setValue(console.getExteriorManager().getDisguise().getHeight());
			}
		});
		return height.getValue();
	}

}
