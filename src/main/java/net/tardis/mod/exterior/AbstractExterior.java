package net.tardis.mod.exterior;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.misc.IDoorSoundScheme;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class AbstractExterior extends ForgeRegistryEntry<AbstractExterior>{
	
	protected boolean isUnlockedByDefault = false;
	protected ResourceLocation blueprint = null;
	protected IDoorType type;
	protected TexVariant[] variants = null;
	protected IDoorSoundScheme sounds;
	protected String translationKey;
	
	public AbstractExterior(boolean isUnlockedByDefault, IDoorType type, IDoorSoundScheme sounds, ResourceLocation blueprint) {
		this.isUnlockedByDefault = isUnlockedByDefault;
		this.blueprint = blueprint;
		this.type = type;
		this.sounds = sounds;
	}
	/** Logic to handle exterior block removal during dematerialisation*/
	public abstract void demat(ConsoleTile console);
	/** Logic to handle exterior block placing during rematerialisation*/
	public abstract void remat(ConsoleTile console);
	/** Get the raw ExteriorTile instance. This holds the logic for the exterior*/
	public abstract ExteriorTile getExteriorTile(ConsoleTile console);
	/** Logic to handle exterior block removal when the exterior is being removed in a manner which is not part of flight
	 * <br> E.g. Removing the Exterior Blocks from its current position so that it can be replaced on a player position to save them */
	public abstract void remove(ConsoleTile tile);
	/** Logic to handle exterior block placing when the exterior is being placed in a manner which is not part of flight
	 * <br> E.g. Adding the Exterior Blocks to a player position to save them 
	 * <br> E.g 2)  Placing the blocks of an exterior which a broken exterior will swap to*/
	public abstract void place(ConsoleTile tile, RegistryKey<World> dimension, BlockPos pos);
	
	/**
	 * Whether this is added to the Unlocked Exterior list (Chameleon Circuit) by default
	 */
	public boolean isUnlockedByDefault() {return isUnlockedByDefault;}
	public abstract BlockState getDefaultState();
	/** Get the width of this exterior. This is used for the Tardis landing spot finding*/
	public abstract int getWidth(ConsoleTile console);
	/** Get the height of this exterior. This is used for the Tardis landing spot finding*/
	public abstract int getHeight(ConsoleTile console);
	/** Get the texture that is drawn in the Change Exterior GUI as a preview of this exterior. The texture is usually in the style of an engineering blueprint*/
	public ResourceLocation getBlueprintPreviewTexture() {return blueprint;}
	/** Get the interior door type that matches this exterior. This is normally done to allow the interior door model and texture to match the exterior*/
	public IDoorType getDoorType() {return type;}
	
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("exterior", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}
	
	@Nullable
	public TexVariant[] getVariants() {return variants;}
	
	public IDoorSoundScheme getDoorSounds() {return sounds;}
}
