package net.tardis.mod.recipe.serializers;

import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.recipe.SpectrometerRecipe;

import javax.annotation.Nullable;

public class SpectrometerRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<SpectrometerRecipe>{

    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, JsonObject json) {
    	SpectrometerRecipe recipe = SpectrometerRecipe.CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(Tardis.LOGGER::error).get();
        recipe.setRegistryName(recipeId);
        return recipe;
    }

    @Nullable
    @Override
    public SpectrometerRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
    	SpectrometerRecipe recipe = SpectrometerRecipe.CODEC.parse(NBTDynamicOps.INSTANCE, buffer.readCompoundTag()).resultOrPartial(Tardis.LOGGER::error).get();
        recipe.setRegistryName(recipeId); //We have to explicitly set the registry name to avoid a NPE
        return recipe;
    }

    @Override
    public void write(PacketBuffer buffer, SpectrometerRecipe recipe) {
    	buffer.writeCompoundTag((CompoundNBT) SpectrometerRecipe.CODEC.encodeStart(NBTDynamicOps.INSTANCE, recipe).resultOrPartial(Tardis.LOGGER::error).orElse(new CompoundNBT()));
    }

}
