package net.tardis.mod.recipe;


import java.util.Collection;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.AlembicJsonDataListener;
import net.tardis.mod.tileentities.AlembicTile;
/** Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
 * <br> All recipes requires Water, a burnable item and fuel.
 * <br> The output can be either an item or fluid (Mercury at the moment)*/
public class AlembicRecipe implements IRecipe<AlembicRecipeWrapper>{

	private ResourceLocation id;
	private int requiredWaterAmount = 0;
	private Ingredient requiredBurnableIngredient;
	private int requiredIngredientCount = 1;
	private ResultType type = ResultType.ITEM;
	private ItemStack result = ItemStack.EMPTY;
	
	@Deprecated
	public static final AlembicJsonDataListener DATA_LOADER = new AlembicJsonDataListener(
			"alembic",
			AlembicRecipe.class,
			Tardis.LOGGER,
			AlembicRecipe::fromJson);
	
	public static Collection<AlembicRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE);
    }
	
	/**
	 * Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
     * <br> This constructor outputs an {@linkplain ItemStack}
     * <br> All recipes requires Water, a burnable item and fuel.
     * <br> The output can be either an item or fluid (Mercury at the moment)
	 * @param requiredWater - amount of water required
	 * @param requiredIngredientCount - the count of the Ingredient required for this recipe
	 * @param requiredBurnableIngredient - the actual ingredient that gets burned in the burnable slot in the Alembic
	 * @param result - the {@linkplain ItemStack} result
	 */
	public AlembicRecipe(int requiredWater, int requiredIngredientCount, Ingredient requiredBurnableIngredient, ItemStack result) {
		this.requiredWaterAmount = requiredWater;
		this.requiredBurnableIngredient = requiredBurnableIngredient;
		this.requiredIngredientCount = requiredIngredientCount;
		this.result = result;
		this.type = ResultType.ITEM;
	}
	
	/**
	 * Custom Recipe format for recipes that are made in the {@linkplain AlembicTile}
     * <br> This constructor outputs a {@linkplain ResultType#MERCURY}, adds Mercury fluid to the Mercury fluid tank in the Alembic
     * <br> All recipes requires Water, a burnable item and fuel.
     * <br> The output can be either an item or fluid (Mercury at the moment)
	 * @param requiredWater - amount of water required
	 * @param requiredIngredientCount - the count of the Ingredient required for this recipe
	 * @param requiredBurnableIngredient - the actual ingredient that gets burned in the burnable slot in the Alembic
	 */
	public AlembicRecipe(int requiredWater, int requiredIngredientCount, Ingredient requiredBurnableIngredient) {
		this.requiredWaterAmount = requiredWater;
		this.requiredIngredientCount = requiredIngredientCount;
		this.requiredBurnableIngredient = requiredBurnableIngredient;
		this.type = ResultType.MERCURY;
	}
	
	public AlembicRecipe(PacketBuffer buf) {
		this.decode(buf);
	}
	
	/** 
	 * Get the required amount of water for this recipe
	 * @return
	 */
	public int getRequiredWater() {
		return requiredWaterAmount;
	}
    
	/** 
	 * Get the raw ingredient that will be burned in the burnable slot in the Alembic
	 * <br> E.g. Plants or Cinnabar
	 * @return
	 */
	public Ingredient getrequiredBurnableIngredient() {
		return this.requiredBurnableIngredient;
	}

    /** 
     * Gets the required number of items needed for the recipe's burnable ingredient*/
	public int getRequiredIngredientCount() {
		return requiredIngredientCount;
	}

    /**
     * Get the raw result itemstack of this recipe
     * @return
     */
	public ItemStack getResult() {
		return result.copy();
	}

	public boolean matches(FluidStack fluid, ItemStack stack) {
		//water
		if(fluid.getFluid().isIn(FluidTags.WATER) && fluid.getAmount() >= this.requiredWaterAmount) {
			//Ingredient
			if(this.requiredBurnableIngredient.test(stack) && stack.getCount() >= this.getRequiredIngredientCount())
				return true;
		}
		return false;
	}
	
	public boolean matches(AlembicTile tile) {
		return this.matches(tile.getWaterTank().getFluid(), tile.getItemStackHandler().getStackInSlot(2));
	}
	
	@Override
	public boolean matches(AlembicRecipeWrapper inv, World worldIn) {
		return matches(inv.getAlembicTile());
	}
	
	public void onCraft(AlembicTile tile) {
		if(type == ResultType.MERCURY) {
			tile.setMercury(tile.getMercury() + this.requiredWaterAmount);
		}
		else {
			boolean empty = this.getResult().isEmpty();
			if (!empty) 
			    tile.getItemStackHandler().insertItem(5, this.getResult(), false);
		}
	}
	
	
	
	public void encode(PacketBuffer buf) {
		buf.writeResourceLocation(id);
		buf.writeInt(this.requiredIngredientCount);
		buf.writeInt(this.type.ordinal());
		buf.writeInt(this.requiredWaterAmount);
		this.requiredBurnableIngredient.write(buf);
		buf.writeItemStack(result);
	}
	
	public void decode(PacketBuffer buf) {
		this.id = buf.readResourceLocation();
		this.requiredIngredientCount = buf.readInt();
		this.type = ResultType.values()[buf.readInt()];
		this.requiredWaterAmount = buf.readInt();
		this.requiredBurnableIngredient = Ingredient.read(buf);
		this.result = buf.readItemStack();
	}
	
	public static AlembicRecipe fromJson(JsonElement root) {
		try {
			JsonObject object = root.getAsJsonObject();
			
			int requiredWater = object.get("water").getAsInt();
			
			JsonObject ingred = object.get("ingredient").getAsJsonObject();
			Ingredient focalPoint = Ingredient.deserialize(ingred);
			int count = ingred.get("count").getAsInt();
			
			JsonObject result = object.get("result").getAsJsonObject();
			if(result.has("item")) {
				Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(result.get("item").getAsString()));
				ItemStack resultStack = new ItemStack(item, result.get("count").getAsInt());
				return new AlembicRecipe(requiredWater, count, focalPoint, resultStack);
			}
			else if(result.has("special")) {
				return new AlembicRecipe(requiredWater, count, focalPoint);
			}
			
			return null;
		}
		catch(Exception e) {
			Tardis.LOGGER.catching(e);
			return null;
		}
	}
	
	public static enum ResultType{
		ITEM(),
		MERCURY();
	}

	
	@Override
	public ItemStack getCraftingResult(AlembicRecipeWrapper inv) {
		return this.result;
	}

	@Override
	public boolean canFit(int width, int height) {
		return true;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return this.result;
	}

	@Override
	public ResourceLocation getId() {
		return this.id;
	}
	
	public AlembicRecipe setRegistryId(ResourceLocation id) {
		this.id = id;
		return this;
	}

	@Override
	public IRecipeSerializer<?> getSerializer() {
		return TardisRecipeSerialisers.ALEMBIC_SERIALISER.get();
	}

	@Override
	public IRecipeType<?> getType() {
		return TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE;
	}
	
	@Override
    public boolean isDynamic() {
        return false; //If false, allows recipe unlocking to be available (though this doesn't actually work)
    }

    @Override
    public String getGroup() {
        return " "; //Must have a value, otherwise vanilla will complain about a null recipe category type
    }
	
	public static class AlembicRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> 
    implements IRecipeSerializer<AlembicRecipe>{

        @Override
        public AlembicRecipe read(ResourceLocation recipeId, JsonObject json) {
        	AlembicRecipe recipe = fromJson(json);
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public AlembicRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        	ResourceLocation id = buffer.readResourceLocation();
        	int requiredCount = buffer.readInt();
        	ResultType type = ResultType.values()[buffer.readInt()];
    		int requiredWater = buffer.readInt();
    		Ingredient focalPoint = Ingredient.read(buffer);
    		ItemStack result = buffer.readItemStack();
        	AlembicRecipe recipe = null;
        	if(type == ResultType.ITEM) {
				Item item = result.getItem();
				ItemStack resultStack = new ItemStack(item, result.getCount());
				recipe = new AlembicRecipe(requiredWater, requiredCount, focalPoint, resultStack);
			}
			else if(type != ResultType.ITEM) {
				recipe = new AlembicRecipe(requiredWater, requiredCount, focalPoint);
			}
            recipe.setRegistryId(id); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public void write(PacketBuffer buffer, AlembicRecipe recipe) {
        	buffer.writeResourceLocation(recipe.id);
        	buffer.writeInt(recipe.requiredIngredientCount);
        	buffer.writeInt(recipe.type.ordinal());
        	buffer.writeInt(recipe.requiredWaterAmount);
        	recipe.requiredBurnableIngredient.write(buffer);
    		buffer.writeItemStack(recipe.result);
        }
    }
}
