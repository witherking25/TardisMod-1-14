package net.tardis.mod.recipe;

import java.util.Collection;
import java.util.Optional;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.items.wrapper.RecipeWrapper;
import net.tardis.mod.misc.IngredientCodec;
import net.tardis.mod.misc.SchematicCodec;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;
/** Recipe type for items that are turned into Unlock Schematics.
 * These unlock schematics are added to a Sonic, which can then be used to unlock objects for a Tardis*/
public class SpectrometerRecipe implements IRecipe<RecipeWrapper> {

    /** A non-empty stack to return as the 'result' so JEI doesn't piss itself */
    private static final ItemStack DUMMY = new ItemStack(Items.APPLE);

    public Ingredient input;
    public RecipeSchematicResult result;
    public Optional<Integer> ticks;
    private ResourceLocation registryName;
    
    public static final int DEFAULT_TICKS = 200;

    /** Bridging object to allow for serialisation/deserialisation to and from json easily 
     * <br> For a non static getter, call {@link SpectrometerRecipe#getCodec()}*/
    public static final Codec<SpectrometerRecipe> CODEC = RecordCodecBuilder.create(instance -> 
    instance.group(
    		Codec.INT.optionalFieldOf("processing_ticks").orElse(Optional.of(DEFAULT_TICKS)).forGetter(SpectrometerRecipe::getTicks),
    		IngredientCodec.INGREDIENT_CODEC.fieldOf("ingredient").forGetter(SpectrometerRecipe::getIngredient),
    		RecipeSchematicResult.CODEC.fieldOf("result").forGetter(SpectrometerRecipe::getResult)
    		).apply(instance, SpectrometerRecipe::new)
    );
    
    public static Collection<SpectrometerRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.SPECTROMETER_TYPE);
    }
    /**
     * Recipe type for items that are turned into Unlock Schematics.
     * <br> When making a new instance of this recipe, make sure to call {@link SpectrometerRecipe#setRegistryName(ResourceLocation)} to prevent NPEs
     * @param ticks - number of ticks it takes to process the item in the input slot
     * @param input - the input ingredient which is being processed
     * @param schematic - the matching schematic that is given once the input ingredient has finished processing
     */
    public SpectrometerRecipe(Optional<Integer> ticks, Ingredient input, RecipeSchematicResult schematic){
        this.input = input;
        this.result = schematic;
        this.ticks = ticks;
    }

    @Override
    public boolean matches(RecipeWrapper inv, World worldIn) {

        ItemStack input = inv.getStackInSlot(NeutronicSpectrometerTile.INPUT_SLOT);

        //If the machine is empty
        if(input.isEmpty())
            return false;

        return this.input.test(input);
    }

    /** Get number of ticks required to process the input ingredient*/
    public Optional<Integer> getTicks(){
        return this.ticks;
    }

    /** Get the input ingredient */
    public Ingredient getIngredient() {
    	return this.input;
    }
    /** Get the result of this recipe.
     * <br> To access the schematic itself, call getOutput*/
    public RecipeSchematicResult getResult(){
        return this.result;
    }
    
    public SpectrometerRecipe setRegistryName(ResourceLocation id) {
    	this.registryName = id;
    	return this;
    }
    
    public Codec<SpectrometerRecipe> getCodec() {
    	return CODEC;
    }

    /**The recipe's itemstack crafting table result. Don't call this anywhere as our output is actually a schematic */
    @Override
    public ItemStack getCraftingResult(RecipeWrapper inv) {
        return DUMMY;
    }

    @Override
    public boolean canFit(int width, int height) {
        return true;
    }

    /**The recipe's itemstack result. Don't call this anywhere as our output is actually a schematic */
    @Override
    public ItemStack getRecipeOutput() {
        return DUMMY;
    }

    @Override
    public ResourceLocation getId() {
        return this.registryName;
    }

    /** Gets the ingredients in the WeldRecipe. <br> Use this for JEI display purposes*/
    @Override
	public NonNullList<Ingredient> getIngredients() {
    	NonNullList<Ingredient> nonnulllist = NonNullList.create();
    	nonnulllist.add(this.input);
		return nonnulllist;
	}

	@Override
    public IRecipeSerializer<?> getSerializer() {
        return TardisRecipeSerialisers.SPECTROMETER_SERIALIZER.get();
    }

    @Override
    public IRecipeType<?> getType() {
        return TardisRecipeSerialisers.SPECTROMETER_TYPE;
    }
    
    public static class RecipeSchematicResult{
        
        public static final Codec<RecipeSchematicResult> CODEC = RecordCodecBuilder.create(instance -> 
            instance.group(
            		SchematicCodec.SCHEMATIC_CODEC.fieldOf("schematic").forGetter(RecipeSchematicResult::getOutput)
            ).apply(instance, RecipeSchematicResult::new)
        );
        
        private Schematic output;
        
        public RecipeSchematicResult(Schematic output) {
            this.output = output;
        }
        
        public Schematic getOutput() {
            return this.output;
        }
        
        public void setOutput(Schematic input) {
            this.output = input;
        }
        
    }
}
