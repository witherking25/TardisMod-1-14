package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.RoundelBlock;

public class TardisBlockModelGen implements IDataProvider {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private final DataGenerator generator;
	
	public TardisBlockModelGen(DataGenerator generator) {
		this.generator = generator;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		final Path path = this.generator.getOutputFolder();
		
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().equals(Tardis.MODID)) {
				IDataProvider.save(GSON, cache, this.createCubeBlockModel(block), getPath(path, block));
			}
		}
		
	}
	
	public JsonObject createCubeBlockModel(Block block) {
		
		ResourceLocation key = block.getRegistryName();
		
		JsonObject object = new JsonObject();
		
		object.add("parent", new JsonPrimitive("block/cube_all"));
		
		JsonObject textureAll = new JsonObject();
		textureAll.add("all", new JsonPrimitive(key.getNamespace() + ":blocks/" + key.getPath()));
		
		object.add("textures", textureAll);
		
		return object;
	}
	
	public static Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/models/block/" + key.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "TARDIS Block Model Generator";
	}

}
