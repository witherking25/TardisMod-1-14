package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AttunableRecipe;
import net.tardis.mod.recipe.AttunableRecipe.RecipeResult;
import net.tardis.mod.recipe.TardisRecipeSerialisers;

public class AttunableRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public AttunableRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        
//        createAttuneableRecipeWithOutTag(path, cache, 18000, Ingredient.fromItems(TItems.KEY.get()), TItems.KEY.get());
//        createAttuneableRecipeWithOutTag(path, cache, 18000, Ingredient.fromItems(TItems.KEY_01.get()), TItems.KEY_01.get());
//        createAttuneableRecipeWithOutTag(path, cache, 18000, Ingredient.fromItems(TItems.KEY_PIRATE.get()), TItems.KEY_PIRATE.get());
//        createAttuneableRecipeWithTag(path, cache, 3600, Ingredient.fromItems(TItems.STATTENHEIM_REMOTE.get()), TItems.STATTENHEIM_REMOTE.get());
//        createAttuneableRecipeWithTag(path, cache, 3600, Ingredient.fromItems(TItems.SONIC.get()), TItems.SONIC.get());
//        createAttuneableRecipeWithTag(path, cache, 3600, Ingredient.fromItems(TItems.DIAGNOSTIC_TOOL.get()), TItems.DIAGNOSTIC_TOOL.get());
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.BEEF), Items.COOKED_BEEF);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.MUTTON), Items.COOKED_MUTTON);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.CHICKEN), Items.COOKED_CHICKEN);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.PORKCHOP), Items.COOKED_PORKCHOP);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.COD), Items.COOKED_COD);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.SALMON), Items.COOKED_SALMON);
        createAttuneableRecipeWithOutTag(path, cache, 120, Ingredient.fromItems(Items.RABBIT), Items.COOKED_RABBIT);
    }

    @Override
    public String getName() {
        return "TARDIS Attuneable Recipe Generator";
    }
    
    public void createAttuneableRecipeWithOutTag(Path path, DirectoryCache cache, int attunementTicks, Ingredient ingredient, Item output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(attunementTicks, ingredient, output), getPath(path, output));
    }
    
    public void createAttuneableRecipeWithTag(Path path, DirectoryCache cache, int attunementTicks, Ingredient ingredient, Item output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithNBTTags(true, attunementTicks, ingredient, output), getPath(path, output));
    }
    
    public static Path getPath(Path path, Item output) {
        ResourceLocation key = output.getRegistryName();
        return path.resolve("data/" + key.getNamespace() + "/recipes/attunable/" + key.getPath() + ".json");
    }

    /**
     * Create a json recipe that allows the result item to receive NBT Tags
     * @param attunementTicks
     * @param ingredient
     * @param output
     * @return
     */
    public JsonObject createRecipeWithNBTTags(boolean addNBTTags, int attunementTicks, Ingredient ingredient, Item output) {
    	AttunableRecipe recipe = new AttunableRecipe(addNBTTags, attunementTicks, ingredient, new RecipeResult(output));
        return recipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
        .ifLeft(element -> {
        	JsonObject json = element.getAsJsonObject();
        	json.addProperty("type", TardisRecipeSerialisers.ATTUNEABLE_TYPE_LOC.toString());
        }).ifRight(right -> {
        	Tardis.LOGGER.error(right.message());
        }).orThrow().getAsJsonObject();
    }
    
    public JsonObject createRecipe(int attunementTicks, Ingredient ingredient, Item output) {
    	AttunableRecipe recipe = new AttunableRecipe(attunementTicks, ingredient, new RecipeResult(output));
        return recipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
        .ifLeft(element -> {
        	JsonObject json = element.getAsJsonObject();
        	json.addProperty("type", TardisRecipeSerialisers.ATTUNEABLE_TYPE_LOC.toString());
        }).ifRight(right -> {
        	Tardis.LOGGER.error(right.message());
        }).orThrow().getAsJsonObject();
    }
}
