package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.ComposterBlock;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.util.IItemProvider;
import net.tardis.mod.Tardis;

public class TardisPlantTag implements IDataProvider {

	DataGenerator generator;
	
	public TardisPlantTag(DataGenerator gen){
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		List<String> items = Lists.newArrayList();
		
		ComposterBlock.CHANCES.forEach((IItemProvider item, Float unused) -> {
			items.add(item.asItem().getRegistryName().toString());
		});
		
		IDataProvider.save(DataGen.GSON, cache, this.serialize(items), getPath(this.generator.getOutputFolder(), "plants"));
		
	}
	
	public static Path getPath(Path base, String path) {
		return base.resolve("data/" + Tardis.MODID + "/tags/" + path + ".json");
	}
	
	public JsonElement serialize(List<String> items) {
		JsonObject root = new JsonObject();
		
		root.add("replace", new JsonPrimitive(false));
		
		JsonArray itemBlock = new JsonArray();
		for(String s : items) {
			itemBlock.add(s);
		}
		root.add("values", itemBlock);
		
		return root;
	}

	@Override
	public String getName() {
		return "TARDIS Plant Tag";
	}

}
