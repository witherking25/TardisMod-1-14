package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TardisBlockItemModelGen implements IDataProvider {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private final DataGenerator generator;
	
	public TardisBlockItemModelGen(DataGenerator generator) {
		this.generator = generator;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		final Path path = this.generator.getOutputFolder();
		
		for(Item item : ForgeRegistries.ITEMS) {
			if(item.getRegistryName().getNamespace().equals(Tardis.MODID) && item instanceof BlockItem) {
				IDataProvider.save(GSON, cache, this.createItemModel(item), getPath(path, item));
			}
		}
		
	}
	
	public JsonObject createItemModel(Item item) {
		
		ResourceLocation key = item.getRegistryName();
		
		JsonObject object = new JsonObject();
		
		object.add("parent", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));
		
		return object;
	}
	
	public static Path getPath(Path path, Item item) {
		ResourceLocation key = item.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "TARDIS Block Item Generator";
	}

}
