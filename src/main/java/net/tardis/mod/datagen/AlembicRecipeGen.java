package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mojang.serialization.JsonOps;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IngredientCodec;
import net.tardis.mod.recipe.AlembicRecipe;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.recipe.AlembicRecipe.ResultType;
import net.tardis.mod.tags.TardisItemTags;
/**
 * Generate recipes and special recipes for Alembic
 */
public class AlembicRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public AlembicRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        createAlembicRecipeWithTag(path, cache, 1000, Ingredient.fromTag(TardisItemTags.PLANTS), 32, new ItemStack(TItems.CIRCUIT_PASTE.get(), 1));
        createAlembicSpecialRecipe(path, cache, "mercury", 1000, Ingredient.fromTag(TardisItemTags.CINNABAR), 1, ResultType.MERCURY, 1);
    }

    @Override
    public String getName() {
        return "TARDIS Alembic Recipe Generator";
    }
    
    public static Path getPath(Path path, Item output) {
        ResourceLocation key = output.getRegistryName();
        return path.resolve("data/" + key.getNamespace() + "/recipes/alembic/" + key.getPath() + ".json");
    }
    
    public static Path getPathSpecial(Path path, String modID, String fileName) {
        return path.resolve("data/" + modID + "/recipes/alembic/" + fileName + ".json");
    }

    /**
     * Create an Alembic recipe using items. Make sure it is a unique recipe
     * @param path
     * @param cache
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @throws IOException 
     */
    public void createAlembicRecipeWithItem(Path path, DirectoryCache cache, int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithItem(requiredWater, ingredient, requiredIngredientCount, output), getPath(path, output.getItem()));
    }
    
    /**
     * Create an Alembic recipe using items. Make sure it is a unique recipe
     * @param path
     * @param cache
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @throws IOException 
     */
    public void createAlembicRecipeWithTag(Path path, DirectoryCache cache, int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithTag(requiredWater, ingredient, requiredIngredientCount, output), getPath(path, output.getItem()));
    }
    
    /**
     * 
     * @param base
     * @param cache
     * @param fileName - name of the file
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param resultType - the type of output result. Currently {@link AlembicRecipe.ResultType} only has ITEM and MERCURY. <br> We only use this one for MERCURY
     * @param resultCount - the amount of merucry/other object we want to output
     * @throws IOException
     */
    public void createAlembicSpecialRecipe(Path base, DirectoryCache cache, String fileName, int requiredWater, Ingredient ingredient, int requiredIngredientCount, AlembicRecipe.ResultType resultType, int resultCount) throws IOException {
        IDataProvider.save(GSON, cache, this.createSpecialRecipe(requiredWater, ingredient, requiredIngredientCount, resultType, resultCount), getPathSpecial(base, Tardis.MODID, fileName));
    }
    
    /**
     * Creates an Alembic Recipe that outputs an item
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @return {@link JsonObject}
     */
    public JsonObject createRecipeWithItem(int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) {
        JsonObject root = new JsonObject();

        root.addProperty("type", TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE.toString());
        root.addProperty("water", requiredWater);
        IngredientCodec.INGREDIENT_TO_JSON_CODEC.encodeStart(JsonOps.INSTANCE, ingredient).get()
		.ifLeft(left -> root.add("ingredient", left));
        root.get("ingredient").getAsJsonObject().add("count", new JsonPrimitive(requiredIngredientCount));
        JsonObject result = new JsonObject();
        result.addProperty("item", output.getItem().getRegistryName().toString());
        result.addProperty("count", output.getCount());
        root.add("result", result);
        return root;
    }
    
    /**
     * Creates an Alembic Recipe that uses tags instead of items
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. Maximum of one tag. Use {@linkplain Ingredient#fromTag} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param output - the output ItemStack. Make sure you define the count of the itemstack too
     * @return {@link JsonObject}
     */
    public JsonObject createRecipeWithTag(int requiredWater, Ingredient ingredient, int requiredIngredientCount, ItemStack output) {
        JsonObject root = new JsonObject();

        root.addProperty("type", TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE.toString());
        root.addProperty("water", requiredWater);
        IngredientCodec.INGREDIENT_TO_JSON_CODEC.encodeStart(JsonOps.INSTANCE, ingredient).get()
		.ifLeft(left -> root.add("ingredient", left));
        root.get("ingredient").getAsJsonObject().add("count", new JsonPrimitive(requiredIngredientCount));
        JsonObject result = new JsonObject();
        result.addProperty("item", output.getItem().getRegistryName().toString());
        result.addProperty("count", output.getCount());
        root.add("result", result);
        return root;
    }
    
    /**
     * Creates a special non item recipe
     * @param requiredWater - amount of water required
     * @param ingredient - the ingredient used in the recipe which is being burnt by the fuel. <br> Maximum of 1 itemstack in the ingredient. <br> Use {@linkplain Ingredient#fromStacks} for this
     * @param requiredIngredientCount - amount of ingredient required
     * @param resultType - the type of output result. Currently {@link AlembicRecipe.ResultType} only has ITEM and MERCURY. <br> We only use this one for MERCURY
     * @param resultCount - the amount of merucry/other object we want to output
     * @return {@link JsonObject}
     */
    public JsonObject createSpecialRecipe(int requiredWater, Ingredient ingredient, int requiredIngredientCount, AlembicRecipe.ResultType resultType, int resultCount) {
    	JsonObject root = new JsonObject();

        root.addProperty("type", TardisRecipeSerialisers.ALEMBIC_RECIPE_TYPE.toString());
        root.addProperty("water", requiredWater);
        IngredientCodec.INGREDIENT_TO_JSON_CODEC.encodeStart(JsonOps.INSTANCE, ingredient).get()
        .ifLeft(left -> root.add("ingredient", left));
        root.get("ingredient").getAsJsonObject().add("count", new JsonPrimitive(requiredIngredientCount));
        JsonObject result = new JsonObject();
        result.addProperty("special", resultType.name().toLowerCase());
        result.addProperty("count", resultCount);
        root.add("result", result);
        return root;
    }

}
