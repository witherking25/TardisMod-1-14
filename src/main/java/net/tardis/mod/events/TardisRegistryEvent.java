package net.tardis.mod.events;

import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.registries.Registry;
/** Custom Event used to allow other mods to add their registries to the Tardis Registries. This was done before custom Forge registries were implemented
 * <p> Since TardisRegisties is deprecated, this is no longer in use*/
@Deprecated
public class TardisRegistryEvent<T> extends Event{

	private Registry<T> registry;
	
	public TardisRegistryEvent(Registry<T> registry) {
		this.registry = registry;
	}
	
	public Registry<T> getRegistry(){
		return this.registry;
	}
}
