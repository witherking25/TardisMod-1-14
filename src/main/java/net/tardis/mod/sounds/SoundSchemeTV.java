package net.tardis.mod.sounds;

public class SoundSchemeTV extends SoundSchemeBase{

	public SoundSchemeTV() {
		super(() -> TSounds.TAKEOFF_TV.get(), () -> TSounds.LAND_TV.get(), () -> TSounds.TARDIS_FLY_LOOP.get());
	}

	@Override
	public int getLandTime() {
		return 240;
	}

	@Override
	public int getTakeoffTime() {
		return 240;
	}
	
}
