package net.tardis.mod.items;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.Level;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonWriter;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.JsonOps;

import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.storage.FolderName;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.Disguise.DisguiseBlockStateEntry;
import net.tardis.mod.properties.Prop;

public class PlasmicShellItem extends Item {

	public PlasmicShellItem() {
        super(Prop.Items.ONE.get().group(null));
    }

    public static AxisAlignedBB getBox(BlockPos pos, BlockPos pos1) {
        int minX, maxX;
        int minY, maxY;
        int minZ, maxZ;

        if (pos.getX() > pos1.getX()) {
            minX = pos1.getX();
            maxX = pos.getX();
        } else {
            minX = pos.getX();
            maxX = pos1.getX();
        }

        if (pos.getY() > pos1.getY()) {
            minY = pos1.getY();
            maxY = pos.getY();
        } else {
            minY = pos.getY();
            maxY = pos1.getY();
        }

        if (pos.getZ() > pos1.getZ()) {
            minZ = pos1.getZ();
            maxZ = pos.getZ();
        } else {
            minZ = pos.getZ();
            maxZ = pos1.getZ();
        }


        return new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
    }

    @SuppressWarnings("deprecation")
    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        ItemStack stack = context.getItem();

        CompoundNBT tag = stack.getOrCreateTag();

        if (tag.contains("pos1") && tag.contains("pos2") && context.getWorld().getBlockState(context.getPos()) == TBlocks.atrium_block.get().getDefaultState()) {
            BlockPos pos1 = BlockPos.fromLong(tag.getLong("pos1"));
            BlockPos pos2 = BlockPos.fromLong(tag.getLong("pos2"));

            List<DisguiseBlockStateEntry> entries = new ArrayList<>();
            for (BlockPos pos : BlockPos.getAllInBoxMutable(pos1, pos2)) {
                BlockState state = context.getWorld().getBlockState(pos);
                if (!state.isAir() && !pos.equals(context.getPos()) && !pos.equals(context.getPos().down())) { //Save everything except the position we ray trace, and the position below our ray trace
                    int x = pos.getX() - context.getPos().getX();
                    int y = pos.getY() - context.getPos().getY();
                    int z = pos.getZ() - context.getPos().getZ();
                	BlockPos offsetPos = new BlockPos(x, y, z);
                    entries.add(new DisguiseBlockStateEntry(state, offsetPos.toImmutable()));
                }
            }

            //Write File
            DataResult<String> dataresult;
           Path path = new File("plasmic_"  + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".json").toPath();
           try {
               Gson gson = new GsonBuilder().setPrettyPrinting().create();
               DataResult<JsonElement> result = DisguiseBlockStateEntry.CODEC_LIST.encodeStart(JsonOps.INSTANCE, entries);
               dataresult = result.flatMap((element) -> {
               try (JsonWriter jsonwriter = gson.newJsonWriter(Files.newBufferedWriter(path, StandardCharsets.UTF_8))) {
                   gson.toJson(element, jsonwriter);
               } catch (JsonIOException | IOException ioexception) {
                   return DataResult.error("Error writing file: " + ioexception.getMessage());
               }
               return DataResult.success(path.toString());
             });
             if (!context.getWorld().isRemote) {
                context.getPlayer().sendMessage(new StringTextComponent("Saved as " +  path.getFileName() + " in the minecraft instance!"), Util.DUMMY_UUID);
                context.getItem().getOrCreateTag().putBoolean("complete", true);
            }
          } 
          catch (Exception e) {
             dataresult = DataResult.error("Failed to parse disguise json");
          }

            return ActionResultType.SUCCESS;
        }

        if (!tag.contains("pos1")) {
            tag.putLong("pos1", context.getPos().up().toLong());
            return ActionResultType.SUCCESS;
        }

        return super.onItemUse(context);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack stack = playerIn.getHeldItem(handIn);
        if (stack.getOrCreateTag().contains("pos1")) {
            stack.getOrCreateTag().putLong("pos2", playerIn.getPosition().toLong());
        }

        if (playerIn.isSneaking())
            stack.setTag(new CompoundNBT());


        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        if (Screen.hasShiftDown()) {
            tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use1"));
            tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use2"));
            tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use3"));
        }
        else {
        	tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
        }
       
    }

}
