package net.tardis.mod.items;

import java.util.List;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Part.PartType;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;

/**
 * Base item for Subsystems and Upgrades
 * Created by Swirtzly on 22/08/2019 @ 20:15
 * Modified by 50ap5ud5 on 12/08/2020 @ 12:51
 */
public class TardisPartItem extends Item {

    private PartType type;
    private boolean requiredForFlight;
    private boolean requiresRepair;
    private TranslationTextComponent dependentItem;

    public TardisPartItem(PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem) {
        super(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE).maxDamage(250));
        this.type = type;
        this.requiredForFlight = requiredForFlight;
        this.requiresRepair = requiresRepair;
        this.dependentItem = dependentItem;
    }

    public TardisPartItem(PartType type, boolean requiredForFlight, boolean requiresRepair) {
        super(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE).maxDamage(250));
        this.type = type;
        this.requiredForFlight = requiredForFlight;
        this.requiresRepair = requiresRepair;
        this.dependentItem = new TranslationTextComponent("");
    }

    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair, TranslationTextComponent dependentItem) {
        super(prop);
        this.type = type;
        this.requiredForFlight = requiredForFlight;
        this.requiresRepair = requiresRepair;
        this.dependentItem = dependentItem;
    }

    public TardisPartItem(Item.Properties prop, PartType type, boolean requiredForFlight, boolean requiresRepair) {
        super(prop);
        this.type = type;
        this.requiredForFlight = requiredForFlight;
        this.requiresRepair = requiresRepair;
        this.dependentItem = new TranslationTextComponent("");
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
        tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
        if (Screen.hasShiftDown()) {
            tooltip.clear();
            tooltip.add(0, this.getDisplayName(stack));
            String typeName = this.type.toString().toLowerCase();
            String typeFriendlyName = typeName.substring(0, 1).toUpperCase() + typeName.substring(1);
            StringTextComponent panelName = new StringTextComponent(this.type == PartType.SUBSYSTEM ? "Components" : "Upgrades");
            
            tooltip.add(new TranslationTextComponent("tooltip.part.type").appendSibling(new StringTextComponent(typeFriendlyName).mergeStyle(TextFormatting.LIGHT_PURPLE)));

            if (type != PartType.UPGRADE)
                tooltip.add(new TranslationTextComponent("tooltip.part.flight.required").appendSibling(new StringTextComponent(String.valueOf(this.requiredForFlight)).mergeStyle(TextFormatting.LIGHT_PURPLE)));

            tooltip.add(new TranslationTextComponent("tooltip.part.repair.required").appendSibling(new StringTextComponent(String.valueOf(this.requiresRepair)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
            if (this.type == PartType.UPGRADE && !this.dependentItem.getKey().isEmpty()) {
                tooltip.add(new TranslationTextComponent("tooltip.part.dependency").appendSibling(dependentItem != null ? dependentItem.mergeStyle(TextFormatting.LIGHT_PURPLE) : new StringTextComponent("None").mergeStyle(TextFormatting.LIGHT_PURPLE)));
            }
            tooltip.add(new TranslationTextComponent("tooltip.part.engine_panel").appendSibling(panelName.mergeStyle(this.type == PartType.SUBSYSTEM ? TextFormatting.BLUE : TextFormatting.GREEN)));
            tooltip.add(Constants.Translations.TOOLTIP_CONTROL);
            if (Screen.hasControlDown()) {
                tooltip.clear();
                tooltip.add(0, this.getDisplayName(stack));
                tooltip.add(new TranslationTextComponent("tooltip.part." + this.getRegistryName().getPath() + ".description"));
            }
        }
    }
}
