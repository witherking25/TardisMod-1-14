package net.tardis.mod.items;

import java.util.Map;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.api.space.items.IGravArmor;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;

public class SpaceSuitItem extends ArmorItem implements ISpaceHelmet, IGravArmor{

	public static final SpaceMaterial spaceMaterial = new SpaceMaterial();
	
	public SpaceSuitItem(EquipmentSlotType slot) {
		super(spaceMaterial, slot, Prop.Items.ONE.get().group(TItemGroups.MAIN));
	}

	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type) {
		return slot == EquipmentSlotType.LEGS ? Tardis.MODID + ":textures/armor/spacesuit_legs.png" : Tardis.MODID + ":textures/armor/spacesuit.png";
	}


	@OnlyIn(Dist.CLIENT)
	@Override
	public <A extends BipedModel<?>> A getArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlotType armorSlot, A _default) {
		Map<Item, BipedModel<?>> map = ClientHelper.getSpacesuitArmorModel(itemStack);
		BipedModel<?> model = map.get(itemStack.getItem());
		return (A) model;
	}


	public static class SpaceMaterial implements IArmorMaterial{

		@Override
		public int getDamageReductionAmount(EquipmentSlotType arg0) {
			return 0;
		}

		@Override
		public int getDurability(EquipmentSlotType arg0) {
			return 0;
		}

		@Override
		public int getEnchantability() {
			return 0;
		}

		@Override
		public String getName() {
			return Tardis.MODID + ":space_material";
		}

		@Override
		public Ingredient getRepairMaterial() {
			return Ingredient.EMPTY;
		}

		@Override
		public SoundEvent getSoundEvent() {
			return SoundEvents.ITEM_ARMOR_EQUIP_GENERIC;
		}

		@Override
		public float getToughness() {
			return 2;
		}

		@Override
		public float getKnockbackResistance() {
			return 0.3F;
		}
		
	}

	@Override
	public boolean shouldSufficate(LivingEntity ent) {
		for(ItemStack stack : ent.getArmorInventoryList()) {
			if(!(stack.getItem() instanceof SpaceSuitItem))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean useNormalGrav(LivingEntity entity, ItemStack stack) {
		BlockPos pos = entity.getPosition();
		for(int i = 0; i < 16; ++i) {
			if(entity.world.getBlockState(pos.down(i)).isSolid())
				return true;
		}
		return false;
	}
	
}
