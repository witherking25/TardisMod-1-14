package net.tardis.mod.items;

import net.minecraft.block.DispenserBlock;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.compat.vanilla.DalekDispenseBehaviour;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.itemgroups.TItemGroups;


public class DalekSpawnItem extends Item {

    ResourceLocation dalekType = null;

    public DalekSpawnItem() {
        super(new Item.Properties().group(TItemGroups.MAIN));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        if (context.getFace() == Direction.UP) {
            if (!context.getWorld().isRemote) {
//                DalekEntity e = TEntities.DALEK.get().create(context.getWorld());

                double x = 0, z = 0;

                if (context.getPlayer().isSneaking()) {
                    double angle = Math.toRadians(WorldHelper.getAngleFromFacing(context.getPlacementHorizontalFacing().rotateY()));
                    x = Math.sin(angle) * 0.5;
                    z = -Math.cos(angle) * 0.5;
                }
//                e.setDalekType(dalekType);
//                e.setPosition(context.getPos().getX() + x + 0.5, context.getHitVec().y, context.getPos().getZ() + z + 0.5);
//                e.rotationYaw = WorldHelper.getAngleFromFacing(context.getPlayer().getHorizontalFacing().getOpposite());
//                context.getWorld().addEntity(e);
                context.getItem().shrink(1);
            }
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.PASS;
    }

    public void setDalekType(ResourceLocation type) {
        this.dalekType = type;
        DispenserBlock.registerDispenseBehavior(this, new DalekDispenseBehaviour(type));
    }

}
