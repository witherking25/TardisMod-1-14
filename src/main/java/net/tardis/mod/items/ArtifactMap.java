package net.tardis.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration.Type;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.world.WorldGen;

public class ArtifactMap extends Item {

    public ArtifactMap(Properties properties) {
        super(properties);
    }


    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (!worldIn.isRemote) {
            BlockPos exterior = WorldGen.getClosestBrokenExterior((int) playerIn.getPosX(), (int) playerIn.getPosZ());
            if(exterior != null && exterior != BlockPos.ZERO) {
                ItemStack map = FilledMapItem.setupNewMap(worldIn, exterior.getX(), exterior.getZ(), (byte) 1, true, true);
                MapData.addTargetDecoration(map, exterior, "Artifact", Type.RED_X);
                FilledMapItem.func_226642_a_((ServerWorld) worldIn, map);
                map.setDisplayName(new StringTextComponent("Ancient Artifact Map"));
                playerIn.getHeldItem(handIn).shrink(1);
                TInventoryHelper.giveStackTo(playerIn, map);

            }
            //WorldGen.generateStation((ServerWorld)worldIn);
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

}
