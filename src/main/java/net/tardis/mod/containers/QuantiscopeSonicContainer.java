package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.QuantiscopeTile;

public class QuantiscopeSonicContainer extends Container{
	
	//Needed for customization
	public BlockPos pos;
	private QuantiscopeTile tile;
	/** Client Only constructor */
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, PacketBuffer extraData) {
		super(TContainers.QUANTISCOPE.get(), id);
		pos = extraData.readBlockPos();
		TileEntity te = inv.player.world.getTileEntity(pos);
		if(te instanceof QuantiscopeTile) {
			init((QuantiscopeTile)te, inv);
		}
	}
	/** Server Only constructor */
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, QuantiscopeTile workbench) {
		super(TContainers.QUANTISCOPE.get(), id);
		init(workbench, inv);
	}
	
	public void init(QuantiscopeTile workbench, PlayerInventory inv) {
		this.tile = workbench;
		
		this.addSlot(new SonicSlot(workbench, 0, 42, 47));
        
		//Player Inventory
		for(int row = 0; row < 3; ++row) {
	         for(int slot = 0; slot < 9; ++slot) {
	            this.addSlot(new Slot(inv, slot + row * 9 + 9, 8 + slot * 18, 81 + row * 18 + 3));
	         }
       }

	   for(int i1 = 0; i1 < 9; ++i1) {
	       this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 139 + 3));
	   }
	}
	
	public QuantiscopeTile getQuantiscope() {
		return this.tile;
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}


	@Override
	public ItemStack transferStackInSlot(PlayerEntity player, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		final Slot slot = inventorySlots.get(index);
		if ((slot != null) && slot.getHasStack()) {
			final ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			final int containerSlots = inventorySlots.size() - player.inventory.mainInventory.size();
			if (index < containerSlots) {
				if (!mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!mergeItemStack(itemstack1, 0, containerSlots, false)) {
				return ItemStack.EMPTY;
			}
			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}
			slot.onTake(player, itemstack1);
		}
		return itemstack;
	}
	
	public void setSlotChangeAction(Runnable run) {
		if(this.getSlot(0) instanceof SonicSlot)
			((SonicSlot)this.getSlot(0)).setOnSlotChanged(run);
	}
	
	public static class SonicSlot extends SlotItemHandler{

		private Runnable onSlotChanged;
		
		public SonicSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
			super(itemHandler, index, xPosition, yPosition);
		}

		@Override
		public boolean isItemValid(ItemStack stack) {
			return stack.getItem() == TItems.SONIC.get();
		}

		@Override
		public void onSlotChanged() {
			super.onSlotChanged();
			if(onSlotChanged != null)
				onSlotChanged.run();
		}
		
		public void setOnSlotChanged(Runnable run) {
			this.onSlotChanged = run;
		}
		
	}
	
}
