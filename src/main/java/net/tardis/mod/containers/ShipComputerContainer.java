package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.ShipComputerTile;

public class ShipComputerContainer extends BaseContainer{

	public ShipComputerContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	/** Client Only constructor */
	public ShipComputerContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		this(TContainers.SHIP_COMPUTER.get(), id);
		init(inv, (ShipComputerTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	
	/** Server Only constructor */
	public ShipComputerContainer(int id, PlayerInventory player, ShipComputerTile tile) {
		this(TContainers.SHIP_COMPUTER.get(), id);
		init(player, tile);
	}
	
	public void init(PlayerInventory player, ShipComputerTile inv) {
		
		inv.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
			for(int i = 0; i < cap.getSlots(); ++i) {
				this.addSlot(new SlotItemHandler(cap, i, 8 + (i % 9) * 18, 18 + (i / 9) * 18));
			}
		});
		
		TInventoryHelper.addPlayerInvContainer(this, player, 0, 0);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		final Slot slot = inventorySlots.get(index);
		if ((slot != null) && slot.getHasStack()) {
			final ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			final int containerSlots = inventorySlots.size() - playerIn.inventory.mainInventory.size();
			if (index < containerSlots) {
				if (!mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!mergeItemStack(itemstack1, 0, containerSlots, false)) {
				return ItemStack.EMPTY;
			}
			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}
			slot.onTake(playerIn, itemstack1);
		}
		return itemstack;
	}

}
