package net.tardis.mod.world.dimensions;

import com.mojang.serialization.Codec;
import net.minecraft.block.BlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryLookupCodec;
import net.minecraft.world.Blockreader;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.tardis.mod.world.biomes.TBiomes;

public class SpaceChunkGenerator extends ChunkGenerator {
	public static final Codec<SpaceChunkGenerator> providerCodec = 
	    	RegistryLookupCodec.getLookUpCodec(Registry.BIOME_KEY)
			.xmap(SpaceChunkGenerator::new, SpaceChunkGenerator::getBiomeRegistry)
			.codec();
	
    private final Registry<Biome> biomeRegistry;

    
    public SpaceChunkGenerator(MinecraftServer server)
	{
		this(server.getDynamicRegistries() // get dynamic registry
			.getRegistry(Registry.BIOME_KEY));
	}

    public SpaceChunkGenerator(Registry<Biome> biomes){
		super(new SingleBiomeProvider(biomes.getOrThrow(TBiomes.SPACE_BIOME_KEY)), new DimensionStructuresSettings(false));
		this.biomeRegistry = biomes;
 	}

    @Override
    public int getGroundHeight() {
        return 128;
    }

    @Override
    protected Codec<? extends ChunkGenerator> func_230347_a_() {
        return providerCodec;
    }
    
    //getBaseColumn
    @Override
    public IBlockReader func_230348_a_(int x, int z) {
    	// 0 - flat chunk generator returns a reader over its blockstate list
    	// 1 - debug chunk generator returns a reader over an empty array
    	// 2 - normal chunk generator returns a column whose contents are either default block, default fluid, or air
        BlockState[] ablockstate = new BlockState[0];
        return new Blockreader(ablockstate);
    }

    @Override
    public ChunkGenerator func_230349_a_(long seedIn) {
        return this;
    }

    /**
     * Generates Structures
     */
    @Override
    public void func_230352_b_(IWorld arg0, StructureManager arg1, IChunk arg2) {
        
    }

    @Override
    public void generateSurface(WorldGenRegion arg0, IChunk arg1) {

    }

    @Override
    public int getHeight(int arg0, int arg1, Type arg2) {
        return 0;
    }
    
    private final Registry<Biome> getBiomeRegistry(){
    	return this.biomeRegistry;
    }

}
