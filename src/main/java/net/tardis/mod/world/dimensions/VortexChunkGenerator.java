package net.tardis.mod.world.dimensions;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryLookupCodec;
import net.minecraft.world.Blockreader;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeManager;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.DimensionSettings;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.world.biomes.TBiomes;

public class VortexChunkGenerator extends ChunkGenerator {
    
	/**
	 * Put chunk in centre of region file. Reduces save file by 4x. If we put in a corner, 3 adjacent regions get loaded
	 */
	public static final ChunkPos CHUNKPOS = new ChunkPos(16,16);
	public static final long CHUNKID = CHUNKPOS.asLong();
	public static final BlockPos CORNER = CHUNKPOS.asBlockPos();
	public static final BlockPos CENTER = CORNER.add(7, 7, 7);
    
    
    /**
     * We must use RegistryLookupCodec#getLookUpCodec because with SingleBiomeProviders,
     * <br> this prevents intense log spam which occurs when the server's DynamicRegistry list of biomes in the ChunkGenerator do not match with the 
     * list present during Mod loading (that resides in WorldGenRegestries)
     * <br> RegistryLookupCodec ensures we are getting the biomes from when the server has already loaded up,
     * which is needed since we want to use this ChunkGenerator for dynamically created dimensions
     */
    public static final Codec<VortexChunkGenerator> providerCodec = 
            RegistryLookupCodec.getLookUpCodec(Registry.BIOME_KEY)
            .xmap(VortexChunkGenerator::new,VortexChunkGenerator::getBiomeRegistry)
            .codec();
        
	private final Registry<Biome> biomeRegistry;
	
	// create chunk generator at runtime when dynamic dimension is created
	public VortexChunkGenerator(MinecraftServer server)
	{
	    this(server.getDynamicRegistries() // get dynamic registry
	         .getRegistry(Registry.BIOME_KEY));
	}
	 
	// create chunk generator when dimension is loaded from the dimension registry on server init
	public VortexChunkGenerator(Registry<Biome> biomes)
	{
	    super(new SingleBiomeProvider(biomes.getOrThrow(TBiomes.VORTEX_BIOME_KEY)), new DimensionStructuresSettings(false));
	    this.biomeRegistry = biomes;
	}


    @Override
    public int getGroundHeight() {
        return 128;
    }

    @Override
    protected Codec<? extends ChunkGenerator> func_230347_a_() {
        return providerCodec;
    }

    //getBaseColumn
    @Override
    public IBlockReader func_230348_a_(int x, int z) {
        // 0 - flat chunk generator returns a reader over its blockstate list
        // 1 - debug chunk generator returns a reader over an empty array
        // 2 - normal chunk generator returns a column whose contents are either default block, default fluid, or air
        BlockState[] ablockstate = new BlockState[0];
        return new Blockreader(ablockstate);
    }

    @Override
    public ChunkGenerator func_230349_a_(long seedIn) {
        return this;
    }
    
    public Registry<Biome> getBiomeRegistry(){
        return this.biomeRegistry;
    }

    /**
     * Generates Structures
     */
    @Override
    public void func_230352_b_(IWorld world, StructureManager structures, IChunk chunk) {

    }

    @Override
    public void generateSurface(WorldGenRegion region, IChunk chunk) {

    }

    @Override
    public int getHeight(int x, int z, Type heightmapType) {
        return 0;
    }
    
//    public Registry<Biome> getBiomeRegistry(){
//        return this.biomeRegistry;
//    }

	@Override
	public int getMaxBuildHeight(){
		return 256;
	}
	
	//Make sure this chunk generator doesn't generate any structures features etc.
	
	
	
	// apply carvers
	@Override
	public void func_230350_a_(long seed, BiomeManager biomes, IChunk chunk, GenerationStage.Carving carvingStage){
		// nope
	}

	//Spawn mobs
	@Override
	public void func_230354_a_(WorldGenRegion p_230354_1_) {
		
	}

	// get structure position
	@Nullable
	@Override
	public BlockPos func_235956_a_(ServerWorld world, Structure<?> structure, BlockPos start, int radius, boolean skipExistingChunks){
		return null;
	}
	
	// decorate biomes with features
	@Override
	public void func_230351_a_(WorldGenRegion world, StructureManager structures){
	}
	
	// has stronghold
	@Override
	public boolean func_235952_a_(ChunkPos chunkPos){
		return false;
	}
	
	// create structures
	@Override
	public void func_242707_a(DynamicRegistries registries, StructureManager structures, IChunk chunk, TemplateManager templates, long seed){
	}
	
	// create structure references
	@Override
	public void func_235953_a_(ISeedReader world, StructureManager structures, IChunk chunk){
	}
}