package net.tardis.mod.world.structures;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.mojang.serialization.Codec;

import net.minecraft.entity.EntityType;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.registry.DynamicRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.MobSpawnInfo;
import net.minecraft.world.biome.MobSpawnInfo.Spawners;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureStart;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.world.WorldGen;

/**
 * Structure version of the CrashedShip as originally intended
 * @author 50ap5ud5
 *
 */
public class CrashedStructure extends Structure<ProbabilityConfig>{

    public CrashedStructure(Codec<ProbabilityConfig> codec) {
        super(codec);
    }
    
    //Required, sets the Structure Start settings
    @Override
    public IStartFactory<ProbabilityConfig> getStartFactory() {
        return CrashedStructure.Start::new;
    }
    
    @Override
    public List<MobSpawnInfo.Spawners> getDefaultSpawnList() {
        return STRUCTURE_MONSTERS;
    }

    private static final List<MobSpawnInfo.Spawners> STRUCTURE_CREATURES = ImmutableList.of(
            new MobSpawnInfo.Spawners(EntityType.BAT, 100, 1, 2)
    );
    
    @Override
    public List<MobSpawnInfo.Spawners> getDefaultCreatureSpawnList() {
        return STRUCTURE_CREATURES;
    }
    
    private static final List<MobSpawnInfo.Spawners> STRUCTURE_MONSTERS = ImmutableList.of(
            new MobSpawnInfo.Spawners(EntityType.SKELETON, 100, 3, 8),
            new MobSpawnInfo.Spawners(EntityType.ZOMBIE, 40, 4, 15),
            new MobSpawnInfo.Spawners(TEntities.SECURITY_DROID.get(), 60, 3, 10)
    );
    
    @Override
	public List<Spawners> getSpawnList() {
		return STRUCTURE_MONSTERS;
	}
    
    //Required, otherwise will cause NPE Crash
    @Override
    public Decoration getDecorationStage() {
        return Decoration.SURFACE_STRUCTURES;
    }
    
    public static class Start extends StructureStart<ProbabilityConfig>  {

    	public Start(Structure<ProbabilityConfig> structureIn, int chunkX, int chunkZ, MutableBoundingBox mutableBoundingBox, int referenceIn, long seedIn) {
            super(structureIn, chunkX, chunkZ, mutableBoundingBox, referenceIn, seedIn);
        }

		@Override
		public void func_230364_a_(DynamicRegistries dynamicRegistryManager, ChunkGenerator chunkGenerator, TemplateManager templateManagerIn, int chunkX, int chunkZ, Biome biomeIn, ProbabilityConfig config) {
			Rotation rotation = Rotation.NONE;
        	int x = (chunkX << 4) + 7;
            int z = (chunkZ << 4) + 7;
            int surfaceY = chunkGenerator.getHeight(x, z, Heightmap.Type.WORLD_SURFACE_WG);
            if (rand.nextFloat() <= config.probability) {
            	BlockPos blockpos = new BlockPos(x, surfaceY, z);
                WorldGen.BROKEN_EXTERIORS.add(blockpos);
                CrashedStructurePieces.start(templateManagerIn, blockpos, rotation, this.components, this.rand);
                this.components.forEach(piece -> piece.offset(0, -20, 0));
                this.components.forEach(piece -> piece.getBoundingBox().minY += 20); //By offseting the structure down by 20 and lifting up the bounding box by the same amount, the structure will become forced into the ground
                this.recalculateStructureSize();
                Tardis.LOGGER.info("Crashed Ship at " + (blockpos.getX()) + " " + blockpos.getY() + " " + (blockpos.getZ()));
            }
		}
    	
    }

}
