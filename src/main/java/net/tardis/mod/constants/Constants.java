package net.tardis.mod.constants;

import java.text.DecimalFormat;


import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class Constants {

    public static float halfPI = 1.57079633f;
	public static DecimalFormat TEXT_FORMAT_NO_DECIMALS = new DecimalFormat("###");
	public static final int ATTUNEMENT_TIME_LESSER = 3600;
	public static final int ATTUNEMENT_TIME_GREATER = 18000;
	public static final String TARDIS_NAME_ATTUNMENT_NBT_KEY = "tardis_name";
	public static final String CONSOLE_ATTUNMENT_NBT_KEY = "attuned_console";
	public static final String TIME_LINK_NBT_KEY = "time_linked_console";
	
	public static final int PACKET_BYTE_LIMIT = 1048576;
	public static final int PACKET_STRING_LENGTH = 32767;
	
	public static final int BUBBLE_PARTICLE_MAX_AGE = (5 * 20);

	public static class Strings {
	    public static final String GUI = "gui.tardis.";
	    public static final String GUI_PROTOCOL_TITLE = "gui.tardis.protocol.";

	    public static final String GUI_VM = "gui.vm.";
	    
	    public static final String INTERIOR_KILL_TAG = "tardis_interior_change_kill";
	    
	    //Monitor Submenu names
	    public static final String EXTERIOR_PROPERTIES = "exterior_prop";
	    public static final String SECURITY_MENU = "security";
	}

    public static class Gui {
    	
		public static final int MONITOR_MAIN_STEAM = 1;
		public static final int MONITOR_ECHANGE = 2;
		
		public static final int NONE = 3;
		public static final int MONITOR_EDIT_INTERIOR = 4;
		
		public static final int VORTEX_MAIN = 5;
		public static final int VORTEX_TELE = 6;
		public static final int VORTEX_DISTRESS = 7;

		public static final int MONITOR_MAIN_EYE = 8;
		public static final int MONITOR_MAIN_GALVANIC = 9;
		
		public static final int MANUAL = 10;
		
		public static final int ARS_EGG = 11;
		
		public static final int TELEPATHIC = 12;
		
		public static final int MONITOR_MAIN_RCA = 13;
		public static final int ARS_TABLET = 14;
		public static final int COMMUNICATOR = 15;
		public static final int TRANSDUCTION_BARRIER_EDIT = 16;
		public static final int ARS_TABLET_KILL = 17;
		public static final int MONITOR_MAIN_XION = 18;
		public static final int MONITOR_MAIN_TOYOTA = 19;
		public static final int MONITOR_MAIN_CORAL = 20;
		public static final int MONITOR_MAIN_ROTATE = 21;
		public static final int DIAGNOSTIC = 22;
		public static final int MONITOR_REMOTE = 23;
		public static final int HOLO_OBSERVATORY = 24;
		public static final int TARDIS_DISTRESS = 25;
		public static final int DIALOG = 26;
		
		public static final StringTextComponent DEFAULT_GUI_TITLE = new StringTextComponent("A Tardis mod GUI");
		
	}

    public static class DalekTypes {
		public static final ResourceLocation DALEK_DEFAULT = new ResourceLocation(Tardis.MODID, "dalek_default");
		public static final ResourceLocation DALEK_RUSTY = new ResourceLocation(Tardis.MODID, "dalek_rusty");
		public static final ResourceLocation DALEK_SPEC = new ResourceLocation(Tardis.MODID, "dalek_spec");
    }

    public static class Translations{
        public static final TranslationTextComponent EXISTING_TARDIS = new TranslationTextComponent("message." + Tardis.MODID + ".existing_tardis");
        
        //components
        public static final TranslationTextComponent DEMAT_CIRCUIT = new TranslationTextComponent("item." + Tardis.MODID + ".subsystem.dematerialisation_circuit");
        public static final TranslationTextComponent TEMPORAL_GRACE = new TranslationTextComponent("item." + Tardis.MODID + ".subsystem.temporal_grace");
        public static final TranslationTextComponent SHIELD_GENERATOR = new TranslationTextComponent("item." + Tardis.MODID + ".subsystem.shield_generator");
        
        //Item use in certain dimensions
        public static final TranslationTextComponent NO_USE_OUTSIDE_TARDIS = new TranslationTextComponent("message." + Tardis.MODID + ".use.outside_tardis");
        public static final TranslationTextComponent CANT_USE_IN_TARDIS = new TranslationTextComponent("message." + Tardis.MODID + ".use.in_tardis");
        public static final TranslationTextComponent CANT_USE_IN_DIM = new TranslationTextComponent("message." + Tardis.MODID + ".use.in_dim");
        public static final String NOT_ENOUGH_ARTRON = "message." + Tardis.MODID + ".not_enough_artron";
        public static final TranslationTextComponent OUTSIDE_BORDER = new TranslationTextComponent("message.tardis.outside_border");
        public static final TranslationTextComponent CORRIDOR_CAN_SPAWN = new TranslationTextComponent("message.tardis.corridor.can_spawn");
        public static final TranslationTextComponent CORRIDOR_CAN_DELETE = new TranslationTextComponent("message.tardis.corridor.can_delete");
        public static final TranslationTextComponent CORRIDOR_BLOCKED = new TranslationTextComponent("message.tardis.corridor.blocked");
        public static final String CORRIDOR_BLOCKED_POSITIONS = "message.tardis.corridor.blocked_positions";
        
        public static final TranslationTextComponent ITEM_NOT_ATTUNED = new TranslationTextComponent("message.tardis.item_not_attuned");
        
        public static final String NO_COMPONENT = "message." + Tardis.MODID + ".no_subsystem";
        public static final TranslationTextComponent NOT_ADMIN = new TranslationTextComponent("message.tardis.not_admin");
		public static final String UNLOCKED_INTERIOR = "message.tardis.interior.unlock";
		public static final String UNLOCKED_EXTERIOR = "message.tardis.exterior.unlock";
		
		public static final String START_INTERIOR_CHANGE = "message.tardis.change_interior.started";
		public static final TranslationTextComponent CANCEL_INTERIOR_CHANGE = new TranslationTextComponent("message.tardis.change_interior.cancelled");
		public static final String INTERIOR_CHANGE_COMPLETE = "message.tardis.change_interior.complete";
		public static final String INTERIOR_CHANGE_INTERRUPTED = "message.tardis.change_interior.interrupted";
		
		public static final TranslationTextComponent TOOLTIP_REDSTONE_REQUIRED = new TranslationTextComponent("tooltip.item.redstone.required");
		
		public static final TranslationTextComponent TOOLTIP_NO_ATTUNED = new TranslationTextComponent("tooltip.item.not_attuned");
		public static final String TOOLTIP_ATTUNED_OWNER = "tooltip.item.attuned.owner";
		public static final TranslationTextComponent TOOLTIP_HOLD_SHIFT = new TranslationTextComponent("tooltip.item.info.shift");
		public static final TranslationTextComponent TOOLTIP_CONTROL = new TranslationTextComponent("tooltip.item.info.control");
		public static final TranslationTextComponent TOOLTIP_SHIFT_AND_CONTROL = new TranslationTextComponent("tooltip.item.info.shift_control");
		
		public static final TranslationTextComponent REQUIRES_SONIC = new TranslationTextComponent("message." + Tardis.MODID + ".use.requires_sonic");
		public static final TranslationTextComponent INVALID_SONIC_RESULT = new TranslationTextComponent("message.sonic.invalid_result");
		public static final String NO_TARDIS_FOUND = "message.tardis.no_tardis_found";

		public static final TranslationTextComponent GUI_NEXT = new TranslationTextComponent("gui.tardis.next");
		public static final TranslationTextComponent GUI_PREV = new TranslationTextComponent("gui.tardis.previous");
		public static final TranslationTextComponent GUI_SELECT = new TranslationTextComponent("gui.tardis.select");
		public static final TranslationTextComponent GUI_CONFIRM = new TranslationTextComponent("gui.tardis.confirm");
		public static final TranslationTextComponent GUI_SAVE = new TranslationTextComponent("gui.tardis.save");
		public static final TranslationTextComponent GUI_CANCEL = new TranslationTextComponent("gui.tardis.cancel");

		public static final TranslationTextComponent GUI_BACK = new TranslationTextComponent("gui.tardis.button.back");

		public static final TranslationTextComponent LOCATION = new TranslationTextComponent("gui.tardis.info.location");
		public static final TranslationTextComponent DIMENSION = new TranslationTextComponent("gui.tardis.info.dimension");
		public static final TranslationTextComponent FACING = new TranslationTextComponent("gui.tardis.info.facing");
		public static final TranslationTextComponent TARGET = new TranslationTextComponent("gui.tardis.info.target_location");
		public static final TranslationTextComponent TARGET_DIM = new TranslationTextComponent("gui.tardis.info.target_dimension");
		public static final TranslationTextComponent ARTRON = new TranslationTextComponent("gui.tardis.info.artron");
		public static final TranslationTextComponent JOURNEY = new TranslationTextComponent("gui.tardis.info.journey");
    }
    
    public static class Suffix{
    	public static final IFormattableTextComponent ARTRON_UNITS = new StringTextComponent(" AU").mergeStyle(TextFormatting.RESET);
    }

    public static class Message {
        public static final String NO_PERMISSION = "You do not have the permission to do that.";
        public static final String NO_TARDIS = "No TARDIS Found for this player. Is the Tardis Console TileEntity invalid?";
    }
    
    public static class Part{
    	public enum PartType{
    		SUBSYSTEM,
    		UPGRADE
    	}
    }
    
    public static class Resources{
    	/** Generic ResourceLocation to allow for all entries in a command suggestion to be used for the command */
    	public static final ResourceLocation ALL = new ResourceLocation(Tardis.MODID, "all");
    }
}
