package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ForcefieldData implements ConsoleData{

	boolean forcefield;
	
	public ForcefieldData(boolean forcefield){
		this.forcefield = forcefield;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> shield.setForceFieldActivated(this.forcefield));
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeBoolean(this.forcefield);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.forcefield = buff.readBoolean();
	}

}
