package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.network.Network;

public class RequestBOTIMessage {

	private BlockPos pos;
	
	public RequestBOTIMessage(BlockPos pos) {
		this.pos = pos;
	}
	
	public static void encode(RequestBOTIMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
	}
	
	public static RequestBOTIMessage decode(PacketBuffer buf) {
		return new RequestBOTIMessage(buf.readBlockPos());
	}
	
	public static void handle(RequestBOTIMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().world.getTileEntity(mes.pos);
			if(te instanceof IBotiEnabled) {
				WorldShell shell = ((IBotiEnabled)te).getBotiWorld();
				if(shell != null)
					Network.sendTo(new BOTIMessage(shell, mes.pos), context.get().getSender());
			}
		});
		context.get().setPacketHandled(true);
	}
}
