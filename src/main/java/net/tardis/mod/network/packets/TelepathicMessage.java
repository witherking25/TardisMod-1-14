package net.tardis.mod.network.packets;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.TelepathicUtils.SearchType;
import net.tardis.mod.tileentities.ConsoleTile;

public class TelepathicMessage {
	
    private static final TranslationTextComponent VALID = new TranslationTextComponent("message.tardis.telepathic.success");
    private String name;
    private SearchType type;
    
    public TelepathicMessage(SearchType type, String key) {
        this.name = key;
        this.type = type;
    }
    
    public static void encode(TelepathicMessage mes, PacketBuffer buf) {
        buf.writeInt(mes.type.ordinal());
        buf.writeString(mes.name, 64);
    }
    
    public static TelepathicMessage decode(PacketBuffer buf) {
        return new TelepathicMessage(SearchType.values[buf.readInt()], buf.readString(64));
    }
    
    public static void handle(TelepathicMessage mes, Supplier<NetworkEvent.Context> cont) {
    	cont.get().enqueueWork(() -> {
            TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile) te;
                ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
                if (world != null) {
                    BlockPos dest = console.getDestinationPosition();
                    PlayerEntity player = cont.get().getSender();
//                    int configRadius = TConfig.CONFIG.telepathicSearchRadius.get();
                    int rangeLimit = 500;
//                    int searchRadius = configRadius >= 500 ? rangeLimit : configRadius; //Use config value if not above 500 blocks
                    switch (mes.type) {
					case BIOME:
						List<Biome> list = Lists.newArrayList();
						Biome destBiome = world.getBiome(dest);
						list.add(destBiome);
						BiomeProvider biomeProvider = world.getChunkProvider().getChunkGenerator().getBiomeProvider();
						BlockPos biome = biomeProvider.findBiomePosition(dest.getX(), dest.getZ(), rangeLimit, rangeLimit, list::contains, world.rand);
						if(biome != null) {
                    		console.setDestination(console.getDestinationDimension(), biome.add(0, 64, 0));
                    		player.sendStatusMessage(VALID, true);
                    		
                    	}
						else player.sendStatusMessage(new TranslationTextComponent("message.tardis.telepathic.not_found", mes.type.toString().toUpperCase() ,rangeLimit), true);
						break;
					case SPAWN:
						BlockPos spawn = world.getSpawnPoint();
                		if(spawn != null) {
                			console.setDestination(console.getDestinationDimension(), console.randomizeCoords(spawn, 25));
                			player.sendStatusMessage(VALID, true);
                		}
						break;
					case STRUCTURE:
						int structureSearchRange = 100;
						StructureFeature<?, ?> structureToSearch = world.func_241828_r().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOptional(new ResourceLocation(mes.name)).get();
                		BlockPos pos = world.getStructureLocation(structureToSearch.field_236268_b_, console.getDestinationPosition(), structureSearchRange, true);
                		if(pos != null) {
                			console.setDestination(console.getDestinationDimension(), pos);
                			player.sendStatusMessage(VALID, true);
                		}
                		else player.sendStatusMessage(new TranslationTextComponent("message.tardis.telepathic.not_found", mes.type.toString().toUpperCase(), structureSearchRange), true);
						break;
					default:
						break;
                    }
                }
            }
        });
        cont.get().setPacketHandled(true);
    }




}
