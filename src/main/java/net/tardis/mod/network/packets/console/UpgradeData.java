package net.tardis.mod.network.packets.console;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.function.Supplier;

public class UpgradeData implements ConsoleData{

    private ResourceLocation upgrade;
    public boolean isActive;

    public UpgradeData(ResourceLocation loc, boolean isActive){
        this.upgrade = loc;
        this.isActive = isActive;
    }

    @Override
    public void applyToConsole(ConsoleTile tile, Supplier<NetworkEvent.Context> context) {
        tile.getUpgrade(this.upgrade).ifPresent(up -> {
            up.setActivated(this.isActive);
        });
    }

    @Override
    public void serialize(PacketBuffer buff) {
        buff.writeResourceLocation(this.upgrade);
        buff.writeBoolean(this.isActive);
    }

    @Override
    public void deserialize(PacketBuffer buff) {
        this.upgrade = buff.readResourceLocation();
        this.isActive = buff.readBoolean();
    }
}
