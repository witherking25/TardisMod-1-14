package net.tardis.mod.network.packets;


import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.ControlEntity;


public class UpdateControlMessage {

    public int entityID;
    public CompoundNBT data;
    public int animTicks;
    
    public UpdateControlMessage(int id, CompoundNBT tag, int animTicks){
        this.entityID = id;
        this.data = tag;
        this.animTicks = animTicks;
    }

    public static UpdateControlMessage decode(PacketBuffer buf) {
        return new UpdateControlMessage(buf.readInt(), buf.readCompoundTag(), buf.readInt());
    }

    public static void encode(UpdateControlMessage mes, PacketBuffer buf) {
    	buf.writeInt(mes.entityID);
    	buf.writeCompoundTag(mes.data);
    	buf.writeInt(mes.animTicks);
    }

    public static void handle(UpdateControlMessage mes, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            Entity entity = Minecraft.getInstance().world.getEntityByID(mes.entityID);
            if(entity instanceof ControlEntity) {
            	((ControlEntity)entity).getControl().deserializeNBT(mes.data);
            	((ControlEntity)entity).setAnimationTicks(mes.animTicks);
            	((ControlEntity)entity).getControl().onPacketUpdate();
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
