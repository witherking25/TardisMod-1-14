package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.recipe.WeldRecipe;

/** The custom sync packet used to sync WeldRecipes using our codec and CodecJsonDataListener
 * <br> Now that we have switched to vanilla's IRecipe interface, this is no longer needed
 * <br> We are keeping this here in case we have issues with vanilla's system and need an quick fully functional solution
 * */
@Deprecated
public class WeldRecipeSyncMessage {
    
	private Map<ResourceLocation, WeldRecipe> recipes = new HashMap<ResourceLocation, WeldRecipe>();
	//We use an unboundedMapCodec. However it is limited in that it can only parse objects whose keys can be serialised to a string, such as ResourceLocation
	//E.g. If you used an int as a key, the unboundedMapCodec will not parse it and will error.
	public static final Codec<Map<ResourceLocation, WeldRecipe>> MAPPER = Codec.unboundedMap(ResourceLocation.CODEC, WeldRecipe.getCodec());
	
	
	public WeldRecipeSyncMessage(Map<ResourceLocation, WeldRecipe> recipes) {
		this.recipes.clear(); //Clear the client's list and readd the entries from the server
        this.recipes.putAll(recipes);
	}

	public static void encode(WeldRecipeSyncMessage mes, PacketBuffer buf) {
		buf.writeCompoundTag((CompoundNBT)(MAPPER.encodeStart(NBTDynamicOps.INSTANCE, mes.recipes).result().orElse(new CompoundNBT())));
	}
	
	public static WeldRecipeSyncMessage decode(PacketBuffer buf) {
		//Parse our Map Codec and send the nbt data over. If there's any errors, populate with empty list
		return new WeldRecipeSyncMessage(MAPPER.parse(NBTDynamicOps.INSTANCE, buf.readCompoundTag()).result().orElse(new HashMap<ResourceLocation, WeldRecipe>()));
	}
	
	public static void handle(WeldRecipeSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
            WeldRecipe.DATA_LOADER.setData(mes.recipes); //Set the ConsoleRoom's Registry to that of the parsed in list
		});
		cont.get().setPacketHandled(true);
	}

}
