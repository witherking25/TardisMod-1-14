package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;

public class AttunementProgressMessage {

	public float progress;
	
	public AttunementProgressMessage(float progress) {
		this.progress = progress;
	}
	
	public static void encode(AttunementProgressMessage mes, PacketBuffer buf) {
		buf.writeFloat(mes.progress);
	}
	
	public static AttunementProgressMessage decode(PacketBuffer buf) {
		return new AttunementProgressMessage(buf.readFloat());
	}
	
	public static void handle(AttunementProgressMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				data.getAttunementHandler().setClientAttunementProgress(mes.progress);
			});
		});
		context.get().setPacketHandled(true);
	}
}
