package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;

public class CrashData implements ConsoleData{

	public CrashData() {}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		ClientHelper.shutTheFuckUp(null, SoundCategory.BLOCKS);
		tile.getWorld().playSound(ClientHelper.getClientPlayer(), TardisHelper.TARDIS_POS, TSounds.CANT_START.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);
	}

	@Override
	public void serialize(PacketBuffer buff) {}

	@Override
	public void deserialize(PacketBuffer buff) {}

}
