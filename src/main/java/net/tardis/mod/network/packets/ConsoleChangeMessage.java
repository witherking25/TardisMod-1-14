package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class ConsoleChangeMessage {
	
	public ResourceLocation console;
	
	public ConsoleChangeMessage(ResourceLocation console) {
		this.console = console;
	}
	
	public static void encode(ConsoleChangeMessage mes, PacketBuffer buffer) {
		buffer.writeResourceLocation(mes.console);
	}
	
	public static ConsoleChangeMessage decode(PacketBuffer buffer) {
		return new ConsoleChangeMessage(buffer.readResourceLocation());
	}
	
	public static void handle(ConsoleChangeMessage mes, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			ServerWorld world = ctx.get().getSender().getServerWorld();
			if(WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
				TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					Console console = ConsoleRegistry.CONSOLE_REGISTRY.get().getValue(mes.console);
					if(console != null) {
						if(((ConsoleTile)te).canDoAdminFunction(ctx.get().getSender())) {
							//Save old Data
							ConsoleTile oldConsole = (ConsoleTile)te;
							CompoundNBT oldData = oldConsole.serializeNBT();
							
							//Replace
							world.setBlockState(oldConsole.getPos(), console.getState(), 2);
							
							TileEntity newConsole = world.getTileEntity(te.getPos());
							if(newConsole instanceof ConsoleTile) {
								((ConsoleTile)newConsole).deserializeNBT(oldData);
								world.getServer().enqueue(new TickDelayedTask(1, () -> ((ConsoleTile)newConsole).updateClient()));
							}
						}
						else ctx.get().getSender().sendStatusMessage(Constants.Translations.NOT_ADMIN, true);
					}
				}
			}
		});
		ctx.get().setPacketHandled(true);
	}
	

}
