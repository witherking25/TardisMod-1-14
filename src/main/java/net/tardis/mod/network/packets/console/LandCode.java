package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.tileentities.ConsoleTile;

public class LandCode implements ConsoleData{

	public static final String TRANSLATION_KEY = "message.tardis.transduction.set_code";
	
	String code;
	
	public LandCode(String code) {
		this.code = code;
		
		if(code.length() > 16)
			this.code = code.substring(0, 16);
		
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<NetworkEvent.Context> context) {
		tile.setLandingCode(code);
		context.get().getSender().sendStatusMessage(new TranslationTextComponent(TRANSLATION_KEY, code), false);
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeString(code, 16);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		code = buff.readString(16);
	}

}
