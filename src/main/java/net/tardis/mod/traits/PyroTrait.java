package net.tardis.mod.traits;

import net.minecraft.block.BlockState;
import net.minecraft.block.FireBlock;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockPos.Mutable;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class PyroTrait extends TardisTrait{

	public PyroTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		if(tile.getWorld().getGameTime() + 9 % 400 == 0 && tile.getEmotionHandler().getMood() < EnumHappyState.ECSTATIC.getTreshold()) {
			int raduis = 16;
			
			ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
			if(ext != null) {
				BlockPos.Mutable start = new BlockPos.Mutable();
				start.setPos(ext.getPos());
				for(int x = -raduis; x < raduis; ++x) {
					for(int y = -raduis; y < raduis; ++y) {
						for(int z = -raduis; z < raduis; ++z) {
							BlockState state = ext.getWorld().getBlockState(start.add(x, y, z));
							if(state.getFluidState().isTagged(FluidTags.LAVA) || state.getBlock() instanceof FireBlock) {
								
								tile.getEmotionHandler().addMood(5 + (this.getModifier() * 30));
								this.warnPlayer(tile, LIKES_LOCATION);
								return;
							}
						}
					}
				}
			}
			
			
		}
	}

}
