package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.monster.SpiderEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ArachnophobiaTrait extends AbstractEntityDeathTrait{

	public ArachnophobiaTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		if(tile.getWorld().getGameTime() + 23 % 200 == 0) {
			ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
			double mod = 1.0 - this.getWeight();
			if(ext != null) {
				if(!ext.getWorld().getEntitiesWithinAABB(SpiderEntity.class, new AxisAlignedBB(ext.getPos()).grow(8)).isEmpty()) {
					if(tile.getEmotionHandler().getMood() > EnumHappyState.DISCONTENT.getTreshold())
						tile.getEmotionHandler().addMood(-(1 + (int)Math.round(4 * mod)));
				}
			}
		}
	}

	@Override
	public void onOwnerKilledEntity(PlayerEntity owner, ConsoleTile tardis, LivingEntity victim) {
		double mod = 1.0 - this.getWeight();
		if(victim instanceof SpiderEntity && tardis.getEmotionHandler().getMood() < EnumHappyState.CONTENT.getTreshold())
			tardis.getEmotionHandler().addMood(4 + (4 * mod));
	}

}
