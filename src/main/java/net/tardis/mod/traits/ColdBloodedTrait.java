package net.tardis.mod.traits;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.RainType;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ColdBloodedTrait extends TardisTrait{
	
	LazyOptional<ExteriorTile> exteriorHolder;

	public ColdBloodedTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		
		if(!tile.isInFlight() && tile.getWorld().getGameTime() % 20 == 0) {
			if(this.exteriorHolder == null || !this.exteriorHolder.isPresent())
				this.exteriorHolder = tile.getOrFindExteriorTile();
			
			this.exteriorHolder.ifPresent(ext -> {
				Biome b = ext.getWorld().getBiome(ext.getPos());
				if((b.getPrecipitation() == RainType.SNOW || b.getTemperature(ext.getPos()) >= 0.15F && tile.getEmotionHandler().getMood() > EnumHappyState.DISCONTENT.getTreshold())) {
					tile.getEmotionHandler().addMood(-(0.05 + (this.getModifier() * 0.20)));
					this.warnPlayerLooped(tile, TardisTrait.DISLIKES_LOCATION, 400);
				}
			});
		}
		
	}

}
