package net.tardis.mod.config;



import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;

import net.minecraftforge.common.ForgeConfig.Server;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.BooleanValue;
import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;

/**
 * Created by Swirtzly
 * on 24/03/2020 @ 21:39
 */


//Making a global config
public class TConfig {

    public static final Common COMMON;
    public static final ForgeConfigSpec COMMON_SPEC;
    public static Client CLIENT;
    public static ForgeConfigSpec CLIENT_SPEC;
    
    public static final Server SERVER;
    public static final ForgeConfigSpec SERVER_SPEC;

    static {
        final Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
        COMMON = specPair.getLeft();
        COMMON_SPEC = specPair.getRight();
        
        Pair<Client, ForgeConfigSpec> specClientPair = new ForgeConfigSpec.Builder().configure(Client::new);
        CLIENT_SPEC = specClientPair.getRight();
        CLIENT = specClientPair.getLeft();
        
        final Pair<Server, ForgeConfigSpec> specServerPair = new ForgeConfigSpec.Builder().configure(Server::new);
        SERVER = specServerPair.getLeft();
        SERVER_SPEC = specServerPair.getRight();
    }
    
    public static class Client {
        public ForgeConfigSpec.BooleanValue openVMEmptyHand;
        public ForgeConfigSpec.BooleanValue interiorShake;
        public BooleanValue enableBoti;
        public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedBlocks;
        public ForgeConfigSpec.ConfigValue<List<? extends String>> botiBlacklistedEntities;
        
        public Client(ForgeConfigSpec.Builder builder) {
            builder.push("Client Settings");
            
            enableBoti = builder.comment("Toggle whether to render the Bigger On the Inside effect on the Tardis Interior Door and Exterior block", "Server Owners: BOTI Packets cannot be disabled on server side because they need to be sent to allow BOTI to update"
            	, "BOTI packets will only be processed on the server if the portal needs to update.")
                .translation("config.tardis.enableBoti")
                .define("enableBoti", true);
            
            botiBlacklistedBlocks = builder.translation("config.tardis.botiBlacklistedBlocks")
                    .comment("List block ids that will be blacklisted from rendering in the Bigger On the Inside Effect", "Example: minecraft:tall_grass", "Seperate every entry except the last one with commas","To blacklist all blocks from a mod, use YourModId:*")
                    .defineList("botiBlacklistedBlocks", Lists.newArrayList(""), String.class::isInstance);
            
            botiBlacklistedEntities = builder.translation("config.tardis.botiBlacklistedEntities")
                    .comment("List entity ids that will be blacklisted from rendering in the Bigger On the Inside Effect", "Example: minecraft:ender_dragon", "Seperate every entry except the last one with commas","To blacklist all entities from a mod, use YourModId:*")
                    .defineList("botiBlacklistedEntities", Lists.newArrayList(""), String.class::isInstance);
            
            builder.push("Vortex Manipulator");
            openVMEmptyHand = builder.comment("Toggles whether the Vortex Manipulator in the inventory will open its GUI if the player right clicks with an empty hand")
                              .translation("config.tardis.openVMEmptyHand")
                              .define("openVMEmptyHand", false);
            builder.pop();
            
            builder.push("Tardis Client Settings");
            interiorShake = builder.comment("Toggles whether to shake the camera during flight and when the exterior is hit")
                              .translation("config.tardis.interiorShake")
                              .define("interiorShake", true);
            builder.pop();
            
            builder.pop();
        }
    }
    
    public static class Server {
    //Tardis Specific
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedDims;
    //entitiesToKill is replaced by interior_change_kill entity type tag
//    public ForgeConfigSpec.ConfigValue<List<? extends String>> entitiesToKill;
    public ConfigValue<Integer> interiorChangeProcessTime;
    public ConfigValue<Integer> interiorChangeCooldownTime;
    public ConfigValue<Integer> interiorChangeArtronUse;
    
    public ConfigValue<Float> FEtoAURate;
    public ConfigValue<Integer> collisionRange;
   // public ConfigValue<Integer> telepathicSearchRadius;
    
    //VM Specific
    public ForgeConfigSpec.BooleanValue toggleVMWhitelistDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> whitelistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDimTypes;
    public ConfigValue<Integer> vmTeleportRange;
    public ConfigValue<Integer> vmCooldownTime;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> vmSideEffects;
    public ForgeConfigSpec.ConfigValue<List<? extends Integer>> vmSideEffectTime;
    public ConfigValue<Integer> vmBaseFuelUsage;
    public ConfigValue<Double> vmFuelUsageMultiplier;
    public ConfigValue<Integer> vmFuelUsageTime;

    
    //Entity
    public ForgeConfigSpec.BooleanValue detonateCreeper;
    public ForgeConfigSpec.BooleanValue shearSheep;
    public ForgeConfigSpec.BooleanValue dismantleSkeleton;
    public ForgeConfigSpec.BooleanValue inkSquid;

    //Block
    public ForgeConfigSpec.BooleanValue detonateTnt;
    public ForgeConfigSpec.BooleanValue redstoneLamps;
    public ForgeConfigSpec.BooleanValue openDoors;
    public ForgeConfigSpec.BooleanValue openTrapDoors;
    //public ForgeConfigSpec.BooleanValue toggleRedstone;

    //Tardis
    public ForgeConfigSpec.BooleanValue coordinateTardis;

    //Laser
//    public ForgeConfigSpec.BooleanValue laserFire;


        public Server(ForgeConfigSpec.Builder builder) {
        
        builder.push("Tardis Configurations");
        blacklistedDims = builder.translation("config.tardis.blacklistedDims")
                .comment("List of dimensions Tardis cannot travel to, everything else is allowed","Seperate every entry except the last one with commas")
                .defineList("blacklistedDims", Lists.newArrayList("minecraft:the_end"), String.class::isInstance);
//        telepathicSearchRadius =  builder.translation("config.tardis.telepathicSearchRadius")
//                .comment("Sets the search radius for the Telepathic Circuit Biome/Structure Search", "NOTICE: This is limited to 500 blocks, anymore will cause performance issues")
//                .define("telepathicSearchRadius", 500, Integer.class::isInstance);
        collisionRange = builder.translation("config.tardis.collision")
                .comment("If two TARDISes are closer than this number, they are considered 'colliding'","When they collide, a Time Ram event is initiated")
                .define("collisionRange", 16, Integer.class::isInstance);
        builder.pop();
        
//        builder.push("Entities to Kill when changing TARDIS interior");
//        this.entitiesToKill = builder.translation("config.tardis.entities_to_kill")
//                .comment("List the IDs of the entities you want to kill when the Tardis interior is changed")
//                .defineList("things_to_kill", Lists.newArrayList(""), String.class::isInstance);
//        builder.pop();
        
        builder.push("Interior Change Configuration");
        interiorChangeProcessTime =  builder.translation("config.tardis.interiorChangeProcessTime")
                .comment("Sets the seconds of time needed before the Tardis finishes changing its interior")
                .define("interiorChangeProcessTime", 300, Integer.class::isInstance);
        interiorChangeCooldownTime =  builder.translation("config.tardis.interiorChangeCooldownTime")
                .comment("Sets the seconds of cooldown time between each changing of the Tardis Interior")
                .define("interiorChangeCooldownTime", 60, Integer.class::isInstance);
        interiorChangeArtronUse = builder.translation("config.tardis.interiorChangeArtronUse")
                .comment("Sets the amount of Artron fuel required between each changing of the Tardis Interior")
                .define("interiorChangeArtronUse", 100, Integer.class::isInstance);
        builder.pop();
        
        builder.push("Machines Configuration");
        FEtoAURate = builder.translation("config.tardis.machines.conversionRate")
                .comment("Defines how much Forge Energy is needed for 1 Artron Unit","Used to balance out the Artron Converter Block", "This is a float value, allows for decimals")
                .define("FEtoAURate", 1000F, Float.class::isInstance);
        
        builder.pop();
        
        builder.push("Vortex Manipulator (VM) Configurations");
        
        builder.push("Travel Configurations");
        toggleVMWhitelistDims = builder.translation("config.tardis.vm.toggleVMDimWhitelist")
        .comment("Toggle whether to use the Whitelist. By default, uses the Blacklist")
        .define("toggleVMWhitelistDims", false);
        whitelistedVMDims = builder.translation("config.tardis.vm.whitelistedDims")
                .comment("List of dimensions the VM can travel to, anything else is not allowed","Seperate every entry except the last one with commas")
                .defineList("whitelistedVMDims", Lists.newArrayList("minecraft:overworld","minecraft:nether","minecraft:the_end"), String.class::isInstance);
        blacklistedVMDims = builder.translation("config.tardis.vm.blacklistedDims")
                .comment("List of dimensions the VM cannot travel to, everything else is allowed","Seperate every entry except the last one with commas")
                .defineList("blacklistedVMDims", Lists.newArrayList("modID:dimensionName"), String.class::isInstance);
        vmTeleportRange = builder.translation("config.tardis.vm.teleportRange")
                .comment("Sets the maximum Teleport Range the Vortex Manipulator can teleport to")
                .define("vmTeleportRange", 1000, Integer.class::isInstance);
        builder.pop();
        
        builder.push("Limitations");
        vmCooldownTime = builder.translation("config.tardis.vm.cooldownTime")
                .comment("Sets the seconds of cooldown time needed between uses of the Vortex Manipulator")
                .define("vmCooldownTime", 10, Integer.class::isInstance);
        vmSideEffects = builder.translation("config.tardis.vm.sideEffects")
                .comment("Sets the effect side effects experienced after using the Vortex Manipulator")
                .defineList("vmSideEffects", Lists.newArrayList("minecraft:nausea","minecraft:blindness","minecraft:weakness"), String.class::isInstance);
        vmSideEffectTime = builder.translation("config.tardis.vm.sideEffectTime")
                .comment("Sets the duration in seconds for each effect in the above value","The order of the numbers should match the effect you are setting the duration for.")
                .defineList("vmSideEffectTime", Lists.newArrayList(5,5,10),Integer.class::isInstance);
        builder.pop();
        
        builder.push("Fuel Control");
        vmBaseFuelUsage =  builder.translation("config.tardis.vm.baseFuelUsage")
                .comment("Sets the base value that scales for how much fuel is used by the VM"
                        ,"Fuel Usage = YourInputValue + (YourMultiplier * TeleportDistance)"
                        ,"E.g. If you want the fuel usage to be 20 charge per 100 blocks, you would set the base value to 20 - (0.005 * 100)")
                .define("vmBaseFuelUsage", 5,Integer.class::isInstance);
        vmFuelUsageMultiplier =  builder.translation("config.tardis.vm.fuelUsageMultiplier")
                .comment("Sets the figure that scales how much fuel is used by the VM per amount of seconds set by user"
                        ,"Fuel Usage = YourBaseInputValue + (YourMultiplier * TeleportDistance)"
                        ,"E.g. If you want the fuel usage to be 20 charge per 100 blocks, you would set the base value to 20 - (0.005 * 100)")
                .define("vmFuelUsageMultiplier", 0.1, Double.class::isInstance);
        vmFuelUsageTime = builder.translation("config.tardis.vm.fuelUsageTime")
                .comment("Sets the seconds of time that must elapse before the VM uses up fuel")
                .define("vmFuelUsageTime", 1, Integer.class::isInstance);
        builder.pop();
        
        builder.pop();
        
        
        builder.push("Sonic Screwdriver");
        builder.push("Block Interactions");
        detonateTnt = builder.translation("config.tardis.detonate_tnt").comment("Toggle whether sonics can detonate TNT").define("detonate_tnt", true);
        redstoneLamps = builder.translation("config.tardis.redstone_lamps").comment("Toggle whether sonics can toggle Lamps").define("redstone_lamps", true);
        openDoors = builder.translation("config.tardis.open_doors").comment("Toggle whether sonics can open doors").define("open_doors", true);
        openTrapDoors = builder.translation("config.tardis.open_trapdoors").comment("Toggle whether sonics can open Trap doors").define("open_trapdoors", true);
        //toggleRedstone = builder.translation("config.tardis.toggle_redstone").comment("Toggle whether enable/disable redstone").define("redstone", true);
        builder.pop();

        builder.push("Entity Interactions");
        detonateCreeper = builder.translation("config.tardis.detonate_creeper").comment("Toggle whether sonics can detonate Creepers").define("detonate_creeper", true);
        shearSheep = builder.translation("config.tardis.shear_sheep").comment("Toggle whether sonics can Shear Sheep").define("shear_sheep", true);
        dismantleSkeleton = builder.translation("config.tardis.dismantle_skeleton").comment("Toggle whether sonics can dismantle Skeleton like entities").define("dismantle_skeletons", true);
        inkSquid = builder.translation("config.tardis.ink_squid").comment("Toggle whether sonics can ink Squids").define("ink_squid", true);
        builder.pop();

        builder.push("Tardis");
        coordinateTardis = builder.translation("config.tardis.coordinate_tardis").comment("Toggle whether sonics can tell the Tardis where to land").define("coordinate_tardis", true);
        builder.pop();

//        builder.push("Laser");
//        laserFire = builder.translation("config.tardis.laser_fire").comment("Toggle where the Laser setting burns things").define("laser_fire", true);
//        builder.pop();

        builder.pop();

        }
    }
    
    public static class Common{
        public ConfigValue<Integer> tardisSpawnProbability;
        public ConfigValue<Integer> xionCrystalSpawnChance;
        public ConfigValue<Integer> cinnabarOreSpawnChance;
        
        public ConfigValue<Integer> crashedStructureSpawnChance;
        public ConfigValue<Integer> crashedStructureSeperation;
        public ConfigValue<Integer> crashedStructureSpacing;
        
        public ConfigValue<Integer> spaceStationMissionSpawnChance;
        public ConfigValue<Integer> spaceStationMissionSeperation;
        public ConfigValue<Integer> spaceStationMissionSpacing;
        
        public ConfigValue<Integer> dalekSaucerSpawnChance;
        public ConfigValue<Integer> dalekSaucerSeperation;
        public ConfigValue<Integer> dalekSaucerSpacing;
        
        public ConfigValue<Integer> abandonedSpaceshipSpawnChance;
        public ConfigValue<Integer> abandonedSpaceshipSeperation;
        public ConfigValue<Integer> abandonedSpaceshipSpacing;
        
        public Common(ForgeConfigSpec.Builder builder) {
            builder.push("World Generation");
            builder.comment("Requires Server Restart to take effect.", "If you don't restart, unknown side effects can occur");
            
            this.tardisSpawnProbability = builder.translation("config.tardis.tardis_spawn_chance")
                    .comment("The probability that a Broken Tardis Exterior will spawn in a valid position", "This must be a whole number, no decimals")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("tardisSpawnProbability", 1);
            
            this.cinnabarOreSpawnChance = builder.translation("config.tardis.cinnabar_ore_spawn_chance")
                    .comment("The chance for Cinnabar Ore to spawn in the world", "This must be a whole number, no decimals")
                    .comment("This value is divided by 1 to form the final probability", "Example: Value = 2, Spawn Chance = 1/2")
                    .define("cinnabarOreSpawnChance", 2);

            this.xionCrystalSpawnChance = builder.translation("config.tardis.xion_crystal_spawn_chance")
                    .comment("The chance for Xion Crystals to spawn in the world", "This option does not allow decimal points")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("xionCrystalSpawnChance", 30);
            
            builder.push("Crashed Structure");
            this.crashedStructureSpawnChance = builder.translation("config.tardis.crashed_structure_spawn_chance")
                    .comment("The chance for Crashed Structures such as Spaceships or Shuttles, to spawn in the world", "This option does not allow decimal points")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("crashedStructureSpawnChance", 20);
            this.crashedStructureSeperation = builder.translation("config.tardis.crashed_structure_seperation")
            		.comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("crashedStructureSeperation", 100);
            this.crashedStructureSpacing = builder.translation("config.tardis.crashed_structure_spacing")
            		.comment("The chunk radius around a potential spawn location in which this structure can spawn.", "This option does not allow decimal points")
                    .define("crashedStructureSpacing", 60);
            builder.pop();
            
            builder.push("Space Station");
            this.spaceStationMissionSpawnChance = builder.translation("config.tardis.space_station_spawn_chance")
                    .comment("The chance for a Space Station Structure, and its mission, to spawn in the world", "This option allows does allow decimal points", "This structure only spawns in the Space Dimension")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("spaceStationMissionSpawnChance", 20);
            this.spaceStationMissionSeperation = builder.translation("config.tardis.space_station_seperation")
            		.comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("spaceStationMissionSeperation", 120);
            this.spaceStationMissionSpacing = builder.translation("config.tardis.space_station_spacing")
            		.comment("The chunk radius around a potential spawn location in which this structure can spawn.", "This option does not allow decimal points")
                    .define("spaceStationMissionSpacing", 30);
            builder.pop();
            
            builder.push("Dalek Saucer");
            this.dalekSaucerSpawnChance = builder.translation("config.tardis.dalek_saucer_spawn_chance")
                    .comment("The chance for a Dalek Saucer Structure, to spawn in the world", "This option does not allow decimal points")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("dalekSaucerSpawnChance", 30);
            this.dalekSaucerSeperation = builder.translation("config.tardis.dalek_saucer_seperation")
            		.comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points")
                    .define("dalekSaucerSeperation", 200);
            this.dalekSaucerSpacing = builder.translation("config.tardis.dalek_saucer_spacing")
            		.comment("The chunk radius around a potential spawn location in which this structure can spawn.", "This option does not allow decimal points")
                    .define("dalekSaucerSpacing", 50);
            builder.pop();
            
            builder.push("Abandoned Spaceship");
            this.abandonedSpaceshipSpawnChance = builder.translation("config.tardis.abandoned_spaceship_spawn_chance")
                    .comment("The chance for Abandoned Spaceship Structure, to spawn in the world", "This option does not allow decimal points", "This structure only spawns in the Space Dimension")
                    .comment("This value is divided by 100 to form the final probability", "Example: Value = 1, Spawn Chance = 1/100")
                    .define("abandonedSpaceshipSpawnChance", 30);
            this.abandonedSpaceshipSeperation = builder.translation("config.tardis.abandoned_spaceship_seperation")
                    .comment("The number of chunks that can seperate two instances of this structure", "This option does not allow decimal points", "This structure only spawns in the Space Dimension")
                    .define("abandonedSpaceshipSeperation", 50);
            this.abandonedSpaceshipSpacing = builder.translation("config.tardis.abandoned_spaceship_spacing")
            		.comment("The chunk radius around a potential spawn location in which this structure can spawn.", "This option does not allow decimal points", "This structure only spawns in the Space Dimension")
                    .define("abandonedSpaceshipSpacing", 20);
            builder.pop();
            
            builder.pop();
        }
        
    }

}
