package net.tardis.mod.compat.jei;

import java.util.Arrays;
import java.util.List;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.AttunableRecipe;


public class AttunableRecipeCategory implements IRecipeCategory<AttunableRecipe> {
	public static final ResourceLocation NAME = new ResourceLocation(Tardis.MODID, "attunable");
	private IDrawable background;
	private IDrawable icon;
	
	public AttunableRecipeCategory(IGuiHelper help){
		background = help.createDrawable(new ResourceLocation(Tardis.MODID, "textures/gui/engine/attunement.png"),
				0, 0, 176, 69);
		icon = help.createDrawableIngredient(new ItemStack(TItems.KEY.get()));
	}
	
	@Override
	public ResourceLocation getUid() {
		return NAME;
	}
	
	@Override
	public Class<? extends AttunableRecipe> getRecipeClass() {
		return AttunableRecipe.class;
	}
	
	@Override
	public String getTitle() {
		return "Attunable";
	}
	
	@Override
	public IDrawable getBackground() {
		return this.background;
	}
	
	@Override
	public IDrawable getIcon() {
		return this.icon;
	}
	
	@Override
	public void setIngredients(AttunableRecipe recipe, IIngredients ingredients) {
		ingredients.setInputIngredients(recipe.getIngredients());
		ingredients.setOutput(VanillaTypes.ITEM, new ItemStack(recipe.getOutputResult().getOutput()));
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AttunableRecipe recipe, IIngredients ingredients) {
		ItemStack[] item = new ItemStack[1];
		List<ItemStack> stacksInIngredient = Arrays.asList(recipe.getInputIngredient().getMatchingStacks());
		for(int i = 0; i < item.length; ++i) {
			if(i < stacksInIngredient.size())
				item[i] = stacksInIngredient.get(i);
			else item[i] = ItemStack.EMPTY;
		}
		
		addSlot(recipeLayout, 0, 118, 26, item[0]);
	}
	
	public static void addSlot(IRecipeLayout layout, int id, int x, int y, ItemStack stack) {
		if(!stack.isEmpty()) {
			layout.getItemStacks().init(id, true, x, y);
			layout.getItemStacks().set(id, stack);
		}
	}
}
