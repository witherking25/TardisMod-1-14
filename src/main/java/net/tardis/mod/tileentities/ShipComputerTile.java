package net.tardis.mod.tileentities;


import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameterSets;
import net.minecraft.loot.LootParameters;
import net.minecraft.loot.LootTable;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.registries.SchematicRegistry;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.schematics.Schematic;

public class ShipComputerTile extends TileEntity {

	private Schematic schematic;
	private ItemStackHandler handler = new ItemStackHandler(27);
	private LazyOptional<ItemStackHandler> itemHolder = LazyOptional.of(() -> handler);
	private ResourceLocation lootTable;
	
	public ShipComputerTile() {
		super(TTiles.SHIP_COMPUTER.get());
	}
	
	public Schematic getSchematic() {
		return this.schematic;
	}
	
	public void setSchematic(Schematic sche) {
		this.schematic = sche;
		this.markDirty();
	}
	
	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		handler.deserializeNBT(compound.getCompound("inv"));
		
		if(compound.contains("schematic"))
			this.schematic = SchematicRegistry.SCHEMATIC_REGISTRY.get().getValue(new ResourceLocation(compound.getString("schematic")));
		
		if(compound.contains("loot_table"))
			this.lootTable = new ResourceLocation(compound.getString("loot_table"));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.schematic != null)
			compound.putString("schematic", this.schematic.getRegistryName().toString());
		compound.put("inv", handler.serializeNBT());
		if(this.lootTable != null)
			compound.putString("loot_table", this.lootTable.toString());
		return super.write(compound);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.write(new CompoundNBT()));
	}
	
	@Override
	public void handleUpdateTag(BlockState state, CompoundNBT tag) {
		super.handleUpdateTag(state, tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? itemHolder.cast() : super.getCapability(cap, side);
	}

	public void setLootTable(ResourceLocation lootTable) {
		this.lootTable = lootTable;
		this.markDirty();
	}
	
	public ResourceLocation getLootTable() {
		return this.lootTable;
	}
	
	public void fillWithLoot(PlayerEntity player){
		if(!world.isRemote && this.lootTable != null) {
			LootTable table = world.getServer().getLootTableManager().getLootTableFromLocation(lootTable);
			
			if(table == LootTable.EMPTY_LOOT_TABLE)
				return;
			
			List<ItemStack> list = table.generate(
					new LootContext.Builder((ServerWorld)world)
					.withParameter(LootParameters.ORIGIN , Vector3d.copyCentered(this.pos))
					.withParameter(LootParameters.THIS_ENTITY, player)
					.build(LootParameterSets.CHEST));
			
			List<Integer> usedSlots = Lists.newArrayList();
			for(ItemStack stack : list) {
				int newSlot = world.rand.nextInt(this.handler.getSlots());
				
				//Eight tries to find an empty slot
				for(int i = 0; i < 8; ++i) {
					if(usedSlots.contains(newSlot))
						newSlot = world.rand.nextInt(this.handler.getSlots());
					else break;
				}
				
				this.handler.insertItem(newSlot, stack, false);
				usedSlots.add(newSlot);
			}
			this.lootTable = null;
			this.markDirty();
		}
	}

	@Override
	public void remove() {
		super.remove();
		this.itemHolder.invalidate();
	}
	

}
