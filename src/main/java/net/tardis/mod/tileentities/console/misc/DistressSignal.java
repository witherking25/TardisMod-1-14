package net.tardis.mod.tileentities.console.misc;

import javax.annotation.Nullable;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.registries.MissionRegistry;

public class DistressSignal {

    public static final int STRING_SIZE = 16;
    SpaceTimeCoord coord;
    String message;
    private MiniMissionType mission;
    
    public DistressSignal(String message, SpaceTimeCoord coord) {
        this.coord = coord;
        this.message = message;
    }
    
    public DistressSignal(String message, SpaceTimeCoord coord, MiniMissionType type) {
        this(message, coord);
        this.mission = type;
    }
    
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        tag.putString("message", this.message);
        if(this.mission != null)
            tag.putString("mission", this.mission.getRegistryName().toString());
        return tag;
    }
    
    public SpaceTimeCoord getSpaceTimeCoord() {
        return this.coord;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    @Nullable
    public MiniMissionType getMission(){
        return this.mission;
    }

    
    public static DistressSignal deserializeNBT(CompoundNBT tag) {
        String message = tag.getString("message");
        SpaceTimeCoord coord = SpaceTimeCoord.deserialize(tag.getCompound("coord"));
        MiniMissionType mission;
        
        if(tag.contains("mission")) {
            ResourceLocation key = new ResourceLocation(tag.getString("mission"));
            mission = MissionRegistry.MISSION_REGISTRY.get().getValue(key);
            if(mission != null)
                return new DistressSignal(message, coord, mission);
        }
        return new DistressSignal(message, coord);
    }
    
    public void encode(PacketBuffer buf) {
        buf.writeCompoundTag(this.coord.serialize());
        buf.writeInt(this.message.length());
        buf.writeString(message, message.length());
        boolean hasMission = this.mission != null;
        buf.writeBoolean(hasMission);
        if(hasMission) {
            buf.writeResourceLocation(mission.getRegistryName());
        }

    }
    
    public static DistressSignal decode(PacketBuffer buf) {
        SpaceTimeCoord coord = SpaceTimeCoord.deserialize(buf.readCompoundTag());
        int stringSize = buf.readInt();
        String message = buf.readString(stringSize);
        MiniMissionType mission;
        
        boolean hasMission = buf.readBoolean();
        if(hasMission) {
            ResourceLocation key = buf.readResourceLocation();
            mission = MissionRegistry.MISSION_REGISTRY.get().getValue(key);
            
            if(mission != null)
                return new DistressSignal(message, coord, mission);
        }

        return new DistressSignal(message, coord);
    }
}
