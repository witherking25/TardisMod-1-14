package net.tardis.mod.tileentities.console.misc;

public enum TardisAction {
	MISSED_CONTROL,
	CRASHED,
	KILLED_PASSIVE,
	KILLED_HOSTILE,
	EMOTION_TICK,
	REPAIRED,
	NONE
}
