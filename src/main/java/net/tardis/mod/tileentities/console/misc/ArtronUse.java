package net.tardis.mod.tileentities.console.misc;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;
/** Wrapper class for segmenting artron fuel usage
 * <br> Allows us to set aside a certain amount of artron for different tasks and be able to track them
 * <br> Note: The use per tick of the artron use
 * @author Spectre0987*/
public class ArtronUse {
	
	private IArtronType type;
	private float usePerTick = 0F;
	private int ticksToDrain = 0;
	
	protected ArtronUse() {}
	
	public ArtronUse(IArtronType type) {
		this.type = type;
		this.usePerTick = type.getUse();
	}
	
	public void setArtronUsePerTick(float usePerTick) {
		this.usePerTick = usePerTick;
	}
	
	public float getArtronUsePerTick() {
		return this.usePerTick;
	}
	
	public boolean isActive() {
		return this.ticksToDrain > -1;
	}
	
	public void setTicksToDrain(int time) {
		this.ticksToDrain = time;
	}
	
	public void tick(ConsoleTile tile) {
		if(this.ticksToDrain >= 0) {
			
			if(this.ticksToDrain > 0) {
				tile.setArtron(tile.getArtron() - this.getArtronUsePerTick());
			}
			
			--this.ticksToDrain;
		}
	}
	
	public IArtronType getType() {
		return this.type;
	}
	
	public static interface IArtronType{
		float getUse();
		TranslationTextComponent getTranslation();
	}

	public static enum ArtronType implements IArtronType{
		FLIGHT(0F, "flight"),
		FORCEFIELD(1.0F, "forcefield"),
		CONVERTER(1.0F, "converter"),
		ANTIGRAVS(1.0F, "antigravs"),
		INTERIOR_CHANGE(1.0F, "interior_change");

		float use;
		TranslationTextComponent trans;
		
		ArtronType(float use, String name){
			this.use = use;
			trans = new TranslationTextComponent("artronuse.tardis." + name);
		}
		
		@Override
		public float getUse() {
			return this.use;
		}

		@Override
		public TranslationTextComponent getTranslation() {
			return this.trans;
		}
		
	}
}
