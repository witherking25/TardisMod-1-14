package net.tardis.mod.tileentities.exteriors;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:35:38 pm
 */
public class PoliceBoxExteriorTile extends ExteriorTile{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	
	public static final AxisAlignedBB NORTH_BOX = new AxisAlignedBB(0, -1, -0.1, 1, 2, 1);
	public static final AxisAlignedBB EAST_BOX = new AxisAlignedBB(0, -1, 0, 1.1, 2, 1);
	public static final AxisAlignedBB SOUTH_BOX = new AxisAlignedBB(0, -1, 0, 1, 2, 1.1);
	public static final AxisAlignedBB WEST_BOX = new AxisAlignedBB(-0.1, -1, 0, 1, 2, 1);

	public PoliceBoxExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public PoliceBoxExteriorTile() {
		super(TTiles.EXTERIOR_POLICE_BOX.get());
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(world.getBlockState(getPos()).hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			switch(world.getBlockState(getPos()).get(BlockStateProperties.HORIZONTAL_FACING)) {
			case EAST: return EAST_BOX;
			case SOUTH: return SOUTH_BOX;
			case WEST: return WEST_BOX;
			default: return NORTH_BOX;
			}
		}
		return NORTH_BOX;
	}

}
