package net.tardis.mod.tileentities.exteriors;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class ApertureExteriorTile extends ExteriorTile{

	public ApertureExteriorTile(TileEntityType<?> type) {
		super(type);
	}
	
	public ApertureExteriorTile() {
		super(TTiles.EXTERIOR_APERTURE.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		return this.getDefaultEntryBox();
	}

}
