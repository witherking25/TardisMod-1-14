package net.tardis.mod.tileentities.consoles;

import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class GalvanicConsoleTile extends ConsoleTile{

	public GalvanicConsoleTile() {
		super(TTiles.CONSOLE_GALVANIC.get());
		this.registerControlEntry(ControlRegistry.MONITOR.get());
		this.variants = ConsoleTextureVariants.GALVANIC;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).expand(2, 4, 2);
	}

}
