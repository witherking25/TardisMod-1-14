package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.StructureBlockTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;

/**
 * Created by Spectre0987 on 19/5/2020
 * 
 */

public class SuperStructureTile extends StructureBlockTileEntity{
	
	private BlockPos custom_size = BlockPos.ZERO;
    
    public SuperStructureTile() {
    }
    
    @Override
    public TileEntityType<?> getType() {
        return TTiles.SUPER_STRUCTURE.get();
    }
    
    @Override
    public void read(BlockState state, final CompoundNBT compound) {
        this.custom_size = BlockPos.fromLong(compound.getLong("custom_size"));
        super.read(state, compound);
        this.setSize(this.custom_size);
    }
    
    @Override
    public CompoundNBT write(final CompoundNBT compound) {
        compound.putLong("custom_size", this.getStructureSize().toLong());
        return super.write(compound);
    }
    
    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return new AxisAlignedBB(this.getPos()).expand((double)this.custom_size.getX(), (double)this.custom_size.getY(), (double)this.custom_size.getZ());
    }
}
