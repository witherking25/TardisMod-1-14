package net.tardis.mod.tileentities;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.BoatEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.INameable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.blocks.AntiGravBlock;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.tileentities.TTiles;

/**
 * Created by Swirtzly
 * on 06/04/2020 @ 22:16
 */
public class AntiGravityTile extends TileEntity implements ITickableTileEntity, INameable {

    private int range = 1;


   public AntiGravityTile() {
       super(TTiles.ANTI_GRAV.get());
    }

    public static boolean isInAntiGrav(AxisAlignedBB box, World world) {

        for (Iterator<BlockPos> iterator = BlockPos.getAllInBox(new BlockPos(box.maxX, box.maxY, box.maxZ), new BlockPos(box.minX, box.minY, box.minZ)).iterator(); iterator.hasNext(); ) {
            BlockPos pos = iterator.next();
            BlockState blockState = world.getBlockState(pos);
            if (blockState.getBlock() instanceof AntiGravBlock) {
                if (blockState.get(AntiGravBlock.ACTIVATED) && !world.getBlockState(pos.up()).isNormalCube(world, pos.up())) {
                    AntiGravityTile gravityTile = (AntiGravityTile) world.getTileEntity(pos);
                    if (gravityTile != null && gravityTile.getUppyDownyBox().intersects(box)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public AxisAlignedBB getUppyDownyBox() {
        if (this.world != null) {
            return new AxisAlignedBB(this.pos).grow(getRange()).expand(0.0D, (double) this.world.getHeight(), 0.0D);
        }
        return null;
    }

    @Override
    public void tick() {
        if (world != null && world.getBlockState(pos.up()).isNormalCube(world, pos.up())) return;
        if (getRange() > 5 || getRange() <= 0) {
            setRange(0);
        }

        if (world != null && world.getBlockState(pos).get(AntiGravBlock.ACTIVATED) && world.isBlockPowered(pos)) {
            List<Entity> list = this.world.getEntitiesWithinAABB(Entity.class, getUppyDownyBox());

            for (Entity living : list) {
                Vector3d motion = living.getMotion();


                if (world.isRemote) {
                    for (int particleRotation = 0; particleRotation < 20; ++particleRotation) {
                        int an = (int) world.getGameTime() % 36;
                        double partAng = Math.toRadians(an * 10);
                        double x = Math.sin(partAng) * this.getRange();
                        double z = Math.cos(partAng) * this.getRange();
                        double y = an / 36.0;

                        this.world.addParticle(ParticleTypes.FIREWORK, pos.getX() + x + 0.5, pos.getY() + y + 1 + particleRotation, pos.getZ() + z + 0.5, 0.0D, 0.0D, 0.0D);
                    }

                }

                if (living.rotationPitch < 0 || living instanceof ItemEntity || living instanceof ArmorStandEntity || living instanceof BoatEntity || living instanceof TardisEntity) {
                    living.setMotion(motion.add(0, 0.1D, 0));
                }

                if (!(living instanceof ItemEntity)) {
                    if (living.rotationPitch > 0 || living.isSneaking()) {
                        living.setMotion(motion.mul(1.0D, 1D, 1.0D));
                    }
                }

                living.fallDistance = 0;

            }
        }
    }



    @Override
    public ITextComponent getName() {
        return new TranslationTextComponent("message.tardis.anti_grav", getRange());
    }

    @Override
    public boolean hasCustomName() {
        return true;
    }

    @Override
    public ITextComponent getDisplayName() {
        return getName();
    }

    @Nullable
    @Override
    public ITextComponent getCustomName() {
        return getName();
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.putInt("range", range);
        return super.write(compound);
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        setRange(compound.getInt("range"));
        super.read(state, compound);
    }

    @Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		setRange(pkt.getNbtCompound().getInt("range"));
    }

    @Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}
	
	@Override
	public void handleUpdateTag(BlockState state, CompoundNBT tag) {
		super.handleUpdateTag(state, tag);
		setRange(tag.getInt("range"));
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.write(new CompoundNBT());
		tag.putInt("range", this.range);
		return tag;
	}
}