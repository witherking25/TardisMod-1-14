package net.tardis.mod.tileentities.machines;

import java.util.Random;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.cap.Capabilities;

import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.particles.TParticleTypes;
import net.tardis.mod.tileentities.TTiles;

public class ArtronCollectorPylonTile extends TileEntity implements ITickableTileEntity{

	private ArtronCollectorTile mainUnit;
	
	public ArtronCollectorPylonTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ArtronCollectorPylonTile() {
		super(TTiles.ARTRON_PYLON.get());
	}

	@Override
	public void tick() {
		
		if(this.getOrFindMainUnit() != null) {
			
			Vector3d start = getPylonPartPos(this.mainUnit.getPos());
			Vector3d end = WorldHelper.vecFromPos(this.getPos());
			Vector3d path = start.subtract(end);
			
			Vector3d speed = path.normalize().scale(0.05);

			
			for(int i = 0; i < 10; ++i) {
				
				double percent = i / 10.0;
				
				world.addParticle(TParticleTypes.ARTRON.get(),
						pos.getX() + 0.5 + path.getX() * percent, pos.getY() + 1.3 + path.getY() * percent, pos.getZ() + 0.5 + path.z * percent,
						speed.x, speed.y, speed.z);
			}
			
			Random rand = world.getRandom();
			
			for(int i = 0; i < 5; ++i) {
				
				Vector3d part = new Vector3d(1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3, 1.5 - rand.nextDouble() * 3);
				
				Vector3d partSpeed = part.subtract(new Vector3d(0.5, 0.5, 0.5)).normalize().scale(-0.05);

				
				world.addParticle(TParticleTypes.ARTRON.get(),
						pos.getX() + 0.5 + part.getX(),pos.getY() + 0.5 + part.getY(), pos.getZ() + 0.5 + part.getZ(),
						partSpeed.x, partSpeed.y, partSpeed.z);
			}
			
		}
		if (!world.isRemote()) {
			world.getChunkAt(getPos()).getCapability(Capabilities.RIFT).ifPresent(rift -> {
				if(rift.isRift()) {
					//TODO: Add logic to drain rift
					if (this.mainUnit != null) {
						mainUnit.recieveArtron(rift.takeArtron(10));
					}
				}
			});
		}
		
	}
	
	public static Vector3d getPylonPartPos(BlockPos pos) {
		return new Vector3d(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
	}

	public ArtronCollectorTile getOrFindMainUnit() {
		if(this.mainUnit != null && !this.mainUnit.isRemoved())
			return this.mainUnit;
		
		BlockPos radius = new BlockPos(16, 16, 16);
		
		ObjectWrapper<ArtronCollectorTile> mainUnitHolder = new ObjectWrapper<>(null);
		BlockPos.getAllInBoxMutable(this.getPos().subtract(radius), this.getPos().add(radius)).forEach(pos -> {
			if(pos.withinDistance(this.getPos(), 16)) {
				TileEntity te = world.getTileEntity(pos);
				if(te instanceof ArtronCollectorTile)
					mainUnitHolder.setValue((ArtronCollectorTile)te);
			}
		});
		
		return this.mainUnit = mainUnitHolder.getValue();
		
	}
}
