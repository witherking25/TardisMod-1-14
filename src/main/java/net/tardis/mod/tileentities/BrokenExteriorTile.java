package net.tardis.mod.tileentities;


import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.misc.BrokenExteriorType;
import net.tardis.mod.misc.TardisLikes;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BrokenTardisParticleSpawn;
import net.tardis.mod.registries.BrokenExteriors;

public class BrokenExteriorTile extends TileEntity implements ITickableTileEntity{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	private BrokenExteriors brokenType = BrokenExteriors.STEAM;
	private HashMap<UUID, Integer> playerLoyalty = Maps.newHashMap();

	private boolean forcedEntryTameStarted = false;
	private boolean forcedEntryTameFinished = false;
	private PlayerEntity forcedEntryPlayer = null;
	private int forcedEntryTickTimer = 0;

	public BrokenExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public BrokenExteriorTile() {
		this(TTiles.BROKEN_TARDIS.get());
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	public BrokenExteriorType getBrokenTypeObject() {
		return this.brokenType.getType();
	}
	
	public BrokenExteriors getBrokenType() {
		return this.brokenType;
	}

	/*
	 * If this should consume the item
	 * 
	 */
	public boolean onPlayerRightClick(PlayerEntity player, ItemStack stack) {
		for(TardisLikes like : TardisLikes.LIKES) {
			if(like.matches(stack)) {
				
				if(this.addLoyalty(player, like.getLoyaltyMod()))

					this.tameTardis(player);
				
				return true;
			}
		}
		return false;
		
	}

	public HashMap<UUID, Integer> getLoyalties() {
		return this.playerLoyalty;
	}
	
	/**
	 * 
	 * @param player
	 * @param amt
	 * @return True if the player has enough loyalty to tame the Tardis fully
	 */ 
	public boolean addLoyalty(PlayerEntity player, int amt) {
		int initialLoyaltyValue = -100;
		int old = this.playerLoyalty.getOrDefault(player.getUniqueID(), initialLoyaltyValue); //Set initial loyalty to negative value so we don't add too much loyalty too quickly
		this.playerLoyalty.put(player.getUniqueID(), old + amt);
		int requiredLoyaltyThreshold = 0; //Since the initial loyalty is -100, and we want player to add 100 loyalty before taming, this should be 0.
		Network.sendToAllAround(new BrokenTardisParticleSpawn(pos), player.getEntityWorld().getDimensionKey(), pos, 24);
		if(old + amt >= requiredLoyaltyThreshold) 
			return true;
		return false;
	}
	
	public void tameTardis(PlayerEntity player) {
	    if(!this.world.isRemote) {
	    	ServerWorld interior = this.brokenType.getType().setup((ServerWorld)world, (ServerPlayerEntity)player, this);
	    	if(interior != null)
	    		this.brokenType.getType().swapWithReal((ServerWorld)world, pos, interior, this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING));
	    }
	}

	public void startForceTame(PlayerEntity player){
		if (forcedEntryTameStarted) {
			//already taming! don't let another player muscle in.
			return;
		}

		// Well the Tardis knows what's coming and certainly isn't going to like it!
		this.addLoyalty(player, -100);
		// set the player who is forcing the tame, which is used to trigger their achievement of taming a tardis.
		this.forcedEntryPlayer = player;
		// allow the timer to start in the tick function.
		this.forcedEntryTameStarted = true;
	}

	@Override
	public void read(BlockState state, CompoundNBT nbt) {
		super.read(state, nbt);
		this.brokenType = BrokenExteriors.values()[nbt.getInt("type")];
		
		ListNBT loyalty = nbt.getList("loyalties", NBT.TAG_COMPOUND);
		this.playerLoyalty.clear();
		for(INBT base : loyalty) {
			CompoundNBT data = (CompoundNBT)base;
			this.playerLoyalty.put(data.getUniqueId("id"), data.getInt("loyalty"));
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("type", this.brokenType.ordinal());
		
		ListNBT loyaltyList = new ListNBT();
		for(Entry<UUID, Integer> entry : this.playerLoyalty.entrySet()) {
			CompoundNBT loyalty = new CompoundNBT();
			loyalty.putUniqueId("id", entry.getKey());
			loyalty.putInt("loyalty", entry.getValue());
			loyaltyList.add(loyalty);
		}
		compound.put("loyalties", loyaltyList);
		return super.write(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public void tick() {
		if (forcedEntryTameStarted && !forcedEntryTameFinished) {
			// Timer has started!
			// delay tame till the leash entity auto pops off by itself
			if (this.forcedEntryTickTimer++ >= 100) {
				// not sure if this is neccessary, with the tile getting replaced,
				// but best to not let it run the same things more than once.
				this.forcedEntryTameFinished = true;
				this.forcedEntryTickTimer = 0;
				// call the tame function, which will in turn go about turning the broken exterior into something the
				// player can then open up and interact with.
				this.tameTardis(forcedEntryPlayer);
			}
		}
	}
	
	public void randomizeType() {
		if(!world.isRemote) {
			this.brokenType = BrokenExteriors.values()[this.world.rand.nextInt(BrokenExteriors.values().length)];
			world.markBlockRangeForRenderUpdate(getPos(), getBlockState(), getBlockState());
		}
	}

}
