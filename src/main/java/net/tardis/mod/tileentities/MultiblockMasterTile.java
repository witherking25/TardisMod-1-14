package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.blocks.MultiblockBlock;
import net.tardis.mod.tileentities.TTiles;

/**
 * @implNote - Master blocks and normal blocks both use {@link MultiblockBlock}, (can be overriden)
 * @author Spectre0987
 *
 */
public class MultiblockMasterTile extends TileEntity implements IMultiblock{

	private List<BlockPos> slavePoses = new ArrayList<>();
	
	public MultiblockMasterTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public MultiblockMasterTile() {
		super(TTiles.MUTIBLOCK_MASTER.get());
	}

	@Override
	public BlockPos getMaster() {
		return this.getPos();
	}

	@Override
	public BlockState getMasterState() {
		return this.getBlockState();
	}

	@Override
	public MultiblockMasterTile getMasterTile() {
		return this;
	}
	
	public void addSlavePos(BlockPos pos) {
		this.slavePoses.add(pos);
		this.markDirty();
	}
	
	public List<BlockPos> getSlavePositions(){
		return this.slavePoses;
	}

	@Override
	public void setMasterPos(BlockPos pos) {}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		ListNBT list = compound.getList("other_pos", NBT.TAG_LONG);
		for(INBT base : list) {
			this.slavePoses.add(BlockPos.fromLong(((LongNBT)base).getLong()));
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT list = new ListNBT();
		for(BlockPos pos : this.slavePoses) {
			list.add(LongNBT.valueOf(pos.toLong()));
		}
		compound.put("other_pos", list);
		return super.write(compound);
	}

}
