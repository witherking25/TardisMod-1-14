package net.tardis.mod.tileentities.monitors;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.tileentities.TTiles;

public class RotateMonitorTile extends MonitorTile{

	public float previousRot;
	public float rotation;
	private float nextRotation;
	public float extendAmount = 1F;
	
	public RotateMonitorTile(TileEntityType<?> type) {
		super(type);
	}
	
	public RotateMonitorTile() {
		super(TTiles.ROTATE_MONITOR.get());
	}
	
	@Override
	public void tick() {
		super.tick();
		
		this.previousRot = this.rotation;
		this.rotation = this.nextRotation;
	}

	public void setRotation(float rot) {
		this.nextRotation = rot;
		this.markDirty();
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 3);
	}

	public void setExtendAnount(float extend) {
		this.extendAmount = extend;
		this.markDirty();
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 3);
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		this.extendAmount = compound.getFloat("extend_amount");
		this.rotation = this.nextRotation = compound.getFloat("rotation"); 
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putFloat("extend_amount", this.extendAmount);
		compound.putFloat("rotation", this.rotation);
		return super.write(compound);
	}
}
