package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Supplier;

import net.minecraft.block.Block;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.blocks.TileBlock;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;
import net.tardis.mod.tileentities.exteriors.ApertureExteriorTile;
import net.tardis.mod.tileentities.exteriors.ClockExteriorTile;
import net.tardis.mod.tileentities.exteriors.DisguiseExteriorTile;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;
import net.tardis.mod.tileentities.exteriors.JapanExteriorTile;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.PoliceBoxExteriorTile;
import net.tardis.mod.tileentities.exteriors.SafeExteriorTile;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;
import net.tardis.mod.tileentities.exteriors.TT2020ExteriorTile;
import net.tardis.mod.tileentities.exteriors.TTCapsuleExteriorTile;
import net.tardis.mod.tileentities.exteriors.TelephoneExteriorTile;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;
import net.tardis.mod.tileentities.machines.*;
import net.tardis.mod.tileentities.monitors.MonitorTile;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class TTiles {
	
	public static List<TileEntityType<?>> TYPES = new ArrayList<TileEntityType<?>>();
	
	public static final DeferredRegister<TileEntityType<?>> TILES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, Tardis.MODID);
	public static final RegistryObject<TileEntityType<SteamConsoleTile>> CONSOLE_STEAM = TILES.register("console_steam", () ->  registerTiles(SteamConsoleTile::new, TBlocks.console_steam.get()));
	public static final RegistryObject<TileEntityType<NemoConsoleTile>> CONSOLE_NEMO = TILES.register("console_nemo", () ->  registerTiles(NemoConsoleTile::new, TBlocks.console_nemo.get()));
	public static final RegistryObject<TileEntityType<GalvanicConsoleTile>> CONSOLE_GALVANIC = TILES.register("console_galvanic", () ->  registerTiles(GalvanicConsoleTile::new, TBlocks.console_galvanic.get()));
	public static final RegistryObject<TileEntityType<CoralConsoleTile>> CONSOLE_CORAL = TILES.register("coral", () ->  registerTiles(CoralConsoleTile::new, TBlocks.console_coral.get()));
	public static final RegistryObject<TileEntityType<HartnellConsoleTile>> CONSOLE_HARTNEL = TILES.register("hartnell", () ->  registerTiles(HartnellConsoleTile::new, TBlocks.console_hartnell.get()));
	public static final RegistryObject<TileEntityType<ToyotaConsoleTile>> CONSOLE_TOYOTA = TILES.register("toyota", () ->  registerTiles(ToyotaConsoleTile::new, TBlocks.console_toyota.get()));
	public static final RegistryObject<TileEntityType<XionConsoleTile>> CONSOLE_XION = TILES.register("xion", () ->  registerTiles(XionConsoleTile::new, TBlocks.console_xion.get()));
	public static final RegistryObject<TileEntityType<NeutronConsoleTile>> CONSOLE_NEUTRON = TILES.register("neutron", () ->  registerTiles(NeutronConsoleTile::new, TBlocks.console_neutron.get()));
	public static final RegistryObject<TileEntityType<KeltConsoleTile>> CONSOLE_POLYMEDICAL = TILES.register("kelt", () ->  registerTiles(KeltConsoleTile::new, TBlocks.console_polymedical.get()));

	public static final RegistryObject<TileEntityType<ClockExteriorTile>> EXTERIOR_CLOCK = TILES.register("exterior_clock", () ->  registerTiles(ClockExteriorTile::new, TBlocks.exterior_clock.get()));
	public static final RegistryObject<TileEntityType<SteampunkExteriorTile>> EXTERIOR_STEAMPUNK = TILES.register("exterior_steampunk", () ->  registerTiles(SteampunkExteriorTile::new, TBlocks.exterior_steampunk.get()));
	public static final RegistryObject<TileEntityType<TrunkExteriorTile>> EXTERIOR_TRUNK = TILES.register("exterior_trunk", () ->  registerTiles(TrunkExteriorTile::new, TBlocks.exterior_trunk.get()));
	public static final RegistryObject<TileEntityType<TelephoneExteriorTile>> EXTERIOR_TELEPHONE = TILES.register("exterior_telephone", () ->  registerTiles(TelephoneExteriorTile::new, TBlocks.exterior_telephone.get()));
	public static final RegistryObject<TileEntityType<PoliceBoxExteriorTile>> EXTERIOR_POLICE_BOX = TILES.register("exterior_police_box", () ->  registerTiles(PoliceBoxExteriorTile::new, TBlocks.exterior_police_box.get()));
	public static final RegistryObject<TileEntityType<FortuneExteriorTile>> EXTERIOR_FORTUNE = TILES.register("exterior_fortune", () ->  registerTiles(FortuneExteriorTile::new, TBlocks.exterior_fortune.get()));
	public static final RegistryObject<TileEntityType<ModernPoliceBoxExteriorTile>> EXTERIOR_MODERN_POLICE_BOX = TILES.register("exterior_modern_police_box", () ->  registerTiles(ModernPoliceBoxExteriorTile::new, TBlocks.exterior_modern_police_box.get()));
	public static final RegistryObject<TileEntityType<SafeExteriorTile>> EXTERIOR_SAFE = TILES.register("exterior_safe", () ->  registerTiles(SafeExteriorTile::new, TBlocks.exterior_safe.get()));
	public static final RegistryObject<TileEntityType<TTCapsuleExteriorTile>> EXTERIOR_TT_CAPSULE = TILES.register("exterior_tt_capsule", () ->  registerTiles(TTCapsuleExteriorTile::new, TBlocks.exterior_tt_capsule.get()));
	public static final RegistryObject<TileEntityType<TT2020ExteriorTile>> EXTERIOR_TT2020_CAPSULE = TILES.register("exterior_tt2020", () ->  registerTiles(TT2020ExteriorTile::new, TBlocks.exterior_tt2020.get()));
	public static final RegistryObject<TileEntityType<JapanExteriorTile>> EXTERIOR_JAPAN = TILES.register("exterior_japan", () ->  registerTiles(JapanExteriorTile::new, TBlocks.exterior_japan.get()));
	public static final RegistryObject<TileEntityType<ApertureExteriorTile>> EXTERIOR_APERTURE = TILES.register("exterior_aperture", () -> registerTiles(ApertureExteriorTile::new, TBlocks.exterior_aperture.get()));
	public static final RegistryObject<TileEntityType<DisguiseExteriorTile>> EXTERIOR_DISGUISE = TILES.register("exterior_disguise", () ->  registerTiles(DisguiseExteriorTile::new, TBlocks.exterior_disguise.get()));

	public static final RegistryObject<TileEntityType<MonitorTile>> STEAMPUNK_MONITOR = TILES.register("steampunk_monitor", () ->  registerTiles(MonitorTile::new, TBlocks.steampunk_monitor.get(), TBlocks.eye_monitor.get(), TBlocks.rca_monitor.get()));
	public static final RegistryObject<TileEntityType<MonitorDynamicTile>> DYNAMIC_MONITOR = TILES.register("dynamic_monitor", () ->  registerTiles(MonitorDynamicTile::new, TBlocks.dynamic_monitor.get()));
	public static final RegistryObject<TileEntityType<BrokenExteriorTile>> BROKEN_TARDIS = TILES.register("broken_tardis", () ->  registerTiles(BrokenExteriorTile::new, TBlocks.broken_exterior.get()));
	public static final RegistryObject<TileEntityType<AlembicTile>> ALEMBIC = TILES.register("alembic", () -> registerTiles(AlembicTile::new, TBlocks.alembic.get()));
	public static final RegistryObject<TileEntityType<TardisEngineTile>> ENGINE = TILES.register("engine", () ->  registerTiles(TardisEngineTile::new, TBlocks.engine.get()));
	public static final RegistryObject<TileEntityType<QuantiscopeTile>> QUANTISCOPE = TILES.register("quantiscope", () ->  registerTiles(QuantiscopeTile::new, TBlocks.quantiscope_brass.get(), TBlocks.quantiscope_iron.get()));
	public static final RegistryObject<TileEntityType<VortexDetectorTile>> VORTEX_DETECTOR = TILES.register("vortex_detector", () ->  registerTiles(VortexDetectorTile::new, TBlocks.VORTEX_DETECTOR.get()));
	public static final RegistryObject<TileEntityType<NeutronicSpectrometerTile>> SPECTROMETER = TILES.register("spectrometer", () -> registerTiles(NeutronicSpectrometerTile::new, TBlocks.NEUTRONIC_SPECTROMETER.get()));
	
	public static final RegistryObject<TileEntityType<ArtronCollectorPylonTile>> ARTRON_PYLON = TILES.register("artron_pylon", () -> registerTiles(ArtronCollectorPylonTile::new, TBlocks.ARTRON_PYLON.get()));
    public static final RegistryObject<TileEntityType<ArtronCollectorTile>> ARTRON_COLLECTOR = TILES.register("artron_collector", () -> registerTiles(ArtronCollectorTile::new, TBlocks.ARTRON_COLLECTOR.get()));
	
	public static final RegistryObject<TileEntityType<AntiGravityTile>> ANTI_GRAV = TILES.register("anti_grav", () ->  registerTiles(AntiGravityTile::new, TBlocks.anti_grav.get()));
	public static final RegistryObject<TileEntityType<SquarenessChamelonTile>> SQUARENESS = TILES.register("squareness", () ->  registerTiles(SquarenessChamelonTile::new, TBlocks.squareness_block.get()));
	public static final RegistryObject<TileEntityType<ShipComputerTile>> SHIP_COMPUTER = TILES.register("ship_computer", () ->  registerTiles(ShipComputerTile::new, TBlocks.ship_computer.get()));
	public static final RegistryObject<TileEntityType<ReclamationTile>> RECLAMATION_UNIT = TILES.register("reclaimation_unit", () ->  registerTiles(ReclamationTile::new, TBlocks.reclamation_unit.get()));
	public static final RegistryObject<TileEntityType<ToyotaSpinnyTile>> TOYOTA_SPIN = TILES.register("spinny_thing", () ->  registerTiles(ToyotaSpinnyTile::new, TBlocks.spinny_block.get()));
	public static final RegistryObject<TileEntityType<RoundelTapTile>> ROUNDEL_TAP = TILES.register("roundel_tap", () ->  registerTiles(RoundelTapTile::new, TBlocks.roundel_tap.get()));
	public static final RegistryObject<TileEntityType<OxygenSealTile>> OXYGEN_SEALER = TILES.register("oxygen_sealer", () ->  registerTiles(OxygenSealTile::new, TBlocks.oxygen_sealer.get()));
	public static final RegistryObject<TileEntityType<LandingPadTile>> LANDING_PAD = TILES.register("landing_pad", () ->  registerTiles(LandingPadTile::new, TBlocks.landing_pad.get()));
	public static final RegistryObject<TileEntityType<TransductionBarrierTile>> TRANSDUCTION_BARRIER = TILES.register("transdunction_barrier", () ->  registerTiles(TransductionBarrierTile::new, TBlocks.transduction_barrier.get()));
	public static final RegistryObject<TileEntityType<CorridorKillTile>> CORRIDOR_KILL = TILES.register("corridor_kill", () ->  registerTiles(CorridorKillTile::new, TBlocks.corridor_kill.get()));
    
	public static final RegistryObject<TileEntityType<PlaqueTile>> PLAQUE = TILES.register("plaque", () ->  registerTiles(PlaqueTile::new, TBlocks.plaque.get()));
	public static final RegistryObject<TileEntityType<RotateMonitorTile>> ROTATE_MONITOR = TILES.register("rotate_monitor", () ->  registerTiles(RotateMonitorTile::new, TBlocks.rotate_monitor.get()));
	public static final RegistryObject<TileEntityType<WaypointBankTile>> WAYPOINT_BANK = TILES.register("waypoint_bank", () ->  registerTiles(WaypointBankTile::new, TBlocks.waypoint_bank.get()));
	public static final RegistryObject<TileEntityType<AccessPanelTile>> ACCESS_PANEL = TILES.register("access_panel", () ->  registerTiles(AccessPanelTile::new, TBlocks.item_access_panel.get()));

	public static final RegistryObject<TileEntityType<MultiblockMasterTile>> MUTIBLOCK_MASTER = TILES.register("multiblock_master", () ->  registerTiles(MultiblockMasterTile::new, TBlocks.multiblock_master.get()));
	public static final RegistryObject<TileEntityType<MultiblockTile>> MUTIBLOCK = TILES.register("multiblock", () ->  registerTiles(MultiblockTile::new, TBlocks.multiblock.get()));
	public static final RegistryObject<TileEntityType<SuperStructureTile>> SUPER_STRUCTURE = TILES.register("structure_block", () ->  registerTiles(SuperStructureTile::new, TBlocks.structure_block.get()));
	
//	@SubscribeEvent
//	public static void register(RegistryEvent.Register<TileEntityType<?>> event) {
//		for(TileEntityType<?> type : TYPES) {
//			event.getRegistry().register(type);
//		}
//	}
//
//	public static <T extends TileEntity> TileEntityType<T> register(Supplier<T> tile, String name, Block... validBlock) {
//		TileEntityType<T> type = TileEntityType.Builder.create(tile, validBlock).build(null);
//		type.setRegistryName(Tardis.MODID, name);
//		TYPES.add(type);
//		for(Block block : validBlock) {
//			if(block instanceof TileBlock) {
//				((TileBlock)block).setTileEntity(type);
//			}
//		}
//		return type;
//	}
	
	private static <T extends TileEntity> TileEntityType<T> registerTiles(Supplier<T> tile, Block... validBlock) {
		TileEntityType<T> type = TileEntityType.Builder.create(tile, validBlock).build(null);
		for(Block block : validBlock) {
			if(block instanceof TileBlock) {
				((TileBlock)block).setTileEntity(type);
			}
		}
		return type;
	}

}
