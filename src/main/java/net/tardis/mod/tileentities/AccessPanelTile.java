package net.tardis.mod.tileentities;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.tileentities.TTiles;

public class AccessPanelTile extends TileEntity{

	private ItemStackHandler handler = new ItemStackHandler(0);
	private LazyOptional<ItemStackHandler> handlerProvider = LazyOptional.of(() -> this.handler);
	
	public AccessPanelTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public AccessPanelTile() {
		this(TTiles.ACCESS_PANEL.get());
	}
	
	@Override
	public void onLoad() {
		super.onLoad();
		
		world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			this.handlerProvider.invalidate();
			this.handlerProvider = LazyOptional.of(() -> cap.getItemBuffer());
		});
		
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			return this.handlerProvider.cast();
		return super.getCapability(cap, side);
	}

}
