package net.tardis.mod.misc;
/*
The MIT License (MIT)
Copyright (c) 2020 Joseph Bettendorff a.k.a. "Commoble"
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;

import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

/**
 * Instance of a Data Listener that uses codecs
 * <br> Auto loads data, subscribes to the player login event and sends a sync packet on reload
 * References: 
 * <br>https://github.com/Commoble/databuddy/blob/1.16.4/src/main/java/commoble/databuddy/data/CodecJsonDataManager.java
 * <br>Minecraft JsonListener
 */
public class CodecJsonDataListener<T> extends JsonReloadListener{
	
	protected final String folderName;
	protected Codec<T> codec;
	protected final Logger logger;
	protected static final Gson STANDARD_GSON = new Gson();
	protected Map<ResourceLocation, T> data = new HashMap<>();
	protected Optional<Runnable> syncDataOnReload = Optional.empty();
	
	/**
	 * @param folderName - the name of the folder we wish to look for json files under
	 * @param codec - codec used 
	 * @param logger
	 * @param gson
	 */
	public CodecJsonDataListener(String folderName, Codec<T> codec, Logger logger, Gson gson) {
		super(gson, folderName);
		this.folderName = folderName;
		this.codec = codec;
		this.logger = logger;
	}
	
	public CodecJsonDataListener(String folderName, Codec<T> codec, Logger logger) {
		this(folderName, codec, logger, STANDARD_GSON);
	}

	@Override
	protected void apply(Map<ResourceLocation, JsonElement> jsons, IResourceManager resourceManagerIn,
	        IProfiler profilerIn) {
		this.logger.info("Beginning loading of data for data loader: {}", this.folderName);
		this.data = this.mapValues(jsons);
		this.logger.info("Data loader for {} loaded {} entries", this.folderName, this.data.size());
		
		// hacky server test until we can find a better way to do this
		if (ServerLifecycleHooks.getCurrentServer() != null) {
			// if we're on the server and we are configured to send syncing packets, send syncing packets
			this.syncDataOnReload.ifPresent(Runnable::run);
		}
	}
	
	public Map<ResourceLocation, T> mapValues(Map<ResourceLocation, JsonElement> inputs) {
		Map<ResourceLocation, T> newMap = new HashMap<>();

		for (Entry<ResourceLocation, JsonElement> entry : inputs.entrySet()){
			ResourceLocation key = entry.getKey();
			JsonElement element = entry.getValue();
			// if we fail to parse json, log an error and continue
			// if we succeeded, add the resulting T to the map
			this.codec.decode(JsonOps.INSTANCE, element)
				.get()
				.ifLeft(result -> {newMap.put(key, result.getFirst()); this.logger.info("Added: {}", key.toString());})
				.ifRight(partial -> this.logger.error("Failed to parse data json for {} due to: {}", key.toString(), partial.message()));
		}
		return newMap;
	}
	
	/**
	 * Autoscribes a packet to be sent to a player during the PlayerLoginEvent
	 * Called in the main Mod constructor
	 * @param <PACKET>
	 * @param channel - the Network Channel to be used
	 * @param packetFactory - a new instance of a the Packet to be sent
	 * @return
	 */
	public <PACKET> CodecJsonDataListener<T> subscribeAsSyncable(final SimpleChannel channel,
		final Function<Map<ResourceLocation, T>, PACKET> packetFactory){
		MinecraftForge.EVENT_BUS.addListener(this.getLoginListener(channel, packetFactory));
		this.syncDataOnReload = Optional.of(() -> channel.send(PacketDistributor.ALL.noArg(), packetFactory.apply(this.data)));
		return this;
	}
	
	private <PACKET> Consumer<PlayerEvent.PlayerLoggedInEvent> getLoginListener(final SimpleChannel channel,
			final Function<Map<ResourceLocation, T>, PACKET> packetFactory){
		return event -> {
			PlayerEntity player = event.getPlayer();
			if (player instanceof ServerPlayerEntity)
			{
				channel.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)player), packetFactory.apply(this.data));
			}
		};
	}
	
	public Map<ResourceLocation, T> getData(){
		return this.data;
	}
	/**
	 * Sets the data loader's data to that of the input
	 * @param input
	 */
	public void setData(Map<ResourceLocation, T> input) {
		this.data = input;
	}
	
	

}
