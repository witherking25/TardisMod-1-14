package net.tardis.mod.misc;

import net.minecraft.item.Item;
/**
 * 50ap5ud5: Do not use this interface to generate BlockItems
 * <br> This used to work in 1.14 when we used static initialisers
 * <br> In 1.16 and DeferredRegisters, class loading order means the block will be null when this is found, so the resulting blockitem will always be "Air"
 * <br> If you need a special BlockItem, manually register an item in TItems with the same registry name as the block
 * @author Spectre0987
 *
 */
@Deprecated
public interface INeedItem {

    Item getItem();

}
