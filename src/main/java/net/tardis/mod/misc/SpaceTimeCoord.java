package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.tardis.mod.helper.WorldHelper;

public class SpaceTimeCoord {

    public static final SpaceTimeCoord UNIVERAL_CENTER = new SpaceTimeCoord(World.OVERWORLD, BlockPos.ZERO, Direction.NORTH);

    private BlockPos pos;
    private ResourceLocation dimRL;
    private Direction facing;
    private String name = "";

    public SpaceTimeCoord(RegistryKey<World> world, BlockPos pos, Direction dir) {
        this(world.getLocation(), pos, dir);
    }

    public SpaceTimeCoord(ResourceLocation dimRL, BlockPos pos, Direction dir) {
        this.dimRL = dimRL;
        this.pos = pos;
        this.facing = dir;
    }

    public SpaceTimeCoord(RegistryKey<World> world, BlockPos pos) {
        this(world, pos, Direction.NORTH);
    }

    public static SpaceTimeCoord deserialize(CompoundNBT tag) {
        SpaceTimeCoord coord = new SpaceTimeCoord(new ResourceLocation(tag.getString("dim")), BlockPos.fromLong(tag.getLong("pos")), Direction.values()[tag.getInt("dir")]);
        if (tag.contains("name"))
            coord.setName(tag.getString("name"));
        return coord;
    }

    public BlockPos getPos() {
        return pos;
    }
    
    /**
     * Get's the SpaceTimeCoord's ResourceLocation of its Dimension
     * @return
     */
    public ResourceLocation getDimRL() {
        return dimRL;
    }
    
    /**
     * Gets the World Registry Key (instance of a dimension)
     * @return
     */
    public RegistryKey<World> getDim() {
        return WorldHelper.getWorldKeyFromRL(dimRL);
    }

    public Direction getFacing() {
        return facing;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompoundNBT serialize() {
        CompoundNBT tag = new CompoundNBT();
        tag.putLong("pos", this.pos.toLong());
        tag.putString("dim", this.dimRL.toString());
        tag.putInt("dir", this.facing.ordinal());
        tag.putString("name", this.name);
        return tag;
    }

    public SpaceTimeCoord toImmutable() {

        SpaceTimeCoord coord = new SpaceTimeCoord(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, dimRL), this.pos);
        if (this.name != null)
            coord.setName(name);
        return coord;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof SpaceTimeCoord))
            return false;
        SpaceTimeCoord other = (SpaceTimeCoord) obj;
        if (pos.equals(other.pos) && dimRL.equals(other.dimRL))
            return true;
        return false;
    }

}
