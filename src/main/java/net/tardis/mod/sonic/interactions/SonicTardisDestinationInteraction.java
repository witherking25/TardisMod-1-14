package net.tardis.mod.sonic.interactions;

import java.util.ArrayList;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.registries.SonicModeRegistry.SonicModeEntry;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by Swirtzly
 * on 17/03/2020 @ 11:02
 */
public class SonicTardisDestinationInteraction extends AbstractSonicMode {

    public SonicTardisDestinationInteraction(SonicModeEntry mode) {
		super(mode);
	}

	@Override
    public boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos) {
    	if (!TConfig.SERVER.coordinateTardis.get()) return false;
    	if (!player.world.isRemote && WorldHelper.canTravelToDimension(player.world)) {
    		if (sonic.getItem() instanceof SonicItem) {
    			SonicItem sonicItem = (SonicItem)sonic.getItem();
    			if (sonicItem.getTardis(sonic) != null) {
    				ConsoleTile console = TardisHelper.getConsole(player.getServer(), sonicItem.getTardis(sonic)).orElse(null);
                    if (console != null) {
                        if (handleDischarge(player, sonic, 25)) {
                            console.setDestination(player.world.getDimensionKey(), pos);
                            console.setExteriorFacingDirection(player.getHorizontalFacing());
                            PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.sonic.tardis_dest_set", WorldHelper.formatBlockPos(pos)), false);
                            return true;
                        } else {
                            return false;
                        }
                    }
    			}
    			else {
    				PlayerHelper.sendMessageToPlayer(player, Constants.Translations.ITEM_NOT_ATTUNED, true);
    				return false;
    			}
    		}
    	}
    	PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.sonic.tardis_dest_fail"), true);
        return false;
    }

    @Override
    public boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic) {
        return false;
    }

    @Override
    public ArrayList<TranslationTextComponent> getAdditionalInfo() {
        ArrayList<TranslationTextComponent> list = new ArrayList<TranslationTextComponent>();
        list.add(new TranslationTextComponent("sonic.modes.info.set_coords"));
        return list;
    }

    @Override
    public boolean hasAdditionalInfo() {
        return true;
    }

    @Override
    public void updateHeld(PlayerEntity playerEntity, ItemStack stack) {

    }

}
