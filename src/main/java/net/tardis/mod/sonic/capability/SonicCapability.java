package net.tardis.mod.sonic.capability;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SyncSonicMessage;
import net.tardis.mod.registries.SchematicRegistry;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sonic.ISonicPart;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Swirtzly
 * on 22/03/2020 @ 22:41
 */
public class SonicCapability implements ISonic {

    public static Random RANDOM = new Random();
    private final ItemStack stack;
    private final float maxCharge = 500;
    private SonicBasePart.SonicComponentTypes[] PARTS = new SonicBasePart.SonicComponentTypes[]{
            SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2, SonicBasePart.SonicComponentTypes.MK_2
    };
    private int mode = 0;
    private float charge = 500, progress = 0;
    private List<Schematic> schematics = new ArrayList<Schematic>();
    private ResourceLocation tardis;
    /**
     * Cached value of a block's Forge Energy value.
     * Gets updated if energy gotten from raytrace result is different to the cached value
     */
    private int forgeEnergy = -1;

    public SonicCapability(ItemStack stack) {
        this.stack = stack;
        for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
            setSonicPart(SonicBasePart.SonicComponentTypes.MK_2, value);
        }
        SonicItem.syncCapability(stack);
    }

    public SonicCapability() {
        stack = null;
    }

    public static LazyOptional<ISonic> getForStack(ItemStack stack) {
        return stack.getCapability(Capabilities.SONIC_CAPABILITY);
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int x = RANDOM.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    @Override
    public void setSonicPart(SonicBasePart.SonicComponentTypes type, ISonicPart.SonicPart part) {
        PARTS[part.getInvID()] = type;
    }

    @Override
    public SonicBasePart.SonicComponentTypes getSonicPart(ISonicPart.SonicPart part) {
        return PARTS[part.getInvID()];
    }

    @Override
    public float getCharge() {
        return charge;
    }

    @Override
    public void setCharge(float charge) {
        this.charge = charge;
    }


    @Override
    public float getMaxCharge() {
        return this.maxCharge;
    }

    @Override
    public float progress() {
        return progress;
    }

    @Override
    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public void sync(PlayerEntity player, Hand hand) {
        SonicItem.syncCapability(stack);
        Network.sendPacketToAll(new SyncSonicMessage(hand, serializeNBT(), player.getUniqueID()));
    }

    @Override
    public int getMode() {
        if (mode >= SonicModeRegistry.SONIC_MODES.getEntries().size()) {
            setMode(0);
        }
        return mode;
    }

    @Override
    public void setMode(int mode) {
        if (mode >= SonicModeRegistry.SONIC_MODES.getEntries().size()) {
            this.mode = 0;
            return;
        }
        this.mode = mode;
    }

    @Override
    public void randomiseParts() {
        for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
            setSonicPart(randomEnum(SonicBasePart.SonicComponentTypes.class), value);
            SonicItem.syncCapability(stack);
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT compoundNBT = new CompoundNBT();
        compoundNBT.putInt("emmiter", PARTS[0].getId());
        compoundNBT.putInt("activator", PARTS[1].getId());
        compoundNBT.putInt("handle", PARTS[2].getId());
        compoundNBT.putInt("end", PARTS[3].getId());
        compoundNBT.putInt("mode", getMode());
        compoundNBT.putFloat("charge", charge);
        compoundNBT.putFloat("progress", progress);
        compoundNBT.putInt("forge_energy", this.forgeEnergy);
        ListNBT schem = new ListNBT();
        for (Schematic s : this.getSchematics())
            schem.add(StringNBT.valueOf(s.getRegistryName().toString()));
        compoundNBT.put("schematics", schem);
        if (this.tardis != null) {
        	compoundNBT.putString("tardis", this.tardis.toString());
        }
        return compoundNBT;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        setSonicPart(findPartByID(nbt.getInt("emmiter")), ISonicPart.SonicPart.EMITTER);
        setSonicPart(findPartByID(nbt.getInt("activator")), ISonicPart.SonicPart.ACTIVATOR);
        setSonicPart(findPartByID(nbt.getInt("handle")), ISonicPart.SonicPart.HANDLE);
        setSonicPart(findPartByID(nbt.getInt("end")), ISonicPart.SonicPart.END);
        setCharge(nbt.getFloat("charge"));
        setProgress(nbt.getFloat("progress"));
        setMode(nbt.getInt("mode"));
        this.schematics.clear();
        this.forgeEnergy = nbt.getInt("forge_energy");
        ListNBT schematicList = nbt.getList("schematics", Constants.NBT.TAG_STRING);
        for (INBT schem : schematicList) {
            this.schematics.add(SchematicRegistry.SCHEMATIC_REGISTRY.get().getValue(new ResourceLocation(((StringNBT) schem).getString())));
        }
        if (nbt.contains("tardis")) {
            this.tardis = new ResourceLocation(nbt.getString("tardis"));
        }
    }

    public SonicBasePart.SonicComponentTypes findPartByID(int id) {
        for (SonicBasePart.SonicComponentTypes value : SonicBasePart.SonicComponentTypes.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return SonicBasePart.SonicComponentTypes.MK_1;
    }

    @Override
    public List<Schematic> getSchematics() {
        return this.schematics;
    }

    @Override
    public void addSchematic(Schematic schematic) {
        this.schematics.add(schematic);
    }

    @Override
    public int getForgeEnergy() {
        return this.forgeEnergy;
    }

    @Override
    public void setForgeEnergy(int energy) {
        this.forgeEnergy = energy;
    }
    
    @Override
	public ResourceLocation getTardis() {
		return this.tardis;
	}

	@Override
	public void setTardis(ResourceLocation tardis) {
		this.tardis = tardis;
	}
}
