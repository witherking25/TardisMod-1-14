package net.tardis.mod.sonic.capability;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Swirtzly on 01/03/2020 @ 11:31
 */
public class SonicProvider implements ICapabilitySerializable<CompoundNBT> {

    private ISonic capability;

    public SonicProvider(ISonic capability) {
        this.capability = capability;
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return cap == Capabilities.SONIC_CAPABILITY ? (LazyOptional<T>) LazyOptional.of(() -> capability) : LazyOptional.empty();
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap) {
        return getCapability(cap, null);
    }

    @Override
    public CompoundNBT serializeNBT() {
        return this.capability.serializeNBT();
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.capability.deserializeNBT(nbt);
    }

}