package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class LandingTypeControl extends BaseControl{

	private EnumLandType landType = EnumLandType.UP;
	
	public LandingTypeControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(2 / 16.0F, 2 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if (getConsole() instanceof CoralConsoleTile) {
			return EntitySize.flexible(0.125F, 0.15F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(5 / 16.0F, 4 / 16.0F);
	}
	
	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-12 / 16.0F, 7 / 16.0F, -3 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.5335388604239691, 0.39374999701976776, 0.5526540125111579);

		if (getConsole() instanceof CoralConsoleTile) {
			return new Vector3d(-0.41553599579609124, 0.53125, -0.625);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.509, 0.5, 0.442);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0.2, 0.469, -0.676);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(-0.2667170339295745, 0.625, 0.5812400555777666);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.7645579019481885, 0.375, -0.689489363943395);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(-0.2943457727866642, 0.46875, 0.7714165309469789);
		
		return new Vector3d(-0.5 / 16.0, 5 / 16.0, -15 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!player.world.isRemote) {
			
			this.landType = this.landType == EnumLandType.UP ? EnumLandType.DOWN : EnumLandType.UP;
			
			player.sendStatusMessage(new TranslationTextComponent("message.tardis.control.land_type")
					.appendSibling(new StringTextComponent(getLandType().name()).mergeStyle(TextFormatting.LIGHT_PURPLE)), true); //TODO: Apply purple text colour to this
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return this.landType == EnumLandType.UP ? TSounds.LANDING_TYPE_UP.get() : TSounds.LANDING_TYPE_DOWN.get();
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.landType = EnumLandType.values()[tag.getInt("land_type")];
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("land_type", this.landType.ordinal());
		return tag;
	}
	
	public void setLandType(EnumLandType type) {
		this.landType = type;
	}
	
	public EnumLandType getLandType() {
		return this.landType;
	}
	
	public static enum EnumLandType{
		UP,
		DOWN
	}

}
