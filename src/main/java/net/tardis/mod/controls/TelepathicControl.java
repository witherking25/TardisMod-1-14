package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicGetStructuresMessage;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.PlayerTelepathicConnection;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;
import net.tardis.mod.upgrades.StructureLocatorUpgrade;

public class TelepathicControl extends BaseControl{

	//private Thread search;
	
	private boolean canUseStructures = false;
	
	public TelepathicControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return EntitySize.fixed(4 / 16.0F, 4 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.375F, 0.375F);
		
        if (this.getConsole() instanceof XionConsoleTile)
             return EntitySize.flexible(0.375F, 0.25F);
        
        if(this.getConsole() instanceof NeutronConsoleTile)
        	return EntitySize.flexible(0.3125F, 0.3125F);
        
        if(this.getConsole() instanceof KeltConsoleTile)
        	return EntitySize.flexible(0.25F, 0.25F);
        
		return EntitySize.fixed(6 / 16.0F, 4 / 16.0F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!player.isSneaking() && console.getWorld().isRemote) {
			ClientHelper.openGUI(Constants.Gui.TELEPATHIC, null);
		}
		else if (!console.getWorld().isRemote()) {
			console.getUpgrade(StructureLocatorUpgrade.class).ifPresent(structure -> {
				if (structure.isActivated() && structure.isUsable()) {
					this.canUseStructures = true;
				}else {
					this.canUseStructures = false;
				}
			});
			if (player.isSneaking()) {
				PlayerTelepathicConnection connection = new PlayerTelepathicConnection(console, player, 13000);
				console.getEmotionHandler().addConnectedPlayer(connection);
				player.sendStatusMessage(new TranslationTextComponent("message.tardis.telepathic.connected"), true);
			}
			else {
				console.getControl(TelepathicControl.class).ifPresent(tele -> {
					if (tele.canSeeStructures()) {
						Network.sendTo(TelepathicGetStructuresMessage.create(player.getServer(), player.getEntityWorld()), (ServerPlayerEntity)player);
					}
				});
			}
		}
		
		this.startAnimation();
		
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(11 / 16.0, 7 / 16.0, 6 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.4108330938524687, 0.59375, -0.2363353443240117);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.8145038135858447, 0.40625, 0.005);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.006, 0.438, -0.654);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0.516, 0.469, 0.316);
		
        if (this.getConsole() instanceof XionConsoleTile)
        	return new Vector3d(0.007514401909703539, 0.21875, -0.6239749761798724);
        
        if(this.getConsole() instanceof NeutronConsoleTile)
        	return new Vector3d(0.6490099209678046, 0.59375, -1.0235097045412442);
        
        if(this.getConsole() instanceof KeltConsoleTile)
        	return new Vector3d(1.0653904217105583, 0.28125, -0.20452082946839545);
        
		return new Vector3d(11 / 16.0, 7 / 16.0, 6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.TELEPATHIC_CIRCUIT.get();
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("structures", this.canUseStructures);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.canUseStructures = nbt.getBoolean("structures");
	}

	public boolean canSeeStructures() {
		return this.canUseStructures;
	}

}
