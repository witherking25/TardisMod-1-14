package net.tardis.mod.contexts.gui;

import net.minecraft.entity.Entity;
import net.tardis.mod.misc.GuiContext;

public class EntityContext extends GuiContext{

	public Entity entity;
	
	public EntityContext(Entity ent) {
		this.entity = ent;
	}
}
