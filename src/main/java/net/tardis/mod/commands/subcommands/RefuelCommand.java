package net.tardis.mod.commands.subcommands;

import java.util.Collections;
import java.util.Optional;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.ArtronUse;

public class RefuelCommand extends TCommand{
	
	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int checkTardisFuel(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		return checkTardisFuel(context, player.getServerWorld());
	}

	private static int checkTardisFuel(CommandContext<CommandSource> context, ServerWorld world){
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
        CommandSource source = context.getSource();
        console.ifPresent(tile -> {
        	tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {

				TextComponent artronUsesTextComponent = new StringTextComponent("");
				artronUsesTextComponent.modifyStyle(style -> {
					style.setFormatting(TextFormatting.WHITE);
					style.setHoverEvent(null);
					style.setClickEvent(null);
					return style;
				});
				
				IFormattableTextComponent totalFuel = new StringTextComponent(Float.toString(tile.getArtron())).appendSibling(Constants.Suffix.ARTRON_UNITS);
				IFormattableTextComponent maxFuel = new StringTextComponent(Float.toString(tile.getMaxArtron())).appendSibling(Constants.Suffix.ARTRON_UNITS);

				for (ArtronUse use : tile.getArtronUses().values()) {
					if (use == null)
						break;
                    TranslationTextComponent useName = use.getType().getTranslation();
					float usePerTick = use.getArtronUsePerTick() * 20;
					
					StringTextComponent artronUseValue = new StringTextComponent(" - " + useName.getString() + ": " + usePerTick + " AU/s" + "\n");
					artronUsesTextComponent
							.appendSibling(artronUseValue);
				}
				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);

				source.sendFeedback(new TranslationTextComponent("command.tardis.refuel.check", tardisIdentifier, totalFuel, maxFuel, artronUsesTextComponent), true);
    		});
        });
        return Command.SINGLE_SUCCESS;
    }

	private static int addFuel(CommandContext<CommandSource> context, ServerPlayerEntity player, int value) {
		return addFuel(context, player.getServerWorld(), player, value);
	}

	private static int addFuel(CommandContext<CommandSource> context, ServerWorld world, ServerPlayerEntity player, int value) {
		Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
		CommandSource source = context.getSource();
		console.ifPresent(tile -> {
			tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				tile.setArtron(tile.getArtron() + (float)value);

				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);

				source.sendFeedback(new TranslationTextComponent("command.tardis.refuel.success", tardisIdentifier, tile.getArtron()), true);
			});
		});
		return Command.SINGLE_SUCCESS;
	}
	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("refuel")
                .requires(context -> context.hasPermissionLevel(2))
                .then(Commands.literal("check")
                        .executes(context -> checkTardisFuel(context, context.getSource().asPlayer()))
                        .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                                .executes(context -> checkTardisFuel(context, DimensionArgument.getDimensionArgument(context, "tardis")))
                        )
                ) // end check
                .then(Commands.literal("add")
                        .then(Commands.argument("value", IntegerArgumentType.integer())
								//if no tardis name specified, then add to the current dimension tardis
                                .executes(context -> addFuel(context, context.getSource().asPlayer(), IntegerArgumentType.getInteger(context, "value"))
                                )//end above
								//else if a tardis name is specified, add it to them
                                .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                                        .executes(context -> addFuel(context, DimensionArgument.getDimensionArgument(context, "tardis"), context.getSource().asPlayer(), IntegerArgumentType.getInteger(context, "value"))
                                        )
                                )//end refuel add to specific tardis
                        )//end value
                ); // end add
    }

}
