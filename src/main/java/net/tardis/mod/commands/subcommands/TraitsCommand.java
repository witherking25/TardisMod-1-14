package net.tardis.mod.commands.subcommands;

import java.util.Collections;
import java.util.Optional;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.traits.TardisTrait;

public class TraitsCommand extends TCommand{
	
	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	private static int checkTardisTraits(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		return checkTardisTraits(context, player.getServerWorld());
	}
	
	private static int checkTardisTraits(CommandContext<CommandSource> context, ServerWorld world){
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
        CommandSource source = context.getSource();
        console.ifPresent(tile -> {
        	tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {

        		StringBuilder traits = new StringBuilder();

				// for each loop
				TardisTrait[] tardisTraits = tile.getEmotionHandler().getTraits();
				for (TardisTrait trait : tardisTraits)
				{
					if (trait == null)
						continue;
					traits.append(trait.getType().getRegistryName().toString()).append("\n");
				}
				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);
				source.sendFeedback(new TranslationTextComponent("command.tardis.traits.check", tardisIdentifier, traits.toString()), true);
    		});
        });
        return Command.SINGLE_SUCCESS;
    }
	
	private static int regenerateTardisTraits(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		return regenerateTardisTraits(context, player.getServerWorld());
	}
	
	private static int regenerateTardisTraits(CommandContext<CommandSource> context, ServerWorld world){
        Optional<ConsoleTile> console = TardisHelper.getConsoleInWorld(world);
        CommandSource source = context.getSource();
        console.ifPresent(tile -> {
        	tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {

				tile.getEmotionHandler().regenerateTraits();

        		StringBuilder traits = new StringBuilder();

				TardisTrait[] tardisTraits = tile.getEmotionHandler().getTraits();
				for (TardisTrait trait : tardisTraits){
					if (trait != null)
					    traits.append(trait.getType().getRegistryName().toString()).append("\n");
				}
				TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world, data);
				source.sendFeedback(new TranslationTextComponent("command.tardis.traits.regenerate", tardisIdentifier, traits.toString()), true);
    		});
        });
        return Command.SINGLE_SUCCESS;
    }

	
	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher){
		return Commands.literal("traits").requires(context -> context.hasPermissionLevel(2))
				.then(Commands.literal("check")
				    .executes(context -> checkTardisTraits(context, context.getSource().asPlayer()))
					.then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
						.executes(context -> checkTardisTraits(context, DimensionArgument.getDimensionArgument(context, "tardis")))
					     ) //End Tardis parameter
					) //End Check command
				.then(Commands.literal("regenerate")
				    .executes(context -> regenerateTardisTraits(context, context.getSource().asPlayer()))
					.then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
					    .executes(context -> regenerateTardisTraits(context, DimensionArgument.getDimensionArgument(context, "tardis")))
						)//End tardis paramater
				     );//End regenerate trait command
	}

}