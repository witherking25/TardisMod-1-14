package net.tardis.mod.loottables;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.Helper;

public class TardisLootTables {

	public static final ResourceLocation SPACESTATION_DRONE = Helper.createRL("chests/spacestation_drone");
	public static final ResourceLocation OBSERVATORY_LOOT = Helper.createRL("chests/observatory");
	public static final ResourceLocation CRASHED_SHIP = Helper.createRL("chests/crashed_ship");
}
