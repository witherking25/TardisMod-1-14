package net.tardis.mod.missions.misc;

import javax.annotation.Nullable;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;

public class DialogOption {

	Dialog next;
	TranslationTextComponent trans;
	DialogAction option;
	
	public DialogOption(@Nullable Dialog dia, TranslationTextComponent trans) {
		this.next = dia;
		this.trans = trans;
	}
	
	public DialogOption(@Nullable Dialog dia, String trans) {
		this(dia, new TranslationTextComponent(trans));
	}
	
	public TranslationTextComponent getTranslation() {
		return this.trans;
	}
	
	public DialogOption setOptionAction(DialogAction action) {
		this.option = action;
		return this;
	}
	
	@Nullable
	public Dialog onClick(LivingEntity entity, PlayerEntity player) {
		if(this.option != null)
			this.option.doAction(entity, player);
		return this.next;
	}
	
	public static interface DialogAction{
		void doAction(LivingEntity speaker, PlayerEntity player);
	}
}
