package net.tardis.mod.missions;

import java.util.function.BiFunction;

import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class MiniMissionType extends ForgeRegistryEntry<MiniMissionType>{

	private IMissionBuilder<MiniMission> supplier;
	private BiFunction<ServerWorld, BlockPos, BlockPos> spawner;
	private RegistryKey<StructureFeature<?, ?>> structure;
	
	/**
	 * New Constructor that takes a Structure
	 * @param supplier
	 * @param spawner
	 * @param structure
	 */
	public MiniMissionType(IMissionBuilder<MiniMission> supplier, BiFunction<ServerWorld, BlockPos, BlockPos> spawner, RegistryKey<StructureFeature<?, ?>> structure) {
		this.supplier = supplier;
		this.spawner = spawner;
		this.structure = structure;
	}
	
	/**
	 * New Constructor that takes a Configured Structure (StructureFeature)
	 * @param supplier
	 * @param structure
	 */
	public MiniMissionType(IMissionBuilder<MiniMission> supplier, RegistryKey<StructureFeature<?, ?>> structure) {
		this(supplier, null, structure);
	}
	
	/**
	 * Legacy Constructor
	 * @param supplier
	 * @param spawner
	 */
	public MiniMissionType(IMissionBuilder<MiniMission> supplier, BiFunction<ServerWorld, BlockPos, BlockPos> spawner) {
		this(supplier, spawner, null);
	}
	
	public MiniMission create(World world, BlockPos pos, int range) {
		return this.supplier.create(world, pos, range);
	}
	
	public BlockPos spawnMissionStage(ServerWorld world, BlockPos pos) {
		return this.spawner.apply(world, pos);
	}
	
	/**
	 * Spawns structure associated with this mission
	 * @param world
	 * @param structureManager
	 * @param pos
	 * @param searchRadius
	 * @param skipExistingChunks
	 * @param seed
	 * @param separationSettings
	 * @return
	 */
	public BlockPos spawnMissionStage(ServerWorld world, StructureManager structureManager, BlockPos pos, int searchRadius, boolean skipExistingChunks, long seed, StructureSeparationSettings separationSettings) {
		return world.getServer().getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOptionalValue(this.structure).get().field_236268_b_.func_236388_a_(world, structureManager , pos, searchRadius, skipExistingChunks, seed, separationSettings);
	}
	
	public RegistryKey<StructureFeature<?, ?>> getStructureKey(){
		return this.structure;
	}
	
	public static interface IMissionBuilder<T extends MiniMission>{
		T create(World world, BlockPos pos, int range);
	}
	
}
