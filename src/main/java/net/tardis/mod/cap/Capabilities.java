package net.tardis.mod.cap;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.items.IDiagnostic;
import net.tardis.mod.cap.items.IRemote;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.sonic.capability.ISonic;

public class Capabilities {

    @CapabilityInject(ILightCap.class)
    public static final Capability<ILightCap> LIGHT = null;

    @CapabilityInject(IChunkLoader.class)
    public static final Capability<IChunkLoader> CHUNK_LOADER = null;

    @CapabilityInject(ITardisWorldData.class)
    public static final Capability<ITardisWorldData> TARDIS_DATA = null;

    @CapabilityInject(IVortexCap.class)
    public static final Capability<IVortexCap> VORTEX_MANIP = null;

    @CapabilityInject(ISonic.class)
    public static final Capability<ISonic> SONIC_CAPABILITY = null;

    @CapabilityInject(IWatch.class)
    public static final Capability<IWatch> WATCH_CAPABILITY = null;

    @CapabilityInject(IRemote.class)
    public static final Capability<IRemote> REMOTE_CAP = null;

    @CapabilityInject(IPlayerData.class)
    public static final Capability<IPlayerData> PLAYER_DATA = null;

    @CapabilityInject(IDiagnostic.class)
    public static final Capability<IDiagnostic> DIAGNOSTIC = null;

    @CapabilityInject(IRift.class)
    public static final Capability<IRift> RIFT = null;

    @CapabilityInject(IMissionCap.class)
    public static final Capability<IMissionCap> MISSION = null;
}
