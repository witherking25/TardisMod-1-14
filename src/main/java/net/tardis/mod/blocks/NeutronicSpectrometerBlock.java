package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.SpectrometerContainer;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;

public class NeutronicSpectrometerBlock extends TileBlock {

    public NeutronicSpectrometerBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

        TileEntity te = worldIn.getTileEntity(pos);
        if(!worldIn.isRemote && te instanceof NeutronicSpectrometerTile){
        	NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {

                @Override
                public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity ent) {
                    return new SpectrometerContainer(id, playerInv, (NeutronicSpectrometerTile)te);
                }

                @Override
                public ITextComponent getDisplayName() {
                    return new TranslationTextComponent("container." + Tardis.MODID + ".spectrometer");
                }
            }, buf -> buf.writeBlockPos(pos));
        }

        return ActionResultType.SUCCESS;
    }
}
