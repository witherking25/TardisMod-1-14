package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants.Gui;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class MonitorRotateBlock extends MonitorBlock {

    private static final VoxelShape SHAPE = VoxelShapes.create(0.25, 0, 0.25, 0.75, 1, 0.75);


    public MonitorRotateBlock(Properties properties) {
        super(properties, Gui.MONITOR_MAIN_ROTATE, 0, 0, 0, 0, 0);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote && player.isSneaking()) {
            float rot = 0F;

            Vector3d p = player.getPositionVec().subtract(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5).normalize();

            float hype = (float) Math.sqrt(p.x * p.x + p.z * p.z);

            if (p.z < 0)
                rot = (float) Math.toDegrees(Math.asin(p.x / hype));
            else rot = -(float) Math.toDegrees(Math.asin(p.x / hype)) - 180;

            ((RotateMonitorTile) worldIn.getTileEntity(pos)).setRotation(rot);
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }
}
