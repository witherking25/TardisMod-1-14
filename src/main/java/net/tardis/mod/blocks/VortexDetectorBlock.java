package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.properties.Prop;

public class VortexDetectorBlock extends TileBlock {

	public static final BooleanProperty POWERED = BooleanProperty.create("powered");
	public static final BooleanProperty INVERTED = BooleanProperty.create("inverted");
	
	public VortexDetectorBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
		this.setDefaultState(this.getDefaultState().with(POWERED, false).with(INVERTED, false));
	}

	
	
	@Override
	public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(handIn == Hand.MAIN_HAND)
			Helper.cycleBlockStateProperty(state, INVERTED);
		return ActionResultType.SUCCESS;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		return VoxelShapes.create(0, 0, 0, 1, 0.375, 1);
	}

	@Override
	public int getWeakPower(BlockState blockState, IBlockReader blockAccess, BlockPos pos, Direction side) {
		return this.getPower(blockState);
	}

	@Override
	public int getStrongPower(BlockState blockState, IBlockReader blockAccess, BlockPos pos, Direction side) {
		return this.getPower(blockState);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(POWERED, INVERTED);
	}
	
	public int getPower(BlockState state) {
		return state.get(POWERED) ? 15 : 0;
		
	}

}
