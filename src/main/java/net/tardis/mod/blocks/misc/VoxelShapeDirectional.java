package net.tardis.mod.blocks.misc;

import net.minecraft.util.Direction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.tardis.mod.helper.VoxelShapeUtils;

public class VoxelShapeDirectional {

    private VoxelShape defaultShape;
    private VoxelShape eastShape;
    private VoxelShape southShape;
    private VoxelShape westShape;

    public VoxelShapeDirectional(VoxelShape shape) {
        this.defaultShape = shape;
        this.eastShape = VoxelShapeUtils.rotateHorizontal(shape, Direction.EAST);
        this.southShape = VoxelShapeUtils.rotateHorizontal(shape, Direction.SOUTH);
        this.westShape = VoxelShapeUtils.rotateHorizontal(shape, Direction.WEST);
    }

    public VoxelShape getFor(Direction dir) {
        switch (dir) {
            case EAST:
                return eastShape;
            case SOUTH:
                return southShape;
            case WEST:
                return westShape;
            default:
                return this.defaultShape;
        }
    }
}
