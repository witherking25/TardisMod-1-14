package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.tardis.mod.sounds.TSounds;

public class BookStackBlock extends Block{


    public static final VoxelShape SHAPE = Block.makeCuboidShape(3.0D, 0.0D, 3.0D, 14.0D, 6.0D, 14.0D);
    public BookStackBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).notSolid().setSuffocates((blockState, p_test_2_, blockPos) -> false).hardnessAndResistance(hardness, resistance));
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

    @Override
    public void onPlayerDestroy(IWorld worldIn, BlockPos pos, BlockState state) {
        worldIn.playSound(null, pos, TSounds.PAPER_DROP.get(), SoundCategory.BLOCKS, 1, 1);
        worldIn.setBlockState(pos, TBlocks.clutter_paper.get().getDefaultState(), 0);

    }
}
