package net.tardis.mod.blocks;


import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.helper.VoxelShapeUtils;

public class CrashChairBlock extends ChairBlock {

    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public CrashChairBlock(float yOffset) {
        super(yOffset);
    }

    public static VoxelShape createVoxelShape() {
        VoxelShape shape = VoxelShapes.create(0.359375, -0.0046875, 0.359375, 0.640625, 0.0890625, 0.640625);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.40625, 0.0234375, 0.40625, 0.59375, 0.1171875, 0.59375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.4625, 0.1171875, 0.453125, 0.55625, 0.3046875, 0.546875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.40625, 0.2578125, 0.40625, 0.59375, 0.3515625, 0.59375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.3125, 0.0, 0.3125, 0.40625, 0.046875, 0.40625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.3125, 0.0, 0.59375, 0.40625, 0.046875, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.59375, 0.0, 0.59375, 0.6875, 0.046875, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.59375, 0.0, 0.3125, 0.6875, 0.046875, 0.40625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.465625, 0.21875, 0.96875, 0.559375, 0.40625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.465625, 0.453125, 0.96875, 0.559375, 0.640625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.465625, -0.015625, 0.96875, 0.559375, 0.171875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0875, 0.4375, -0.053125, 0.93125, 0.53125, 0.696875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.275, 0.34375, 0.040625, 0.74375, 0.4375, 0.790625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.18125, 0.4375, -0.146875, 0.8375, 0.53125, -0.053125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 0.316594375, 0.646238125, 1.015625, 0.504094375, 0.833738125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 0.53125, 0.646238125, 1.015625, 0.71875, 0.833738125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 0.71875, 0.703125, 1.015625, 0.90625, 0.890625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 0.90625, 0.765625, 1.015625, 1.09375, 0.953125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 1.09375, 0.84375, 1.015625, 1.28125, 1.03125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.015625, 1.28125, 0.921875, 1.015625, 1.46875, 1.109375), IBooleanFunction.OR);
        return shape;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.HORIZONTAL_FACING)) {
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case WEST:
                return WEST;
            default:
                return NORTH;
        }
    }


}
