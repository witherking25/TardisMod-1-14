package net.tardis.mod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.AlembicContainer;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicBlock extends TileBlock {

    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public AlembicBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.getDefaultState().with(BlockStateProperties.ENABLED, false);
    }

    public static VoxelShape createVoxelShape() {
        VoxelShape shape = VoxelShapes.create(0.578125, 0.515625, 0.3375, 0.890625, 0.578125, 0.65);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.90625, 0.0, 0.28125, 0.96875, 0.25, 0.34375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.90625, 0.0, 0.65625, 0.96875, 0.25, 0.71875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.0, 0.65625, 0.09375, 0.25, 0.71875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.0, 0.28125, 0.09375, 0.25, 0.34375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.203125, 0.25, 1.0, 0.265625, 0.75), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.46875, 0.1, 0.28125, 0.53125, 0.1625, 0.34375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.0625, 0.3125, 0.9375, 0.25, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.671875, 0.75, 0.43125, 0.796875, 0.8125, 0.55625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.546875, 0.265625, 0.3125, 0.921875, 0.453125, 0.6875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.921875, 0.34375, 0.484375, 0.953125, 0.375, 0.515625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.953125, 0.328125, 0.46875, 0.984375, 0.390625, 0.53125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.609375, 0.421875, 0.36875, 0.859375, 0.484375, 0.61875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.578125, 0.484375, 0.3375, 0.890625, 0.546875, 0.65), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.578125, 0.721875, 0.3375, 0.890625, 0.784375, 0.65), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.640625, 0.796875, 0.4, 0.828125, 0.984375, 0.5875), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.546875, 0.5, 0.30625, 0.921875, 0.75, 0.68125), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.03125, 0.265625, 0.453125, 0.09375, 0.578125, 0.515625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.34375, 0.265625, 0.453125, 0.40625, 0.578125, 0.515625), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.09375, 0.375, 0.359375, 0.34375, 0.625, 0.609375), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.34375, 0.390625, 0.3125, 0.40625, 0.578125), IBooleanFunction.OR);
        return shape;
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        BlockState state = super.getStateForPlacement(context);
        return state.with(BlockStateProperties.HORIZONTAL_FACING, context.getPlacementHorizontalFacing().getOpposite()).with(BlockStateProperties.ENABLED, false);
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
        builder.add(BlockStateProperties.ENABLED);
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
    	TileEntity tileEntity = worldIn.getTileEntity(pos); 
		 if (tileEntity instanceof AlembicTile && state.getBlock() != newState.getBlock()) {
			 TInventoryHelper.dropInventoryItems(worldIn, pos, ((AlembicTile) tileEntity).getItemStackHandler());
			 worldIn.removeTileEntity(pos);
		 }
		 super.onReplaced(state, worldIn, pos, newState, isMoving);
    }

    //	WIP
    @Override
    @OnlyIn(Dist.CLIENT)
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (stateIn.get(BlockStateProperties.ENABLED)) {
            Direction facing = stateIn.get(BlockStateProperties.HORIZONTAL_FACING);
            double d1 = rand.nextDouble() * 0.2D;
            double x = (double) pos.getX() + 0.4D;
            double y = (double) pos.getY() + 1 + rand.nextFloat() * 0.3D;
            double z = (double) pos.getZ() + 0.4D;
            switch (facing) {
                case NORTH:
                    x += d1 + 0.2D;
                    z += 0.1D;
                    break;
                case EAST:
                    x += 0.1D;
                    z += d1 + 0.2D;
                    break;
                case SOUTH:
                    x -= d1 + 0.2D;
                    z += 0.2D;
                case WEST:
                    x += 0.1D;
                    z -= d1;
                    break;
                default:
                    break;
            }
            worldIn.addParticle(ParticleTypes.SMOKE, x, y, z, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.HORIZONTAL_FACING)) {
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case WEST:
                return WEST;
            default:
                return NORTH;
        }
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }


    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {

                @Override
                public Container createMenu(int id, PlayerInventory inv, PlayerEntity p_createMenu_3_) {
                    return new AlembicContainer(id, inv, (AlembicTile) worldIn.getTileEntity(pos));
                }

                @Override
                public ITextComponent getDisplayName() {
                    return new TranslationTextComponent("container." + Tardis.MODID + ".alembic");
                }
            }, pos);
        }
        return ActionResultType.SUCCESS;
    }

}
