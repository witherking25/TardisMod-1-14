package net.tardis.mod.ars;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.mojang.serialization.Codec;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.item.LeashKnotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.ConsoleRoomCodecListener;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ReclamationTile;

public class ConsoleRoom {
	
	/**
	 * The Codec used for serialising/deserialising of ConsoleRooms
	 * <br> RecordCodecBuilder automatically allows for Java -> Json, and Json -> Java conversion
	 */
	private static final Codec<ConsoleRoom> CODEC = RecordCodecBuilder.create(instance -> {
		return instance.group(
			ResourceLocation.CODEC.fieldOf("schematic_resource_location").forGetter(ConsoleRoom::getStructureFilePath),
			Codec.STRING.fieldOf("display_name").forGetter( name -> name.getDisplayName().getString()),
			ResourceLocation.CODEC.orElse(new ResourceLocation(Tardis.MODID,"textures/gui/interiors/generic_preview.png")).fieldOf("image_resource_location").forGetter(ConsoleRoom::getTexture),
			Codec.INT.fieldOf("x_offset").forGetter(offset -> offset.getOffset().getX()),
			Codec.INT.fieldOf("y_offset").forGetter(offset -> offset.getOffset().getY()),
			Codec.INT.fieldOf("z_offset").forGetter(offset -> offset.getOffset().getZ()),
			Codec.BOOL.orElse(false).optionalFieldOf("using_remote_image").forGetter(ConsoleRoom::isUsingRemoteImage),
			Codec.STRING.orElse("").optionalFieldOf("remote_image").forGetter(ConsoleRoom::getImageUrl)
		).apply(instance, ConsoleRoom::new);
	});
	
	/**
	 * The data loader which contains both Datapack entries and static initialised entries in its data
	 * <br> To access the actual Map of key-value pairs, use {@link ConsoleRoom#getRegistry}
	 */
	public static final ConsoleRoomCodecListener DATA_LOADER = new ConsoleRoomCodecListener(
	        "console_rooms",
	        CODEC,
	        Tardis.LOGGER);
	
	public static ConsoleRoom STEAM;
	public static ConsoleRoom JADE;
	public static ConsoleRoom NAUTILUS;
	public static ConsoleRoom OMEGA;
	public static ConsoleRoom ALABASTER;
	public static ConsoleRoom CORAL;
	public static ConsoleRoom METALLIC;
	public static ConsoleRoom ARCHITECT;
	public static ConsoleRoom TOYOTA;
	public static ConsoleRoom PANAMAX;
	public static ConsoleRoom TRAVELER;
	public static ConsoleRoom ENVOY;
	public static ConsoleRoom AMETHYST;
	public static ConsoleRoom IMPERIAL;

	//Broken exteriors
	public static ConsoleRoom ABANDONED_STEAM;
	public static ConsoleRoom ABANDONED_PANAMAX;
	public static ConsoleRoom ABANDONED_ALABASTER;
	public static ConsoleRoom ABANDONED_JADE;
	public static ConsoleRoom ABANDONED_NAUTILUS;
	public static ConsoleRoom ABANDONED_IMPERIAL;
	
	//Debug Use Only
	public static ConsoleRoom DEBUG_CLEAN;

	private ResourceLocation registryName;
	private ResourceLocation texture;
	private BlockPos offset;
	private ResourceLocation filePath;
	private boolean isDatapack = false; //If this is a datapack entry. May be useful for modifying behaviour of datapack entries
	private boolean has_remote_url = false;
	private String remote_url = "";
	@Nullable
	private String translationKey;

	public ConsoleRoom(ResourceLocation filePath, BlockPos offset, ResourceLocation texture, String translationKey, boolean isDatapack, String imageUrl, boolean has_remote_url) {
		this.has_remote_url = has_remote_url;
		this.remote_url = imageUrl;
		this.offset = offset;
		this.filePath = filePath;
		this.texture = texture;
		this.translationKey = translationKey;
		this.isDatapack = isDatapack;
	}
	
	/**
     * Constructor for datapack json structure
     * @param filePath
     * @param translationKey
     * @param texture
     * @param x
     * @param y
     * @param z
     * @param has_remote_url
     * @param imageUrl
     */
    public ConsoleRoom(ResourceLocation filePath, String translationKey, ResourceLocation texture, int x, int y, int z, Optional<Boolean> has_remote_url, Optional<String> imageUrl) {
    	this.has_remote_url = has_remote_url.get();
		this.remote_url = imageUrl.get();
		this.offset = new BlockPos(x, y, z);
		this.filePath = filePath;
		this.texture = texture;
		this.translationKey = translationKey;
		this.isDatapack = true;
    }

    /**
     * Convenience Constructor for non-datapack entries
     *
     * @param offset
     * @param fileName
     * @param texture
     */
    public ConsoleRoom(BlockPos offset, String fileName, String texture) {
        this(new ResourceLocation(Tardis.MODID, fileName), offset,
                new ResourceLocation(Tardis.MODID, "textures/gui/interiors/" + texture + ".png"),
                "interiors.tardis." + fileName, false, "", false);
    }

    public static ConsoleRoom register(ConsoleRoom room, ResourceLocation registryName) {
    	DATA_LOADER.getData().put(registryName, room.setRegistryName(registryName));
        if (registryName == null)
            throw new NullPointerException();
        return room;
    }

    private static ConsoleRoom register(ConsoleRoom room, String registryName) {
        return ConsoleRoom.register(room, new ResourceLocation(Tardis.MODID, registryName));
    }

    public static Map<ResourceLocation, ConsoleRoom> registerCoreConsoleRooms() {
        ConsoleRoom.DATA_LOADER.getData().clear();
        Tardis.LOGGER.log(Level.INFO, "Cleared ConsoleRoom Registry!");
        ConsoleRoom.STEAM = register(new ConsoleRoom(new BlockPos(13, 11, 30), "interior_steam", "steam"), "steam");
        ConsoleRoom.JADE = register(new ConsoleRoom(new BlockPos(17, 8, 30), "interior_jade", "jade"), "jade");
        ConsoleRoom.NAUTILUS = register(new ConsoleRoom(new BlockPos(17, 17, 30), "interior_nautilus", "nautilus"), "nautilus");
        ConsoleRoom.OMEGA = register(new ConsoleRoom(new BlockPos(14, 15, 30), "interior_omega", "omega"), "omega"); //Enable this as a test
        ConsoleRoom.ALABASTER = register(new ConsoleRoom(new BlockPos(15, 8, 30), "interior_alabaster", "alabaster"), "alabaster");
        ConsoleRoom.ARCHITECT = register(new ConsoleRoom(new BlockPos(14, 12, 30), "interior_architect", "architect"), "architect");
        ConsoleRoom.CORAL = register(new ConsoleRoom(new BlockPos(11, 11, 30), "interior_coral", "coral"), "coral");
        ConsoleRoom.PANAMAX = register(new ConsoleRoom(new BlockPos(14, 11, 30), "interior_panamax", "panamax"), "panamax");
        ConsoleRoom.TOYOTA = register(new ConsoleRoom(new BlockPos(20, 7, 30), "interior_toyota", "toyota"), "toyota");
        ConsoleRoom.TRAVELER = register(new ConsoleRoom(new BlockPos(14, 8, 30), "interior_traveler", "traveler"), "traveler");
        ConsoleRoom.ENVOY = register(new ConsoleRoom(new BlockPos(19, 3, 30), "interior_envoy", "envoy"), "envoy");

        ConsoleRoom.AMETHYST = register(new ConsoleRoom(new BlockPos(17, 8, 30), "interior_amethyst", "amethyst"), "amethyst");
        ConsoleRoom.IMPERIAL = register(new ConsoleRoom(new BlockPos(13, 6, 30), "interior_imperial", "imperial"), "imperial");
        
        ConsoleRoom.DEBUG_CLEAN = register(new ConsoleRoom(new BlockPos(2, 1, 4), "interior_debug_clean", "debug"), "debug");

        ConsoleRoom.ABANDONED_STEAM = register(new ConsoleRoom(new BlockPos(13, 11, 30), "interior_abandoned_steam", "abandoned_steam"), "abandoned_steam");
        ConsoleRoom.ABANDONED_PANAMAX = register(new ConsoleRoom(new BlockPos(14, 11, 30), "interior_abandoned_panamax", "abandoned_panamax"), "abandoned_panamax");
        ConsoleRoom.ABANDONED_NAUTILUS = register(new ConsoleRoom(new BlockPos(17, 17, 30), "interior_abandoned_nautilus", "abandoned_nautilus"), "abandoned_nautilus");
        ConsoleRoom.ABANDONED_ALABASTER = register(new ConsoleRoom(new BlockPos(15, 8, 30), "interior_abandoned_alabaster", "abandoned_alabaster"), "abandoned_alabaster");
        ConsoleRoom.ABANDONED_IMPERIAL = register(new ConsoleRoom(new BlockPos(13, 6, 30), "interior_abandoned_imperial", "abandoned_imperial"), "abandoned_imperial");
        ConsoleRoom.ABANDONED_JADE = register(new ConsoleRoom(new BlockPos(17, 8, 30), "interior_abandoned_jade", "abandoned_jade"), "abandoned_jade");
        DATA_LOADER.getData().values().forEach(interior -> {

            Tardis.LOGGER.log(Level.INFO, "Registered core Console Room: " + interior.getRegistryName());
        });
        return DATA_LOADER.getData();
    }

    public static Codec<ConsoleRoom> getCodec(){
    	return CODEC;
    }

    public static Map<ResourceLocation, ConsoleRoom> getRegistry() {
        return DATA_LOADER.getData();
    }

    public JsonElement toJson() {
    	return CODEC.encodeStart(JsonOps.INSTANCE, this).result().get();
    }
    
    public static void readFromJson(JsonElement json, Consumer<String> logMessageOnBadData, Consumer<ConsoleRoom> onGoodData) {
    	CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(logMessageOnBadData).ifPresent(onGoodData);
    }
    
    public CompoundNBT serialize() {
        CompoundNBT tag = new CompoundNBT();
        CODEC.encodeStart(NBTDynamicOps.INSTANCE, this).resultOrPartial(Tardis.LOGGER::error).ifPresent(data -> {
           tag.put("console_room", data);	
        });
        return tag;
    }

    public static ConsoleRoom deserialize(CompoundNBT tag) {
    	ConsoleRoom interior = CODEC.parse(new Dynamic<>(NBTDynamicOps.INSTANCE)).resultOrPartial(Tardis.LOGGER::error).get();
    	return interior;
    }

    public ResourceLocation getRegistryName() {
        return registryName;
    }

    public ConsoleRoom setRegistryName(ResourceLocation name) {
        this.registryName = name;
        return this;
    }
    
    public ResourceLocation getStructureFilePath() {
    	return this.filePath;
    }
    
    public BlockPos getOffset() {
    	return this.offset;
    }

    public ResourceLocation getTexture() {
        return this.texture;
    }
    
    /** Get the string based key used for the display name*/
    public String getTranslationKey() {
    	if (this.translationKey == null) {
    		this.translationKey = Util.makeTranslationKey("interiors", this.getRegistryName());
    	}
    	return this.translationKey;
    }

    public TranslationTextComponent getDisplayName() {
        return new TranslationTextComponent(this.getTranslationKey());
    }

    /**
     * Check if this console room is from a datapack
     *
     * @implSpec If true, render the TranslationTextComponent key in InteriorChangeScreen, else use the formatted text
     */
    public boolean isDataPack() {
        return this.isDatapack;
    }

    public Optional<String> getImageUrl() {
        return Optional.ofNullable(this.remote_url);
    }

    public Optional<Boolean> isUsingRemoteImage() {
        return Optional.ofNullable(this.has_remote_url);
    }
    
	public void spawnConsoleRoom(ServerWorld world, boolean overrideStructure) {
		ConsoleTile console = null;
		CompoundNBT consoleData = null;
		BlockState consoleState = null;
		
		int radius = 30;
		BlockPos clearRadius = new BlockPos(radius, radius, radius);

		//Save the console
		if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
			console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			consoleData = console.serializeNBT();
			consoleState = world.getBlockState(TardisHelper.TARDIS_POS);
		}
		

		boolean hasCorridors = overrideStructure || world.getBlockState(new BlockPos(0, 129, -30)).getBlock() != TBlocks.corridor_spawn.get();
		
		List<ItemStack> allInvs = ARSHelper.fillReclamationUnitAndRemove(world, TardisHelper.TARDIS_POS.subtract(clearRadius), TardisHelper.TARDIS_POS.add(clearRadius));
		

		//Kill all non-living entities
		AxisAlignedBB killBox = new AxisAlignedBB(-radius, -radius, -radius, radius, radius, radius).offset(TardisHelper.TARDIS_POS);
		BlockPos tpPos = TardisHelper.TARDIS_POS.offset(Direction.NORTH);
		for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
			
			if(entity.getTags().contains(Constants.Strings.INTERIOR_KILL_TAG)) //TODO: Document this. Tell people that you can use /tag to add "tardis_interior_change_kill" tag to blacklist entities to kill them when interior change, regardless of type
				entity.remove();
			
			if(!(entity instanceof LivingEntity))
				entity.remove();
			
			if(entity instanceof PlayerEntity) //Keep legacy behaviour if for some reason there are players inside the Tardis when the room spawning occurs
				entity.setPositionAndUpdate(tpPos.getX() + 0.5, tpPos.getY() + 0.5, tpPos.getZ() + 0.5);
			
			if(entity instanceof ArmorStandEntity) {
				for (ItemStack armor : entity.getArmorInventoryList())
					allInvs.add(armor.copy());
				entity.remove();
			}
			if (entity instanceof ItemFrameEntity) {
				allInvs.add(((ItemFrameEntity)entity).getDisplayedItem());
				entity.remove();
			}
			if (entity instanceof LeashKnotEntity) {
				allInvs.add(new ItemStack(Items.LEAD));
				entity.remove();
			}
			
		}

		//Spawn console room
		Template temp = world.getStructureTemplateManager().getTemplate(filePath);
		temp.func_237144_a_(world, TardisHelper.TARDIS_POS.subtract(this.offset), new PlacementSettings().setIgnoreEntities(false), world.rand);

		//Remove End Cap if corridors have already been generated
		if(hasCorridors && !overrideStructure) {
			BlockPos corridor = new BlockPos(0, 129, -30);
			for(int x = -1; x < 2; ++x) {
				for(int y = -1; y < 2; ++y) {
					world.setBlockState(corridor.add(x, y, 0), Blocks.AIR.getDefaultState());
				}
			}
		}
		
		//Re-add console
		if(console != null) {
			world.setBlockState(TardisHelper.TARDIS_POS, consoleState);
			world.getTileEntity(TardisHelper.TARDIS_POS).deserializeNBT(consoleData);
		}

		//Spawn Reclamation Unit
		if(!allInvs.isEmpty()) {
			world.setBlockState(TardisHelper.TARDIS_POS.south(2), TBlocks.reclamation_unit.get().getDefaultState());
			TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS.south(2));
			if(te instanceof ReclamationTile) {
				ReclamationTile unit = (ReclamationTile)te;
				for(ItemStack stack : allInvs) {
					unit.addItemStack(stack);
				}
			}
		}

		//Update lighting
		ChunkPos startPos = world.getChunk(TardisHelper.TARDIS_POS.subtract(temp.getSize())).getPos();
		ChunkPos endPos = world.getChunk(TardisHelper.TARDIS_POS.add(temp.getSize())).getPos();

		for(int x = startPos.x; x < endPos.x; ++x) {
			for(int z = startPos.z; z < endPos.z; ++z) {
				world.getChunkProvider().getLightManager().lightChunk(world.getChunk(x, z), true);
			}
		}

	}
}
