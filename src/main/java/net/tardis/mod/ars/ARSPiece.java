package net.tardis.mod.ars;

import java.util.Map;

import org.apache.logging.log4j.Level;

import com.mojang.serialization.Codec;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.ARSPieceCodecListener;
import net.tardis.mod.tileentities.CorridorKillTile;

public class ARSPiece {
	
	private static final Codec<ARSPiece> CODEC = RecordCodecBuilder.create(instance -> {
		return instance.group(
				ResourceLocation.CODEC.fieldOf("schematic_resource_location").forGetter(ARSPiece::getStructureFilePath),
				Codec.STRING.fieldOf("display_name").forGetter( name -> name.getDisplayName().getString()),
				Codec.INT.fieldOf("x_offset").forGetter(offset -> offset.getOffset().getX()),
				Codec.INT.fieldOf("y_offset").forGetter(offset -> offset.getOffset().getY()),
				Codec.INT.fieldOf("z_offset").forGetter(offset -> offset.getOffset().getZ())
			).apply(instance, ARSPiece::new);
	});
	
	public static final ARSPieceCodecListener DATA_LOADER = new ARSPieceCodecListener(
	        "ars_pieces",
	        CODEC,
	        Tardis.LOGGER);
	
	public static ARSPiece ALABASTER_SHORT;
	public static ARSPiece ALABASTER_LONG;

	public static ARSPiece ALABASTER_FOURWAY;
	public static ARSPiece ALABASTER_T_JUNCTION;
	public static ARSPiece ALABASTER_TURN_RIGHT;
	public static ARSPiece ALABASTER_TURN_LEFT;
	
	public static ARSPiece DARK_SHORT;
	public static ARSPiece DARK_LONG;

	public static ARSPiece DARK_FOURWAY;
	public static ARSPiece DARK_T_JUNCTION;
	public static ARSPiece DARK_TURN_RIGHT;
	public static ARSPiece DARK_TURN_LEFT;


	public static ARSPiece LARGE_ROOM;
	public static ARSPiece LIBRARY;
	public static ARSPiece SMALL_LAB;
	public static ARSPiece LAB;
	public static ARSPiece WORKROOM_1;
	public static ARSPiece WORKROOM_2;
	public static ARSPiece WORKROOM_3;
	public static ARSPiece STOREROOM;
	public static ARSPiece HYDROPONICS;
	public static ARSPiece HYDROPONICS_LARGE;
	public static ARSPiece ARS_TREE;
	public static ARSPiece GRAVITY_SHAFT;
	public static ARSPiece ARCHIVES;
	public static ARSPiece CLOISTER;
	

    private ResourceLocation registryName;
    private ResourceLocation filePath;
    private BlockPos offset;
    private String translationKey;
    private boolean isDatapack = false;

    /**
     * @param filePath
     * @param displayName
     * @param offset
     * @param isDatapack
     * @implSpec Use BlockPos difference between structure Block and the point where the corridor starts
     * @implNote The Tape Measure item calculates the diff 1 block larger for the y and 1 block smaller for the z value.
     * Hence, subtract 1 to your y and add 1 to your z value to allow the structure to spawn successfully
     */
    public ARSPiece(ResourceLocation filePath, String translationKey, BlockPos offset, boolean isDatapack) {
        this.filePath = filePath;
        this.offset = offset;
        this.translationKey = translationKey;
        this.isDatapack = isDatapack;
    }
    
    public ARSPiece(ResourceLocation filePath, String translationKey, int x, int y, int z, boolean isDatapack) {
        this.filePath = filePath;
        this.offset = new BlockPos(x, y, z);
        this.translationKey = translationKey;
        this.isDatapack = isDatapack;
    }
    
    /**
     * Datapack Entry Constructor
     * @param filePath
     * @param translationKey
     * @param offset
     */
    public ARSPiece(ResourceLocation filePath, String translationKey, BlockPos offset) {
        this.filePath = filePath;
        this.offset = offset;
        this.translationKey = translationKey;
        this.isDatapack = true;
    }
    
    /**
     * Datapack Entry Constructor
     * @param filePath
     * @param translationKey
     * @param x
     * @param y
     * @param z
     */
    public ARSPiece(ResourceLocation filePath, String translationKey, int x, int y, int z) {
        this.filePath = filePath;
        this.offset = new BlockPos(x, y, z);
        this.translationKey = translationKey;
        this.isDatapack = true;
    }

    /**
     * Convenience Constructor for non datapack entries
     * @implSpec Use BlockPos difference between structure Block and the point where the corridor starts
     * @implNote The Tape Measure item calculates the diff 1 block larger for the y and 1 block smaller for the z value.
     * Hence, subtract 1 to your y and add 1 to your z value to allow the structure to spawn successfully
     * @param fileName
     * @param translationKey
     * @param x
     * @param y
     * @param z
     */
    public ARSPiece(String fileName, String translationKey, int x, int y, int z) {
        this(new ResourceLocation(Tardis.MODID, "ars/" + fileName), "ars.piece."  + Tardis.MODID  + "." + translationKey, new BlockPos(x, y, z), false);
    }
    
    public ARSPiece(String fileName, String translationKey, BlockPos offset) {
        this(new ResourceLocation(Tardis.MODID, "ars/" + fileName), "ars.piece."  + Tardis.MODID  + "." + translationKey, offset, false);
    }
    
    public static ARSPiece register(ResourceLocation registryName, ARSPiece room) {
    	DATA_LOADER.getData().put(registryName, room.setRegistryName(registryName));
        if (registryName == null)
            throw new NullPointerException();
        return room;
    }

    private static ARSPiece register(String registryName, ARSPiece room) {
        return ARSPiece.register(new ResourceLocation(Tardis.MODID, registryName), room);
    }
    
    private static ARSPiece registerRoom(String registryName, String fileName, String translationKey, BlockPos offset) {
        return ARSPiece.register(new ResourceLocation(Tardis.MODID, "room/" + registryName), new ARSPiece("room/" + fileName,  "room." + translationKey, offset));
    }
    
    private static ARSPiece registerCorridor(String registryName, String fileName, String translationKey, BlockPos offset) {
        return ARSPiece.register(new ResourceLocation(Tardis.MODID, "corridor/" + registryName), new ARSPiece("corridor/" + fileName,  "corridor." + translationKey, offset));
    }
    
    public static Map<ResourceLocation, ARSPiece> registerCoreARSPieces() {
    	ARSPiece.DATA_LOADER.getData().clear();
    	Tardis.LOGGER.log(Level.INFO, "Cleared ARS Registry!");
    	
    	ARSPiece.ALABASTER_SHORT = registerCorridor("alabaster/short", "alabaster/short","alabaster.short", new BlockPos(4, 3, 11));
        ARSPiece.ALABASTER_LONG = registerCorridor("alabaster/long", "alabaster/long","alabaster.long", new BlockPos(4, 3, 26));
    	ARSPiece.ALABASTER_FOURWAY = registerCorridor("alabaster/fourway", "alabaster/fourway","alabaster.fourway", new BlockPos(8, 3, 15));
    	ARSPiece.ALABASTER_T_JUNCTION = registerCorridor("alabaster/t_junction", "alabaster/t_junction","alabaster.t_junction", new BlockPos(8, 3, 11));
    	ARSPiece.ALABASTER_TURN_RIGHT = registerCorridor("alabaster/turn_right", "alabaster/turn_right","alabaster.turn_right", new BlockPos(4, 3, 11));
    	ARSPiece.ALABASTER_TURN_LEFT = registerCorridor("alabaster/turn_left", "alabaster/turn_left","alabaster.turn_left", new BlockPos(8, 3, 11));

    	ARSPiece.DARK_SHORT = registerCorridor("dark/short", "dark/short","dark.short", new BlockPos(4, 3, 11));
    	ARSPiece.DARK_LONG = registerCorridor("dark/long", "dark/long","dark.long", new BlockPos(4, 3, 26));
    	ARSPiece.DARK_FOURWAY = registerCorridor("dark/fourway", "dark/fourway", "dark.fourway", new BlockPos(8, 3, 15));
    	ARSPiece.DARK_T_JUNCTION = registerCorridor("dark/t_junction", "dark/t_junction","dark.t_junction", new BlockPos(8, 3, 11));
    	ARSPiece.DARK_TURN_RIGHT = registerCorridor("dark/turn_right", "dark/turn_right","dark.turn_right", new BlockPos(4, 3, 11));
    	ARSPiece.DARK_TURN_LEFT = registerCorridor("dark/turn_left", "dark/turn_left","dark.turn_left", new BlockPos(8, 3, 11));
    	
    	ARSPiece.LARGE_ROOM = registerRoom("large_room", "large_room", "large_room", new BlockPos(10, 2, 27));
    	
    	ARSPiece.LIBRARY = registerRoom("library", "library", "library", new BlockPos(14, 2, 30));
    	ARSPiece.SMALL_LAB = registerRoom("small_lab", "small_lab", "small_lab", new BlockPos(10, 2, 18));
    	ARSPiece.LAB = registerRoom("lab", "lab", "lab", new BlockPos(10, 3, 24));
    	
    	ARSPiece.WORKROOM_1 = registerRoom("workroom_1", "workroom_1","workroom_1", new BlockPos(6, 3, 13));
    	ARSPiece.WORKROOM_2 = registerRoom("workroom_2", "workroom_2","workroom_2", new BlockPos(7, 3, 15));
    	ARSPiece.WORKROOM_3 = registerRoom("workroom_3", "workroom_3","workroom_3", new BlockPos(7, 3, 15));
    	
    	ARSPiece.STOREROOM = registerRoom("storeroom", "storeroom","storeroom", new BlockPos(3, 3, 7));
    	ARSPiece.HYDROPONICS = registerRoom("hydroponics", "hydroponics","hydroponics", new BlockPos(8, 4, 23));
    	ARSPiece.HYDROPONICS_LARGE = registerRoom("hydroponics_large", "hydroponics_large","hydroponics_large", new BlockPos(13, 5, 27));
    	ARSPiece.ARS_TREE = registerRoom("ars_tree", "ars_tree","ars_tree", new BlockPos(8, 3, 18));
    	ARSPiece.GRAVITY_SHAFT = registerRoom("gravity_shaft", "gravity_shaft","gravity_shaft", new BlockPos(5, 15, 11));
    	ARSPiece.ARCHIVES = registerRoom("archives", "archives","archives", new BlockPos(11, 3, 25));
    	
    	ARSPiece.CLOISTER = registerRoom("cloister", "cloister","cloister", new BlockPos(15, 4, 30));
    	
    	//Test room to test categories and pieces in the same heirachy level
//    	registerRoom("test/cloister", "cloister","cloister", new BlockPos(15, 4, 30));
    	
    	DATA_LOADER.getData().values().forEach(piece -> {

            Tardis.LOGGER.log(Level.INFO, "Registered core ARSPiece: " + piece.getRegistryName());
        });
        return DATA_LOADER.getData();
    }

    public boolean spawn(ServerWorld world, BlockPos pos, PlayerEntity player, Direction dir) {
        Template temp = world.getStructureTemplateManager().getTemplate(filePath);

        //this.offset = new BlockPos(11, 3, 25);

        BlockPos offset = WorldHelper.rotateBlockPos(this.offset, dir);

        if (temp != null) {

            BlockPos spawnStart = pos.subtract(offset);
            BlockPos endPos = spawnStart.add(WorldHelper.rotateBlockPos(temp.getSize().north().west(), dir));
            for (BlockPos check : BlockPos.getAllInBoxMutable(spawnStart, endPos)) {
                if (!world.getBlockState(check).isAir(world, check)) {
                	player.sendStatusMessage(Constants.Translations.CORRIDOR_BLOCKED, false);
                    TextComponent startPosPrint = TextHelper.getBlockPosObject(spawnStart);
                    TextComponent endPosPrint = TextHelper.getBlockPosObject(endPos);
                    TextComponent conflictPos = TextHelper.getBlockPosObject(check);
                    player.sendStatusMessage(new TranslationTextComponent(Constants.Translations.CORRIDOR_BLOCKED_POSITIONS, startPosPrint, endPosPrint, conflictPos), false);
                    System.out.println("Structure tried to start spawning at: " + spawnStart + " and End Pos of: " + endPos + "With Offset of: " + offset + " from Start Pos");
                    System.out.println("Conflict in structure creation detected at: " + check);
                    return false;
                }
            }

            for (int x = -1; x <= 1; ++x) {
                for (int y = -1; y < 3; ++y) {
                    if (dir == Direction.SOUTH || dir == Direction.NORTH)
                        world.setBlockState(pos.add(x, y, 0), Blocks.AIR.getDefaultState());
                    else world.setBlockState(pos.add(0, y, x), Blocks.AIR.getDefaultState());
                }
            }

            PlacementSettings set = new PlacementSettings().setIgnoreEntities(false).setRotation(WorldHelper.getRotationFromDirection(dir));
            //I think this is corret
            //temp.addBlocksToWorld(world, spawnStart, set);
            temp.func_237144_a_(world, spawnStart, set, world.rand);
            for (BlockInfo info : temp.func_215381_a(spawnStart, set, TBlocks.corridor_kill.get())) {
                TileEntity te = world.getTileEntity(info.pos);
                if (te instanceof CorridorKillTile) {
                    CorridorKillTile tile = (CorridorKillTile) te;
                    tile.setStart(spawnStart);
                    tile.setEnd(endPos);
                    tile.setRestorePos(pos, dir);
                }
            }
            return true;

        } else {
        	System.err.println("WARNING: Could not load structure " + this.filePath);
        	PlayerHelper.sendMessageToPlayer(player, new TranslationTextComponent("message.tardis.ars.structure_not_found", this.filePath), false);
        }
        return false;

    }
    
    public static Codec<ARSPiece> getCodec(){
    	return CODEC;
    }

    public static Map<ResourceLocation, ARSPiece> getRegistry() {
        return DATA_LOADER.getData();
    }

    public ResourceLocation getRegistryName() {
        return this.registryName;
    }

    public ARSPiece setRegistryName(ResourceLocation regName) {
        this.registryName = regName;
        return this;
    }
    
    public String getTranslationKey() {
    	if (this.translationKey == null) {
    		this.translationKey = Util.makeTranslationKey("ars.piece", this.getRegistryName());
    	}
    	return this.translationKey;
    }

    public TranslationTextComponent getDisplayName() {
        return new TranslationTextComponent(this.getTranslationKey());
    }
    
    public ResourceLocation getStructureFilePath() {
    	return this.filePath;
    }

    public BlockPos getOffset() {
    	return this.offset;
    }
    
    /**
     * Check if this console room is from a datapack
     *
     * @implSpec If true, render the TranslationTextComponent key in ArsTabletScreen, else use the formatted text
     */
    public boolean isDataPack() {
        return this.isDatapack;
    }
    
    public CompoundNBT serialize() {
        CompoundNBT tag = new CompoundNBT();
        CODEC.encodeStart(NBTDynamicOps.INSTANCE, this).resultOrPartial(Tardis.LOGGER::error).ifPresent(data -> {
           tag.put("ars_piece", data);	
        });
        return tag;
    }

    public static ARSPiece deserialize(CompoundNBT tag) {
    	ARSPiece interior = CODEC.parse(new Dynamic<>(NBTDynamicOps.INSTANCE)).resultOrPartial(Tardis.LOGGER::error).get();
    	return interior;
    }
}