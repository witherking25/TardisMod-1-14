package net.tardis.mod.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.AgeableEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.controller.FlyingMovementController;
import net.minecraft.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.entity.ai.goal.LookAtGoal;
import net.minecraft.entity.ai.goal.LookRandomlyGoal;
import net.minecraft.entity.ai.goal.NearestAttackableTargetGoal;
import net.minecraft.entity.ai.goal.NonTamedTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtByTargetGoal;
import net.minecraft.entity.ai.goal.OwnerHurtTargetGoal;
import net.minecraft.entity.ai.goal.RangedAttackGoal;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomFlyingGoal;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.IFlyingAnimal;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.pathfinding.FlyingPathNavigator;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.api.space.entities.ISpaceImmuneEntity;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.projectiles.LaserEntity;

public class SecDroidEntity extends TameableEntity implements IFlyingAnimal, IRangedAttackMob, ISpaceImmuneEntity{

	public SecDroidEntity(EntityType<? extends TameableEntity> type, World worldIn) {
		super(type, worldIn);
		this.moveController = new FlyingMovementController(this, 4, false); //entityIn, flySpeed (I think), shouldIgnoreGravity
		this.setNoGravity(true);
	}
	
	public SecDroidEntity(World worldIn) {
		this(TEntities.SECURITY_DROID.get(), worldIn);
	}
	
	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		this.goalSelector.addGoal(1, new LookAtGoal(this, PlayerEntity.class, 1.0F));
		//FollowOwnerFlyingGoal is gone in 1.16 so until then we use this.
		this.goalSelector.addGoal(3, new FollowOwnerGoal(this, 1.0, 4, 10, false));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomFlyingGoal(this, 1.0D));
		
		this.goalSelector.addGoal(5, new LookRandomlyGoal(this));
		
		this.goalSelector.addGoal(1, new RangedAttackGoal(this, 1.5, 60, 6));
		
		this.goalSelector.addGoal(1, new NonTamedTargetGoal<PlayerEntity>(this, PlayerEntity.class, true, null));
		this.goalSelector.addGoal(2, new OwnerHurtTargetGoal(this));
		this.goalSelector.addGoal(2, new OwnerHurtByTargetGoal(this));
		this.goalSelector.addGoal(3, new NearestAttackableTargetGoal<MonsterEntity>(this, MonsterEntity.class, true));
	}

	@Override
	public boolean shouldAttackEntity(LivingEntity target, LivingEntity owner) {
		return target instanceof TameableEntity && ((TameableEntity)target).getOwner() == owner ? false : super.shouldAttackEntity(target, owner);
	}

	@Override
	protected PathNavigator createNavigator(World worldIn) {
		FlyingPathNavigator flyingpathnavigator = new FlyingPathNavigator(this, worldIn);
	    flyingpathnavigator.setCanOpenDoors(false);
	    flyingpathnavigator.setCanSwim(true);
	    flyingpathnavigator.setCanEnterDoors(true);
	    return flyingpathnavigator;
	}

	
	public static AttributeModifierMap.MutableAttribute createAttributes() {
	    return MobEntity.func_233666_p_()
		        .createMutableAttribute(Attributes.ATTACK_DAMAGE, 2D)
		        .createMutableAttribute(Attributes.FLYING_SPEED, 0.4D)
		        .createMutableAttribute(Attributes.MAX_HEALTH, 20D)
		        .createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.2D);
	}

	@Override
	public void livingTick() {
		super.livingTick();
		
		
	}
	
	@Override
	public void writeAdditional(CompoundNBT compound) {
		super.writeAdditional(compound);
	}

	@Override
	public void readAdditional(CompoundNBT compound) {
		super.readAdditional(compound);
	}
    
	//createChild
	@Nullable
	@Override
	public AgeableEntity createChild(ServerWorld p_241840_1_, AgeableEntity ageable) {
		return null;
	}

	@Override
	public ActionResultType getEntityInteractionResult(PlayerEntity player, Hand hand) {
		return super.getEntityInteractionResult(player, hand);
	}

	@Override
	public boolean canDespawn(double distanceToClosestPlayer) {
		return false;
	}

	@Override
	public boolean onLivingFall(float distance, float damageMultiplier) {
		return super.onLivingFall(distance, damageMultiplier);
	}

	@Override
	public void attackEntityWithRangedAttack(LivingEntity target, float distanceFactor) {
		if(!world.isRemote && this.canEntityBeSeen(target)) {
			LaserEntity lazer = new LaserEntity(TEntities.LASER.get(), this.getPosX(), this.getPosY(), this.getPosZ(), this.getEntityWorld(), this, 2F, TDamageSources.LASER, new Vector3d(0,1,1));
			lazer.setPosition(getPosX(), getPosY(), getPosZ());
			lazer.shoot(this.rotationPitch, this.rotationYawHead, 0, 2, 0);
			world.addEntity(lazer);
			lazer.setDamage(4.0F);
		}
	}

	@Override
	public boolean shouldTakeSpaceDamage() {
		return false;
	}
}
