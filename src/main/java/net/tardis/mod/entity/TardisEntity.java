package net.tardis.mod.entity;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TardisEntity extends Entity{

	public static final DataParameter<String> EXTERIOR = EntityDataManager.createKey(TardisEntity.class, DataSerializers.STRING);
	public static final DataParameter<CompoundNBT> EXTERIOR_DATA = EntityDataManager.createKey(TardisEntity.class, DataSerializers.COMPOUND_NBT);
	private ConsoleTile console;
	private int sneakTicks = 0;
    private AbstractExterior exterior;
    
    private boolean hasLanded = false;
	
    //Readonly
    private ExteriorTile tile;
    
	public TardisEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public TardisEntity(World worldIn) {
		this(TEntities.TARDIS.get(), worldIn);
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(EXTERIOR, ExteriorRegistry.TRUNK.get().getRegistryName().toString());
		this.getDataManager().register(EXTERIOR_DATA, new CompoundNBT());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(EXTERIOR, compound.getString("exterior"));
		
		if(compound.contains("exterior_data")) {
			CompoundNBT tag = compound.getCompound("exterior_data");
			this.dataManager.set(EXTERIOR_DATA, tag);
			this.readExteriorFromData(tag);
		}
		
		if(!world.isRemote && compound.contains("interior_dimension")) {
			TardisHelper.getConsole(world.getServer(), world.getServer().getWorld(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(compound.getString("interior_dimension")))))
				.ifPresent(console -> this.console = console);
		}
	}
	
	public void readExteriorFromData(CompoundNBT tag) {
		TileEntityType<?> type = ForgeRegistries.TILE_ENTITIES.getValue(new ResourceLocation(tag.getString("id")));
		if(type != null) {
			this.tile = (ExteriorTile) type.create();
			this.tile.setWorldAndPos(world, tile.getPos());
			tile.deserializeNBT(tag);
		}
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		compound.putString("exterior", this.getDataManager().get(EXTERIOR));
		
		if(tile != null)
			compound.put("exterior_data", tile.serializeNBT());
		
		if(this.console != null)
			compound.putString("interior_dimension", this.console.getWorld().getDimensionKey().getLocation().toString());
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		
		
		this.move(MoverType.SELF, this.getMotion());
		if(!this.hasNoGravity() && this.getMotion().y > -3)
			this.setMotion(this.getMotion().add(0, -0.08, 0));
		
		//Apply friction
		if(this.hasNoGravity()) {
			float slippery = this.world.getBlockState(getPosition().down()).getSlipperiness(world, getPosition().down(), this);
			this.setMotion(this.getMotion().scale(slippery * 0.4));
		}
		
		
		if(!world.isRemote) {
			
			if(this.getConsole() != null && world.getGameTime() % 20 == 0) {
				console.setCurrentLocation(world.getDimensionKey(), this.getPosition());
			}
			
			//Damage
			if(this.getPassengers().isEmpty()) {
				boolean hasWacked = false;
				for(Entity ent : world.getEntitiesWithinAABB(Entity.class, this.getBoundingBox())) {
					ent.attackEntityFrom(DamageSource.ANVIL, 5);
				}
				if(hasWacked)
					world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
			}
			
			if(this.onGround)
				remove();
			//Loop back around if it's gone past bedrock
			if(getPosY() <= 0) {
//				this.setPosition(getPosX(), 2, getPosZ());
				this.setPosition(getPosX(), this.world.getHeight(), getPosZ());
				this.setMotion(Vector3d.ZERO);
//				if(console != null) {
//					console.setAntiGrav(true);
//					this.remove();
//				}
			}
		}
	}
	
	@Override
	public void remove() {
		super.remove();
		if(!this.hasLanded)
			land();
	}

	@Override
	public void updateRidden() {
		super.updateRidden();
		/*this.prevgetPosX() = this.getPosX();
		this.prevgetPosY() = this.getPosY();
		this.prevgetPosZ() = this.getPosZ();
		
		this.getPosX() = this.getRidingEntity().getPosX();
		this.getPosY() = this.getRidingEntity().getPosY();
		this.getPosZ() = this.getRidingEntity().getPosZ();*/

        if (this.onGround) {
        	
            if (this.getRidingEntity().isSneaking())
                ++sneakTicks;
            else this.sneakTicks = 0;
            if (sneakTicks > 30)
                remove();
        }
        else this.sneakTicks = 0;
	}
	
	@Override
	public ActionResultType processInitialInteract(PlayerEntity player, Hand hand) {
		if(!world.isRemote && player.getActiveHand() == hand)
			this.remove();
		return super.processInitialInteract(player, hand);
	}

	public void land() {
        if (!world.isRemote && console != null && !hasLanded) {
			console.getExteriorType().place(console, world.getDimensionKey(), this.getPosition());
			console.setCurrentLocation(world.getDimensionKey(), this.getPosition());
			console.setEntity(null);
        }
        hasLanded = true;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return true;
	}

    public AbstractExterior getExterior() {
        if (exterior == null) {
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
        }
        return this.exterior;
    }
    
    @Nullable
    public ExteriorTile getExteriorTile() {
    	if(this.tile == null)
    		this.readExteriorFromData(this.getDataManager().get(EXTERIOR_DATA));
    	return this.tile;
    }
    
    public void setExteriorTile(ExteriorTile tile) {
    	this.tile = tile;
    	this.dataManager.set(EXTERIOR_DATA, tile.serializeNBT());
    }
    
    public ConsoleTile getConsole() {
    	return console;
    }

    @Override
	public Entity changeDimension(ServerWorld destination) {
		return super.changeDimension(destination);
	}

	public void setConsole(ConsoleTile tile) {
        this.console = tile;
        this.exterior = tile.getExteriorType();
        if (!world.isRemote)
        	this.getDataManager().set(EXTERIOR, this.exterior.getRegistryName().toString());
    }

}
