package net.tardis.mod.entity.ai;

import java.util.List;

import net.minecraft.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;
import net.tardis.mod.tileentities.ConsoleTile;

public class HelpFlyTARDISGoal extends MoveToBlockGoal{

	private ConsoleTile console;
	private boolean stop = false;
	private AbstractHumanoidEntity creature;
	private int timeToHit = 0;
	
	public HelpFlyTARDISGoal(AbstractHumanoidEntity creature, double speedIn, int length) {
		super(creature, speedIn, length, 2);
		this.creature = creature;
	}

	@Override
	protected boolean shouldMoveTo(IWorldReader worldIn, BlockPos pos) {
		if(worldIn.getBlockState(pos).getBlock() instanceof ConsoleBlock) {
			console = (ConsoleTile)worldIn.getTileEntity(pos);
			if(console.isInFlight())
				return true;
		}
		return false;
	}

	@Override
	public boolean shouldContinueExecuting() {
		return super.shouldContinueExecuting() && !this.stop;
	}

	@Override
	public void tick() {
		super.tick();
		
		if(this.timeToHit > 0)
			--this.timeToHit;
		
		if(this.console != null && this.console.isInFlight()) {
			if(console.getFlightEvent() != null && this.timeToHit <= 0) {
				List<ControlEntity> controls = this.creature.world.getEntitiesWithinAABB(ControlEntity.class, this.creature.getBoundingBox().grow(1));
				for(ControlEntity control : controls) {
					
					if(control.getControl() == null)
						continue;
					
					ResourceLocation key = control.getControl().getEntry().getRegistryName();
					
					if(console.getFlightEvent().getControls().contains(key)) {
						console.getFlightEvent().getControls().remove(key);
						control.getControl().setAnimationTicks(control.getControl().getMaxAnimationTicks());
						this.creature.world.playSound(null, console.getPos(), control.getControl().getSuccessSound(console), SoundCategory.BLOCKS, 1F, 1F);
						this.timeToHit = 60 + this.creature.getRNG().nextInt(120);
						return;
					}
				}
			}
		}
		else this.stop = true;
		
	}
	
	public boolean shouldExecute() {
		this.stop = false;
		return super.shouldExecute();
	}

}
