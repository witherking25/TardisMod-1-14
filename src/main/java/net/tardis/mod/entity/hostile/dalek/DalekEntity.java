package net.tardis.mod.entity.hostile.dalek;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.Difficulty;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.damagesources.TSource;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.TDataSerializers;
import net.tardis.mod.entity.hostile.dalek.types.DalekType;
import net.tardis.mod.registries.DalekTypeRegistry;
import net.tardis.mod.world.structures.DalekShipStructure;

import javax.annotation.Nullable;
import java.util.EnumSet;

public class DalekEntity extends CreatureEntity {

    public static final DataParameter< ResourceLocation > DALEK_TYPE = EntityDataManager.createKey(DalekEntity.class, TDataSerializers.RESOURCE_LOCATION);
    public static final DataParameter< String > TEXTURE = EntityDataManager.createKey(DalekEntity.class, DataSerializers.STRING);
    private static final DataParameter< Integer > TARGET_ENTITY = EntityDataManager.createKey(DalekEntity.class, DataSerializers.VARINT);
    private static final DataParameter< BlockPos > BLOCK_POS = EntityDataManager.createKey(DalekEntity.class, DataSerializers.BLOCK_POS);


    public DalekEntity(EntityType< ? extends CreatureEntity > type, World worldIn) {
        super(type, worldIn);
    }

    public DalekEntity(World worldIn) {
        super(TEntities.DALEK.get(), worldIn);

    }

    public void setDalekType(DalekType dalekType) {
        ResourceLocation oldType = getDataManager().get(DALEK_TYPE);

        boolean hasChanged = !oldType.equals(dalekType.getRegistryName());
        getDataManager().set(DALEK_TYPE, dalekType.getRegistryName());
        if (hasChanged) {
            getDataManager().set(TEXTURE, dalekType.getRandomTexture(this));
            getDalekType().setupDalek(this);
        }
    }

    public static AttributeModifierMap.MutableAttribute createAttributes() {
        return MobEntity.func_233666_p_()
                .createMutableAttribute(Attributes.ATTACK_DAMAGE, 5D)
                .createMutableAttribute(Attributes.FLYING_SPEED, 0.4D)
                .createMutableAttribute(Attributes.MAX_HEALTH, 20D)
                .createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.2D);
    }


    @Override
    protected SoundEvent getAmbientSound() {
        return getDalekType().getAmbientSound(this);
    }

    @Override
    protected SoundEvent getDeathSound() {
        return getDalekType().getDeathSound(this);
    }

    @Override
    protected void registerData() {
        super.registerData();
        DalekType dalek = DalekTypeRegistry.getRandom(rand);
        getDataManager().register(DALEK_TYPE, dalek.getRegistryName());
        getDataManager().register(TEXTURE, getDalekType().getRandomTexture(this));
        getDataManager().register(TARGET_ENTITY, 0);
        getDataManager().register(BLOCK_POS, BlockPos.ZERO);
    }

    @Override
    protected void registerGoals() {
        super.registerGoals();
        getDalekType().setupDalek(this);
    }

    public DalekType getDalekType() {
        return DalekTypeRegistry.DALEK_TYPE_REGISTRY.get().getValue(getDataManager().get(DALEK_TYPE));
    }

    public String getTexture() {
        return getDataManager().get(TEXTURE);
    }

    public boolean hasTargetedEntity() {
        return this.getDataManager().get(TARGET_ENTITY) != 0;
    }

    private void setTargetedEntity(int entityId) {
        this.dataManager.set(TARGET_ENTITY, entityId);
    }

    private LivingEntity targetedEntity;

    @Override
    public void notifyDataManagerChange(DataParameter< ? > key) {
        super.notifyDataManagerChange(key);
    }

    @Nullable
    public LivingEntity getTargetedEntity() {
        if (!this.hasTargetedEntity()) {
            return null;
        } else if (this.world.isRemote) {
            if (this.targetedEntity != null) {
                return this.targetedEntity;
            } else {
                Entity entity = this.world.getEntityByID(this.dataManager.get(TARGET_ENTITY));
                if (entity instanceof LivingEntity) {
                    this.targetedEntity = (LivingEntity) entity;
                    return this.targetedEntity;
                } else {
                    return null;
                }
            }
        } else {
            return this.getAttackTarget();
        }
    }


    @Override
	public boolean canBreatheUnderwater() {
		return true;
	}

	@Override
	public boolean isInvulnerableTo(DamageSource source) {
		return super.isInvulnerableTo(source) || (source.getTrueSource() == null && source != DamageSource.OUT_OF_WORLD && source != TDamageSources.SPACE && !source.isCreativePlayer() && (source.isProjectile() ? (this.world.getDifficulty() == Difficulty.HARD ? true : false) : false)); //Prevents damage from things like suffocation etc
	}

	@Override
	protected float getSoundVolume() {
		return getDalekType() == DalekTypeRegistry.RUSTY.get() ? 0.2F : 0.8F;
	}

	@Override
    protected float getSoundPitch() {
        return getDalekType() == DalekTypeRegistry.RUSTY.get() ? 0.1F : 1F;
    }

    @Override
    public void tick() {
        super.tick();
        if (getAttackTarget() != null) {
            setTargetedEntity(getAttackTarget().getEntityId());
            getDataManager().set(BLOCK_POS, getAttackTarget().getPosition());
        } else {
            getDataManager().set(BLOCK_POS, BlockPos.ZERO);
        }


        if (getDalekType() != null)
            getDalekType().tickSpecial(this);

    }

    @Override
    public void readAdditional(CompoundNBT compound) {
        super.readAdditional(compound);

        if (compound.contains("dalek_type")) {
            getDataManager().set(DALEK_TYPE, new ResourceLocation(compound.getString("dalek_type")));
        }

        if (compound.contains("texture")) {
            getDataManager().set(TEXTURE, compound.getString("texture"));
        }
    }

    @Override
    public void writeAdditional(CompoundNBT compound) {
        super.writeAdditional(compound);

        compound.putString("dalek_type", getDataManager().get(DALEK_TYPE).toString());

        compound.putString("texture", getDataManager().get(TEXTURE));

    }

    public BlockPos getBlockPos() {
        return getDataManager().get(BLOCK_POS);
    }

    //Commander stuff
    private static final AxisAlignedBB SCAN_RANGE = new AxisAlignedBB(0, 0, 0, 1, 1, 1).grow(20);
    private final DalekEntity commander = null;
    private static final DataParameter< Boolean > IS_COMMANDER = EntityDataManager.createKey(DalekEntity.class, DataSerializers.BOOLEAN);


    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_IRON_GOLEM_HURT;
    }

    public static class AttackGoal extends Goal {
        private final DalekEntity dalek;
        private int tickCounter;

        public AttackGoal(DalekEntity dalek) {
            this.dalek = dalek;
            this.setMutexFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
        }

        @Override
        public boolean shouldExecute() {
            LivingEntity livingentity = this.dalek.getAttackTarget();
            return livingentity != null && livingentity.isAlive();
        }

        @Override
        public boolean shouldContinueExecuting() {
            return super.shouldContinueExecuting();
        }

        @Override
        public void startExecuting() {
            this.tickCounter = 0;
            this.dalek.getNavigator().clearPath();
            this.dalek.getLookController().setLookPositionWithEntity(this.dalek.getAttackTarget(), 90.0F, 90.0F);
        }

        @Override
        public void resetTask() {
            this.dalek.setTargetedEntity(0);
            this.dalek.setAttackTarget(null);
        }

        @Override
        public void tick() {
            LivingEntity livingentity = this.dalek.getAttackTarget();
            this.dalek.getNavigator().clearPath();
            this.dalek.getLookController().setLookPositionWithEntity(livingentity, 90.0F, 90.0F);
            if (!this.dalek.canEntityBeSeen(livingentity)) {
                this.dalek.setAttackTarget(null);
            } else {
                ++this.tickCounter;
                if (this.tickCounter == 0) {
                    this.dalek.setTargetedEntity(this.dalek.getAttackTarget().getEntityId());
                } else if (this.tickCounter >= 40) {
                    livingentity.attackEntityFrom(TDamageSources.DALEK, (float) this.dalek.getAttributeValue(Attributes.ATTACK_DAMAGE));
                    this.dalek.playSound(this.dalek.getDalekType().getFireSound(dalek), this.dalek.getSoundVolume(), this.dalek.getSoundPitch());
                    this.dalek.setAttackTarget(null);
                }

                super.tick();
            }
        }
    }
}
