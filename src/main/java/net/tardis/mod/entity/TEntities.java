package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EntityType.Builder;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.humanoid.CompanionEntity;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.entity.projectiles.LaserEntity;

public class TEntities {
    
    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, Tardis.MODID);
    
    public static final RegistryObject<EntityType<ControlEntity>> CONTROL = ENTITIES.register("control", () -> registerStatic(ControlEntity::new, ControlEntity::new, EntityClassification.MISC, 1F, 1F, "control"));
    public static final RegistryObject<EntityType<DoorEntity>> DOOR = ENTITIES.register("interior_door", () -> registerStatic(DoorEntity::new, DoorEntity::new, EntityClassification.MISC, 1F, 2F, "interior_door"));
    public static final RegistryObject<EntityType<ChairEntity>> CHAIR = ENTITIES.register("chair", () -> registerStatic(ChairEntity::new, ChairEntity::new, EntityClassification.MISC, 1F, 1F, "chair"));
    public static final RegistryObject<EntityType<TardisEntity>> TARDIS = ENTITIES.register("tardis", () -> registerBase(TardisEntity::new, TardisEntity::new, EntityClassification.MISC, 1F, 2F, 64, 1, true, "tardis"));
    public static final RegistryObject<EntityType<HoloPilotEntity>> HOLO_PILOT = ENTITIES.register("holo_pilot", () -> registerStatic(HoloPilotEntity::new, HoloPilotEntity::new, EntityClassification.AMBIENT, 0.75F, 1.75F, "holo_pilot"));
    public static final RegistryObject<EntityType<SecDroidEntity>> SECURITY_DROID = ENTITIES.register("sec_drone", () -> registerBase(SecDroidEntity::new, SecDroidEntity::new, EntityClassification.CREATURE, 0.8F, 0.8F, 64, 1, false, "sec_drone"));
    public static final RegistryObject<EntityType<LaserEntity>> LASER = ENTITIES.register("laser", () -> registerFireResistantMob(LaserEntity::new, LaserEntity::new, EntityClassification.MISC, 0.8F, 0.8F, "laser", true));
    public static final RegistryObject<EntityType<BessieEntity>> BESSIE = ENTITIES.register("bessie", () -> registerFireResistantMob(BessieEntity::new, BessieEntity::new, EntityClassification.MISC, 1.75F, 1.75F, "bessie", false));
    public static final RegistryObject<EntityType<TardisDisplayEntity>> DISPLAY_TARDIS = ENTITIES.register("display_tardis", () -> registerBase(TardisDisplayEntity::new, TardisDisplayEntity::new, EntityClassification.MISC, 0.75F, 2.5F, 64, 1, true, "display_tardis"));
    public static final RegistryObject<EntityType<ShipCaptainEntity>> SHIP_CAPTAIN = ENTITIES.register("ship_captain", () -> registerFireResistantMob(ShipCaptainEntity::new, ShipCaptainEntity::new, EntityClassification.AMBIENT, 0.8F, 1.7F, "ship_captain", false));
    public static final RegistryObject<EntityType<CrewmateEntity>> CREWMATE = ENTITIES.register("crewmate", () -> registerFireResistantMob(CrewmateEntity::new, CrewmateEntity::new, EntityClassification.AMBIENT, 0.8F, 1.7F, "crewmate", false));
    public static final RegistryObject<EntityType<CompanionEntity>> COMPANION = ENTITIES.register("companion", () -> registerFireResistantMob(CompanionEntity::new, CompanionEntity::new, EntityClassification.CREATURE, 0.8F, 1.7F, "companion", false));
    public static final RegistryObject<EntityType<DalekEntity>> DALEK = ENTITIES.register("dalek", () -> registerFireResistantMob(DalekEntity::new, DalekEntity::new, EntityClassification.MONSTER, 1, 1.75F, "dalek", false));

    public static <T extends Entity> EntityType<T> registerBase(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, int trackingRange, int updateFreq, boolean sendUpdate, String name){
        ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
        EntityType.Builder<T> builder = EntityType.Builder.create(factory, classification);
		builder.setShouldReceiveVelocityUpdates(sendUpdate);
		builder.setTrackingRange(trackingRange);
		builder.setUpdateInterval(updateFreq);
		builder.size(width, height);
		builder.setCustomClientFactory((spawnEntity, world) -> client.spawn(world));
		return builder.build(loc.toString());
    }
    
    public static <T extends Entity> EntityType<T> registerFireResistantBase(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, int trackingRange, int updateFreq, boolean sendUpdate, String name){
        ResourceLocation loc = new ResourceLocation(Tardis.MODID, name);
        EntityType.Builder<T> builder = EntityType.Builder.create(factory, classification);
		builder.setShouldReceiveVelocityUpdates(sendUpdate);
		builder.setTrackingRange(trackingRange);
		builder.setUpdateInterval(updateFreq);
		builder.immuneToFire();
		builder.size(width, height);
		builder.setCustomClientFactory((spawnEntity, world) -> client.spawn(world));
		return builder.build(loc.toString());
    }
    
    public static <T extends Entity> EntityType<T> registerStatic(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name){
        return TEntities.registerBase(factory, client, classification, width, height, 64, 40, false, name);
    }

    public static <T extends Entity> EntityType<T> registerMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity) {
        return TEntities.registerBase(factory, client, classification, width, height, 80, 3, velocity, name);
    }
    
    public static <T extends Entity> EntityType<T> registerFireResistantMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity) {
        return TEntities.registerFireResistantBase(factory, client, classification, width, height, 80, 3, velocity, name);
    }


    public static <T extends Entity> EntityType<T> registerMob(EntityType.IFactory<T> factory, IClientSpawner<T> client, EntityClassification classification, float width, float height, String name, boolean velocity, int trackingRange, int updateFreq) {
        return TEntities.registerBase(factory, client, classification, width, height, trackingRange, updateFreq, velocity, name);
    }

    public interface IClientSpawner<T> {
        T spawn(World world);
    }

}
