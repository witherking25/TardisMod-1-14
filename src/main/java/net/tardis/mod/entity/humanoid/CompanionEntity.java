package net.tardis.mod.entity.humanoid;

import java.util.UUID;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.HelpFlyTARDISGoal;
import net.tardis.mod.entity.ai.humanoids.CompanionFollowGoal;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;

public class CompanionEntity extends AbstractHumanoidEntity{

	private boolean isStaying = false;
	private UUID owner = null;
	private PlayerEntity ownerEntity;
	
	public CompanionEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CompanionEntity(World worldIn) {
		this(TEntities.COMPANION.get(), worldIn);
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		
		Dialog root = new Dialog("What's up?");
		
		//Follow type
		DialogOption followType = new DialogOption(null, this.isStaying ? "I need you to wait here" : "Follow me");
		followType.setOptionAction((companion, player1) -> {
			CompanionEntity comp = (CompanionEntity)companion;
			comp.setSitting(!comp.isStaying);
		});
		DialogOption learnConsole = new DialogOption(null, "I want you to watch me, learn how to fly the TARDIS");
		
		root.addDialogOption(learnConsole);
		root.addDialogOption(followType);
		
		return root;
	}

	@Override
	public ResourceLocation getSkin() {
		return new ResourceLocation("textures/entity/steve.png");
	}
    
	/**
	 * processInteract equivalent
	 * <br> In 1.16.2+ this is essentially a fallback method that is being called if the vanilla logic is not being met on right click
	 */
	@Override
	public ActionResultType applyPlayerInteraction(PlayerEntity player, Vector3d vec, Hand hand) {
		
		return super.applyPlayerInteraction(player, vec, hand);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		this.goalSelector.addGoal(3, new CompanionFollowGoal(this, 0.2334));
		this.goalSelector.addGoal(2, new HelpFlyTARDISGoal(this, 0.24, 16));
	}
	
	public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 10D)
    			.createMutableAttribute(Attributes.MOVEMENT_SPEED, 5D);
    }

	public UUID getOwner() {
		return this.owner;
	}
	
	public PlayerEntity getOwnerEntity() {
		
		if(this.owner == null)
			return null;
		
		if(this.ownerEntity != null && this.ownerEntity.isAlive())
			return ownerEntity;
		
		if(!world.isRemote) {
			Entity ent = ((ServerWorld)world).getEntityByUuid(owner);
			if(ent instanceof PlayerEntity)
				return this.ownerEntity = (PlayerEntity)ent;
		}
		return null;
	}
	
	public boolean isSitting() {
		return this.isStaying;
	}
	
	public void setSitting(boolean sit) {
		this.isStaying = sit;
	}
}
