package net.tardis.mod.artron;

import net.minecraft.item.ItemStack;

public interface IArtronBattery {

    /**
     * @param stack  - the itemstack we are targeting
     * @param amount - how much the source offers to the battery
     * @return how much charge was accepted from the source
     */
    float charge(ItemStack stack, float amount);

    /**
     * @param stack  - the itemstack we are targeting
     * @param amount - how much the object tries to take from the battery
     * @return how much charge was taken from the battery
     */
    float discharge(ItemStack stack, float amount);

    float getMaxCharge(ItemStack stack);

    /**
     * Convenience method to get the current charge of the itemstack
     *
     * @param stack
     * @return
     */
    float getCharge(ItemStack stack);
}
