package net.tardis.mod.particles.client;

import net.minecraft.client.particle.IAnimatedSprite;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.SpriteTexturedParticle;
import net.minecraft.client.world.ClientWorld;

public class ArtronParticle extends SpriteTexturedParticle{

	private float maxAlpha;
	private IAnimatedSprite sprite;
	
	public ArtronParticle(ClientWorld worldIn, double x, double y, double z) {
		super(worldIn, x, y, z);
	}
	
	public ArtronParticle(ClientWorld worldIn, double x, double y, double z, double speedX, double speedY, double speedZ, IAnimatedSprite sprite) {
		super(worldIn, x, y, z, speedX, speedY, speedZ);
		this.canCollide = true;
		this.particleGravity = 0.0F;
		this.maxAge = 150;
		this.selectSpriteWithAge(sprite);
		this.sprite = sprite;
		this.motionX = speedX;
		this.motionY = speedY;
		this.motionZ = speedZ;
	}

	@Override
	public void tick() {
		super.tick();
		this.particleAlpha = this.maxAlpha * (1.0F - (this.age / (float)this.maxAge));
		
		this.selectSpriteWithAge(sprite);
	}

	@Override
	public IParticleRenderType getRenderType() {
		return IParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

}
