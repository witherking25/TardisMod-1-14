package net.tardis.api.space;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import net.minecraft.block.AbstractGlassBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/** Helper for the Tardis Mod Oxygen System
 * <p> Oxgyen is merely a set of block positions in which entities can remain in without suffocating
 * <p> This oxygen system does not add any blocks to the world, unlike something like Galacticraft
 * */
public class OxygenHelper {

    private static File config = new File("./config/tardis-oxygen-config.json");
    /** If these block ids allow oxygen to pass through (permeable) */
    private static List<ResourceLocation> permeable = new ArrayList<ResourceLocation>();
    /** If these block ids don't allow oxygen to pass through (non-permeable)*/
    private static List<ResourceLocation> nonpermeable = new ArrayList<ResourceLocation>();
    
    /**
     * Determine if Oxygen is able to pass between these blocks and positions
     * @param world
     * @param state
     * @param pos
     * @return If true, oxygen can escape through these blocks and initiate suffocation. If false, it cannot pass through and the environment will be safe
     */
    public static boolean canOxygenPass(World world, BlockState state, BlockPos pos) {
        if (permeable.contains(state.getBlock().getRegistryName()))
            return true;
        if (nonpermeable.contains(state.getBlock().getRegistryName()))
            return false;
        //1.17 - use Blockstate#isAir() - No world or positions
        return state.isAir(world, pos) || (!state.isSolid() && !(state.getBlock() instanceof AbstractGlassBlock));
    }
    /** Read the config file and add its entries to permeable/nonpermeable list*/
    public static void readOrCreate() {
        try {
            if (config.exists()) {
                JsonObject doc = new JsonParser().parse(new FileReader(config)).getAsJsonObject();
                for (JsonElement perm : doc.get("permeable").getAsJsonArray()) {
                    permeable.add(new ResourceLocation(perm.getAsString()));
                }
                for (JsonElement perm : doc.get("nonpermeable").getAsJsonArray()) {
                    nonpermeable.add(new ResourceLocation(perm.getAsString()));
                }
            } else {
                JsonWriter write = new GsonBuilder().setPrettyPrinting().create().newJsonWriter(new FileWriter(config));
                write.beginObject();

                write.name("permeable");
                write.beginArray();
                write.endArray();

                write.name("nonpermeable");
                write.beginArray();
                write.endArray();

                write.endObject();
                write.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
